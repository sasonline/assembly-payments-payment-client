# Assembly Payments Payment Client
An opinionated wrapper around Assembly Payment's POS payment library 
to allow for more modern (and easier) usage.

## Before You Begin
You'll need to obtain the following information from Assembly Payments:

- API Key

This is designed to run in the browser so the following restrictions apply:

- Shopfront only supports the last two version of Google Chrome
other browsers and versions may work, but there are no guarantees.
- You can only use the the terminal in FQDN mode (auto resolving address)
as it allows for secure websockets and to make the library easier to use.


## Getting Started
You'll need to install the library from npm using a command like:

```bash
npm install --save @shopfront/assembly-payments-payment-client
```

You'll can then go ahead and import the library and perform a transaction:

```javascript
// Import the library
import { PaymentClient } from "@shopfront/assembly-payments-payment-client";

// Initialise the library
const apiKey   = "Your API key loaded from an environment file";
const testMode = false;
const client   = new PaymentClient(apiKey, testMode);

try {
    await client.connect();
} catch(e) {
    // Deal with being unable the connect...
}

try {
    const amount    = 100;
    const cashout   = 0;
    const surcharge = 0;
    
    await client.transaction(amount, cashout, surcharge);
} catch(e) {
    // Deal with the failed transaction
}
```

And that's the basics, you'll also need to deal with pairing and storing
the data, but we've got that covered too.

## A full (opinionated) example
This will walk you through all of the parts that are required to be implemented 
to get the library working correctly. It is highly recommended that you have a
test terminal from Assembly Payments to ensure your integration works correctly.

First we need to import and initialise the library.

```js
// Import the client and other required data
import { 
    PaymentClient,
    InvalidApiKeyException,
    InvalidClientException,
    UnpairedException,
    PairingFailedException,
    TransactionInProgressException,
    TransactionCancelledException,
    TransactionDeclinedException,
    TransactionUnknownException
} from "@shopfront/assembly-payments-payment-client";

// Load the required data from your environment file
const apiKey   = process.env.ASSEMBLY_PAYMENTS_API_KEY;
const testMode = process.env.ASSEMBLY_PAYMENTS_TEST_MODE;

// Initialise the client
const client = new PaymentClient(apiKey, testMode);
```

You can then attempt to connect to the terminal (you can do this even if you 
have never connected to a terminal before).

```js
try {
    await client.connect();
} catch(e) {
    await handleConnectionError(e);
}
```

If the connection fails, we'll need to find out why and deal with it.

```js
async function handleConnectionError(e) {
    if(e instanceof InvalidApiKeyException) {
        // Your API key is incorrect, not much you can do here except
        // destroy your current `PaymentClient` and initialise a new
        // one with the correct API key
        throw e;
    } else if(e instanceof InvalidClientException) {
        // Some options in the client are invalid or not set
        await ensureClientIsCorrect();
    } else if(e instanceof UnpairedException) {
        // The terminal is not paired
        await pairTerminal();
    } else {
        // An unknown error occurred
        throw e;
    }
}
```

If we receive an `InvalidClientException` it means that some data provided
to the `PaymentClient` is incorrect - you'll likely receive this if you have
never used the `PaymentClient` before or the settings have been removed.

```js
async function ensureClientIsCorrect() {
    // You'll probably display some options to the user here 
    // so they can select the correct terminal
    const options = await getUserOptions();
    
    // We'll presume that `options` looks like:
    /**
    {
        registerName : "Register #1",
        serialNumber : "123-456-789",
        posName      : "APPC",
        posVersion   : "version 1",
        cashoutPrompt: false,
        receipts     : {
            promptForCustomerCopy: true,
            additionalHeaderText : "",
            additionalFooterText : "",
        },
    }
    */
    
    // You can checkout what each option does under the reference
    
    // We will now need to let the `PaymentClient` know about the
    // new options. The `PaymentClient` attempts to connect to
    // the terminal when the options have changed.
    try {
        // The `PaymentClient` will handle the storing of the 
        // options for you so you don't have to worry about it.
        await client.setOptions(options);
    } catch(e) {
        // Send the error back to the error handling
        await handleConnectionError(e);
    }    
}
```

Hopefully you no longer receive an `InvalidClientException`, if you do then it
is likely that the user has entered an incorrect serial number.

You'll then need to deal with the terminal being unpaired.

```js
async function pairTerminal() {
    // You might need to display some information about the pairing
    // process to the user, check Assembly Payments' documentation
    // for more information.
    try {
        await client.pair({
            keyCheck: async (code) => {
                // Show the code to the user and ask if it is
                // also shown on the EFTPOS terminal
                
                // If it is, return true otherwise return false
                return true;
            },
            terminalConfirm: async (code, hasCompleted) => {
                // This occurs when the user has said the code
                // is the same in the browser, but has not done
                // so on the EFTPOS terminal.
                
                // You can through an error from here to cancel
                // the pairing
                setTimeout(() => {
                    throw new Error("Manual timeout");
                }, 5000);
                
                // Otherwise, just wait until the `PaymentClient`
                // has confirmed that the terminal has approved
                // the connection
                while(true) {
                    if(hasCompleted()) {
                        break;
                    }
                }
                
                // NOTE: If you return from here, before the
                // terminal has confirmed the pairing code
                // we will presume you are no longer showing
                // a user interface but will continue with
                // the pairing (preventing you from cancelling
                // the process).
            },
        });
    } catch(e) {
        if(e instanceof AlreadyPairedException) {
            // This shouldn't have happened based off the current flow
            // but if you do get this error, you'll need to unpair
            // the terminal first, example:
            await client.unpair();
            await pairTerminal();
        } else if(e instanceof PairingFailedException) {
            // We could not pair the terminal, this is likely
            // because the pairing timed out or we dropped
            // internet during the pairing flow. You can attempt
            // to pair again, but you should let the user decide.
        } else {
            // An unknown error occurred
            throw e;
        }
    }
    
    // You should probably display a message that pairing was successful
    // here before returning to the flow.
}
```

You're now successfully connected and are paired to the terminal. You can now
perform the transaction or settlement. In this example we will perform a 
transaction for one dollar.

```js
// First we need to register callbacks to deal with situations that arise
// during a transaction

// For printing an EFTPOS receipt
client.onPrint((receipt, force) => {
    // Print the receipt out however you normally print receipts
    // If force is true, you should print this straight away
    // If force is false, you can print this at any time (e.g. by appending to your POS receipt)
    window.print();
});

// For requesting a signature
client.onSignature(async () => {
    // Display the signature dialog to the user
    // If this returns true, the signature was correct
    // If this returns false, the signature was incorrect
    return true;
});

// For phone authorization
client.onPhoneAuthorization(async (phoneNumber, merchantId) => {
    // You need to display the phone number and merchant ID
    // so the user can authorize the transaction over the phone
    // You'll need to return the auth code provided when 
    // calling the phone number
    return true;
});

// Attempt the transaction
try {
    // The transaction amount is in cents
    const result = await client.transaction(100);
} catch (e) {
    if(e instanceof TransactionInProgressException) {
        // A transaction is already in progress. You can attempt to 
        // recover this transaction by calling `client.recover()`
        // but this will ignore the current transaction that was
        // originally requested, once the recovered transaction
        // is dealt with, you'll probably want to continue with the
        // original transaction.
    } else if(e instanceof TransactionCancelledException) {
        // The transaction was cancelled
    } else if(e instanceof TransactionDeclinedException) {
        // The transaction was declined for some reason
    } else if(e instanceof TransactionUnknownException) {
        // The library is unsure of what happened to the transaction
        // (e.g. the internet dropped) you'll probably want the user
        // to check the terminal before proceeding and manually
        // approve the transaction or retry it.
    } else {
        // An unknown exception has occurred
        throw e;
    }
}

// Once we're done with all of our transactions, we should unload the `PaymentClient`
client.destroy();
```

After following the example, your EFTPOS should be performing transactions.
We would suggest referring to the reference to view all available features.

## Reference
### PaymentClient
#### Static Properties

**PaymentClient.LOGGER**

This allows you to inject your own logger for the `PaymentClient` to use.
It is recommended to do this before initialising any clients.

The logger needs to implement the `LogInterface`:

```typescript
interface LogInterface {
    debug(description: string, relatedData?: object): void,
    info(description: string, relatedData?: object): void,
    error(description: string, relatedData?: object): void,
}
```

By default, the logger defaults to using `window.console`.

**PaymentClient.SETTINGS**

This allows you to override how the `PaymentClient` stores and retrieves
the persisted settings.

The settings needs to implement the `SettingsInterface`:

```typescript
type SettingValue = string | number | boolean;

interface SettingsInterface {
    getItem(setting: string, defaultValue: SettingValue): Promise<SettingValue>,
    setItem(setting: string, value: SettingValue): Promise<void>;
}
```

By default, the settings uses a custom wrapper around `window.localStorage`.

#### Instance Properties
**Events**

This allows you to access the `EventHandler` that the `PaymentClient` is
currently using.

#### Instance Methods
**`new PaymentClient(apiKey, testMode)`**

This initiates a new `PaymentClient` instance.

*Parameters:*

| Parameter | Type | Description |
| --- | --- | --- |
| apiKey | string | Your API key from Assembly Payments |
| testMode | boolean | Whether to communicate with test terminals or not |

*Returns:* `PaymentClient`

**`.destroy()`**

This destroys the `PaymentClient` instance, you should call this method when unloading the `PaymentClient` (e.g. 
inside `componentWillUnmount` if you're using React).

*Parameters:*

None.

*Returns:* `void`

**`.connect()`**

This connects the `PaymentClient` to the EFTPOS machine.

*Parameters:*

None.

*Returns:* `Promise<void>`

*Throws:*

- `InvalidApiKeyException`
- `InvalidClientException`
- `UnpairedException`

**`.setTestMode(enabled)`**

This sets whether test mode is enabled or not (if enabled it swaps from using production EFTPOS terminals 
to testing EFTPOS terminals). You'll need to reconnect and pair after calling this.

*Parameters:*

| Parameter | Type | Description |
| --- | --- | --- |
| enabled | boolean | Whether test mode is enabled or not |

*Returns:* `Promise<void>`

**`.setOptions(options)`**

This sets the options to use with the EFTPOS terminal. You'll need to reconnect and pair after calling this.

*Parameters:*

| Parameter | Type | Description |
| --- | --- | --- |
| options | object | The options to set |
| options.registerName | string | The name of the register (displayed on the EFTPOS terminal) |
| options.serialNumber | string | The serial number of the terminal |
| options.posName | string | The name of your POS system |
| options.posVersion | string | The version of your POS system |
| options.cashoutPrompt | boolean | Whether to prompt the customer for cashout |
| options.receipts | object | The options relating to receipts |
| options.receipts.promptForCustomerCopy | boolean | Whether to prompt for a customer copy when the transaction is finished |
| options.receipts.additionalHeaderText | string | Text to display at the top of the merchant and customer receipts |
| options.receipts.additionalFooterText | string | Text to display at the bottom of the merchant and customer receipts |

*Returns:* `Promise<void>`

*Throws:*

- `TypeError`

**`.pair(callbacks)`**

This pairs the `PaymentClient` with the EFTPOS terminal.

*Parameters:*

| Parameter | Type | Description |
| --- | --- | --- |
| callbacks | object | The callbacks to call when certain pairing events happen |
| callbacks.keyCheck | `KeyCheckCallback` | The callback for when a confirmation code is required |
| callbacks.terminalConfirm | `TerminalConfirmCallback` | The callback for the code is confirmed on the PC, but not the terminal |

*Returns:* `Promise<void>`

*Throws:*

- `AlreadyPairedException`
- `PairingCancelledException`
- `PairingFailedException`

**`.cancelPair()`**

This cancels the current pairing being performed.

*Parameters:*

None.

*Throws:*

- `NotPairingException`

**`.unpair()`**

This unpairs the `PaymentClient` with the EFTPOS terminal.

*Parameters:*

None.

*Returns:* `Promise<void>`

*Throws:*

- `UnpairedException`

**`.onPrint(printCallback)`**

This is called whenever printing is required. It is expected that this is set before performing a transaction.

*Parameters:*

| Parameter | Type | Description |
| --- | --- | --- |
| printCallback | `PrintCallback` | The callback to fire when printing is required |

*Returns:* `void`

**`.onSignature(signatureCallback)`**

This is called whenever a signature is required. It is expected that this is set before performing a transaction.

*Parameters:*

| Parameter | Type | Description |
| --- | --- | --- |
| signatureCallback | `SignatureCallback` | The callback to fire when a signature requires confirming |

*Returns:* `void`

**`.onPhoneAuthorization(authorizationCallback)`**

This is called whenever phone authorization is required. It is expected that this is set before performing a transaction.

*Parameters:*

| Parameter | Type | Description |
| --- | --- | --- |
| authorizationCallback | `AuthorizationCallback` | The callback to fire when phone authorization is required |

**`.transaction(amount, cashout?, tip?)`**

Perform a transaction or refund on the EFTPOS terminal

*Parameters:*

| Parameter | Type | Description |
| --- | --- | --- |
| amount | number | The amount to purchase (when positive) or refund (when negative) in cents |
| cashout | number | (optional) The amount to cashout in cents (not applicable when refunding) |
| tip | number | (optional) The amount to tip in cents (not applicable when refunding) |

*Returns:* `Promise<TransactionResult>`

*Throws:*

- `UnpairedException`
- `TransactionInProgressException`
- `TransactionCancelledException`
- `TransactionDeclinedException`
- `TransactionUnknownException`

**`.cancelTransaction()`**

Cancel the current transaction running on the EFTPOS terminal

*Parameters:*

None.

*Returns:* `Promise<void>`

*Throws:*

- `UnpairedException`
- `NoTransactionInProgressException`
- `CancelTransactionFailedException`

**`.recover()`**

Recover the EFTPOS transaction that is currently in progress on the EFTPOS terminal

*Parameters:*

None.

*Returns:* `Promise<TransactionResult>`

*Throws:*

- `UnpairedException`
- `RecoveryFailedException`
- `NoTransactionInProgressException`
- `TransactionCancelledException`
- `TransactionDeclinedException`
- `TransactionUnknownException`

**`.settlement()`**

Perform a settlement on the EFTPOS terminal

*Parameters:*

None.

*Returns:* `Promise<SettlementResult>`

*Throws:*

- `UnpairedException`
- `AlreadySettledException`
- `SettlementUnsuccessfulException`

**`.settlementEnquiry()`**

Perform a settlement enquiry on the EFTPOS terminal

*Parameters:*

None.

*Returns:* `Promise<SettlmentEnquiryResult>`

*Throws:*

- `UnpairedException`
- `SettlementUnsuccessfulException`

### EventHandler


### Miscellaneous


### Exceptions
