export declare type SettingValue = string | number | boolean;
export interface SettingsInterface {
    getItem(setting: string, defaultValue: SettingValue | null): Promise<SettingValue>;
    setItem(setting: string, value: SettingValue): Promise<void>;
    deleteItem(setting: string): Promise<void>;
}
export declare class Settings implements SettingsInterface {
    protected settingKey: string;
    protected parseItem(item: string): SettingValue;
    getItem(setting: string, defaultValue: SettingValue): Promise<SettingValue>;
    setItem(setting: string, value: SettingValue): Promise<void>;
    deleteItem(setting: string): Promise<void>;
}
