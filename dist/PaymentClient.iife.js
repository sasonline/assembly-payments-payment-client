var AssemblyPaymentsPaymentClient = (function (exports, buffer) {
    'use strict';

    buffer = buffer && buffer.hasOwnProperty('default') ? buffer['default'] : buffer;

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

    function createCommonjsModule(fn, module) {
    	return module = { exports: {} }, fn(module, module.exports), module.exports;
    }

    var sha = createCommonjsModule(function (module, exports) {
    (function(Y){function C(c,a,b){var e=0,h=[],n=0,g,l,d,f,m,q,u,r,I=!1,v=[],w=[],t,y=!1,z=!1,x=-1;b=b||{};g=b.encoding||"UTF8";t=b.numRounds||1;if(t!==parseInt(t,10)||1>t)throw Error("numRounds must a integer >= 1");if("SHA-1"===c)m=512,q=K,u=Z,f=160,r=function(a){return a.slice()};else if(0===c.lastIndexOf("SHA-",0))if(q=function(a,b){return L(a,b,c)},u=function(a,b,h,e){var k,f;if("SHA-224"===c||"SHA-256"===c)k=(b+65>>>9<<4)+15,f=16;else if("SHA-384"===c||"SHA-512"===c)k=(b+129>>>10<<
    5)+31,f=32;else throw Error("Unexpected error in SHA-2 implementation");for(;a.length<=k;)a.push(0);a[b>>>5]|=128<<24-b%32;b=b+h;a[k]=b&4294967295;a[k-1]=b/4294967296|0;h=a.length;for(b=0;b<h;b+=f)e=L(a.slice(b,b+f),e,c);if("SHA-224"===c)a=[e[0],e[1],e[2],e[3],e[4],e[5],e[6]];else if("SHA-256"===c)a=e;else if("SHA-384"===c)a=[e[0].a,e[0].b,e[1].a,e[1].b,e[2].a,e[2].b,e[3].a,e[3].b,e[4].a,e[4].b,e[5].a,e[5].b];else if("SHA-512"===c)a=[e[0].a,e[0].b,e[1].a,e[1].b,e[2].a,e[2].b,e[3].a,e[3].b,e[4].a,
    e[4].b,e[5].a,e[5].b,e[6].a,e[6].b,e[7].a,e[7].b];else throw Error("Unexpected error in SHA-2 implementation");return a},r=function(a){return a.slice()},"SHA-224"===c)m=512,f=224;else if("SHA-256"===c)m=512,f=256;else if("SHA-384"===c)m=1024,f=384;else if("SHA-512"===c)m=1024,f=512;else throw Error("Chosen SHA variant is not supported");else if(0===c.lastIndexOf("SHA3-",0)||0===c.lastIndexOf("SHAKE",0)){var F=6;q=D;r=function(a){var c=[],e;for(e=0;5>e;e+=1)c[e]=a[e].slice();return c};x=1;if("SHA3-224"===
    c)m=1152,f=224;else if("SHA3-256"===c)m=1088,f=256;else if("SHA3-384"===c)m=832,f=384;else if("SHA3-512"===c)m=576,f=512;else if("SHAKE128"===c)m=1344,f=-1,F=31,z=!0;else if("SHAKE256"===c)m=1088,f=-1,F=31,z=!0;else throw Error("Chosen SHA variant is not supported");u=function(a,c,e,b,h){e=m;var k=F,f,g=[],n=e>>>5,l=0,d=c>>>5;for(f=0;f<d&&c>=e;f+=n)b=D(a.slice(f,f+n),b),c-=e;a=a.slice(f);for(c%=e;a.length<n;)a.push(0);f=c>>>3;a[f>>2]^=k<<f%4*8;a[n-1]^=2147483648;for(b=D(a,b);32*g.length<h;){a=b[l%
    5][l/5|0];g.push(a.b);if(32*g.length>=h)break;g.push(a.a);l+=1;0===64*l%e&&D(null,b);}return g};}else throw Error("Chosen SHA variant is not supported");d=M(a,g,x);l=A(c);this.setHMACKey=function(a,b,h){var k;if(!0===I)throw Error("HMAC key already set");if(!0===y)throw Error("Cannot set HMAC key after calling update");if(!0===z)throw Error("SHAKE is not supported for HMAC");g=(h||{}).encoding||"UTF8";b=M(b,g,x)(a);a=b.binLen;b=b.value;k=m>>>3;h=k/4-1;if(k<a/8){for(b=u(b,a,0,A(c),f);b.length<=h;)b.push(0);
    b[h]&=4294967040;}else if(k>a/8){for(;b.length<=h;)b.push(0);b[h]&=4294967040;}for(a=0;a<=h;a+=1)v[a]=b[a]^909522486,w[a]=b[a]^1549556828;l=q(v,l);e=m;I=!0;};this.update=function(a){var c,b,k,f=0,g=m>>>5;c=d(a,h,n);a=c.binLen;b=c.value;c=a>>>5;for(k=0;k<c;k+=g)f+m<=a&&(l=q(b.slice(k,k+g),l),f+=m);e+=f;h=b.slice(f>>>5);n=a%m;y=!0;};this.getHash=function(a,b){var k,g,d,m;if(!0===I)throw Error("Cannot call getHash after setting HMAC key");d=N(b);if(!0===z){if(-1===d.shakeLen)throw Error("shakeLen must be specified in options");
    f=d.shakeLen;}switch(a){case "HEX":k=function(a){return O(a,f,x,d)};break;case "B64":k=function(a){return P(a,f,x,d)};break;case "BYTES":k=function(a){return Q(a,f,x)};break;case "ARRAYBUFFER":try{g=new ArrayBuffer(0);}catch(p){throw Error("ARRAYBUFFER not supported by this environment");}k=function(a){return R(a,f,x)};break;default:throw Error("format must be HEX, B64, BYTES, or ARRAYBUFFER");}m=u(h.slice(),n,e,r(l),f);for(g=1;g<t;g+=1)!0===z&&0!==f%32&&(m[m.length-1]&=16777215>>>24-f%32),m=u(m,f,
    0,A(c),f);return k(m)};this.getHMAC=function(a,b){var k,g,d,p;if(!1===I)throw Error("Cannot call getHMAC without first setting HMAC key");d=N(b);switch(a){case "HEX":k=function(a){return O(a,f,x,d)};break;case "B64":k=function(a){return P(a,f,x,d)};break;case "BYTES":k=function(a){return Q(a,f,x)};break;case "ARRAYBUFFER":try{k=new ArrayBuffer(0);}catch(v){throw Error("ARRAYBUFFER not supported by this environment");}k=function(a){return R(a,f,x)};break;default:throw Error("outputFormat must be HEX, B64, BYTES, or ARRAYBUFFER");
    }g=u(h.slice(),n,e,r(l),f);p=q(w,A(c));p=u(g,f,m,p,f);return k(p)};}function b(c,a){this.a=c;this.b=a;}function O(c,a,b,e){var h="";a/=8;var n,g,d;d=-1===b?3:0;for(n=0;n<a;n+=1)g=c[n>>>2]>>>8*(d+n%4*b),h+="0123456789abcdef".charAt(g>>>4&15)+"0123456789abcdef".charAt(g&15);return e.outputUpper?h.toUpperCase():h}function P(c,a,b,e){var h="",n=a/8,g,d,p,f;f=-1===b?3:0;for(g=0;g<n;g+=3)for(d=g+1<n?c[g+1>>>2]:0,p=g+2<n?c[g+2>>>2]:0,p=(c[g>>>2]>>>8*(f+g%4*b)&255)<<16|(d>>>8*(f+(g+1)%4*b)&255)<<8|p>>>8*(f+
    (g+2)%4*b)&255,d=0;4>d;d+=1)8*g+6*d<=a?h+="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(p>>>6*(3-d)&63):h+=e.b64Pad;return h}function Q(c,a,b){var e="";a/=8;var h,d,g;g=-1===b?3:0;for(h=0;h<a;h+=1)d=c[h>>>2]>>>8*(g+h%4*b)&255,e+=String.fromCharCode(d);return e}function R(c,a,b){a/=8;var e,h=new ArrayBuffer(a),d,g;g=new Uint8Array(h);d=-1===b?3:0;for(e=0;e<a;e+=1)g[e]=c[e>>>2]>>>8*(d+e%4*b)&255;return h}function N(c){var a={outputUpper:!1,b64Pad:"=",shakeLen:-1};c=c||{};
    a.outputUpper=c.outputUpper||!1;!0===c.hasOwnProperty("b64Pad")&&(a.b64Pad=c.b64Pad);if(!0===c.hasOwnProperty("shakeLen")){if(0!==c.shakeLen%8)throw Error("shakeLen must be a multiple of 8");a.shakeLen=c.shakeLen;}if("boolean"!==typeof a.outputUpper)throw Error("Invalid outputUpper formatting option");if("string"!==typeof a.b64Pad)throw Error("Invalid b64Pad formatting option");return a}function M(c,a,b){switch(a){case "UTF8":case "UTF16BE":case "UTF16LE":break;default:throw Error("encoding must be UTF8, UTF16BE, or UTF16LE");
    }switch(c){case "HEX":c=function(a,c,d){var g=a.length,l,p,f,m,q,u;if(0!==g%2)throw Error("String of HEX type must be in byte increments");c=c||[0];d=d||0;q=d>>>3;u=-1===b?3:0;for(l=0;l<g;l+=2){p=parseInt(a.substr(l,2),16);if(isNaN(p))throw Error("String of HEX type contains invalid characters");m=(l>>>1)+q;for(f=m>>>2;c.length<=f;)c.push(0);c[f]|=p<<8*(u+m%4*b);}return {value:c,binLen:4*g+d}};break;case "TEXT":c=function(c,h,d){var g,l,p=0,f,m,q,u,r,t;h=h||[0];d=d||0;q=d>>>3;if("UTF8"===a)for(t=-1===
    b?3:0,f=0;f<c.length;f+=1)for(g=c.charCodeAt(f),l=[],128>g?l.push(g):2048>g?(l.push(192|g>>>6),l.push(128|g&63)):55296>g||57344<=g?l.push(224|g>>>12,128|g>>>6&63,128|g&63):(f+=1,g=65536+((g&1023)<<10|c.charCodeAt(f)&1023),l.push(240|g>>>18,128|g>>>12&63,128|g>>>6&63,128|g&63)),m=0;m<l.length;m+=1){r=p+q;for(u=r>>>2;h.length<=u;)h.push(0);h[u]|=l[m]<<8*(t+r%4*b);p+=1;}else if("UTF16BE"===a||"UTF16LE"===a)for(t=-1===b?2:0,l="UTF16LE"===a&&1!==b||"UTF16LE"!==a&&1===b,f=0;f<c.length;f+=1){g=c.charCodeAt(f);
    !0===l&&(m=g&255,g=m<<8|g>>>8);r=p+q;for(u=r>>>2;h.length<=u;)h.push(0);h[u]|=g<<8*(t+r%4*b);p+=2;}return {value:h,binLen:8*p+d}};break;case "B64":c=function(a,c,d){var g=0,l,p,f,m,q,u,r,t;if(-1===a.search(/^[a-zA-Z0-9=+\/]+$/))throw Error("Invalid character in base-64 string");p=a.indexOf("=");a=a.replace(/\=/g,"");if(-1!==p&&p<a.length)throw Error("Invalid '=' found in base-64 string");c=c||[0];d=d||0;u=d>>>3;t=-1===b?3:0;for(p=0;p<a.length;p+=4){q=a.substr(p,4);for(f=m=0;f<q.length;f+=1)l="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".indexOf(q[f]),
    m|=l<<18-6*f;for(f=0;f<q.length-1;f+=1){r=g+u;for(l=r>>>2;c.length<=l;)c.push(0);c[l]|=(m>>>16-8*f&255)<<8*(t+r%4*b);g+=1;}}return {value:c,binLen:8*g+d}};break;case "BYTES":c=function(a,c,d){var g,l,p,f,m,q;c=c||[0];d=d||0;p=d>>>3;q=-1===b?3:0;for(l=0;l<a.length;l+=1)g=a.charCodeAt(l),m=l+p,f=m>>>2,c.length<=f&&c.push(0),c[f]|=g<<8*(q+m%4*b);return {value:c,binLen:8*a.length+d}};break;case "ARRAYBUFFER":try{c=new ArrayBuffer(0);}catch(e){throw Error("ARRAYBUFFER not supported by this environment");}c=
    function(a,c,d){var g,l,p,f,m,q;c=c||[0];d=d||0;l=d>>>3;m=-1===b?3:0;q=new Uint8Array(a);for(g=0;g<a.byteLength;g+=1)f=g+l,p=f>>>2,c.length<=p&&c.push(0),c[p]|=q[g]<<8*(m+f%4*b);return {value:c,binLen:8*a.byteLength+d}};break;default:throw Error("format must be HEX, TEXT, B64, BYTES, or ARRAYBUFFER");}return c}function y(c,a){return c<<a|c>>>32-a}function S(c,a){return 32<a?(a-=32,new b(c.b<<a|c.a>>>32-a,c.a<<a|c.b>>>32-a)):0!==a?new b(c.a<<a|c.b>>>32-a,c.b<<a|c.a>>>32-a):c}function w(c,a){return c>>>
    a|c<<32-a}function t(c,a){var k=null,k=new b(c.a,c.b);return k=32>=a?new b(k.a>>>a|k.b<<32-a&4294967295,k.b>>>a|k.a<<32-a&4294967295):new b(k.b>>>a-32|k.a<<64-a&4294967295,k.a>>>a-32|k.b<<64-a&4294967295)}function T(c,a){var k=null;return k=32>=a?new b(c.a>>>a,c.b>>>a|c.a<<32-a&4294967295):new b(0,c.a>>>a-32)}function aa(c,a,b){return c&a^~c&b}function ba(c,a,k){return new b(c.a&a.a^~c.a&k.a,c.b&a.b^~c.b&k.b)}function U(c,a,b){return c&a^c&b^a&b}function ca(c,a,k){return new b(c.a&a.a^c.a&k.a^a.a&
    k.a,c.b&a.b^c.b&k.b^a.b&k.b)}function da(c){return w(c,2)^w(c,13)^w(c,22)}function ea(c){var a=t(c,28),k=t(c,34);c=t(c,39);return new b(a.a^k.a^c.a,a.b^k.b^c.b)}function fa(c){return w(c,6)^w(c,11)^w(c,25)}function ga(c){var a=t(c,14),k=t(c,18);c=t(c,41);return new b(a.a^k.a^c.a,a.b^k.b^c.b)}function ha(c){return w(c,7)^w(c,18)^c>>>3}function ia(c){var a=t(c,1),k=t(c,8);c=T(c,7);return new b(a.a^k.a^c.a,a.b^k.b^c.b)}function ja(c){return w(c,17)^w(c,19)^c>>>10}function ka(c){var a=t(c,19),k=t(c,61);
    c=T(c,6);return new b(a.a^k.a^c.a,a.b^k.b^c.b)}function G(c,a){var b=(c&65535)+(a&65535);return ((c>>>16)+(a>>>16)+(b>>>16)&65535)<<16|b&65535}function la(c,a,b,e){var h=(c&65535)+(a&65535)+(b&65535)+(e&65535);return ((c>>>16)+(a>>>16)+(b>>>16)+(e>>>16)+(h>>>16)&65535)<<16|h&65535}function H(c,a,b,e,h){var d=(c&65535)+(a&65535)+(b&65535)+(e&65535)+(h&65535);return ((c>>>16)+(a>>>16)+(b>>>16)+(e>>>16)+(h>>>16)+(d>>>16)&65535)<<16|d&65535}function ma(c,a){var d,e,h;d=(c.b&65535)+(a.b&65535);e=(c.b>>>16)+
    (a.b>>>16)+(d>>>16);h=(e&65535)<<16|d&65535;d=(c.a&65535)+(a.a&65535)+(e>>>16);e=(c.a>>>16)+(a.a>>>16)+(d>>>16);return new b((e&65535)<<16|d&65535,h)}function na(c,a,d,e){var h,n,g;h=(c.b&65535)+(a.b&65535)+(d.b&65535)+(e.b&65535);n=(c.b>>>16)+(a.b>>>16)+(d.b>>>16)+(e.b>>>16)+(h>>>16);g=(n&65535)<<16|h&65535;h=(c.a&65535)+(a.a&65535)+(d.a&65535)+(e.a&65535)+(n>>>16);n=(c.a>>>16)+(a.a>>>16)+(d.a>>>16)+(e.a>>>16)+(h>>>16);return new b((n&65535)<<16|h&65535,g)}function oa(c,a,d,e,h){var n,g,l;n=(c.b&
    65535)+(a.b&65535)+(d.b&65535)+(e.b&65535)+(h.b&65535);g=(c.b>>>16)+(a.b>>>16)+(d.b>>>16)+(e.b>>>16)+(h.b>>>16)+(n>>>16);l=(g&65535)<<16|n&65535;n=(c.a&65535)+(a.a&65535)+(d.a&65535)+(e.a&65535)+(h.a&65535)+(g>>>16);g=(c.a>>>16)+(a.a>>>16)+(d.a>>>16)+(e.a>>>16)+(h.a>>>16)+(n>>>16);return new b((g&65535)<<16|n&65535,l)}function B(c,a){return new b(c.a^a.a,c.b^a.b)}function A(c){var a=[],d;if("SHA-1"===c)a=[1732584193,4023233417,2562383102,271733878,3285377520];else if(0===c.lastIndexOf("SHA-",0))switch(a=
    [3238371032,914150663,812702999,4144912697,4290775857,1750603025,1694076839,3204075428],d=[1779033703,3144134277,1013904242,2773480762,1359893119,2600822924,528734635,1541459225],c){case "SHA-224":break;case "SHA-256":a=d;break;case "SHA-384":a=[new b(3418070365,a[0]),new b(1654270250,a[1]),new b(2438529370,a[2]),new b(355462360,a[3]),new b(1731405415,a[4]),new b(41048885895,a[5]),new b(3675008525,a[6]),new b(1203062813,a[7])];break;case "SHA-512":a=[new b(d[0],4089235720),new b(d[1],2227873595),
    new b(d[2],4271175723),new b(d[3],1595750129),new b(d[4],2917565137),new b(d[5],725511199),new b(d[6],4215389547),new b(d[7],327033209)];break;default:throw Error("Unknown SHA variant");}else if(0===c.lastIndexOf("SHA3-",0)||0===c.lastIndexOf("SHAKE",0))for(c=0;5>c;c+=1)a[c]=[new b(0,0),new b(0,0),new b(0,0),new b(0,0),new b(0,0)];else throw Error("No SHA variants supported");return a}function K(c,a){var b=[],e,d,n,g,l,p,f;e=a[0];d=a[1];n=a[2];g=a[3];l=a[4];for(f=0;80>f;f+=1)b[f]=16>f?c[f]:y(b[f-
    3]^b[f-8]^b[f-14]^b[f-16],1),p=20>f?H(y(e,5),d&n^~d&g,l,1518500249,b[f]):40>f?H(y(e,5),d^n^g,l,1859775393,b[f]):60>f?H(y(e,5),U(d,n,g),l,2400959708,b[f]):H(y(e,5),d^n^g,l,3395469782,b[f]),l=g,g=n,n=y(d,30),d=e,e=p;a[0]=G(e,a[0]);a[1]=G(d,a[1]);a[2]=G(n,a[2]);a[3]=G(g,a[3]);a[4]=G(l,a[4]);return a}function Z(c,a,b,e){var d;for(d=(a+65>>>9<<4)+15;c.length<=d;)c.push(0);c[a>>>5]|=128<<24-a%32;a+=b;c[d]=a&4294967295;c[d-1]=a/4294967296|0;a=c.length;for(d=0;d<a;d+=16)e=K(c.slice(d,d+16),e);return e}function L(c,
    a,k){var e,h,n,g,l,p,f,m,q,u,r,t,v,w,y,A,z,x,F,B,C,D,E=[],J;if("SHA-224"===k||"SHA-256"===k)u=64,t=1,D=Number,v=G,w=la,y=H,A=ha,z=ja,x=da,F=fa,C=U,B=aa,J=d;else if("SHA-384"===k||"SHA-512"===k)u=80,t=2,D=b,v=ma,w=na,y=oa,A=ia,z=ka,x=ea,F=ga,C=ca,B=ba,J=V;else throw Error("Unexpected error in SHA-2 implementation");k=a[0];e=a[1];h=a[2];n=a[3];g=a[4];l=a[5];p=a[6];f=a[7];for(r=0;r<u;r+=1)16>r?(q=r*t,m=c.length<=q?0:c[q],q=c.length<=q+1?0:c[q+1],E[r]=new D(m,q)):E[r]=w(z(E[r-2]),E[r-7],A(E[r-15]),E[r-
    16]),m=y(f,F(g),B(g,l,p),J[r],E[r]),q=v(x(k),C(k,e,h)),f=p,p=l,l=g,g=v(n,m),n=h,h=e,e=k,k=v(m,q);a[0]=v(k,a[0]);a[1]=v(e,a[1]);a[2]=v(h,a[2]);a[3]=v(n,a[3]);a[4]=v(g,a[4]);a[5]=v(l,a[5]);a[6]=v(p,a[6]);a[7]=v(f,a[7]);return a}function D(c,a){var d,e,h,n,g=[],l=[];if(null!==c)for(e=0;e<c.length;e+=2)a[(e>>>1)%5][(e>>>1)/5|0]=B(a[(e>>>1)%5][(e>>>1)/5|0],new b(c[e+1],c[e]));for(d=0;24>d;d+=1){n=A("SHA3-");for(e=0;5>e;e+=1){h=a[e][0];var p=a[e][1],f=a[e][2],m=a[e][3],q=a[e][4];g[e]=new b(h.a^p.a^f.a^
    m.a^q.a,h.b^p.b^f.b^m.b^q.b);}for(e=0;5>e;e+=1)l[e]=B(g[(e+4)%5],S(g[(e+1)%5],1));for(e=0;5>e;e+=1)for(h=0;5>h;h+=1)a[e][h]=B(a[e][h],l[e]);for(e=0;5>e;e+=1)for(h=0;5>h;h+=1)n[h][(2*e+3*h)%5]=S(a[e][h],W[e][h]);for(e=0;5>e;e+=1)for(h=0;5>h;h+=1)a[e][h]=B(n[e][h],new b(~n[(e+1)%5][h].a&n[(e+2)%5][h].a,~n[(e+1)%5][h].b&n[(e+2)%5][h].b));a[0][0]=B(a[0][0],X[d]);}return a}var d,V,W,X;d=[1116352408,1899447441,3049323471,3921009573,961987163,1508970993,2453635748,2870763221,3624381080,310598401,607225278,
    1426881987,1925078388,2162078206,2614888103,3248222580,3835390401,4022224774,264347078,604807628,770255983,1249150122,1555081692,1996064986,2554220882,2821834349,2952996808,3210313671,3336571891,3584528711,113926993,338241895,666307205,773529912,1294757372,1396182291,1695183700,1986661051,2177026350,2456956037,2730485921,2820302411,3259730800,3345764771,3516065817,3600352804,4094571909,275423344,430227734,506948616,659060556,883997877,958139571,1322822218,1537002063,1747873779,1955562222,2024104815,
    2227730452,2361852424,2428436474,2756734187,3204031479,3329325298];V=[new b(d[0],3609767458),new b(d[1],602891725),new b(d[2],3964484399),new b(d[3],2173295548),new b(d[4],4081628472),new b(d[5],3053834265),new b(d[6],2937671579),new b(d[7],3664609560),new b(d[8],2734883394),new b(d[9],1164996542),new b(d[10],1323610764),new b(d[11],3590304994),new b(d[12],4068182383),new b(d[13],991336113),new b(d[14],633803317),new b(d[15],3479774868),new b(d[16],2666613458),new b(d[17],944711139),new b(d[18],2341262773),
    new b(d[19],2007800933),new b(d[20],1495990901),new b(d[21],1856431235),new b(d[22],3175218132),new b(d[23],2198950837),new b(d[24],3999719339),new b(d[25],766784016),new b(d[26],2566594879),new b(d[27],3203337956),new b(d[28],1034457026),new b(d[29],2466948901),new b(d[30],3758326383),new b(d[31],168717936),new b(d[32],1188179964),new b(d[33],1546045734),new b(d[34],1522805485),new b(d[35],2643833823),new b(d[36],2343527390),new b(d[37],1014477480),new b(d[38],1206759142),new b(d[39],344077627),
    new b(d[40],1290863460),new b(d[41],3158454273),new b(d[42],3505952657),new b(d[43],106217008),new b(d[44],3606008344),new b(d[45],1432725776),new b(d[46],1467031594),new b(d[47],851169720),new b(d[48],3100823752),new b(d[49],1363258195),new b(d[50],3750685593),new b(d[51],3785050280),new b(d[52],3318307427),new b(d[53],3812723403),new b(d[54],2003034995),new b(d[55],3602036899),new b(d[56],1575990012),new b(d[57],1125592928),new b(d[58],2716904306),new b(d[59],442776044),new b(d[60],593698344),new b(d[61],
    3733110249),new b(d[62],2999351573),new b(d[63],3815920427),new b(3391569614,3928383900),new b(3515267271,566280711),new b(3940187606,3454069534),new b(4118630271,4000239992),new b(116418474,1914138554),new b(174292421,2731055270),new b(289380356,3203993006),new b(460393269,320620315),new b(685471733,587496836),new b(852142971,1086792851),new b(1017036298,365543100),new b(1126000580,2618297676),new b(1288033470,3409855158),new b(1501505948,4234509866),new b(1607167915,987167468),new b(1816402316,
    1246189591)];X=[new b(0,1),new b(0,32898),new b(2147483648,32906),new b(2147483648,2147516416),new b(0,32907),new b(0,2147483649),new b(2147483648,2147516545),new b(2147483648,32777),new b(0,138),new b(0,136),new b(0,2147516425),new b(0,2147483658),new b(0,2147516555),new b(2147483648,139),new b(2147483648,32905),new b(2147483648,32771),new b(2147483648,32770),new b(2147483648,128),new b(0,32778),new b(2147483648,2147483658),new b(2147483648,2147516545),new b(2147483648,32896),new b(0,2147483649),
    new b(2147483648,2147516424)];W=[[0,36,3,41,18],[1,44,10,45,2],[62,6,43,15,61],[28,55,25,21,56],[27,20,39,8,14]];(module.exports&&(module.exports=C),exports=C);})(commonjsGlobal);
    });

    var aesJs = createCommonjsModule(function (module, exports) {
    /*! MIT License. Copyright 2015-2018 Richard Moore <me@ricmoo.com>. See LICENSE.txt. */
    (function(root) {

        function checkInt(value) {
            return (parseInt(value) === value);
        }

        function checkInts(arrayish) {
            if (!checkInt(arrayish.length)) { return false; }

            for (var i = 0; i < arrayish.length; i++) {
                if (!checkInt(arrayish[i]) || arrayish[i] < 0 || arrayish[i] > 255) {
                    return false;
                }
            }

            return true;
        }

        function coerceArray(arg, copy) {

            // ArrayBuffer view
            if (arg.buffer && arg.name === 'Uint8Array') {

                if (copy) {
                    if (arg.slice) {
                        arg = arg.slice();
                    } else {
                        arg = Array.prototype.slice.call(arg);
                    }
                }

                return arg;
            }

            // It's an array; check it is a valid representation of a byte
            if (Array.isArray(arg)) {
                if (!checkInts(arg)) {
                    throw new Error('Array contains invalid value: ' + arg);
                }

                return new Uint8Array(arg);
            }

            // Something else, but behaves like an array (maybe a Buffer? Arguments?)
            if (checkInt(arg.length) && checkInts(arg)) {
                return new Uint8Array(arg);
            }

            throw new Error('unsupported array-like object');
        }

        function createArray(length) {
            return new Uint8Array(length);
        }

        function copyArray(sourceArray, targetArray, targetStart, sourceStart, sourceEnd) {
            if (sourceStart != null || sourceEnd != null) {
                if (sourceArray.slice) {
                    sourceArray = sourceArray.slice(sourceStart, sourceEnd);
                } else {
                    sourceArray = Array.prototype.slice.call(sourceArray, sourceStart, sourceEnd);
                }
            }
            targetArray.set(sourceArray, targetStart);
        }



        var convertUtf8 = (function() {
            function toBytes(text) {
                var result = [], i = 0;
                text = encodeURI(text);
                while (i < text.length) {
                    var c = text.charCodeAt(i++);

                    // if it is a % sign, encode the following 2 bytes as a hex value
                    if (c === 37) {
                        result.push(parseInt(text.substr(i, 2), 16));
                        i += 2;

                    // otherwise, just the actual byte
                    } else {
                        result.push(c);
                    }
                }

                return coerceArray(result);
            }

            function fromBytes(bytes) {
                var result = [], i = 0;

                while (i < bytes.length) {
                    var c = bytes[i];

                    if (c < 128) {
                        result.push(String.fromCharCode(c));
                        i++;
                    } else if (c > 191 && c < 224) {
                        result.push(String.fromCharCode(((c & 0x1f) << 6) | (bytes[i + 1] & 0x3f)));
                        i += 2;
                    } else {
                        result.push(String.fromCharCode(((c & 0x0f) << 12) | ((bytes[i + 1] & 0x3f) << 6) | (bytes[i + 2] & 0x3f)));
                        i += 3;
                    }
                }

                return result.join('');
            }

            return {
                toBytes: toBytes,
                fromBytes: fromBytes,
            }
        })();

        var convertHex = (function() {
            function toBytes(text) {
                var result = [];
                for (var i = 0; i < text.length; i += 2) {
                    result.push(parseInt(text.substr(i, 2), 16));
                }

                return result;
            }

            // http://ixti.net/development/javascript/2011/11/11/base64-encodedecode-of-utf8-in-browser-with-js.html
            var Hex = '0123456789abcdef';

            function fromBytes(bytes) {
                    var result = [];
                    for (var i = 0; i < bytes.length; i++) {
                        var v = bytes[i];
                        result.push(Hex[(v & 0xf0) >> 4] + Hex[v & 0x0f]);
                    }
                    return result.join('');
            }

            return {
                toBytes: toBytes,
                fromBytes: fromBytes,
            }
        })();


        // Number of rounds by keysize
        var numberOfRounds = {16: 10, 24: 12, 32: 14};

        // Round constant words
        var rcon = [0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91];

        // S-box and Inverse S-box (S is for Substitution)
        var S = [0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76, 0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, 0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, 0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, 0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, 0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, 0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, 0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, 0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73, 0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb, 0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, 0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08, 0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, 0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, 0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, 0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16];
        var Si =[0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb, 0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb, 0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e, 0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25, 0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92, 0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84, 0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06, 0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b, 0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73, 0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e, 0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b, 0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4, 0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f, 0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef, 0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61, 0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d];

        // Transformations for encryption
        var T1 = [0xc66363a5, 0xf87c7c84, 0xee777799, 0xf67b7b8d, 0xfff2f20d, 0xd66b6bbd, 0xde6f6fb1, 0x91c5c554, 0x60303050, 0x02010103, 0xce6767a9, 0x562b2b7d, 0xe7fefe19, 0xb5d7d762, 0x4dababe6, 0xec76769a, 0x8fcaca45, 0x1f82829d, 0x89c9c940, 0xfa7d7d87, 0xeffafa15, 0xb25959eb, 0x8e4747c9, 0xfbf0f00b, 0x41adadec, 0xb3d4d467, 0x5fa2a2fd, 0x45afafea, 0x239c9cbf, 0x53a4a4f7, 0xe4727296, 0x9bc0c05b, 0x75b7b7c2, 0xe1fdfd1c, 0x3d9393ae, 0x4c26266a, 0x6c36365a, 0x7e3f3f41, 0xf5f7f702, 0x83cccc4f, 0x6834345c, 0x51a5a5f4, 0xd1e5e534, 0xf9f1f108, 0xe2717193, 0xabd8d873, 0x62313153, 0x2a15153f, 0x0804040c, 0x95c7c752, 0x46232365, 0x9dc3c35e, 0x30181828, 0x379696a1, 0x0a05050f, 0x2f9a9ab5, 0x0e070709, 0x24121236, 0x1b80809b, 0xdfe2e23d, 0xcdebeb26, 0x4e272769, 0x7fb2b2cd, 0xea75759f, 0x1209091b, 0x1d83839e, 0x582c2c74, 0x341a1a2e, 0x361b1b2d, 0xdc6e6eb2, 0xb45a5aee, 0x5ba0a0fb, 0xa45252f6, 0x763b3b4d, 0xb7d6d661, 0x7db3b3ce, 0x5229297b, 0xdde3e33e, 0x5e2f2f71, 0x13848497, 0xa65353f5, 0xb9d1d168, 0x00000000, 0xc1eded2c, 0x40202060, 0xe3fcfc1f, 0x79b1b1c8, 0xb65b5bed, 0xd46a6abe, 0x8dcbcb46, 0x67bebed9, 0x7239394b, 0x944a4ade, 0x984c4cd4, 0xb05858e8, 0x85cfcf4a, 0xbbd0d06b, 0xc5efef2a, 0x4faaaae5, 0xedfbfb16, 0x864343c5, 0x9a4d4dd7, 0x66333355, 0x11858594, 0x8a4545cf, 0xe9f9f910, 0x04020206, 0xfe7f7f81, 0xa05050f0, 0x783c3c44, 0x259f9fba, 0x4ba8a8e3, 0xa25151f3, 0x5da3a3fe, 0x804040c0, 0x058f8f8a, 0x3f9292ad, 0x219d9dbc, 0x70383848, 0xf1f5f504, 0x63bcbcdf, 0x77b6b6c1, 0xafdada75, 0x42212163, 0x20101030, 0xe5ffff1a, 0xfdf3f30e, 0xbfd2d26d, 0x81cdcd4c, 0x180c0c14, 0x26131335, 0xc3ecec2f, 0xbe5f5fe1, 0x359797a2, 0x884444cc, 0x2e171739, 0x93c4c457, 0x55a7a7f2, 0xfc7e7e82, 0x7a3d3d47, 0xc86464ac, 0xba5d5de7, 0x3219192b, 0xe6737395, 0xc06060a0, 0x19818198, 0x9e4f4fd1, 0xa3dcdc7f, 0x44222266, 0x542a2a7e, 0x3b9090ab, 0x0b888883, 0x8c4646ca, 0xc7eeee29, 0x6bb8b8d3, 0x2814143c, 0xa7dede79, 0xbc5e5ee2, 0x160b0b1d, 0xaddbdb76, 0xdbe0e03b, 0x64323256, 0x743a3a4e, 0x140a0a1e, 0x924949db, 0x0c06060a, 0x4824246c, 0xb85c5ce4, 0x9fc2c25d, 0xbdd3d36e, 0x43acacef, 0xc46262a6, 0x399191a8, 0x319595a4, 0xd3e4e437, 0xf279798b, 0xd5e7e732, 0x8bc8c843, 0x6e373759, 0xda6d6db7, 0x018d8d8c, 0xb1d5d564, 0x9c4e4ed2, 0x49a9a9e0, 0xd86c6cb4, 0xac5656fa, 0xf3f4f407, 0xcfeaea25, 0xca6565af, 0xf47a7a8e, 0x47aeaee9, 0x10080818, 0x6fbabad5, 0xf0787888, 0x4a25256f, 0x5c2e2e72, 0x381c1c24, 0x57a6a6f1, 0x73b4b4c7, 0x97c6c651, 0xcbe8e823, 0xa1dddd7c, 0xe874749c, 0x3e1f1f21, 0x964b4bdd, 0x61bdbddc, 0x0d8b8b86, 0x0f8a8a85, 0xe0707090, 0x7c3e3e42, 0x71b5b5c4, 0xcc6666aa, 0x904848d8, 0x06030305, 0xf7f6f601, 0x1c0e0e12, 0xc26161a3, 0x6a35355f, 0xae5757f9, 0x69b9b9d0, 0x17868691, 0x99c1c158, 0x3a1d1d27, 0x279e9eb9, 0xd9e1e138, 0xebf8f813, 0x2b9898b3, 0x22111133, 0xd26969bb, 0xa9d9d970, 0x078e8e89, 0x339494a7, 0x2d9b9bb6, 0x3c1e1e22, 0x15878792, 0xc9e9e920, 0x87cece49, 0xaa5555ff, 0x50282878, 0xa5dfdf7a, 0x038c8c8f, 0x59a1a1f8, 0x09898980, 0x1a0d0d17, 0x65bfbfda, 0xd7e6e631, 0x844242c6, 0xd06868b8, 0x824141c3, 0x299999b0, 0x5a2d2d77, 0x1e0f0f11, 0x7bb0b0cb, 0xa85454fc, 0x6dbbbbd6, 0x2c16163a];
        var T2 = [0xa5c66363, 0x84f87c7c, 0x99ee7777, 0x8df67b7b, 0x0dfff2f2, 0xbdd66b6b, 0xb1de6f6f, 0x5491c5c5, 0x50603030, 0x03020101, 0xa9ce6767, 0x7d562b2b, 0x19e7fefe, 0x62b5d7d7, 0xe64dabab, 0x9aec7676, 0x458fcaca, 0x9d1f8282, 0x4089c9c9, 0x87fa7d7d, 0x15effafa, 0xebb25959, 0xc98e4747, 0x0bfbf0f0, 0xec41adad, 0x67b3d4d4, 0xfd5fa2a2, 0xea45afaf, 0xbf239c9c, 0xf753a4a4, 0x96e47272, 0x5b9bc0c0, 0xc275b7b7, 0x1ce1fdfd, 0xae3d9393, 0x6a4c2626, 0x5a6c3636, 0x417e3f3f, 0x02f5f7f7, 0x4f83cccc, 0x5c683434, 0xf451a5a5, 0x34d1e5e5, 0x08f9f1f1, 0x93e27171, 0x73abd8d8, 0x53623131, 0x3f2a1515, 0x0c080404, 0x5295c7c7, 0x65462323, 0x5e9dc3c3, 0x28301818, 0xa1379696, 0x0f0a0505, 0xb52f9a9a, 0x090e0707, 0x36241212, 0x9b1b8080, 0x3ddfe2e2, 0x26cdebeb, 0x694e2727, 0xcd7fb2b2, 0x9fea7575, 0x1b120909, 0x9e1d8383, 0x74582c2c, 0x2e341a1a, 0x2d361b1b, 0xb2dc6e6e, 0xeeb45a5a, 0xfb5ba0a0, 0xf6a45252, 0x4d763b3b, 0x61b7d6d6, 0xce7db3b3, 0x7b522929, 0x3edde3e3, 0x715e2f2f, 0x97138484, 0xf5a65353, 0x68b9d1d1, 0x00000000, 0x2cc1eded, 0x60402020, 0x1fe3fcfc, 0xc879b1b1, 0xedb65b5b, 0xbed46a6a, 0x468dcbcb, 0xd967bebe, 0x4b723939, 0xde944a4a, 0xd4984c4c, 0xe8b05858, 0x4a85cfcf, 0x6bbbd0d0, 0x2ac5efef, 0xe54faaaa, 0x16edfbfb, 0xc5864343, 0xd79a4d4d, 0x55663333, 0x94118585, 0xcf8a4545, 0x10e9f9f9, 0x06040202, 0x81fe7f7f, 0xf0a05050, 0x44783c3c, 0xba259f9f, 0xe34ba8a8, 0xf3a25151, 0xfe5da3a3, 0xc0804040, 0x8a058f8f, 0xad3f9292, 0xbc219d9d, 0x48703838, 0x04f1f5f5, 0xdf63bcbc, 0xc177b6b6, 0x75afdada, 0x63422121, 0x30201010, 0x1ae5ffff, 0x0efdf3f3, 0x6dbfd2d2, 0x4c81cdcd, 0x14180c0c, 0x35261313, 0x2fc3ecec, 0xe1be5f5f, 0xa2359797, 0xcc884444, 0x392e1717, 0x5793c4c4, 0xf255a7a7, 0x82fc7e7e, 0x477a3d3d, 0xacc86464, 0xe7ba5d5d, 0x2b321919, 0x95e67373, 0xa0c06060, 0x98198181, 0xd19e4f4f, 0x7fa3dcdc, 0x66442222, 0x7e542a2a, 0xab3b9090, 0x830b8888, 0xca8c4646, 0x29c7eeee, 0xd36bb8b8, 0x3c281414, 0x79a7dede, 0xe2bc5e5e, 0x1d160b0b, 0x76addbdb, 0x3bdbe0e0, 0x56643232, 0x4e743a3a, 0x1e140a0a, 0xdb924949, 0x0a0c0606, 0x6c482424, 0xe4b85c5c, 0x5d9fc2c2, 0x6ebdd3d3, 0xef43acac, 0xa6c46262, 0xa8399191, 0xa4319595, 0x37d3e4e4, 0x8bf27979, 0x32d5e7e7, 0x438bc8c8, 0x596e3737, 0xb7da6d6d, 0x8c018d8d, 0x64b1d5d5, 0xd29c4e4e, 0xe049a9a9, 0xb4d86c6c, 0xfaac5656, 0x07f3f4f4, 0x25cfeaea, 0xafca6565, 0x8ef47a7a, 0xe947aeae, 0x18100808, 0xd56fbaba, 0x88f07878, 0x6f4a2525, 0x725c2e2e, 0x24381c1c, 0xf157a6a6, 0xc773b4b4, 0x5197c6c6, 0x23cbe8e8, 0x7ca1dddd, 0x9ce87474, 0x213e1f1f, 0xdd964b4b, 0xdc61bdbd, 0x860d8b8b, 0x850f8a8a, 0x90e07070, 0x427c3e3e, 0xc471b5b5, 0xaacc6666, 0xd8904848, 0x05060303, 0x01f7f6f6, 0x121c0e0e, 0xa3c26161, 0x5f6a3535, 0xf9ae5757, 0xd069b9b9, 0x91178686, 0x5899c1c1, 0x273a1d1d, 0xb9279e9e, 0x38d9e1e1, 0x13ebf8f8, 0xb32b9898, 0x33221111, 0xbbd26969, 0x70a9d9d9, 0x89078e8e, 0xa7339494, 0xb62d9b9b, 0x223c1e1e, 0x92158787, 0x20c9e9e9, 0x4987cece, 0xffaa5555, 0x78502828, 0x7aa5dfdf, 0x8f038c8c, 0xf859a1a1, 0x80098989, 0x171a0d0d, 0xda65bfbf, 0x31d7e6e6, 0xc6844242, 0xb8d06868, 0xc3824141, 0xb0299999, 0x775a2d2d, 0x111e0f0f, 0xcb7bb0b0, 0xfca85454, 0xd66dbbbb, 0x3a2c1616];
        var T3 = [0x63a5c663, 0x7c84f87c, 0x7799ee77, 0x7b8df67b, 0xf20dfff2, 0x6bbdd66b, 0x6fb1de6f, 0xc55491c5, 0x30506030, 0x01030201, 0x67a9ce67, 0x2b7d562b, 0xfe19e7fe, 0xd762b5d7, 0xabe64dab, 0x769aec76, 0xca458fca, 0x829d1f82, 0xc94089c9, 0x7d87fa7d, 0xfa15effa, 0x59ebb259, 0x47c98e47, 0xf00bfbf0, 0xadec41ad, 0xd467b3d4, 0xa2fd5fa2, 0xafea45af, 0x9cbf239c, 0xa4f753a4, 0x7296e472, 0xc05b9bc0, 0xb7c275b7, 0xfd1ce1fd, 0x93ae3d93, 0x266a4c26, 0x365a6c36, 0x3f417e3f, 0xf702f5f7, 0xcc4f83cc, 0x345c6834, 0xa5f451a5, 0xe534d1e5, 0xf108f9f1, 0x7193e271, 0xd873abd8, 0x31536231, 0x153f2a15, 0x040c0804, 0xc75295c7, 0x23654623, 0xc35e9dc3, 0x18283018, 0x96a13796, 0x050f0a05, 0x9ab52f9a, 0x07090e07, 0x12362412, 0x809b1b80, 0xe23ddfe2, 0xeb26cdeb, 0x27694e27, 0xb2cd7fb2, 0x759fea75, 0x091b1209, 0x839e1d83, 0x2c74582c, 0x1a2e341a, 0x1b2d361b, 0x6eb2dc6e, 0x5aeeb45a, 0xa0fb5ba0, 0x52f6a452, 0x3b4d763b, 0xd661b7d6, 0xb3ce7db3, 0x297b5229, 0xe33edde3, 0x2f715e2f, 0x84971384, 0x53f5a653, 0xd168b9d1, 0x00000000, 0xed2cc1ed, 0x20604020, 0xfc1fe3fc, 0xb1c879b1, 0x5bedb65b, 0x6abed46a, 0xcb468dcb, 0xbed967be, 0x394b7239, 0x4ade944a, 0x4cd4984c, 0x58e8b058, 0xcf4a85cf, 0xd06bbbd0, 0xef2ac5ef, 0xaae54faa, 0xfb16edfb, 0x43c58643, 0x4dd79a4d, 0x33556633, 0x85941185, 0x45cf8a45, 0xf910e9f9, 0x02060402, 0x7f81fe7f, 0x50f0a050, 0x3c44783c, 0x9fba259f, 0xa8e34ba8, 0x51f3a251, 0xa3fe5da3, 0x40c08040, 0x8f8a058f, 0x92ad3f92, 0x9dbc219d, 0x38487038, 0xf504f1f5, 0xbcdf63bc, 0xb6c177b6, 0xda75afda, 0x21634221, 0x10302010, 0xff1ae5ff, 0xf30efdf3, 0xd26dbfd2, 0xcd4c81cd, 0x0c14180c, 0x13352613, 0xec2fc3ec, 0x5fe1be5f, 0x97a23597, 0x44cc8844, 0x17392e17, 0xc45793c4, 0xa7f255a7, 0x7e82fc7e, 0x3d477a3d, 0x64acc864, 0x5de7ba5d, 0x192b3219, 0x7395e673, 0x60a0c060, 0x81981981, 0x4fd19e4f, 0xdc7fa3dc, 0x22664422, 0x2a7e542a, 0x90ab3b90, 0x88830b88, 0x46ca8c46, 0xee29c7ee, 0xb8d36bb8, 0x143c2814, 0xde79a7de, 0x5ee2bc5e, 0x0b1d160b, 0xdb76addb, 0xe03bdbe0, 0x32566432, 0x3a4e743a, 0x0a1e140a, 0x49db9249, 0x060a0c06, 0x246c4824, 0x5ce4b85c, 0xc25d9fc2, 0xd36ebdd3, 0xacef43ac, 0x62a6c462, 0x91a83991, 0x95a43195, 0xe437d3e4, 0x798bf279, 0xe732d5e7, 0xc8438bc8, 0x37596e37, 0x6db7da6d, 0x8d8c018d, 0xd564b1d5, 0x4ed29c4e, 0xa9e049a9, 0x6cb4d86c, 0x56faac56, 0xf407f3f4, 0xea25cfea, 0x65afca65, 0x7a8ef47a, 0xaee947ae, 0x08181008, 0xbad56fba, 0x7888f078, 0x256f4a25, 0x2e725c2e, 0x1c24381c, 0xa6f157a6, 0xb4c773b4, 0xc65197c6, 0xe823cbe8, 0xdd7ca1dd, 0x749ce874, 0x1f213e1f, 0x4bdd964b, 0xbddc61bd, 0x8b860d8b, 0x8a850f8a, 0x7090e070, 0x3e427c3e, 0xb5c471b5, 0x66aacc66, 0x48d89048, 0x03050603, 0xf601f7f6, 0x0e121c0e, 0x61a3c261, 0x355f6a35, 0x57f9ae57, 0xb9d069b9, 0x86911786, 0xc15899c1, 0x1d273a1d, 0x9eb9279e, 0xe138d9e1, 0xf813ebf8, 0x98b32b98, 0x11332211, 0x69bbd269, 0xd970a9d9, 0x8e89078e, 0x94a73394, 0x9bb62d9b, 0x1e223c1e, 0x87921587, 0xe920c9e9, 0xce4987ce, 0x55ffaa55, 0x28785028, 0xdf7aa5df, 0x8c8f038c, 0xa1f859a1, 0x89800989, 0x0d171a0d, 0xbfda65bf, 0xe631d7e6, 0x42c68442, 0x68b8d068, 0x41c38241, 0x99b02999, 0x2d775a2d, 0x0f111e0f, 0xb0cb7bb0, 0x54fca854, 0xbbd66dbb, 0x163a2c16];
        var T4 = [0x6363a5c6, 0x7c7c84f8, 0x777799ee, 0x7b7b8df6, 0xf2f20dff, 0x6b6bbdd6, 0x6f6fb1de, 0xc5c55491, 0x30305060, 0x01010302, 0x6767a9ce, 0x2b2b7d56, 0xfefe19e7, 0xd7d762b5, 0xababe64d, 0x76769aec, 0xcaca458f, 0x82829d1f, 0xc9c94089, 0x7d7d87fa, 0xfafa15ef, 0x5959ebb2, 0x4747c98e, 0xf0f00bfb, 0xadadec41, 0xd4d467b3, 0xa2a2fd5f, 0xafafea45, 0x9c9cbf23, 0xa4a4f753, 0x727296e4, 0xc0c05b9b, 0xb7b7c275, 0xfdfd1ce1, 0x9393ae3d, 0x26266a4c, 0x36365a6c, 0x3f3f417e, 0xf7f702f5, 0xcccc4f83, 0x34345c68, 0xa5a5f451, 0xe5e534d1, 0xf1f108f9, 0x717193e2, 0xd8d873ab, 0x31315362, 0x15153f2a, 0x04040c08, 0xc7c75295, 0x23236546, 0xc3c35e9d, 0x18182830, 0x9696a137, 0x05050f0a, 0x9a9ab52f, 0x0707090e, 0x12123624, 0x80809b1b, 0xe2e23ddf, 0xebeb26cd, 0x2727694e, 0xb2b2cd7f, 0x75759fea, 0x09091b12, 0x83839e1d, 0x2c2c7458, 0x1a1a2e34, 0x1b1b2d36, 0x6e6eb2dc, 0x5a5aeeb4, 0xa0a0fb5b, 0x5252f6a4, 0x3b3b4d76, 0xd6d661b7, 0xb3b3ce7d, 0x29297b52, 0xe3e33edd, 0x2f2f715e, 0x84849713, 0x5353f5a6, 0xd1d168b9, 0x00000000, 0xeded2cc1, 0x20206040, 0xfcfc1fe3, 0xb1b1c879, 0x5b5bedb6, 0x6a6abed4, 0xcbcb468d, 0xbebed967, 0x39394b72, 0x4a4ade94, 0x4c4cd498, 0x5858e8b0, 0xcfcf4a85, 0xd0d06bbb, 0xefef2ac5, 0xaaaae54f, 0xfbfb16ed, 0x4343c586, 0x4d4dd79a, 0x33335566, 0x85859411, 0x4545cf8a, 0xf9f910e9, 0x02020604, 0x7f7f81fe, 0x5050f0a0, 0x3c3c4478, 0x9f9fba25, 0xa8a8e34b, 0x5151f3a2, 0xa3a3fe5d, 0x4040c080, 0x8f8f8a05, 0x9292ad3f, 0x9d9dbc21, 0x38384870, 0xf5f504f1, 0xbcbcdf63, 0xb6b6c177, 0xdada75af, 0x21216342, 0x10103020, 0xffff1ae5, 0xf3f30efd, 0xd2d26dbf, 0xcdcd4c81, 0x0c0c1418, 0x13133526, 0xecec2fc3, 0x5f5fe1be, 0x9797a235, 0x4444cc88, 0x1717392e, 0xc4c45793, 0xa7a7f255, 0x7e7e82fc, 0x3d3d477a, 0x6464acc8, 0x5d5de7ba, 0x19192b32, 0x737395e6, 0x6060a0c0, 0x81819819, 0x4f4fd19e, 0xdcdc7fa3, 0x22226644, 0x2a2a7e54, 0x9090ab3b, 0x8888830b, 0x4646ca8c, 0xeeee29c7, 0xb8b8d36b, 0x14143c28, 0xdede79a7, 0x5e5ee2bc, 0x0b0b1d16, 0xdbdb76ad, 0xe0e03bdb, 0x32325664, 0x3a3a4e74, 0x0a0a1e14, 0x4949db92, 0x06060a0c, 0x24246c48, 0x5c5ce4b8, 0xc2c25d9f, 0xd3d36ebd, 0xacacef43, 0x6262a6c4, 0x9191a839, 0x9595a431, 0xe4e437d3, 0x79798bf2, 0xe7e732d5, 0xc8c8438b, 0x3737596e, 0x6d6db7da, 0x8d8d8c01, 0xd5d564b1, 0x4e4ed29c, 0xa9a9e049, 0x6c6cb4d8, 0x5656faac, 0xf4f407f3, 0xeaea25cf, 0x6565afca, 0x7a7a8ef4, 0xaeaee947, 0x08081810, 0xbabad56f, 0x787888f0, 0x25256f4a, 0x2e2e725c, 0x1c1c2438, 0xa6a6f157, 0xb4b4c773, 0xc6c65197, 0xe8e823cb, 0xdddd7ca1, 0x74749ce8, 0x1f1f213e, 0x4b4bdd96, 0xbdbddc61, 0x8b8b860d, 0x8a8a850f, 0x707090e0, 0x3e3e427c, 0xb5b5c471, 0x6666aacc, 0x4848d890, 0x03030506, 0xf6f601f7, 0x0e0e121c, 0x6161a3c2, 0x35355f6a, 0x5757f9ae, 0xb9b9d069, 0x86869117, 0xc1c15899, 0x1d1d273a, 0x9e9eb927, 0xe1e138d9, 0xf8f813eb, 0x9898b32b, 0x11113322, 0x6969bbd2, 0xd9d970a9, 0x8e8e8907, 0x9494a733, 0x9b9bb62d, 0x1e1e223c, 0x87879215, 0xe9e920c9, 0xcece4987, 0x5555ffaa, 0x28287850, 0xdfdf7aa5, 0x8c8c8f03, 0xa1a1f859, 0x89898009, 0x0d0d171a, 0xbfbfda65, 0xe6e631d7, 0x4242c684, 0x6868b8d0, 0x4141c382, 0x9999b029, 0x2d2d775a, 0x0f0f111e, 0xb0b0cb7b, 0x5454fca8, 0xbbbbd66d, 0x16163a2c];

        // Transformations for decryption
        var T5 = [0x51f4a750, 0x7e416553, 0x1a17a4c3, 0x3a275e96, 0x3bab6bcb, 0x1f9d45f1, 0xacfa58ab, 0x4be30393, 0x2030fa55, 0xad766df6, 0x88cc7691, 0xf5024c25, 0x4fe5d7fc, 0xc52acbd7, 0x26354480, 0xb562a38f, 0xdeb15a49, 0x25ba1b67, 0x45ea0e98, 0x5dfec0e1, 0xc32f7502, 0x814cf012, 0x8d4697a3, 0x6bd3f9c6, 0x038f5fe7, 0x15929c95, 0xbf6d7aeb, 0x955259da, 0xd4be832d, 0x587421d3, 0x49e06929, 0x8ec9c844, 0x75c2896a, 0xf48e7978, 0x99583e6b, 0x27b971dd, 0xbee14fb6, 0xf088ad17, 0xc920ac66, 0x7dce3ab4, 0x63df4a18, 0xe51a3182, 0x97513360, 0x62537f45, 0xb16477e0, 0xbb6bae84, 0xfe81a01c, 0xf9082b94, 0x70486858, 0x8f45fd19, 0x94de6c87, 0x527bf8b7, 0xab73d323, 0x724b02e2, 0xe31f8f57, 0x6655ab2a, 0xb2eb2807, 0x2fb5c203, 0x86c57b9a, 0xd33708a5, 0x302887f2, 0x23bfa5b2, 0x02036aba, 0xed16825c, 0x8acf1c2b, 0xa779b492, 0xf307f2f0, 0x4e69e2a1, 0x65daf4cd, 0x0605bed5, 0xd134621f, 0xc4a6fe8a, 0x342e539d, 0xa2f355a0, 0x058ae132, 0xa4f6eb75, 0x0b83ec39, 0x4060efaa, 0x5e719f06, 0xbd6e1051, 0x3e218af9, 0x96dd063d, 0xdd3e05ae, 0x4de6bd46, 0x91548db5, 0x71c45d05, 0x0406d46f, 0x605015ff, 0x1998fb24, 0xd6bde997, 0x894043cc, 0x67d99e77, 0xb0e842bd, 0x07898b88, 0xe7195b38, 0x79c8eedb, 0xa17c0a47, 0x7c420fe9, 0xf8841ec9, 0x00000000, 0x09808683, 0x322bed48, 0x1e1170ac, 0x6c5a724e, 0xfd0efffb, 0x0f853856, 0x3daed51e, 0x362d3927, 0x0a0fd964, 0x685ca621, 0x9b5b54d1, 0x24362e3a, 0x0c0a67b1, 0x9357e70f, 0xb4ee96d2, 0x1b9b919e, 0x80c0c54f, 0x61dc20a2, 0x5a774b69, 0x1c121a16, 0xe293ba0a, 0xc0a02ae5, 0x3c22e043, 0x121b171d, 0x0e090d0b, 0xf28bc7ad, 0x2db6a8b9, 0x141ea9c8, 0x57f11985, 0xaf75074c, 0xee99ddbb, 0xa37f60fd, 0xf701269f, 0x5c72f5bc, 0x44663bc5, 0x5bfb7e34, 0x8b432976, 0xcb23c6dc, 0xb6edfc68, 0xb8e4f163, 0xd731dcca, 0x42638510, 0x13972240, 0x84c61120, 0x854a247d, 0xd2bb3df8, 0xaef93211, 0xc729a16d, 0x1d9e2f4b, 0xdcb230f3, 0x0d8652ec, 0x77c1e3d0, 0x2bb3166c, 0xa970b999, 0x119448fa, 0x47e96422, 0xa8fc8cc4, 0xa0f03f1a, 0x567d2cd8, 0x223390ef, 0x87494ec7, 0xd938d1c1, 0x8ccaa2fe, 0x98d40b36, 0xa6f581cf, 0xa57ade28, 0xdab78e26, 0x3fadbfa4, 0x2c3a9de4, 0x5078920d, 0x6a5fcc9b, 0x547e4662, 0xf68d13c2, 0x90d8b8e8, 0x2e39f75e, 0x82c3aff5, 0x9f5d80be, 0x69d0937c, 0x6fd52da9, 0xcf2512b3, 0xc8ac993b, 0x10187da7, 0xe89c636e, 0xdb3bbb7b, 0xcd267809, 0x6e5918f4, 0xec9ab701, 0x834f9aa8, 0xe6956e65, 0xaaffe67e, 0x21bccf08, 0xef15e8e6, 0xbae79bd9, 0x4a6f36ce, 0xea9f09d4, 0x29b07cd6, 0x31a4b2af, 0x2a3f2331, 0xc6a59430, 0x35a266c0, 0x744ebc37, 0xfc82caa6, 0xe090d0b0, 0x33a7d815, 0xf104984a, 0x41ecdaf7, 0x7fcd500e, 0x1791f62f, 0x764dd68d, 0x43efb04d, 0xccaa4d54, 0xe49604df, 0x9ed1b5e3, 0x4c6a881b, 0xc12c1fb8, 0x4665517f, 0x9d5eea04, 0x018c355d, 0xfa877473, 0xfb0b412e, 0xb3671d5a, 0x92dbd252, 0xe9105633, 0x6dd64713, 0x9ad7618c, 0x37a10c7a, 0x59f8148e, 0xeb133c89, 0xcea927ee, 0xb761c935, 0xe11ce5ed, 0x7a47b13c, 0x9cd2df59, 0x55f2733f, 0x1814ce79, 0x73c737bf, 0x53f7cdea, 0x5ffdaa5b, 0xdf3d6f14, 0x7844db86, 0xcaaff381, 0xb968c43e, 0x3824342c, 0xc2a3405f, 0x161dc372, 0xbce2250c, 0x283c498b, 0xff0d9541, 0x39a80171, 0x080cb3de, 0xd8b4e49c, 0x6456c190, 0x7bcb8461, 0xd532b670, 0x486c5c74, 0xd0b85742];
        var T6 = [0x5051f4a7, 0x537e4165, 0xc31a17a4, 0x963a275e, 0xcb3bab6b, 0xf11f9d45, 0xabacfa58, 0x934be303, 0x552030fa, 0xf6ad766d, 0x9188cc76, 0x25f5024c, 0xfc4fe5d7, 0xd7c52acb, 0x80263544, 0x8fb562a3, 0x49deb15a, 0x6725ba1b, 0x9845ea0e, 0xe15dfec0, 0x02c32f75, 0x12814cf0, 0xa38d4697, 0xc66bd3f9, 0xe7038f5f, 0x9515929c, 0xebbf6d7a, 0xda955259, 0x2dd4be83, 0xd3587421, 0x2949e069, 0x448ec9c8, 0x6a75c289, 0x78f48e79, 0x6b99583e, 0xdd27b971, 0xb6bee14f, 0x17f088ad, 0x66c920ac, 0xb47dce3a, 0x1863df4a, 0x82e51a31, 0x60975133, 0x4562537f, 0xe0b16477, 0x84bb6bae, 0x1cfe81a0, 0x94f9082b, 0x58704868, 0x198f45fd, 0x8794de6c, 0xb7527bf8, 0x23ab73d3, 0xe2724b02, 0x57e31f8f, 0x2a6655ab, 0x07b2eb28, 0x032fb5c2, 0x9a86c57b, 0xa5d33708, 0xf2302887, 0xb223bfa5, 0xba02036a, 0x5ced1682, 0x2b8acf1c, 0x92a779b4, 0xf0f307f2, 0xa14e69e2, 0xcd65daf4, 0xd50605be, 0x1fd13462, 0x8ac4a6fe, 0x9d342e53, 0xa0a2f355, 0x32058ae1, 0x75a4f6eb, 0x390b83ec, 0xaa4060ef, 0x065e719f, 0x51bd6e10, 0xf93e218a, 0x3d96dd06, 0xaedd3e05, 0x464de6bd, 0xb591548d, 0x0571c45d, 0x6f0406d4, 0xff605015, 0x241998fb, 0x97d6bde9, 0xcc894043, 0x7767d99e, 0xbdb0e842, 0x8807898b, 0x38e7195b, 0xdb79c8ee, 0x47a17c0a, 0xe97c420f, 0xc9f8841e, 0x00000000, 0x83098086, 0x48322bed, 0xac1e1170, 0x4e6c5a72, 0xfbfd0eff, 0x560f8538, 0x1e3daed5, 0x27362d39, 0x640a0fd9, 0x21685ca6, 0xd19b5b54, 0x3a24362e, 0xb10c0a67, 0x0f9357e7, 0xd2b4ee96, 0x9e1b9b91, 0x4f80c0c5, 0xa261dc20, 0x695a774b, 0x161c121a, 0x0ae293ba, 0xe5c0a02a, 0x433c22e0, 0x1d121b17, 0x0b0e090d, 0xadf28bc7, 0xb92db6a8, 0xc8141ea9, 0x8557f119, 0x4caf7507, 0xbbee99dd, 0xfda37f60, 0x9ff70126, 0xbc5c72f5, 0xc544663b, 0x345bfb7e, 0x768b4329, 0xdccb23c6, 0x68b6edfc, 0x63b8e4f1, 0xcad731dc, 0x10426385, 0x40139722, 0x2084c611, 0x7d854a24, 0xf8d2bb3d, 0x11aef932, 0x6dc729a1, 0x4b1d9e2f, 0xf3dcb230, 0xec0d8652, 0xd077c1e3, 0x6c2bb316, 0x99a970b9, 0xfa119448, 0x2247e964, 0xc4a8fc8c, 0x1aa0f03f, 0xd8567d2c, 0xef223390, 0xc787494e, 0xc1d938d1, 0xfe8ccaa2, 0x3698d40b, 0xcfa6f581, 0x28a57ade, 0x26dab78e, 0xa43fadbf, 0xe42c3a9d, 0x0d507892, 0x9b6a5fcc, 0x62547e46, 0xc2f68d13, 0xe890d8b8, 0x5e2e39f7, 0xf582c3af, 0xbe9f5d80, 0x7c69d093, 0xa96fd52d, 0xb3cf2512, 0x3bc8ac99, 0xa710187d, 0x6ee89c63, 0x7bdb3bbb, 0x09cd2678, 0xf46e5918, 0x01ec9ab7, 0xa8834f9a, 0x65e6956e, 0x7eaaffe6, 0x0821bccf, 0xe6ef15e8, 0xd9bae79b, 0xce4a6f36, 0xd4ea9f09, 0xd629b07c, 0xaf31a4b2, 0x312a3f23, 0x30c6a594, 0xc035a266, 0x37744ebc, 0xa6fc82ca, 0xb0e090d0, 0x1533a7d8, 0x4af10498, 0xf741ecda, 0x0e7fcd50, 0x2f1791f6, 0x8d764dd6, 0x4d43efb0, 0x54ccaa4d, 0xdfe49604, 0xe39ed1b5, 0x1b4c6a88, 0xb8c12c1f, 0x7f466551, 0x049d5eea, 0x5d018c35, 0x73fa8774, 0x2efb0b41, 0x5ab3671d, 0x5292dbd2, 0x33e91056, 0x136dd647, 0x8c9ad761, 0x7a37a10c, 0x8e59f814, 0x89eb133c, 0xeecea927, 0x35b761c9, 0xede11ce5, 0x3c7a47b1, 0x599cd2df, 0x3f55f273, 0x791814ce, 0xbf73c737, 0xea53f7cd, 0x5b5ffdaa, 0x14df3d6f, 0x867844db, 0x81caaff3, 0x3eb968c4, 0x2c382434, 0x5fc2a340, 0x72161dc3, 0x0cbce225, 0x8b283c49, 0x41ff0d95, 0x7139a801, 0xde080cb3, 0x9cd8b4e4, 0x906456c1, 0x617bcb84, 0x70d532b6, 0x74486c5c, 0x42d0b857];
        var T7 = [0xa75051f4, 0x65537e41, 0xa4c31a17, 0x5e963a27, 0x6bcb3bab, 0x45f11f9d, 0x58abacfa, 0x03934be3, 0xfa552030, 0x6df6ad76, 0x769188cc, 0x4c25f502, 0xd7fc4fe5, 0xcbd7c52a, 0x44802635, 0xa38fb562, 0x5a49deb1, 0x1b6725ba, 0x0e9845ea, 0xc0e15dfe, 0x7502c32f, 0xf012814c, 0x97a38d46, 0xf9c66bd3, 0x5fe7038f, 0x9c951592, 0x7aebbf6d, 0x59da9552, 0x832dd4be, 0x21d35874, 0x692949e0, 0xc8448ec9, 0x896a75c2, 0x7978f48e, 0x3e6b9958, 0x71dd27b9, 0x4fb6bee1, 0xad17f088, 0xac66c920, 0x3ab47dce, 0x4a1863df, 0x3182e51a, 0x33609751, 0x7f456253, 0x77e0b164, 0xae84bb6b, 0xa01cfe81, 0x2b94f908, 0x68587048, 0xfd198f45, 0x6c8794de, 0xf8b7527b, 0xd323ab73, 0x02e2724b, 0x8f57e31f, 0xab2a6655, 0x2807b2eb, 0xc2032fb5, 0x7b9a86c5, 0x08a5d337, 0x87f23028, 0xa5b223bf, 0x6aba0203, 0x825ced16, 0x1c2b8acf, 0xb492a779, 0xf2f0f307, 0xe2a14e69, 0xf4cd65da, 0xbed50605, 0x621fd134, 0xfe8ac4a6, 0x539d342e, 0x55a0a2f3, 0xe132058a, 0xeb75a4f6, 0xec390b83, 0xefaa4060, 0x9f065e71, 0x1051bd6e, 0x8af93e21, 0x063d96dd, 0x05aedd3e, 0xbd464de6, 0x8db59154, 0x5d0571c4, 0xd46f0406, 0x15ff6050, 0xfb241998, 0xe997d6bd, 0x43cc8940, 0x9e7767d9, 0x42bdb0e8, 0x8b880789, 0x5b38e719, 0xeedb79c8, 0x0a47a17c, 0x0fe97c42, 0x1ec9f884, 0x00000000, 0x86830980, 0xed48322b, 0x70ac1e11, 0x724e6c5a, 0xfffbfd0e, 0x38560f85, 0xd51e3dae, 0x3927362d, 0xd9640a0f, 0xa621685c, 0x54d19b5b, 0x2e3a2436, 0x67b10c0a, 0xe70f9357, 0x96d2b4ee, 0x919e1b9b, 0xc54f80c0, 0x20a261dc, 0x4b695a77, 0x1a161c12, 0xba0ae293, 0x2ae5c0a0, 0xe0433c22, 0x171d121b, 0x0d0b0e09, 0xc7adf28b, 0xa8b92db6, 0xa9c8141e, 0x198557f1, 0x074caf75, 0xddbbee99, 0x60fda37f, 0x269ff701, 0xf5bc5c72, 0x3bc54466, 0x7e345bfb, 0x29768b43, 0xc6dccb23, 0xfc68b6ed, 0xf163b8e4, 0xdccad731, 0x85104263, 0x22401397, 0x112084c6, 0x247d854a, 0x3df8d2bb, 0x3211aef9, 0xa16dc729, 0x2f4b1d9e, 0x30f3dcb2, 0x52ec0d86, 0xe3d077c1, 0x166c2bb3, 0xb999a970, 0x48fa1194, 0x642247e9, 0x8cc4a8fc, 0x3f1aa0f0, 0x2cd8567d, 0x90ef2233, 0x4ec78749, 0xd1c1d938, 0xa2fe8cca, 0x0b3698d4, 0x81cfa6f5, 0xde28a57a, 0x8e26dab7, 0xbfa43fad, 0x9de42c3a, 0x920d5078, 0xcc9b6a5f, 0x4662547e, 0x13c2f68d, 0xb8e890d8, 0xf75e2e39, 0xaff582c3, 0x80be9f5d, 0x937c69d0, 0x2da96fd5, 0x12b3cf25, 0x993bc8ac, 0x7da71018, 0x636ee89c, 0xbb7bdb3b, 0x7809cd26, 0x18f46e59, 0xb701ec9a, 0x9aa8834f, 0x6e65e695, 0xe67eaaff, 0xcf0821bc, 0xe8e6ef15, 0x9bd9bae7, 0x36ce4a6f, 0x09d4ea9f, 0x7cd629b0, 0xb2af31a4, 0x23312a3f, 0x9430c6a5, 0x66c035a2, 0xbc37744e, 0xcaa6fc82, 0xd0b0e090, 0xd81533a7, 0x984af104, 0xdaf741ec, 0x500e7fcd, 0xf62f1791, 0xd68d764d, 0xb04d43ef, 0x4d54ccaa, 0x04dfe496, 0xb5e39ed1, 0x881b4c6a, 0x1fb8c12c, 0x517f4665, 0xea049d5e, 0x355d018c, 0x7473fa87, 0x412efb0b, 0x1d5ab367, 0xd25292db, 0x5633e910, 0x47136dd6, 0x618c9ad7, 0x0c7a37a1, 0x148e59f8, 0x3c89eb13, 0x27eecea9, 0xc935b761, 0xe5ede11c, 0xb13c7a47, 0xdf599cd2, 0x733f55f2, 0xce791814, 0x37bf73c7, 0xcdea53f7, 0xaa5b5ffd, 0x6f14df3d, 0xdb867844, 0xf381caaf, 0xc43eb968, 0x342c3824, 0x405fc2a3, 0xc372161d, 0x250cbce2, 0x498b283c, 0x9541ff0d, 0x017139a8, 0xb3de080c, 0xe49cd8b4, 0xc1906456, 0x84617bcb, 0xb670d532, 0x5c74486c, 0x5742d0b8];
        var T8 = [0xf4a75051, 0x4165537e, 0x17a4c31a, 0x275e963a, 0xab6bcb3b, 0x9d45f11f, 0xfa58abac, 0xe303934b, 0x30fa5520, 0x766df6ad, 0xcc769188, 0x024c25f5, 0xe5d7fc4f, 0x2acbd7c5, 0x35448026, 0x62a38fb5, 0xb15a49de, 0xba1b6725, 0xea0e9845, 0xfec0e15d, 0x2f7502c3, 0x4cf01281, 0x4697a38d, 0xd3f9c66b, 0x8f5fe703, 0x929c9515, 0x6d7aebbf, 0x5259da95, 0xbe832dd4, 0x7421d358, 0xe0692949, 0xc9c8448e, 0xc2896a75, 0x8e7978f4, 0x583e6b99, 0xb971dd27, 0xe14fb6be, 0x88ad17f0, 0x20ac66c9, 0xce3ab47d, 0xdf4a1863, 0x1a3182e5, 0x51336097, 0x537f4562, 0x6477e0b1, 0x6bae84bb, 0x81a01cfe, 0x082b94f9, 0x48685870, 0x45fd198f, 0xde6c8794, 0x7bf8b752, 0x73d323ab, 0x4b02e272, 0x1f8f57e3, 0x55ab2a66, 0xeb2807b2, 0xb5c2032f, 0xc57b9a86, 0x3708a5d3, 0x2887f230, 0xbfa5b223, 0x036aba02, 0x16825ced, 0xcf1c2b8a, 0x79b492a7, 0x07f2f0f3, 0x69e2a14e, 0xdaf4cd65, 0x05bed506, 0x34621fd1, 0xa6fe8ac4, 0x2e539d34, 0xf355a0a2, 0x8ae13205, 0xf6eb75a4, 0x83ec390b, 0x60efaa40, 0x719f065e, 0x6e1051bd, 0x218af93e, 0xdd063d96, 0x3e05aedd, 0xe6bd464d, 0x548db591, 0xc45d0571, 0x06d46f04, 0x5015ff60, 0x98fb2419, 0xbde997d6, 0x4043cc89, 0xd99e7767, 0xe842bdb0, 0x898b8807, 0x195b38e7, 0xc8eedb79, 0x7c0a47a1, 0x420fe97c, 0x841ec9f8, 0x00000000, 0x80868309, 0x2bed4832, 0x1170ac1e, 0x5a724e6c, 0x0efffbfd, 0x8538560f, 0xaed51e3d, 0x2d392736, 0x0fd9640a, 0x5ca62168, 0x5b54d19b, 0x362e3a24, 0x0a67b10c, 0x57e70f93, 0xee96d2b4, 0x9b919e1b, 0xc0c54f80, 0xdc20a261, 0x774b695a, 0x121a161c, 0x93ba0ae2, 0xa02ae5c0, 0x22e0433c, 0x1b171d12, 0x090d0b0e, 0x8bc7adf2, 0xb6a8b92d, 0x1ea9c814, 0xf1198557, 0x75074caf, 0x99ddbbee, 0x7f60fda3, 0x01269ff7, 0x72f5bc5c, 0x663bc544, 0xfb7e345b, 0x4329768b, 0x23c6dccb, 0xedfc68b6, 0xe4f163b8, 0x31dccad7, 0x63851042, 0x97224013, 0xc6112084, 0x4a247d85, 0xbb3df8d2, 0xf93211ae, 0x29a16dc7, 0x9e2f4b1d, 0xb230f3dc, 0x8652ec0d, 0xc1e3d077, 0xb3166c2b, 0x70b999a9, 0x9448fa11, 0xe9642247, 0xfc8cc4a8, 0xf03f1aa0, 0x7d2cd856, 0x3390ef22, 0x494ec787, 0x38d1c1d9, 0xcaa2fe8c, 0xd40b3698, 0xf581cfa6, 0x7ade28a5, 0xb78e26da, 0xadbfa43f, 0x3a9de42c, 0x78920d50, 0x5fcc9b6a, 0x7e466254, 0x8d13c2f6, 0xd8b8e890, 0x39f75e2e, 0xc3aff582, 0x5d80be9f, 0xd0937c69, 0xd52da96f, 0x2512b3cf, 0xac993bc8, 0x187da710, 0x9c636ee8, 0x3bbb7bdb, 0x267809cd, 0x5918f46e, 0x9ab701ec, 0x4f9aa883, 0x956e65e6, 0xffe67eaa, 0xbccf0821, 0x15e8e6ef, 0xe79bd9ba, 0x6f36ce4a, 0x9f09d4ea, 0xb07cd629, 0xa4b2af31, 0x3f23312a, 0xa59430c6, 0xa266c035, 0x4ebc3774, 0x82caa6fc, 0x90d0b0e0, 0xa7d81533, 0x04984af1, 0xecdaf741, 0xcd500e7f, 0x91f62f17, 0x4dd68d76, 0xefb04d43, 0xaa4d54cc, 0x9604dfe4, 0xd1b5e39e, 0x6a881b4c, 0x2c1fb8c1, 0x65517f46, 0x5eea049d, 0x8c355d01, 0x877473fa, 0x0b412efb, 0x671d5ab3, 0xdbd25292, 0x105633e9, 0xd647136d, 0xd7618c9a, 0xa10c7a37, 0xf8148e59, 0x133c89eb, 0xa927eece, 0x61c935b7, 0x1ce5ede1, 0x47b13c7a, 0xd2df599c, 0xf2733f55, 0x14ce7918, 0xc737bf73, 0xf7cdea53, 0xfdaa5b5f, 0x3d6f14df, 0x44db8678, 0xaff381ca, 0x68c43eb9, 0x24342c38, 0xa3405fc2, 0x1dc37216, 0xe2250cbc, 0x3c498b28, 0x0d9541ff, 0xa8017139, 0x0cb3de08, 0xb4e49cd8, 0x56c19064, 0xcb84617b, 0x32b670d5, 0x6c5c7448, 0xb85742d0];

        // Transformations for decryption key expansion
        var U1 = [0x00000000, 0x0e090d0b, 0x1c121a16, 0x121b171d, 0x3824342c, 0x362d3927, 0x24362e3a, 0x2a3f2331, 0x70486858, 0x7e416553, 0x6c5a724e, 0x62537f45, 0x486c5c74, 0x4665517f, 0x547e4662, 0x5a774b69, 0xe090d0b0, 0xee99ddbb, 0xfc82caa6, 0xf28bc7ad, 0xd8b4e49c, 0xd6bde997, 0xc4a6fe8a, 0xcaaff381, 0x90d8b8e8, 0x9ed1b5e3, 0x8ccaa2fe, 0x82c3aff5, 0xa8fc8cc4, 0xa6f581cf, 0xb4ee96d2, 0xbae79bd9, 0xdb3bbb7b, 0xd532b670, 0xc729a16d, 0xc920ac66, 0xe31f8f57, 0xed16825c, 0xff0d9541, 0xf104984a, 0xab73d323, 0xa57ade28, 0xb761c935, 0xb968c43e, 0x9357e70f, 0x9d5eea04, 0x8f45fd19, 0x814cf012, 0x3bab6bcb, 0x35a266c0, 0x27b971dd, 0x29b07cd6, 0x038f5fe7, 0x0d8652ec, 0x1f9d45f1, 0x119448fa, 0x4be30393, 0x45ea0e98, 0x57f11985, 0x59f8148e, 0x73c737bf, 0x7dce3ab4, 0x6fd52da9, 0x61dc20a2, 0xad766df6, 0xa37f60fd, 0xb16477e0, 0xbf6d7aeb, 0x955259da, 0x9b5b54d1, 0x894043cc, 0x87494ec7, 0xdd3e05ae, 0xd33708a5, 0xc12c1fb8, 0xcf2512b3, 0xe51a3182, 0xeb133c89, 0xf9082b94, 0xf701269f, 0x4de6bd46, 0x43efb04d, 0x51f4a750, 0x5ffdaa5b, 0x75c2896a, 0x7bcb8461, 0x69d0937c, 0x67d99e77, 0x3daed51e, 0x33a7d815, 0x21bccf08, 0x2fb5c203, 0x058ae132, 0x0b83ec39, 0x1998fb24, 0x1791f62f, 0x764dd68d, 0x7844db86, 0x6a5fcc9b, 0x6456c190, 0x4e69e2a1, 0x4060efaa, 0x527bf8b7, 0x5c72f5bc, 0x0605bed5, 0x080cb3de, 0x1a17a4c3, 0x141ea9c8, 0x3e218af9, 0x302887f2, 0x223390ef, 0x2c3a9de4, 0x96dd063d, 0x98d40b36, 0x8acf1c2b, 0x84c61120, 0xaef93211, 0xa0f03f1a, 0xb2eb2807, 0xbce2250c, 0xe6956e65, 0xe89c636e, 0xfa877473, 0xf48e7978, 0xdeb15a49, 0xd0b85742, 0xc2a3405f, 0xccaa4d54, 0x41ecdaf7, 0x4fe5d7fc, 0x5dfec0e1, 0x53f7cdea, 0x79c8eedb, 0x77c1e3d0, 0x65daf4cd, 0x6bd3f9c6, 0x31a4b2af, 0x3fadbfa4, 0x2db6a8b9, 0x23bfa5b2, 0x09808683, 0x07898b88, 0x15929c95, 0x1b9b919e, 0xa17c0a47, 0xaf75074c, 0xbd6e1051, 0xb3671d5a, 0x99583e6b, 0x97513360, 0x854a247d, 0x8b432976, 0xd134621f, 0xdf3d6f14, 0xcd267809, 0xc32f7502, 0xe9105633, 0xe7195b38, 0xf5024c25, 0xfb0b412e, 0x9ad7618c, 0x94de6c87, 0x86c57b9a, 0x88cc7691, 0xa2f355a0, 0xacfa58ab, 0xbee14fb6, 0xb0e842bd, 0xea9f09d4, 0xe49604df, 0xf68d13c2, 0xf8841ec9, 0xd2bb3df8, 0xdcb230f3, 0xcea927ee, 0xc0a02ae5, 0x7a47b13c, 0x744ebc37, 0x6655ab2a, 0x685ca621, 0x42638510, 0x4c6a881b, 0x5e719f06, 0x5078920d, 0x0a0fd964, 0x0406d46f, 0x161dc372, 0x1814ce79, 0x322bed48, 0x3c22e043, 0x2e39f75e, 0x2030fa55, 0xec9ab701, 0xe293ba0a, 0xf088ad17, 0xfe81a01c, 0xd4be832d, 0xdab78e26, 0xc8ac993b, 0xc6a59430, 0x9cd2df59, 0x92dbd252, 0x80c0c54f, 0x8ec9c844, 0xa4f6eb75, 0xaaffe67e, 0xb8e4f163, 0xb6edfc68, 0x0c0a67b1, 0x02036aba, 0x10187da7, 0x1e1170ac, 0x342e539d, 0x3a275e96, 0x283c498b, 0x26354480, 0x7c420fe9, 0x724b02e2, 0x605015ff, 0x6e5918f4, 0x44663bc5, 0x4a6f36ce, 0x587421d3, 0x567d2cd8, 0x37a10c7a, 0x39a80171, 0x2bb3166c, 0x25ba1b67, 0x0f853856, 0x018c355d, 0x13972240, 0x1d9e2f4b, 0x47e96422, 0x49e06929, 0x5bfb7e34, 0x55f2733f, 0x7fcd500e, 0x71c45d05, 0x63df4a18, 0x6dd64713, 0xd731dcca, 0xd938d1c1, 0xcb23c6dc, 0xc52acbd7, 0xef15e8e6, 0xe11ce5ed, 0xf307f2f0, 0xfd0efffb, 0xa779b492, 0xa970b999, 0xbb6bae84, 0xb562a38f, 0x9f5d80be, 0x91548db5, 0x834f9aa8, 0x8d4697a3];
        var U2 = [0x00000000, 0x0b0e090d, 0x161c121a, 0x1d121b17, 0x2c382434, 0x27362d39, 0x3a24362e, 0x312a3f23, 0x58704868, 0x537e4165, 0x4e6c5a72, 0x4562537f, 0x74486c5c, 0x7f466551, 0x62547e46, 0x695a774b, 0xb0e090d0, 0xbbee99dd, 0xa6fc82ca, 0xadf28bc7, 0x9cd8b4e4, 0x97d6bde9, 0x8ac4a6fe, 0x81caaff3, 0xe890d8b8, 0xe39ed1b5, 0xfe8ccaa2, 0xf582c3af, 0xc4a8fc8c, 0xcfa6f581, 0xd2b4ee96, 0xd9bae79b, 0x7bdb3bbb, 0x70d532b6, 0x6dc729a1, 0x66c920ac, 0x57e31f8f, 0x5ced1682, 0x41ff0d95, 0x4af10498, 0x23ab73d3, 0x28a57ade, 0x35b761c9, 0x3eb968c4, 0x0f9357e7, 0x049d5eea, 0x198f45fd, 0x12814cf0, 0xcb3bab6b, 0xc035a266, 0xdd27b971, 0xd629b07c, 0xe7038f5f, 0xec0d8652, 0xf11f9d45, 0xfa119448, 0x934be303, 0x9845ea0e, 0x8557f119, 0x8e59f814, 0xbf73c737, 0xb47dce3a, 0xa96fd52d, 0xa261dc20, 0xf6ad766d, 0xfda37f60, 0xe0b16477, 0xebbf6d7a, 0xda955259, 0xd19b5b54, 0xcc894043, 0xc787494e, 0xaedd3e05, 0xa5d33708, 0xb8c12c1f, 0xb3cf2512, 0x82e51a31, 0x89eb133c, 0x94f9082b, 0x9ff70126, 0x464de6bd, 0x4d43efb0, 0x5051f4a7, 0x5b5ffdaa, 0x6a75c289, 0x617bcb84, 0x7c69d093, 0x7767d99e, 0x1e3daed5, 0x1533a7d8, 0x0821bccf, 0x032fb5c2, 0x32058ae1, 0x390b83ec, 0x241998fb, 0x2f1791f6, 0x8d764dd6, 0x867844db, 0x9b6a5fcc, 0x906456c1, 0xa14e69e2, 0xaa4060ef, 0xb7527bf8, 0xbc5c72f5, 0xd50605be, 0xde080cb3, 0xc31a17a4, 0xc8141ea9, 0xf93e218a, 0xf2302887, 0xef223390, 0xe42c3a9d, 0x3d96dd06, 0x3698d40b, 0x2b8acf1c, 0x2084c611, 0x11aef932, 0x1aa0f03f, 0x07b2eb28, 0x0cbce225, 0x65e6956e, 0x6ee89c63, 0x73fa8774, 0x78f48e79, 0x49deb15a, 0x42d0b857, 0x5fc2a340, 0x54ccaa4d, 0xf741ecda, 0xfc4fe5d7, 0xe15dfec0, 0xea53f7cd, 0xdb79c8ee, 0xd077c1e3, 0xcd65daf4, 0xc66bd3f9, 0xaf31a4b2, 0xa43fadbf, 0xb92db6a8, 0xb223bfa5, 0x83098086, 0x8807898b, 0x9515929c, 0x9e1b9b91, 0x47a17c0a, 0x4caf7507, 0x51bd6e10, 0x5ab3671d, 0x6b99583e, 0x60975133, 0x7d854a24, 0x768b4329, 0x1fd13462, 0x14df3d6f, 0x09cd2678, 0x02c32f75, 0x33e91056, 0x38e7195b, 0x25f5024c, 0x2efb0b41, 0x8c9ad761, 0x8794de6c, 0x9a86c57b, 0x9188cc76, 0xa0a2f355, 0xabacfa58, 0xb6bee14f, 0xbdb0e842, 0xd4ea9f09, 0xdfe49604, 0xc2f68d13, 0xc9f8841e, 0xf8d2bb3d, 0xf3dcb230, 0xeecea927, 0xe5c0a02a, 0x3c7a47b1, 0x37744ebc, 0x2a6655ab, 0x21685ca6, 0x10426385, 0x1b4c6a88, 0x065e719f, 0x0d507892, 0x640a0fd9, 0x6f0406d4, 0x72161dc3, 0x791814ce, 0x48322bed, 0x433c22e0, 0x5e2e39f7, 0x552030fa, 0x01ec9ab7, 0x0ae293ba, 0x17f088ad, 0x1cfe81a0, 0x2dd4be83, 0x26dab78e, 0x3bc8ac99, 0x30c6a594, 0x599cd2df, 0x5292dbd2, 0x4f80c0c5, 0x448ec9c8, 0x75a4f6eb, 0x7eaaffe6, 0x63b8e4f1, 0x68b6edfc, 0xb10c0a67, 0xba02036a, 0xa710187d, 0xac1e1170, 0x9d342e53, 0x963a275e, 0x8b283c49, 0x80263544, 0xe97c420f, 0xe2724b02, 0xff605015, 0xf46e5918, 0xc544663b, 0xce4a6f36, 0xd3587421, 0xd8567d2c, 0x7a37a10c, 0x7139a801, 0x6c2bb316, 0x6725ba1b, 0x560f8538, 0x5d018c35, 0x40139722, 0x4b1d9e2f, 0x2247e964, 0x2949e069, 0x345bfb7e, 0x3f55f273, 0x0e7fcd50, 0x0571c45d, 0x1863df4a, 0x136dd647, 0xcad731dc, 0xc1d938d1, 0xdccb23c6, 0xd7c52acb, 0xe6ef15e8, 0xede11ce5, 0xf0f307f2, 0xfbfd0eff, 0x92a779b4, 0x99a970b9, 0x84bb6bae, 0x8fb562a3, 0xbe9f5d80, 0xb591548d, 0xa8834f9a, 0xa38d4697];
        var U3 = [0x00000000, 0x0d0b0e09, 0x1a161c12, 0x171d121b, 0x342c3824, 0x3927362d, 0x2e3a2436, 0x23312a3f, 0x68587048, 0x65537e41, 0x724e6c5a, 0x7f456253, 0x5c74486c, 0x517f4665, 0x4662547e, 0x4b695a77, 0xd0b0e090, 0xddbbee99, 0xcaa6fc82, 0xc7adf28b, 0xe49cd8b4, 0xe997d6bd, 0xfe8ac4a6, 0xf381caaf, 0xb8e890d8, 0xb5e39ed1, 0xa2fe8cca, 0xaff582c3, 0x8cc4a8fc, 0x81cfa6f5, 0x96d2b4ee, 0x9bd9bae7, 0xbb7bdb3b, 0xb670d532, 0xa16dc729, 0xac66c920, 0x8f57e31f, 0x825ced16, 0x9541ff0d, 0x984af104, 0xd323ab73, 0xde28a57a, 0xc935b761, 0xc43eb968, 0xe70f9357, 0xea049d5e, 0xfd198f45, 0xf012814c, 0x6bcb3bab, 0x66c035a2, 0x71dd27b9, 0x7cd629b0, 0x5fe7038f, 0x52ec0d86, 0x45f11f9d, 0x48fa1194, 0x03934be3, 0x0e9845ea, 0x198557f1, 0x148e59f8, 0x37bf73c7, 0x3ab47dce, 0x2da96fd5, 0x20a261dc, 0x6df6ad76, 0x60fda37f, 0x77e0b164, 0x7aebbf6d, 0x59da9552, 0x54d19b5b, 0x43cc8940, 0x4ec78749, 0x05aedd3e, 0x08a5d337, 0x1fb8c12c, 0x12b3cf25, 0x3182e51a, 0x3c89eb13, 0x2b94f908, 0x269ff701, 0xbd464de6, 0xb04d43ef, 0xa75051f4, 0xaa5b5ffd, 0x896a75c2, 0x84617bcb, 0x937c69d0, 0x9e7767d9, 0xd51e3dae, 0xd81533a7, 0xcf0821bc, 0xc2032fb5, 0xe132058a, 0xec390b83, 0xfb241998, 0xf62f1791, 0xd68d764d, 0xdb867844, 0xcc9b6a5f, 0xc1906456, 0xe2a14e69, 0xefaa4060, 0xf8b7527b, 0xf5bc5c72, 0xbed50605, 0xb3de080c, 0xa4c31a17, 0xa9c8141e, 0x8af93e21, 0x87f23028, 0x90ef2233, 0x9de42c3a, 0x063d96dd, 0x0b3698d4, 0x1c2b8acf, 0x112084c6, 0x3211aef9, 0x3f1aa0f0, 0x2807b2eb, 0x250cbce2, 0x6e65e695, 0x636ee89c, 0x7473fa87, 0x7978f48e, 0x5a49deb1, 0x5742d0b8, 0x405fc2a3, 0x4d54ccaa, 0xdaf741ec, 0xd7fc4fe5, 0xc0e15dfe, 0xcdea53f7, 0xeedb79c8, 0xe3d077c1, 0xf4cd65da, 0xf9c66bd3, 0xb2af31a4, 0xbfa43fad, 0xa8b92db6, 0xa5b223bf, 0x86830980, 0x8b880789, 0x9c951592, 0x919e1b9b, 0x0a47a17c, 0x074caf75, 0x1051bd6e, 0x1d5ab367, 0x3e6b9958, 0x33609751, 0x247d854a, 0x29768b43, 0x621fd134, 0x6f14df3d, 0x7809cd26, 0x7502c32f, 0x5633e910, 0x5b38e719, 0x4c25f502, 0x412efb0b, 0x618c9ad7, 0x6c8794de, 0x7b9a86c5, 0x769188cc, 0x55a0a2f3, 0x58abacfa, 0x4fb6bee1, 0x42bdb0e8, 0x09d4ea9f, 0x04dfe496, 0x13c2f68d, 0x1ec9f884, 0x3df8d2bb, 0x30f3dcb2, 0x27eecea9, 0x2ae5c0a0, 0xb13c7a47, 0xbc37744e, 0xab2a6655, 0xa621685c, 0x85104263, 0x881b4c6a, 0x9f065e71, 0x920d5078, 0xd9640a0f, 0xd46f0406, 0xc372161d, 0xce791814, 0xed48322b, 0xe0433c22, 0xf75e2e39, 0xfa552030, 0xb701ec9a, 0xba0ae293, 0xad17f088, 0xa01cfe81, 0x832dd4be, 0x8e26dab7, 0x993bc8ac, 0x9430c6a5, 0xdf599cd2, 0xd25292db, 0xc54f80c0, 0xc8448ec9, 0xeb75a4f6, 0xe67eaaff, 0xf163b8e4, 0xfc68b6ed, 0x67b10c0a, 0x6aba0203, 0x7da71018, 0x70ac1e11, 0x539d342e, 0x5e963a27, 0x498b283c, 0x44802635, 0x0fe97c42, 0x02e2724b, 0x15ff6050, 0x18f46e59, 0x3bc54466, 0x36ce4a6f, 0x21d35874, 0x2cd8567d, 0x0c7a37a1, 0x017139a8, 0x166c2bb3, 0x1b6725ba, 0x38560f85, 0x355d018c, 0x22401397, 0x2f4b1d9e, 0x642247e9, 0x692949e0, 0x7e345bfb, 0x733f55f2, 0x500e7fcd, 0x5d0571c4, 0x4a1863df, 0x47136dd6, 0xdccad731, 0xd1c1d938, 0xc6dccb23, 0xcbd7c52a, 0xe8e6ef15, 0xe5ede11c, 0xf2f0f307, 0xfffbfd0e, 0xb492a779, 0xb999a970, 0xae84bb6b, 0xa38fb562, 0x80be9f5d, 0x8db59154, 0x9aa8834f, 0x97a38d46];
        var U4 = [0x00000000, 0x090d0b0e, 0x121a161c, 0x1b171d12, 0x24342c38, 0x2d392736, 0x362e3a24, 0x3f23312a, 0x48685870, 0x4165537e, 0x5a724e6c, 0x537f4562, 0x6c5c7448, 0x65517f46, 0x7e466254, 0x774b695a, 0x90d0b0e0, 0x99ddbbee, 0x82caa6fc, 0x8bc7adf2, 0xb4e49cd8, 0xbde997d6, 0xa6fe8ac4, 0xaff381ca, 0xd8b8e890, 0xd1b5e39e, 0xcaa2fe8c, 0xc3aff582, 0xfc8cc4a8, 0xf581cfa6, 0xee96d2b4, 0xe79bd9ba, 0x3bbb7bdb, 0x32b670d5, 0x29a16dc7, 0x20ac66c9, 0x1f8f57e3, 0x16825ced, 0x0d9541ff, 0x04984af1, 0x73d323ab, 0x7ade28a5, 0x61c935b7, 0x68c43eb9, 0x57e70f93, 0x5eea049d, 0x45fd198f, 0x4cf01281, 0xab6bcb3b, 0xa266c035, 0xb971dd27, 0xb07cd629, 0x8f5fe703, 0x8652ec0d, 0x9d45f11f, 0x9448fa11, 0xe303934b, 0xea0e9845, 0xf1198557, 0xf8148e59, 0xc737bf73, 0xce3ab47d, 0xd52da96f, 0xdc20a261, 0x766df6ad, 0x7f60fda3, 0x6477e0b1, 0x6d7aebbf, 0x5259da95, 0x5b54d19b, 0x4043cc89, 0x494ec787, 0x3e05aedd, 0x3708a5d3, 0x2c1fb8c1, 0x2512b3cf, 0x1a3182e5, 0x133c89eb, 0x082b94f9, 0x01269ff7, 0xe6bd464d, 0xefb04d43, 0xf4a75051, 0xfdaa5b5f, 0xc2896a75, 0xcb84617b, 0xd0937c69, 0xd99e7767, 0xaed51e3d, 0xa7d81533, 0xbccf0821, 0xb5c2032f, 0x8ae13205, 0x83ec390b, 0x98fb2419, 0x91f62f17, 0x4dd68d76, 0x44db8678, 0x5fcc9b6a, 0x56c19064, 0x69e2a14e, 0x60efaa40, 0x7bf8b752, 0x72f5bc5c, 0x05bed506, 0x0cb3de08, 0x17a4c31a, 0x1ea9c814, 0x218af93e, 0x2887f230, 0x3390ef22, 0x3a9de42c, 0xdd063d96, 0xd40b3698, 0xcf1c2b8a, 0xc6112084, 0xf93211ae, 0xf03f1aa0, 0xeb2807b2, 0xe2250cbc, 0x956e65e6, 0x9c636ee8, 0x877473fa, 0x8e7978f4, 0xb15a49de, 0xb85742d0, 0xa3405fc2, 0xaa4d54cc, 0xecdaf741, 0xe5d7fc4f, 0xfec0e15d, 0xf7cdea53, 0xc8eedb79, 0xc1e3d077, 0xdaf4cd65, 0xd3f9c66b, 0xa4b2af31, 0xadbfa43f, 0xb6a8b92d, 0xbfa5b223, 0x80868309, 0x898b8807, 0x929c9515, 0x9b919e1b, 0x7c0a47a1, 0x75074caf, 0x6e1051bd, 0x671d5ab3, 0x583e6b99, 0x51336097, 0x4a247d85, 0x4329768b, 0x34621fd1, 0x3d6f14df, 0x267809cd, 0x2f7502c3, 0x105633e9, 0x195b38e7, 0x024c25f5, 0x0b412efb, 0xd7618c9a, 0xde6c8794, 0xc57b9a86, 0xcc769188, 0xf355a0a2, 0xfa58abac, 0xe14fb6be, 0xe842bdb0, 0x9f09d4ea, 0x9604dfe4, 0x8d13c2f6, 0x841ec9f8, 0xbb3df8d2, 0xb230f3dc, 0xa927eece, 0xa02ae5c0, 0x47b13c7a, 0x4ebc3774, 0x55ab2a66, 0x5ca62168, 0x63851042, 0x6a881b4c, 0x719f065e, 0x78920d50, 0x0fd9640a, 0x06d46f04, 0x1dc37216, 0x14ce7918, 0x2bed4832, 0x22e0433c, 0x39f75e2e, 0x30fa5520, 0x9ab701ec, 0x93ba0ae2, 0x88ad17f0, 0x81a01cfe, 0xbe832dd4, 0xb78e26da, 0xac993bc8, 0xa59430c6, 0xd2df599c, 0xdbd25292, 0xc0c54f80, 0xc9c8448e, 0xf6eb75a4, 0xffe67eaa, 0xe4f163b8, 0xedfc68b6, 0x0a67b10c, 0x036aba02, 0x187da710, 0x1170ac1e, 0x2e539d34, 0x275e963a, 0x3c498b28, 0x35448026, 0x420fe97c, 0x4b02e272, 0x5015ff60, 0x5918f46e, 0x663bc544, 0x6f36ce4a, 0x7421d358, 0x7d2cd856, 0xa10c7a37, 0xa8017139, 0xb3166c2b, 0xba1b6725, 0x8538560f, 0x8c355d01, 0x97224013, 0x9e2f4b1d, 0xe9642247, 0xe0692949, 0xfb7e345b, 0xf2733f55, 0xcd500e7f, 0xc45d0571, 0xdf4a1863, 0xd647136d, 0x31dccad7, 0x38d1c1d9, 0x23c6dccb, 0x2acbd7c5, 0x15e8e6ef, 0x1ce5ede1, 0x07f2f0f3, 0x0efffbfd, 0x79b492a7, 0x70b999a9, 0x6bae84bb, 0x62a38fb5, 0x5d80be9f, 0x548db591, 0x4f9aa883, 0x4697a38d];

        function convertToInt32(bytes) {
            var result = [];
            for (var i = 0; i < bytes.length; i += 4) {
                result.push(
                    (bytes[i    ] << 24) |
                    (bytes[i + 1] << 16) |
                    (bytes[i + 2] <<  8) |
                     bytes[i + 3]
                );
            }
            return result;
        }

        var AES = function(key) {
            if (!(this instanceof AES)) {
                throw Error('AES must be instanitated with `new`');
            }

            Object.defineProperty(this, 'key', {
                value: coerceArray(key, true)
            });

            this._prepare();
        };


        AES.prototype._prepare = function() {

            var rounds = numberOfRounds[this.key.length];
            if (rounds == null) {
                throw new Error('invalid key size (must be 16, 24 or 32 bytes)');
            }

            // encryption round keys
            this._Ke = [];

            // decryption round keys
            this._Kd = [];

            for (var i = 0; i <= rounds; i++) {
                this._Ke.push([0, 0, 0, 0]);
                this._Kd.push([0, 0, 0, 0]);
            }

            var roundKeyCount = (rounds + 1) * 4;
            var KC = this.key.length / 4;

            // convert the key into ints
            var tk = convertToInt32(this.key);

            // copy values into round key arrays
            var index;
            for (var i = 0; i < KC; i++) {
                index = i >> 2;
                this._Ke[index][i % 4] = tk[i];
                this._Kd[rounds - index][i % 4] = tk[i];
            }

            // key expansion (fips-197 section 5.2)
            var rconpointer = 0;
            var t = KC, tt;
            while (t < roundKeyCount) {
                tt = tk[KC - 1];
                tk[0] ^= ((S[(tt >> 16) & 0xFF] << 24) ^
                          (S[(tt >>  8) & 0xFF] << 16) ^
                          (S[ tt        & 0xFF] <<  8) ^
                           S[(tt >> 24) & 0xFF]        ^
                          (rcon[rconpointer] << 24));
                rconpointer += 1;

                // key expansion (for non-256 bit)
                if (KC != 8) {
                    for (var i = 1; i < KC; i++) {
                        tk[i] ^= tk[i - 1];
                    }

                // key expansion for 256-bit keys is "slightly different" (fips-197)
                } else {
                    for (var i = 1; i < (KC / 2); i++) {
                        tk[i] ^= tk[i - 1];
                    }
                    tt = tk[(KC / 2) - 1];

                    tk[KC / 2] ^= (S[ tt        & 0xFF]        ^
                                  (S[(tt >>  8) & 0xFF] <<  8) ^
                                  (S[(tt >> 16) & 0xFF] << 16) ^
                                  (S[(tt >> 24) & 0xFF] << 24));

                    for (var i = (KC / 2) + 1; i < KC; i++) {
                        tk[i] ^= tk[i - 1];
                    }
                }

                // copy values into round key arrays
                var i = 0, r, c;
                while (i < KC && t < roundKeyCount) {
                    r = t >> 2;
                    c = t % 4;
                    this._Ke[r][c] = tk[i];
                    this._Kd[rounds - r][c] = tk[i++];
                    t++;
                }
            }

            // inverse-cipher-ify the decryption round key (fips-197 section 5.3)
            for (var r = 1; r < rounds; r++) {
                for (var c = 0; c < 4; c++) {
                    tt = this._Kd[r][c];
                    this._Kd[r][c] = (U1[(tt >> 24) & 0xFF] ^
                                      U2[(tt >> 16) & 0xFF] ^
                                      U3[(tt >>  8) & 0xFF] ^
                                      U4[ tt        & 0xFF]);
                }
            }
        };

        AES.prototype.encrypt = function(plaintext) {
            if (plaintext.length != 16) {
                throw new Error('invalid plaintext size (must be 16 bytes)');
            }

            var rounds = this._Ke.length - 1;
            var a = [0, 0, 0, 0];

            // convert plaintext to (ints ^ key)
            var t = convertToInt32(plaintext);
            for (var i = 0; i < 4; i++) {
                t[i] ^= this._Ke[0][i];
            }

            // apply round transforms
            for (var r = 1; r < rounds; r++) {
                for (var i = 0; i < 4; i++) {
                    a[i] = (T1[(t[ i         ] >> 24) & 0xff] ^
                            T2[(t[(i + 1) % 4] >> 16) & 0xff] ^
                            T3[(t[(i + 2) % 4] >>  8) & 0xff] ^
                            T4[ t[(i + 3) % 4]        & 0xff] ^
                            this._Ke[r][i]);
                }
                t = a.slice();
            }

            // the last round is special
            var result = createArray(16), tt;
            for (var i = 0; i < 4; i++) {
                tt = this._Ke[rounds][i];
                result[4 * i    ] = (S[(t[ i         ] >> 24) & 0xff] ^ (tt >> 24)) & 0xff;
                result[4 * i + 1] = (S[(t[(i + 1) % 4] >> 16) & 0xff] ^ (tt >> 16)) & 0xff;
                result[4 * i + 2] = (S[(t[(i + 2) % 4] >>  8) & 0xff] ^ (tt >>  8)) & 0xff;
                result[4 * i + 3] = (S[ t[(i + 3) % 4]        & 0xff] ^  tt       ) & 0xff;
            }

            return result;
        };

        AES.prototype.decrypt = function(ciphertext) {
            if (ciphertext.length != 16) {
                throw new Error('invalid ciphertext size (must be 16 bytes)');
            }

            var rounds = this._Kd.length - 1;
            var a = [0, 0, 0, 0];

            // convert plaintext to (ints ^ key)
            var t = convertToInt32(ciphertext);
            for (var i = 0; i < 4; i++) {
                t[i] ^= this._Kd[0][i];
            }

            // apply round transforms
            for (var r = 1; r < rounds; r++) {
                for (var i = 0; i < 4; i++) {
                    a[i] = (T5[(t[ i          ] >> 24) & 0xff] ^
                            T6[(t[(i + 3) % 4] >> 16) & 0xff] ^
                            T7[(t[(i + 2) % 4] >>  8) & 0xff] ^
                            T8[ t[(i + 1) % 4]        & 0xff] ^
                            this._Kd[r][i]);
                }
                t = a.slice();
            }

            // the last round is special
            var result = createArray(16), tt;
            for (var i = 0; i < 4; i++) {
                tt = this._Kd[rounds][i];
                result[4 * i    ] = (Si[(t[ i         ] >> 24) & 0xff] ^ (tt >> 24)) & 0xff;
                result[4 * i + 1] = (Si[(t[(i + 3) % 4] >> 16) & 0xff] ^ (tt >> 16)) & 0xff;
                result[4 * i + 2] = (Si[(t[(i + 2) % 4] >>  8) & 0xff] ^ (tt >>  8)) & 0xff;
                result[4 * i + 3] = (Si[ t[(i + 1) % 4]        & 0xff] ^  tt       ) & 0xff;
            }

            return result;
        };


        /**
         *  Mode Of Operation - Electonic Codebook (ECB)
         */
        var ModeOfOperationECB = function(key) {
            if (!(this instanceof ModeOfOperationECB)) {
                throw Error('AES must be instanitated with `new`');
            }

            this.description = "Electronic Code Block";
            this.name = "ecb";

            this._aes = new AES(key);
        };

        ModeOfOperationECB.prototype.encrypt = function(plaintext) {
            plaintext = coerceArray(plaintext);

            if ((plaintext.length % 16) !== 0) {
                throw new Error('invalid plaintext size (must be multiple of 16 bytes)');
            }

            var ciphertext = createArray(plaintext.length);
            var block = createArray(16);

            for (var i = 0; i < plaintext.length; i += 16) {
                copyArray(plaintext, block, 0, i, i + 16);
                block = this._aes.encrypt(block);
                copyArray(block, ciphertext, i);
            }

            return ciphertext;
        };

        ModeOfOperationECB.prototype.decrypt = function(ciphertext) {
            ciphertext = coerceArray(ciphertext);

            if ((ciphertext.length % 16) !== 0) {
                throw new Error('invalid ciphertext size (must be multiple of 16 bytes)');
            }

            var plaintext = createArray(ciphertext.length);
            var block = createArray(16);

            for (var i = 0; i < ciphertext.length; i += 16) {
                copyArray(ciphertext, block, 0, i, i + 16);
                block = this._aes.decrypt(block);
                copyArray(block, plaintext, i);
            }

            return plaintext;
        };


        /**
         *  Mode Of Operation - Cipher Block Chaining (CBC)
         */
        var ModeOfOperationCBC = function(key, iv) {
            if (!(this instanceof ModeOfOperationCBC)) {
                throw Error('AES must be instanitated with `new`');
            }

            this.description = "Cipher Block Chaining";
            this.name = "cbc";

            if (!iv) {
                iv = createArray(16);

            } else if (iv.length != 16) {
                throw new Error('invalid initialation vector size (must be 16 bytes)');
            }

            this._lastCipherblock = coerceArray(iv, true);

            this._aes = new AES(key);
        };

        ModeOfOperationCBC.prototype.encrypt = function(plaintext) {
            plaintext = coerceArray(plaintext);

            if ((plaintext.length % 16) !== 0) {
                throw new Error('invalid plaintext size (must be multiple of 16 bytes)');
            }

            var ciphertext = createArray(plaintext.length);
            var block = createArray(16);

            for (var i = 0; i < plaintext.length; i += 16) {
                copyArray(plaintext, block, 0, i, i + 16);

                for (var j = 0; j < 16; j++) {
                    block[j] ^= this._lastCipherblock[j];
                }

                this._lastCipherblock = this._aes.encrypt(block);
                copyArray(this._lastCipherblock, ciphertext, i);
            }

            return ciphertext;
        };

        ModeOfOperationCBC.prototype.decrypt = function(ciphertext) {
            ciphertext = coerceArray(ciphertext);

            if ((ciphertext.length % 16) !== 0) {
                throw new Error('invalid ciphertext size (must be multiple of 16 bytes)');
            }

            var plaintext = createArray(ciphertext.length);
            var block = createArray(16);

            for (var i = 0; i < ciphertext.length; i += 16) {
                copyArray(ciphertext, block, 0, i, i + 16);
                block = this._aes.decrypt(block);

                for (var j = 0; j < 16; j++) {
                    plaintext[i + j] = block[j] ^ this._lastCipherblock[j];
                }

                copyArray(ciphertext, this._lastCipherblock, 0, i, i + 16);
            }

            return plaintext;
        };


        /**
         *  Mode Of Operation - Cipher Feedback (CFB)
         */
        var ModeOfOperationCFB = function(key, iv, segmentSize) {
            if (!(this instanceof ModeOfOperationCFB)) {
                throw Error('AES must be instanitated with `new`');
            }

            this.description = "Cipher Feedback";
            this.name = "cfb";

            if (!iv) {
                iv = createArray(16);

            } else if (iv.length != 16) {
                throw new Error('invalid initialation vector size (must be 16 size)');
            }

            if (!segmentSize) { segmentSize = 1; }

            this.segmentSize = segmentSize;

            this._shiftRegister = coerceArray(iv, true);

            this._aes = new AES(key);
        };

        ModeOfOperationCFB.prototype.encrypt = function(plaintext) {
            if ((plaintext.length % this.segmentSize) != 0) {
                throw new Error('invalid plaintext size (must be segmentSize bytes)');
            }

            var encrypted = coerceArray(plaintext, true);

            var xorSegment;
            for (var i = 0; i < encrypted.length; i += this.segmentSize) {
                xorSegment = this._aes.encrypt(this._shiftRegister);
                for (var j = 0; j < this.segmentSize; j++) {
                    encrypted[i + j] ^= xorSegment[j];
                }

                // Shift the register
                copyArray(this._shiftRegister, this._shiftRegister, 0, this.segmentSize);
                copyArray(encrypted, this._shiftRegister, 16 - this.segmentSize, i, i + this.segmentSize);
            }

            return encrypted;
        };

        ModeOfOperationCFB.prototype.decrypt = function(ciphertext) {
            if ((ciphertext.length % this.segmentSize) != 0) {
                throw new Error('invalid ciphertext size (must be segmentSize bytes)');
            }

            var plaintext = coerceArray(ciphertext, true);

            var xorSegment;
            for (var i = 0; i < plaintext.length; i += this.segmentSize) {
                xorSegment = this._aes.encrypt(this._shiftRegister);

                for (var j = 0; j < this.segmentSize; j++) {
                    plaintext[i + j] ^= xorSegment[j];
                }

                // Shift the register
                copyArray(this._shiftRegister, this._shiftRegister, 0, this.segmentSize);
                copyArray(ciphertext, this._shiftRegister, 16 - this.segmentSize, i, i + this.segmentSize);
            }

            return plaintext;
        };

        /**
         *  Mode Of Operation - Output Feedback (OFB)
         */
        var ModeOfOperationOFB = function(key, iv) {
            if (!(this instanceof ModeOfOperationOFB)) {
                throw Error('AES must be instanitated with `new`');
            }

            this.description = "Output Feedback";
            this.name = "ofb";

            if (!iv) {
                iv = createArray(16);

            } else if (iv.length != 16) {
                throw new Error('invalid initialation vector size (must be 16 bytes)');
            }

            this._lastPrecipher = coerceArray(iv, true);
            this._lastPrecipherIndex = 16;

            this._aes = new AES(key);
        };

        ModeOfOperationOFB.prototype.encrypt = function(plaintext) {
            var encrypted = coerceArray(plaintext, true);

            for (var i = 0; i < encrypted.length; i++) {
                if (this._lastPrecipherIndex === 16) {
                    this._lastPrecipher = this._aes.encrypt(this._lastPrecipher);
                    this._lastPrecipherIndex = 0;
                }
                encrypted[i] ^= this._lastPrecipher[this._lastPrecipherIndex++];
            }

            return encrypted;
        };

        // Decryption is symetric
        ModeOfOperationOFB.prototype.decrypt = ModeOfOperationOFB.prototype.encrypt;


        /**
         *  Counter object for CTR common mode of operation
         */
        var Counter = function(initialValue) {
            if (!(this instanceof Counter)) {
                throw Error('Counter must be instanitated with `new`');
            }

            // We allow 0, but anything false-ish uses the default 1
            if (initialValue !== 0 && !initialValue) { initialValue = 1; }

            if (typeof(initialValue) === 'number') {
                this._counter = createArray(16);
                this.setValue(initialValue);

            } else {
                this.setBytes(initialValue);
            }
        };

        Counter.prototype.setValue = function(value) {
            if (typeof(value) !== 'number' || parseInt(value) != value) {
                throw new Error('invalid counter value (must be an integer)');
            }

            // We cannot safely handle numbers beyond the safe range for integers
            if (value > Number.MAX_SAFE_INTEGER) {
                throw new Error('integer value out of safe range');
            }

            for (var index = 15; index >= 0; --index) {
                this._counter[index] = value % 256;
                value = parseInt(value / 256);
            }
        };

        Counter.prototype.setBytes = function(bytes) {
            bytes = coerceArray(bytes, true);

            if (bytes.length != 16) {
                throw new Error('invalid counter bytes size (must be 16 bytes)');
            }

            this._counter = bytes;
        };

        Counter.prototype.increment = function() {
            for (var i = 15; i >= 0; i--) {
                if (this._counter[i] === 255) {
                    this._counter[i] = 0;
                } else {
                    this._counter[i]++;
                    break;
                }
            }
        };


        /**
         *  Mode Of Operation - Counter (CTR)
         */
        var ModeOfOperationCTR = function(key, counter) {
            if (!(this instanceof ModeOfOperationCTR)) {
                throw Error('AES must be instanitated with `new`');
            }

            this.description = "Counter";
            this.name = "ctr";

            if (!(counter instanceof Counter)) {
                counter = new Counter(counter);
            }

            this._counter = counter;

            this._remainingCounter = null;
            this._remainingCounterIndex = 16;

            this._aes = new AES(key);
        };

        ModeOfOperationCTR.prototype.encrypt = function(plaintext) {
            var encrypted = coerceArray(plaintext, true);

            for (var i = 0; i < encrypted.length; i++) {
                if (this._remainingCounterIndex === 16) {
                    this._remainingCounter = this._aes.encrypt(this._counter._counter);
                    this._remainingCounterIndex = 0;
                    this._counter.increment();
                }
                encrypted[i] ^= this._remainingCounter[this._remainingCounterIndex++];
            }

            return encrypted;
        };

        // Decryption is symetric
        ModeOfOperationCTR.prototype.decrypt = ModeOfOperationCTR.prototype.encrypt;


        ///////////////////////
        // Padding

        // See:https://tools.ietf.org/html/rfc2315
        function pkcs7pad(data) {
            data = coerceArray(data, true);
            var padder = 16 - (data.length % 16);
            var result = createArray(data.length + padder);
            copyArray(data, result);
            for (var i = data.length; i < result.length; i++) {
                result[i] = padder;
            }
            return result;
        }

        function pkcs7strip(data) {
            data = coerceArray(data, true);
            if (data.length < 16) { throw new Error('PKCS#7 invalid length'); }

            var padder = data[data.length - 1];
            if (padder > 16) { throw new Error('PKCS#7 padding byte out of range'); }

            var length = data.length - padder;
            for (var i = 0; i < padder; i++) {
                if (data[length + i] !== padder) {
                    throw new Error('PKCS#7 invalid padding byte');
                }
            }

            var result = createArray(length);
            copyArray(data, result, 0, 0, length);
            return result;
        }

        ///////////////////////
        // Exporting


        // The block cipher
        var aesjs = {
            AES: AES,
            Counter: Counter,

            ModeOfOperation: {
                ecb: ModeOfOperationECB,
                cbc: ModeOfOperationCBC,
                cfb: ModeOfOperationCFB,
                ofb: ModeOfOperationOFB,
                ctr: ModeOfOperationCTR
            },

            utils: {
                hex: convertHex,
                utf8: convertUtf8
            },

            padding: {
                pkcs7: {
                    pad: pkcs7pad,
                    strip: pkcs7strip
                }
            },

            _arrayTest: {
                coerceArray: coerceArray,
                createArray: createArray,
                copyArray: copyArray,
            }
        };


        // node.js
        {
            module.exports = aesjs;

        // RequireJS/AMD
        // http://www.requirejs.org/docs/api.html
        // https://github.com/amdjs/amdjs-api/wiki/AMD
        }


    })(commonjsGlobal);
    });

    class Crypto {
        
        constructor() {

        }

        // <summary>
        // Encrypt a block using CBC and PKCS7.
        // </summary>
        // <param name="key">The key value</param>
        // <param name="data">The message to encrypt</param>
        // <returns>Returns the resulting encrypted string data as HEX.</returns>
        static AesEncrypt (key, data) {
            let bytes = aesJs.utils.hex.toBytes(key);
            const iv = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 ];
            const textBytes = aesJs.padding.pkcs7.pad(aesJs.utils.utf8.toBytes(data));
            const aesCbc = new aesJs.ModeOfOperation.cbc(bytes, iv);
            const encryptedBytes = aesCbc.encrypt(textBytes);
            const encryptedString = aesJs.utils.hex.fromBytes(encryptedBytes);

            return encryptedString;
        }
        
        // <summary>
        // Decrypt a block using a CBC and PKCS7.
        // </summary>
        // <param name="key">The key value</param>
        // <param name="data">the data to decrypt</param>
        // <returns>Returns the resulting data decrypted in plaintext.</returns>
        static AesDecrypt(key, data) {
            let bytes = aesJs.utils.hex.toBytes(key);
            const iv = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 ];
            const encryptedBytes = aesJs.utils.hex.toBytes(data);
            const aesCbc = new aesJs.ModeOfOperation.cbc(bytes, iv);
            const decryptedBytes = aesCbc.decrypt(encryptedBytes);
            const decrypted = aesJs.utils.utf8.fromBytes(aesJs.padding.pkcs7.strip(decryptedBytes));
        
            return decrypted;
        }

        // <summary>
        // Calculates the HMACSHA256 signature of a message.
        // </summary>
        // <param name="key">The Hmac Key as HEX</param>
        // <param name="messageToSign">The message to sign</param>
        // <returns>The HMACSHA256 signature as a hex string</returns>
        static HmacSignature(key, messageToSign) {
            let shaObj = new sha("SHA-256", "TEXT");

            shaObj.setHMACKey(key,'HEX');
            shaObj.update(messageToSign);
            
            return shaObj.getHMAC("HEX");
        }


        /**
         * This utility function calculates the SHA-256 value in hexadecimal format
         * @param {String} value the value to be hashed
         */
        static GenerateHash(value) {
            let shaObj = new sha('SHA-256', 'HEX');
            shaObj.update(value);
            const shaHash = shaObj.getHash('HEX');
            return shaHash;
        }
    }

    // <summary>
    // Events statically declares the various event names in messages.
    // </summary>
    const Events = {
         PairRequest : "pair_request",
         KeyRequest : "key_request",
         KeyResponse : "key_response",
         KeyCheck : "key_check",
         PairResponse : "pair_response",
         DropKeysAdvice : "drop_keys",

         LoginRequest : "login_request",
         LoginResponse : "login_response",

         Ping : "ping",
         Pong : "pong",

         PurchaseRequest : "purchase",
         PurchaseResponse : "purchase_response",
         CancelTransactionRequest : "cancel_transaction",
         CancelTransactionResponse : "cancel_response",
         GetLastTransactionRequest : "get_last_transaction",
         GetLastTransactionResponse : "last_transaction",
         RefundRequest : "refund",
         RefundResponse : "refund_response",
         SignatureRequired : "signature_required",
         SignatureDeclined : "signature_decline",
         SignatureAccepted : "signature_accept",
         AuthCodeRequired : "authorisation_code_required",
         AuthCodeAdvice : "authorisation_code_advice",

         CashoutOnlyRequest : "cash",
         CashoutOnlyResponse : "cash_response",

         MotoPurchaseRequest : "moto_purchase",
         MotoPurchaseResponse : "moto_purchase_response",

         SettleRequest : "settle",
         SettleResponse : "settle_response",
         SettlementEnquiryRequest : "settlement_enquiry",
         SettlementEnquiryResponse : "settlement_enquiry_response",

         SetPosInfoRequest : "set_pos_info",
         SetPosInfoResponse : "set_pos_info_response",

         KeyRollRequest : "request_use_next_keys",
         KeyRollResponse : "response_use_next_keys",

         Error : "error",
        
         InvalidHmacSignature : "_INVALID_SIGNATURE_",

        // Pay At Table Related Messages
        PayAtTableGetTableConfig : "get_table_config", // incoming. When eftpos wants to ask us for P@T configuration.
        PayAtTableSetTableConfig : "set_table_config", // outgoing. When we want to instruct eftpos with the P@T configuration.
        PayAtTableGetBillDetails : "get_bill_details", // incoming. When eftpos wants to aretrieve the bill for a table.
        PayAtTableBillDetails : "bill_details",        // outgoing. We reply with this when eftpos requests to us get_bill_details.
        PayAtTableBillPayment : "bill_payment",        // incoming. When the eftpos advices 

        PrintingRequest : "print",
        PrintingResponse : "print_response",

        TerminalStatusRequest : "get_terminal_status",
        TerminalStatusResponse : "terminal_status",

        BatteryLevelChanged : "battery_level_changed"
    };

    const SuccessState = {
        Unknown: 'Unknown', Success: 'Success', Failed: 'Failed'
    };

    // <summary>
    // MessageStamp represents what is required to turn an outgoing Message into Json
    // including encryption and date setting.
    // </summary>
    class MessageStamp {
        constructor(posId, secrets, serverTimeDelta) {
            this.PosId = posId;
            this.Secrets = secrets;
            this.ServerTimeDelta = serverTimeDelta;
        }
    }

    // <summary>
    // Message represents the contents of a Message.
    // See http://www.simplepaymentapi.com/#/api/message-encryption
    // </summary>
    class Message {
        constructor(id, eventName, data, needsEncryption) {
            this.Id = id;
            this.EventName = eventName;
            this.Data = data;
            this.DateTimeStamp = '';
            this.PosId = ''; // Pos_id is set here only for outgoing Un-encrypted messages. 
            this.IncommingHmac = ''; // Sometimes the logic around the incoming message might need access to the sugnature, for example in the key_check.
            this._needsEncryption = needsEncryption; // Denotes whether an outgoing message needs to be encrypted in ToJson()
            this.DecryptedJson = ''; // Set on an incoming message just so you can have a look at what it looked like in its json form.
        }

        GetSuccessState() {
            if(!this.Data || typeof this.Data.success === "undefined") {
                return SuccessState.Unknown;
            }

            return this.Data.success ? SuccessState.Success : SuccessState.Failed;
        }

        GetError() {
            return this.Data.error_reason ? this.Data.error_reason : "";
        }

        GetErrorDetail() {
            return this.Data.error_detail;
        }

        GetServerTimeDelta()
        {
            let now = Date.now();
            
            // Stamp format: 2018-04-19T01:42:38.279
            let dts = this.DateTimeStamp.split(/[\-\+\. :T]/);
            let msgTime = new Date(
                // year, month, date
                dts[0], dts[1] - 1, dts[2],
                // hour, minute, second, millis
                dts[3], dts[4], dts[5], dts[6]
            ).getTime(); // Local time zone

            return msgTime - now;
        }

        // Helper method to parse bank date format 20042018 (ddMMyyyy)
        static ParseBankDate(bankDate) {
            if(bankDate.length !== 8) return null;

            return new Date(`${bankDate.substr(4,4)}-${bankDate.substr(2,2)}-${bankDate.substr(0,2)}`);
        }

        // Parses a bank date & time str from "05Oct17" / "05:00" ("ddMMMyy/HH:mm") into date obj
        static ParseBankDateTimeStr(date, time) {
            return new Date(`${date.substr(0,2)} ${date.substr(2,3)} ${date.substr(5,2)} ${time}`);
        }

        static FromJson(msgJson, secrets) {
            let env = JSON.parse(msgJson);

            if(env.message != null) {
                let message = new Message(env.message.id, env.message.event, env.message.data, false);
                message.DecryptedJson = msgJson;
                return message;
            }

            if (secrets == null)
            {
                // This may happen if we somehow received an encrypted message from eftpos but we're not configered with secrets.
                // For example, if we cancel the pairing process a little late in the game and we get an encrypted key_check message after we've dropped the keys.
                return new Message("UNKNOWN", "NOSECRETS", null, false);
            }

            // Its encrypted, verify sig
            let sig = Crypto.HmacSignature(secrets.HmacKey, env.enc);
            if (sig.toUpperCase() != env.hmac) {
                return new Message("_", Events.InvalidHmacSignature, null, false);
            }

            let decryptedJson = Crypto.AesDecrypt(secrets.EncKey, env.enc);

            try {
                let decryptedMsg = JSON.parse(decryptedJson);

                let message = new Message(decryptedMsg.message.id, decryptedMsg.message.event, decryptedMsg.message.data, true);

                message.DateTimeStamp = decryptedMsg.message.datetime;
                message.PosId = decryptedMsg.message.pos_id;
                message.IncomingHmac = env.hmac; 
                message.DecryptedJson = decryptedJson;

                return message;

            } catch(e) {
                return new Message("UNKNOWN", "UNPARSEABLE", {"msg": decryptedJson}, false);
            }
        }

        ToJson(stamp) {
            let now = Date.now();
            let tzoffset = new Date().getTimezoneOffset() * 60 * 1000;
            let adjustedTime = new Date(now - tzoffset + stamp.ServerTimeDelta);

            // Format date: "yyyy-MM-ddTHH:mm:ss.fff"
            this.DateTimeStamp = adjustedTime.toISOString().slice(0,-1);
            this.PosId = stamp.PosId;
            
            var envelope = {
                message: {
                    id: this.Id,
                    event: this.EventName,
                    data: this.Data,
                    datetime: this.DateTimeStamp
                }
            };

            if (!this._needsEncryption) {
                // Unencrypted Messages need PosID inside the message
                envelope.message.pos_id = this.PosId;
            }
            this.DecryptedJson = JSON.stringify(envelope);

            if (!this._needsEncryption) {
                return this.DecryptedJson;
            }

            let encMsg = Crypto.AesEncrypt(stamp.Secrets.EncKey, this.DecryptedJson);
            let hmacSig = Crypto.HmacSignature(stamp.Secrets.HmacKey, encMsg);
            let encrMessageEnvelope = {enc: encMsg, hmac: hmacSig.toUpperCase(), pos_id: stamp.PosId};

            return JSON.stringify(encrMessageEnvelope);
        }
    }

    // <summary>
    // Represents the 3 Pairing statuses that the Spi instanxce can be in.
    // </summary>
    const SpiStatus = 
    {
        // <summary>
        // Paired and Connected
        // </summary>
        PairedConnected: 'PairedConnected',
        
        // <summary>
        // Paired but trying to establish a connection 
        // </summary>
        PairedConnecting: 'PairedConnecting',
        
        // <summary>
        // Unpaired
        // </summary>
        Unpaired: 'Unpaired'
    };

    // <summary>
    // The Spi instance can be in one of these flows at any point in time.
    // </summary>
    const SpiFlow = 
    {
        // <summary>
        // Currently going through the Pairing Process Flow.
        // Happens during the Unpaired SpiStatus.
        // </summary>
        Pairing: 'Pairing',
        
        // <summary>
        // Currently going through the transaction Process Flow.
        // Cannot happen in the Unpaired SpiStatus.
        // </summary>
        Transaction: 'Transaction',

        // <summary>
        // Not in any of the other states.
        // </summary>
        Idle: 'Idle'
    };

    // <summary>
    // Represents the Pairing Flow State during the pairing process 
    // </summary>
    class PairingFlowState
    {
        constructor(state) {
            // <summary>
            // Some text that can be displayed in the Pairing Process Screen
            // that indicates what the pairing process is up to.
            // </summary>
            this.Message = null;

            // <summary>
            // When true, it means that the EFTPOS is shoing the confirmation code,
            // and your user needs to press YES or NO on the EFTPOS.
            // </summary>
            this.AwaitingCheckFromEftpos = null;
            
            // <summary>
            // When true, you need to display the YES/NO buttons on you pairing screen
            // for your user to confirm the code.
            // </summary>
            this.AwaitingCheckFromPos = null;
            
            // <summary>
            // This is the confirmation code for the pairing process.
            // </summary>
            this.ConfirmationCode = null;
            
            // <summary>
            // Indicates whether the Pairing Flow has finished its job.
            // </summary>
            this.Finished = null;
            
            // <summary>
            // Indicates whether pairing was successful or not.
            // </summary>
            this.Successful = null;

            if(state) {
                Object.assign(this, state);
            }
        }   
    }

    const TransactionType = 
    {
        Purchase: 'Purchase',
        Refund: 'Refund',
        CashoutOnly: 'CashoutOnly',
        MOTO: 'MOTO',
        Settle: 'Settle',
        SettlementEnquiry: 'SettlementEnquiry',
        GetLastTransaction: 'GetLastTransaction',
        
        Preauth: 'Preauth',
        AccountVerify: 'AccountVerify'
    };

    // <summary>
    // Used as a return in the InitiateTx methods to signify whether 
    // the transaction was initiated or not, and a reason to go with it.
    // </summary>
    class InitiateTxResult
    {
        constructor(initiated, message)
        {
            // <summary>
            // Whether the tx was initiated.
            // When true, you can expect updated to your registered callback.
            // When false, you can retry calling the InitiateX method.
            // </summary>
            this.Initiated = initiated;

            // <summary>
            // Text that gives reason for the Initiated flag, especially in case of false. 
            // </summary>
            this.Message = message;
        }
    }

    // <summary>
    // Used as a return in calls mid transaction to let you know
    // whether the call was valid or not.
    // These attributes work for COM interop.
    // </summary>
    class MidTxResult
    {
        // <summary>
        // This default stucture works for COM interop.
        // </summary>
        constructor(valid, message)
        {
            this.Valid = valid;
            this.Message = message;
        }
    }    

    // <summary>
    // Represents the State during a TransactionFlow
    // </summary>
    class TransactionFlowState
    {
        constructor(posRefId, type, amountCents, message, msg)
        {
            // <summary>
            //  The id given to this transaction
            // </summary>
            this.PosRefId   = posRefId;
            this.Id         = posRefId; // obsolete, but let's maintain it for now, to mean same as PosRefId.

            // <summary>
            // Purchase/Refund/Settle/...
            // </summary>
            this.Type = type;

            // <summary>
            // A text message to display on your Transaction Flow Screen
            // </summary>
            this.DisplayMessage = msg;

            // <summary>
            // Amount in cents for this transaction
            // </summary>
            this.AmountCents = amountCents;

            // <summary>
            // Whther the request has been sent to the EFTPOS yet or not.
            // In the PairedConnecting state, the transaction is initiated
            // but the request is only sent once the connection is recovered.
            // </summary>
            this.RequestSent = false;

            // <summary>
            // The time when the request was sent to the EFTPOS.
            // </summary>
            this.RequestTime = null;
                    
            // <summary>
            // The time when we last asked for an update, including the original request at first
            // </summary>
            this.LastStateRequestTime = null;
            
            // <summary>
            // Whether we're currently attempting to Cancel the transaction.
            // </summary>
            this.AttemptingToCancel = null;
        
            // <summary>
            // When this flag is on, you need to display the dignature accept/decline buttons in your 
            // transaction flow screen.
            // </summary>
            this.AwaitingSignatureCheck = false;

            // <summary>
            // When this flag is on, you need to show your user the phone number to call to get the authorisation code.
            // Then you need to provide your user means to enter that given code and submit it via SubmitAuthCode().
            // </summary>
            this.AwaitingPhoneForAuth = null;

            // <summary>
            // Whether this transaction flow is over or not.
            // </summary>
            this.Finished = false;

            // <summary>
            // The success state of this transaction. Starts off as Unknown.
            // When finished, can be Success, Failed OR Unknown.
            // </summary>
            this.Success = SuccessState.Unknown;

            // <summary>
            // The response at the end of the transaction. 
            // Might not be present in all edge cases.
            // You can then turn this Message into the appropriate structure,
            // such as PurchaseResponse, RefundResponse, etc
            // </summary>
            this.Response = null;

            // <summary>
            // The message the we received from EFTPOS that told us that signature is required.
            // </summary>
            this.SignatureRequiredMessage = null;
        
            // <summary>
            // The message the we received from EFTPOS that told us that Phone For Auth is required.
            // </summary>
            this.PhoneForAuthRequiredMessage = null;

            // <summary>
            // The time when the cancel attempt was made.
            // </summary>
            this.CancelAttemptTime = null;

            // <summary>
            // The request message that we are sending/sent to the server.
            // </summary>
            this.Request = message;

            // <summary>
            // Whether we're currently waiting for a Get Last Transaction Response to get an update. 
            // </summary>
            this.AwaitingGltResponse = null;

            this.GLTResponsePosRefId = null;
        }

        Sent(msg)
        {
            this.RequestSent = true;
            this.RequestTime = Date.now();
            this.LastStateRequestTime = Date.now();
            this.DisplayMessage = msg;
        }

        Cancelling(msg)
        {
            this.AttemptingToCancel = true;
            this.CancelAttemptTime = Date.now();
            this.DisplayMessage = msg;
        }

        CancelFailed(msg)
        {
            this.AttemptingToCancel = false;
            this.DisplayMessage = msg;
        }

        CallingGlt()
        {
            this.AwaitingGltResponse = true;
            this.LastStateRequestTime = Date.now();
        }

        GotGltResponse()
        {
            this.AwaitingGltResponse = false;
        }
        
        Failed(response, msg)
        {
            this.Success = SuccessState.Failed;
            this.Finished = true;
            this.Response = response;
            this.DisplayMessage = msg;
        }

        SignatureRequired(spiMessage, msg)
        {
            this.SignatureRequiredMessage = spiMessage;
            this.AwaitingSignatureCheck = true;
            this.DisplayMessage = msg;
        }

        SignatureResponded(msg)
        {
            this.AwaitingSignatureCheck = false;
            this.DisplayMessage = msg;
        }
        
        PhoneForAuthRequired(spiMessage, msg)
        {
            this.PhoneForAuthRequiredMessage = spiMessage;
            this.AwaitingPhoneForAuth = true;
            this.DisplayMessage = msg;
        }
        
        AuthCodeSent(msg)
        {
            this.AwaitingPhoneForAuth = false;
            this.DisplayMessage = msg;
        }

        Completed(state, response, msg)
        {
            this.Success = state;
            this.Response = response;
            this.Finished = true;
            this.AttemptingToCancel = false;
            this.AwaitingGltResponse = false;
            this.AwaitingSignatureCheck = false;
            this.AwaitingPhoneForAuth = false;
            this.DisplayMessage = msg;
        }

        UnknownCompleted(msg)
        {
            this.Success = SuccessState.Unknown;
            this.Response = null;
            this.Finished = true;
            this.AttemptingToCancel = false;
            this.AwaitingGltResponse = false;
            this.AwaitingSignatureCheck = false;
            this.AwaitingPhoneForAuth = false;
            this.DisplayMessage = msg;
        }
    }

    // <summary>
    // Used as a return in the SubmitAuthCode method to signify whether Code is valid
    // </summary>
    class SubmitAuthCodeResult
    {
        constructor(validFormat, message)
        {
            this.ValidFormat = validFormat;

            // <summary>
            // Text that gives reason for Invalidity
            // </summary>
            this.Message = message;
        }
    }

    class SpiConfig
    {
        constructor() {
            this.PromptForCustomerCopyOnEftpos  = false;
            this.SignatureFlowOnEftpos          = false;
            this.PrintMerchantCopy              = false;
        }

        addReceiptConfig(messageData)
        {
            if (this.PromptForCustomerCopyOnEftpos)
            {
                messageData.prompt_for_customer_copy = this.PromptForCustomerCopyOnEftpos;
            }
            if (this.SignatureFlowOnEftpos)
            {
                messageData.print_for_signature_required_transactions = this.SignatureFlowOnEftpos;
            }
            if (this.PrintMerchantCopy)
            {
                messageData.print_merchant_copy = this.PrintMerchantCopy;
            }
            return messageData;
        }

        ToString()
        {
            return `PromptForCustomerCopyOnEftpos:${this.PromptForCustomerCopyOnEftpos} SignatureFlowOnEftpos:${this.SignatureFlowOnEftpos} PrintMerchantCopy: ${this.PrintMerchantCopy}`;
        }
    }

    class TransactionOptions
    {
        constructor() {
            this._customerReceiptHeader = null;
            this._customerReceiptFooter = null;
            this._merchantReceiptHeader = null;
            this._merchantReceiptFooter = null;
        }

        SetCustomerReceiptHeader(customerReceiptHeader)
        {
            this._customerReceiptHeader = customerReceiptHeader;
        }

        SetCustomerReceiptFooter(customerReceiptFooter)
        {
            this._customerReceiptFooter = customerReceiptFooter;
        }
        SetMerchantReceiptHeader(merchantReceiptHeader)
        {
            this._merchantReceiptHeader = merchantReceiptHeader;
        }
        SetMerchantReceiptFooter(merchantReceiptFooter)
        {
            this._merchantReceiptFooter = merchantReceiptFooter;
        }
        AddOptions(messageData)
        {
            messageData.customer_receipt_header = this._customerReceiptHeader;
            messageData.customer_receipt_footer = this._customerReceiptFooter;
            messageData.merchant_receipt_header = this._merchantReceiptHeader;
            messageData.merchant_receipt_footer = this._merchantReceiptFooter;

            return messageData;
        }
    }

    let __RequestIdHelperCounter = 1;

    class RequestIdHelper {
        static Id(prefix) {
            return prefix + __RequestIdHelperCounter++;
        }
    }

    // <summary>
    // Pairing Interaction 1: Outgoing
    // </summary>
    class PairRequest {
        ToMessage() {
            let data = {padding: true};
            return new Message(RequestIdHelper.Id("pr"), Events.PairRequest, data, false);
        }
    }

    // Pairing Interaction 2: Incoming
    class KeyRequest {
        constructor(m) {
            this.RequestId = m.Id;
            this.Aenc = m.Data.enc.A;
            this.Ahmac = m.Data.hmac.A;
        }
    }

    // Pairing Interaction 3: Outgoing
    class KeyResponse {
        constructor(requestId, Benc, Bhmac) {
            this.RequestId = requestId;
            this.Benc = Benc;
            this.Bhmac = Bhmac;
        }

        ToMessage() {
            let data = {
                enc: {
                    B: this.Benc
                },
                hmac: {
                    B: this.Bhmac
                }
            };

            return new Message(this.RequestId, Events.KeyResponse, data, false);
        }
    }

    // Pairing Interaction 4: Incoming
    class KeyCheck {
        constructor(m) {
            this.ConfirmationCode = m.IncomingHmac.substring(0,6);
        }
    }

    // Pairing Interaction 5: Incoming
    class PairResponse {
        constructor(m) {
            this.Success = m.Data.success;
        }
    }

    // Holder class for Secrets and KeyResponse, so that we can use them together in method signatures.
    class SecretsAndKeyResponse {
        constructor(secrets, keyResponse) {
            this.Secrets = secrets;
            this.KeyResponse = keyResponse;
        }
    }

    class DropKeysRequest
    {
        ToMessage()
        {
            return new Message(RequestIdHelper.Id("drpkys"), Events.DropKeysAdvice, null, true);
        }
    }

    class Secrets {
        constructor(encKey, hmacKey) {
            this.EncKey     = encKey;
            this.HmacKey    = hmacKey;
        }

        static save(EncKey, HmacKey) {
            localStorage.setItem('EncKey', EncKey);
            localStorage.setItem('HmacKey', HmacKey);
        }

        static restore() {
            return new Secrets(localStorage.getItem('EncKey'), localStorage.getItem('HmacKey'));
        }

        static isSaved() {
            return localStorage.getItem('EncKey') && localStorage.getItem('HmacKey');
        }

        static Reset() {
            localStorage.removeItem('EncKey');
            localStorage.removeItem('HmacKey');
        }
    }

    var bn = createCommonjsModule(function (module) {
    (function (module, exports) {

      // Utils
      function assert (val, msg) {
        if (!val) throw new Error(msg || 'Assertion failed');
      }

      // Could use `inherits` module, but don't want to move from single file
      // architecture yet.
      function inherits (ctor, superCtor) {
        ctor.super_ = superCtor;
        var TempCtor = function () {};
        TempCtor.prototype = superCtor.prototype;
        ctor.prototype = new TempCtor();
        ctor.prototype.constructor = ctor;
      }

      // BN

      function BN (number, base, endian) {
        if (BN.isBN(number)) {
          return number;
        }

        this.negative = 0;
        this.words = null;
        this.length = 0;

        // Reduction context
        this.red = null;

        if (number !== null) {
          if (base === 'le' || base === 'be') {
            endian = base;
            base = 10;
          }

          this._init(number || 0, base || 10, endian || 'be');
        }
      }
      if (typeof module === 'object') {
        module.exports = BN;
      } else {
        exports.BN = BN;
      }

      BN.BN = BN;
      BN.wordSize = 26;

      var Buffer;
      try {
        Buffer = buffer.Buffer;
      } catch (e) {
      }

      BN.isBN = function isBN (num) {
        if (num instanceof BN) {
          return true;
        }

        return num !== null && typeof num === 'object' &&
          num.constructor.wordSize === BN.wordSize && Array.isArray(num.words);
      };

      BN.max = function max (left, right) {
        if (left.cmp(right) > 0) return left;
        return right;
      };

      BN.min = function min (left, right) {
        if (left.cmp(right) < 0) return left;
        return right;
      };

      BN.prototype._init = function init (number, base, endian) {
        if (typeof number === 'number') {
          return this._initNumber(number, base, endian);
        }

        if (typeof number === 'object') {
          return this._initArray(number, base, endian);
        }

        if (base === 'hex') {
          base = 16;
        }
        assert(base === (base | 0) && base >= 2 && base <= 36);

        number = number.toString().replace(/\s+/g, '');
        var start = 0;
        if (number[0] === '-') {
          start++;
        }

        if (base === 16) {
          this._parseHex(number, start);
        } else {
          this._parseBase(number, base, start);
        }

        if (number[0] === '-') {
          this.negative = 1;
        }

        this.strip();

        if (endian !== 'le') return;

        this._initArray(this.toArray(), base, endian);
      };

      BN.prototype._initNumber = function _initNumber (number, base, endian) {
        if (number < 0) {
          this.negative = 1;
          number = -number;
        }
        if (number < 0x4000000) {
          this.words = [ number & 0x3ffffff ];
          this.length = 1;
        } else if (number < 0x10000000000000) {
          this.words = [
            number & 0x3ffffff,
            (number / 0x4000000) & 0x3ffffff
          ];
          this.length = 2;
        } else {
          assert(number < 0x20000000000000); // 2 ^ 53 (unsafe)
          this.words = [
            number & 0x3ffffff,
            (number / 0x4000000) & 0x3ffffff,
            1
          ];
          this.length = 3;
        }

        if (endian !== 'le') return;

        // Reverse the bytes
        this._initArray(this.toArray(), base, endian);
      };

      BN.prototype._initArray = function _initArray (number, base, endian) {
        // Perhaps a Uint8Array
        assert(typeof number.length === 'number');
        if (number.length <= 0) {
          this.words = [ 0 ];
          this.length = 1;
          return this;
        }

        this.length = Math.ceil(number.length / 3);
        this.words = new Array(this.length);
        for (var i = 0; i < this.length; i++) {
          this.words[i] = 0;
        }

        var j, w;
        var off = 0;
        if (endian === 'be') {
          for (i = number.length - 1, j = 0; i >= 0; i -= 3) {
            w = number[i] | (number[i - 1] << 8) | (number[i - 2] << 16);
            this.words[j] |= (w << off) & 0x3ffffff;
            this.words[j + 1] = (w >>> (26 - off)) & 0x3ffffff;
            off += 24;
            if (off >= 26) {
              off -= 26;
              j++;
            }
          }
        } else if (endian === 'le') {
          for (i = 0, j = 0; i < number.length; i += 3) {
            w = number[i] | (number[i + 1] << 8) | (number[i + 2] << 16);
            this.words[j] |= (w << off) & 0x3ffffff;
            this.words[j + 1] = (w >>> (26 - off)) & 0x3ffffff;
            off += 24;
            if (off >= 26) {
              off -= 26;
              j++;
            }
          }
        }
        return this.strip();
      };

      function parseHex (str, start, end) {
        var r = 0;
        var len = Math.min(str.length, end);
        for (var i = start; i < len; i++) {
          var c = str.charCodeAt(i) - 48;

          r <<= 4;

          // 'a' - 'f'
          if (c >= 49 && c <= 54) {
            r |= c - 49 + 0xa;

          // 'A' - 'F'
          } else if (c >= 17 && c <= 22) {
            r |= c - 17 + 0xa;

          // '0' - '9'
          } else {
            r |= c & 0xf;
          }
        }
        return r;
      }

      BN.prototype._parseHex = function _parseHex (number, start) {
        // Create possibly bigger array to ensure that it fits the number
        this.length = Math.ceil((number.length - start) / 6);
        this.words = new Array(this.length);
        for (var i = 0; i < this.length; i++) {
          this.words[i] = 0;
        }

        var j, w;
        // Scan 24-bit chunks and add them to the number
        var off = 0;
        for (i = number.length - 6, j = 0; i >= start; i -= 6) {
          w = parseHex(number, i, i + 6);
          this.words[j] |= (w << off) & 0x3ffffff;
          // NOTE: `0x3fffff` is intentional here, 26bits max shift + 24bit hex limb
          this.words[j + 1] |= w >>> (26 - off) & 0x3fffff;
          off += 24;
          if (off >= 26) {
            off -= 26;
            j++;
          }
        }
        if (i + 6 !== start) {
          w = parseHex(number, start, i + 6);
          this.words[j] |= (w << off) & 0x3ffffff;
          this.words[j + 1] |= w >>> (26 - off) & 0x3fffff;
        }
        this.strip();
      };

      function parseBase (str, start, end, mul) {
        var r = 0;
        var len = Math.min(str.length, end);
        for (var i = start; i < len; i++) {
          var c = str.charCodeAt(i) - 48;

          r *= mul;

          // 'a'
          if (c >= 49) {
            r += c - 49 + 0xa;

          // 'A'
          } else if (c >= 17) {
            r += c - 17 + 0xa;

          // '0' - '9'
          } else {
            r += c;
          }
        }
        return r;
      }

      BN.prototype._parseBase = function _parseBase (number, base, start) {
        // Initialize as zero
        this.words = [ 0 ];
        this.length = 1;

        // Find length of limb in base
        for (var limbLen = 0, limbPow = 1; limbPow <= 0x3ffffff; limbPow *= base) {
          limbLen++;
        }
        limbLen--;
        limbPow = (limbPow / base) | 0;

        var total = number.length - start;
        var mod = total % limbLen;
        var end = Math.min(total, total - mod) + start;

        var word = 0;
        for (var i = start; i < end; i += limbLen) {
          word = parseBase(number, i, i + limbLen, base);

          this.imuln(limbPow);
          if (this.words[0] + word < 0x4000000) {
            this.words[0] += word;
          } else {
            this._iaddn(word);
          }
        }

        if (mod !== 0) {
          var pow = 1;
          word = parseBase(number, i, number.length, base);

          for (i = 0; i < mod; i++) {
            pow *= base;
          }

          this.imuln(pow);
          if (this.words[0] + word < 0x4000000) {
            this.words[0] += word;
          } else {
            this._iaddn(word);
          }
        }
      };

      BN.prototype.copy = function copy (dest) {
        dest.words = new Array(this.length);
        for (var i = 0; i < this.length; i++) {
          dest.words[i] = this.words[i];
        }
        dest.length = this.length;
        dest.negative = this.negative;
        dest.red = this.red;
      };

      BN.prototype.clone = function clone () {
        var r = new BN(null);
        this.copy(r);
        return r;
      };

      BN.prototype._expand = function _expand (size) {
        while (this.length < size) {
          this.words[this.length++] = 0;
        }
        return this;
      };

      // Remove leading `0` from `this`
      BN.prototype.strip = function strip () {
        while (this.length > 1 && this.words[this.length - 1] === 0) {
          this.length--;
        }
        return this._normSign();
      };

      BN.prototype._normSign = function _normSign () {
        // -0 = 0
        if (this.length === 1 && this.words[0] === 0) {
          this.negative = 0;
        }
        return this;
      };

      BN.prototype.inspect = function inspect () {
        return (this.red ? '<BN-R: ' : '<BN: ') + this.toString(16) + '>';
      };

      /*

      var zeros = [];
      var groupSizes = [];
      var groupBases = [];

      var s = '';
      var i = -1;
      while (++i < BN.wordSize) {
        zeros[i] = s;
        s += '0';
      }
      groupSizes[0] = 0;
      groupSizes[1] = 0;
      groupBases[0] = 0;
      groupBases[1] = 0;
      var base = 2 - 1;
      while (++base < 36 + 1) {
        var groupSize = 0;
        var groupBase = 1;
        while (groupBase < (1 << BN.wordSize) / base) {
          groupBase *= base;
          groupSize += 1;
        }
        groupSizes[base] = groupSize;
        groupBases[base] = groupBase;
      }

      */

      var zeros = [
        '',
        '0',
        '00',
        '000',
        '0000',
        '00000',
        '000000',
        '0000000',
        '00000000',
        '000000000',
        '0000000000',
        '00000000000',
        '000000000000',
        '0000000000000',
        '00000000000000',
        '000000000000000',
        '0000000000000000',
        '00000000000000000',
        '000000000000000000',
        '0000000000000000000',
        '00000000000000000000',
        '000000000000000000000',
        '0000000000000000000000',
        '00000000000000000000000',
        '000000000000000000000000',
        '0000000000000000000000000'
      ];

      var groupSizes = [
        0, 0,
        25, 16, 12, 11, 10, 9, 8,
        8, 7, 7, 7, 7, 6, 6,
        6, 6, 6, 6, 6, 5, 5,
        5, 5, 5, 5, 5, 5, 5,
        5, 5, 5, 5, 5, 5, 5
      ];

      var groupBases = [
        0, 0,
        33554432, 43046721, 16777216, 48828125, 60466176, 40353607, 16777216,
        43046721, 10000000, 19487171, 35831808, 62748517, 7529536, 11390625,
        16777216, 24137569, 34012224, 47045881, 64000000, 4084101, 5153632,
        6436343, 7962624, 9765625, 11881376, 14348907, 17210368, 20511149,
        24300000, 28629151, 33554432, 39135393, 45435424, 52521875, 60466176
      ];

      BN.prototype.toString = function toString (base, padding) {
        base = base || 10;
        padding = padding | 0 || 1;

        var out;
        if (base === 16 || base === 'hex') {
          out = '';
          var off = 0;
          var carry = 0;
          for (var i = 0; i < this.length; i++) {
            var w = this.words[i];
            var word = (((w << off) | carry) & 0xffffff).toString(16);
            carry = (w >>> (24 - off)) & 0xffffff;
            if (carry !== 0 || i !== this.length - 1) {
              out = zeros[6 - word.length] + word + out;
            } else {
              out = word + out;
            }
            off += 2;
            if (off >= 26) {
              off -= 26;
              i--;
            }
          }
          if (carry !== 0) {
            out = carry.toString(16) + out;
          }
          while (out.length % padding !== 0) {
            out = '0' + out;
          }
          if (this.negative !== 0) {
            out = '-' + out;
          }
          return out;
        }

        if (base === (base | 0) && base >= 2 && base <= 36) {
          // var groupSize = Math.floor(BN.wordSize * Math.LN2 / Math.log(base));
          var groupSize = groupSizes[base];
          // var groupBase = Math.pow(base, groupSize);
          var groupBase = groupBases[base];
          out = '';
          var c = this.clone();
          c.negative = 0;
          while (!c.isZero()) {
            var r = c.modn(groupBase).toString(base);
            c = c.idivn(groupBase);

            if (!c.isZero()) {
              out = zeros[groupSize - r.length] + r + out;
            } else {
              out = r + out;
            }
          }
          if (this.isZero()) {
            out = '0' + out;
          }
          while (out.length % padding !== 0) {
            out = '0' + out;
          }
          if (this.negative !== 0) {
            out = '-' + out;
          }
          return out;
        }

        assert(false, 'Base should be between 2 and 36');
      };

      BN.prototype.toNumber = function toNumber () {
        var ret = this.words[0];
        if (this.length === 2) {
          ret += this.words[1] * 0x4000000;
        } else if (this.length === 3 && this.words[2] === 0x01) {
          // NOTE: at this stage it is known that the top bit is set
          ret += 0x10000000000000 + (this.words[1] * 0x4000000);
        } else if (this.length > 2) {
          assert(false, 'Number can only safely store up to 53 bits');
        }
        return (this.negative !== 0) ? -ret : ret;
      };

      BN.prototype.toJSON = function toJSON () {
        return this.toString(16);
      };

      BN.prototype.toBuffer = function toBuffer (endian, length) {
        assert(typeof Buffer !== 'undefined');
        return this.toArrayLike(Buffer, endian, length);
      };

      BN.prototype.toArray = function toArray (endian, length) {
        return this.toArrayLike(Array, endian, length);
      };

      BN.prototype.toArrayLike = function toArrayLike (ArrayType, endian, length) {
        var byteLength = this.byteLength();
        var reqLength = length || Math.max(1, byteLength);
        assert(byteLength <= reqLength, 'byte array longer than desired length');
        assert(reqLength > 0, 'Requested array length <= 0');

        this.strip();
        var littleEndian = endian === 'le';
        var res = new ArrayType(reqLength);

        var b, i;
        var q = this.clone();
        if (!littleEndian) {
          // Assume big-endian
          for (i = 0; i < reqLength - byteLength; i++) {
            res[i] = 0;
          }

          for (i = 0; !q.isZero(); i++) {
            b = q.andln(0xff);
            q.iushrn(8);

            res[reqLength - i - 1] = b;
          }
        } else {
          for (i = 0; !q.isZero(); i++) {
            b = q.andln(0xff);
            q.iushrn(8);

            res[i] = b;
          }

          for (; i < reqLength; i++) {
            res[i] = 0;
          }
        }

        return res;
      };

      if (Math.clz32) {
        BN.prototype._countBits = function _countBits (w) {
          return 32 - Math.clz32(w);
        };
      } else {
        BN.prototype._countBits = function _countBits (w) {
          var t = w;
          var r = 0;
          if (t >= 0x1000) {
            r += 13;
            t >>>= 13;
          }
          if (t >= 0x40) {
            r += 7;
            t >>>= 7;
          }
          if (t >= 0x8) {
            r += 4;
            t >>>= 4;
          }
          if (t >= 0x02) {
            r += 2;
            t >>>= 2;
          }
          return r + t;
        };
      }

      BN.prototype._zeroBits = function _zeroBits (w) {
        // Short-cut
        if (w === 0) return 26;

        var t = w;
        var r = 0;
        if ((t & 0x1fff) === 0) {
          r += 13;
          t >>>= 13;
        }
        if ((t & 0x7f) === 0) {
          r += 7;
          t >>>= 7;
        }
        if ((t & 0xf) === 0) {
          r += 4;
          t >>>= 4;
        }
        if ((t & 0x3) === 0) {
          r += 2;
          t >>>= 2;
        }
        if ((t & 0x1) === 0) {
          r++;
        }
        return r;
      };

      // Return number of used bits in a BN
      BN.prototype.bitLength = function bitLength () {
        var w = this.words[this.length - 1];
        var hi = this._countBits(w);
        return (this.length - 1) * 26 + hi;
      };

      function toBitArray (num) {
        var w = new Array(num.bitLength());

        for (var bit = 0; bit < w.length; bit++) {
          var off = (bit / 26) | 0;
          var wbit = bit % 26;

          w[bit] = (num.words[off] & (1 << wbit)) >>> wbit;
        }

        return w;
      }

      // Number of trailing zero bits
      BN.prototype.zeroBits = function zeroBits () {
        if (this.isZero()) return 0;

        var r = 0;
        for (var i = 0; i < this.length; i++) {
          var b = this._zeroBits(this.words[i]);
          r += b;
          if (b !== 26) break;
        }
        return r;
      };

      BN.prototype.byteLength = function byteLength () {
        return Math.ceil(this.bitLength() / 8);
      };

      BN.prototype.toTwos = function toTwos (width) {
        if (this.negative !== 0) {
          return this.abs().inotn(width).iaddn(1);
        }
        return this.clone();
      };

      BN.prototype.fromTwos = function fromTwos (width) {
        if (this.testn(width - 1)) {
          return this.notn(width).iaddn(1).ineg();
        }
        return this.clone();
      };

      BN.prototype.isNeg = function isNeg () {
        return this.negative !== 0;
      };

      // Return negative clone of `this`
      BN.prototype.neg = function neg () {
        return this.clone().ineg();
      };

      BN.prototype.ineg = function ineg () {
        if (!this.isZero()) {
          this.negative ^= 1;
        }

        return this;
      };

      // Or `num` with `this` in-place
      BN.prototype.iuor = function iuor (num) {
        while (this.length < num.length) {
          this.words[this.length++] = 0;
        }

        for (var i = 0; i < num.length; i++) {
          this.words[i] = this.words[i] | num.words[i];
        }

        return this.strip();
      };

      BN.prototype.ior = function ior (num) {
        assert((this.negative | num.negative) === 0);
        return this.iuor(num);
      };

      // Or `num` with `this`
      BN.prototype.or = function or (num) {
        if (this.length > num.length) return this.clone().ior(num);
        return num.clone().ior(this);
      };

      BN.prototype.uor = function uor (num) {
        if (this.length > num.length) return this.clone().iuor(num);
        return num.clone().iuor(this);
      };

      // And `num` with `this` in-place
      BN.prototype.iuand = function iuand (num) {
        // b = min-length(num, this)
        var b;
        if (this.length > num.length) {
          b = num;
        } else {
          b = this;
        }

        for (var i = 0; i < b.length; i++) {
          this.words[i] = this.words[i] & num.words[i];
        }

        this.length = b.length;

        return this.strip();
      };

      BN.prototype.iand = function iand (num) {
        assert((this.negative | num.negative) === 0);
        return this.iuand(num);
      };

      // And `num` with `this`
      BN.prototype.and = function and (num) {
        if (this.length > num.length) return this.clone().iand(num);
        return num.clone().iand(this);
      };

      BN.prototype.uand = function uand (num) {
        if (this.length > num.length) return this.clone().iuand(num);
        return num.clone().iuand(this);
      };

      // Xor `num` with `this` in-place
      BN.prototype.iuxor = function iuxor (num) {
        // a.length > b.length
        var a;
        var b;
        if (this.length > num.length) {
          a = this;
          b = num;
        } else {
          a = num;
          b = this;
        }

        for (var i = 0; i < b.length; i++) {
          this.words[i] = a.words[i] ^ b.words[i];
        }

        if (this !== a) {
          for (; i < a.length; i++) {
            this.words[i] = a.words[i];
          }
        }

        this.length = a.length;

        return this.strip();
      };

      BN.prototype.ixor = function ixor (num) {
        assert((this.negative | num.negative) === 0);
        return this.iuxor(num);
      };

      // Xor `num` with `this`
      BN.prototype.xor = function xor (num) {
        if (this.length > num.length) return this.clone().ixor(num);
        return num.clone().ixor(this);
      };

      BN.prototype.uxor = function uxor (num) {
        if (this.length > num.length) return this.clone().iuxor(num);
        return num.clone().iuxor(this);
      };

      // Not ``this`` with ``width`` bitwidth
      BN.prototype.inotn = function inotn (width) {
        assert(typeof width === 'number' && width >= 0);

        var bytesNeeded = Math.ceil(width / 26) | 0;
        var bitsLeft = width % 26;

        // Extend the buffer with leading zeroes
        this._expand(bytesNeeded);

        if (bitsLeft > 0) {
          bytesNeeded--;
        }

        // Handle complete words
        for (var i = 0; i < bytesNeeded; i++) {
          this.words[i] = ~this.words[i] & 0x3ffffff;
        }

        // Handle the residue
        if (bitsLeft > 0) {
          this.words[i] = ~this.words[i] & (0x3ffffff >> (26 - bitsLeft));
        }

        // And remove leading zeroes
        return this.strip();
      };

      BN.prototype.notn = function notn (width) {
        return this.clone().inotn(width);
      };

      // Set `bit` of `this`
      BN.prototype.setn = function setn (bit, val) {
        assert(typeof bit === 'number' && bit >= 0);

        var off = (bit / 26) | 0;
        var wbit = bit % 26;

        this._expand(off + 1);

        if (val) {
          this.words[off] = this.words[off] | (1 << wbit);
        } else {
          this.words[off] = this.words[off] & ~(1 << wbit);
        }

        return this.strip();
      };

      // Add `num` to `this` in-place
      BN.prototype.iadd = function iadd (num) {
        var r;

        // negative + positive
        if (this.negative !== 0 && num.negative === 0) {
          this.negative = 0;
          r = this.isub(num);
          this.negative ^= 1;
          return this._normSign();

        // positive + negative
        } else if (this.negative === 0 && num.negative !== 0) {
          num.negative = 0;
          r = this.isub(num);
          num.negative = 1;
          return r._normSign();
        }

        // a.length > b.length
        var a, b;
        if (this.length > num.length) {
          a = this;
          b = num;
        } else {
          a = num;
          b = this;
        }

        var carry = 0;
        for (var i = 0; i < b.length; i++) {
          r = (a.words[i] | 0) + (b.words[i] | 0) + carry;
          this.words[i] = r & 0x3ffffff;
          carry = r >>> 26;
        }
        for (; carry !== 0 && i < a.length; i++) {
          r = (a.words[i] | 0) + carry;
          this.words[i] = r & 0x3ffffff;
          carry = r >>> 26;
        }

        this.length = a.length;
        if (carry !== 0) {
          this.words[this.length] = carry;
          this.length++;
        // Copy the rest of the words
        } else if (a !== this) {
          for (; i < a.length; i++) {
            this.words[i] = a.words[i];
          }
        }

        return this;
      };

      // Add `num` to `this`
      BN.prototype.add = function add (num) {
        var res;
        if (num.negative !== 0 && this.negative === 0) {
          num.negative = 0;
          res = this.sub(num);
          num.negative ^= 1;
          return res;
        } else if (num.negative === 0 && this.negative !== 0) {
          this.negative = 0;
          res = num.sub(this);
          this.negative = 1;
          return res;
        }

        if (this.length > num.length) return this.clone().iadd(num);

        return num.clone().iadd(this);
      };

      // Subtract `num` from `this` in-place
      BN.prototype.isub = function isub (num) {
        // this - (-num) = this + num
        if (num.negative !== 0) {
          num.negative = 0;
          var r = this.iadd(num);
          num.negative = 1;
          return r._normSign();

        // -this - num = -(this + num)
        } else if (this.negative !== 0) {
          this.negative = 0;
          this.iadd(num);
          this.negative = 1;
          return this._normSign();
        }

        // At this point both numbers are positive
        var cmp = this.cmp(num);

        // Optimization - zeroify
        if (cmp === 0) {
          this.negative = 0;
          this.length = 1;
          this.words[0] = 0;
          return this;
        }

        // a > b
        var a, b;
        if (cmp > 0) {
          a = this;
          b = num;
        } else {
          a = num;
          b = this;
        }

        var carry = 0;
        for (var i = 0; i < b.length; i++) {
          r = (a.words[i] | 0) - (b.words[i] | 0) + carry;
          carry = r >> 26;
          this.words[i] = r & 0x3ffffff;
        }
        for (; carry !== 0 && i < a.length; i++) {
          r = (a.words[i] | 0) + carry;
          carry = r >> 26;
          this.words[i] = r & 0x3ffffff;
        }

        // Copy rest of the words
        if (carry === 0 && i < a.length && a !== this) {
          for (; i < a.length; i++) {
            this.words[i] = a.words[i];
          }
        }

        this.length = Math.max(this.length, i);

        if (a !== this) {
          this.negative = 1;
        }

        return this.strip();
      };

      // Subtract `num` from `this`
      BN.prototype.sub = function sub (num) {
        return this.clone().isub(num);
      };

      function smallMulTo (self, num, out) {
        out.negative = num.negative ^ self.negative;
        var len = (self.length + num.length) | 0;
        out.length = len;
        len = (len - 1) | 0;

        // Peel one iteration (compiler can't do it, because of code complexity)
        var a = self.words[0] | 0;
        var b = num.words[0] | 0;
        var r = a * b;

        var lo = r & 0x3ffffff;
        var carry = (r / 0x4000000) | 0;
        out.words[0] = lo;

        for (var k = 1; k < len; k++) {
          // Sum all words with the same `i + j = k` and accumulate `ncarry`,
          // note that ncarry could be >= 0x3ffffff
          var ncarry = carry >>> 26;
          var rword = carry & 0x3ffffff;
          var maxJ = Math.min(k, num.length - 1);
          for (var j = Math.max(0, k - self.length + 1); j <= maxJ; j++) {
            var i = (k - j) | 0;
            a = self.words[i] | 0;
            b = num.words[j] | 0;
            r = a * b + rword;
            ncarry += (r / 0x4000000) | 0;
            rword = r & 0x3ffffff;
          }
          out.words[k] = rword | 0;
          carry = ncarry | 0;
        }
        if (carry !== 0) {
          out.words[k] = carry | 0;
        } else {
          out.length--;
        }

        return out.strip();
      }

      // TODO(indutny): it may be reasonable to omit it for users who don't need
      // to work with 256-bit numbers, otherwise it gives 20% improvement for 256-bit
      // multiplication (like elliptic secp256k1).
      var comb10MulTo = function comb10MulTo (self, num, out) {
        var a = self.words;
        var b = num.words;
        var o = out.words;
        var c = 0;
        var lo;
        var mid;
        var hi;
        var a0 = a[0] | 0;
        var al0 = a0 & 0x1fff;
        var ah0 = a0 >>> 13;
        var a1 = a[1] | 0;
        var al1 = a1 & 0x1fff;
        var ah1 = a1 >>> 13;
        var a2 = a[2] | 0;
        var al2 = a2 & 0x1fff;
        var ah2 = a2 >>> 13;
        var a3 = a[3] | 0;
        var al3 = a3 & 0x1fff;
        var ah3 = a3 >>> 13;
        var a4 = a[4] | 0;
        var al4 = a4 & 0x1fff;
        var ah4 = a4 >>> 13;
        var a5 = a[5] | 0;
        var al5 = a5 & 0x1fff;
        var ah5 = a5 >>> 13;
        var a6 = a[6] | 0;
        var al6 = a6 & 0x1fff;
        var ah6 = a6 >>> 13;
        var a7 = a[7] | 0;
        var al7 = a7 & 0x1fff;
        var ah7 = a7 >>> 13;
        var a8 = a[8] | 0;
        var al8 = a8 & 0x1fff;
        var ah8 = a8 >>> 13;
        var a9 = a[9] | 0;
        var al9 = a9 & 0x1fff;
        var ah9 = a9 >>> 13;
        var b0 = b[0] | 0;
        var bl0 = b0 & 0x1fff;
        var bh0 = b0 >>> 13;
        var b1 = b[1] | 0;
        var bl1 = b1 & 0x1fff;
        var bh1 = b1 >>> 13;
        var b2 = b[2] | 0;
        var bl2 = b2 & 0x1fff;
        var bh2 = b2 >>> 13;
        var b3 = b[3] | 0;
        var bl3 = b3 & 0x1fff;
        var bh3 = b3 >>> 13;
        var b4 = b[4] | 0;
        var bl4 = b4 & 0x1fff;
        var bh4 = b4 >>> 13;
        var b5 = b[5] | 0;
        var bl5 = b5 & 0x1fff;
        var bh5 = b5 >>> 13;
        var b6 = b[6] | 0;
        var bl6 = b6 & 0x1fff;
        var bh6 = b6 >>> 13;
        var b7 = b[7] | 0;
        var bl7 = b7 & 0x1fff;
        var bh7 = b7 >>> 13;
        var b8 = b[8] | 0;
        var bl8 = b8 & 0x1fff;
        var bh8 = b8 >>> 13;
        var b9 = b[9] | 0;
        var bl9 = b9 & 0x1fff;
        var bh9 = b9 >>> 13;

        out.negative = self.negative ^ num.negative;
        out.length = 19;
        /* k = 0 */
        lo = Math.imul(al0, bl0);
        mid = Math.imul(al0, bh0);
        mid = (mid + Math.imul(ah0, bl0)) | 0;
        hi = Math.imul(ah0, bh0);
        var w0 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w0 >>> 26)) | 0;
        w0 &= 0x3ffffff;
        /* k = 1 */
        lo = Math.imul(al1, bl0);
        mid = Math.imul(al1, bh0);
        mid = (mid + Math.imul(ah1, bl0)) | 0;
        hi = Math.imul(ah1, bh0);
        lo = (lo + Math.imul(al0, bl1)) | 0;
        mid = (mid + Math.imul(al0, bh1)) | 0;
        mid = (mid + Math.imul(ah0, bl1)) | 0;
        hi = (hi + Math.imul(ah0, bh1)) | 0;
        var w1 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w1 >>> 26)) | 0;
        w1 &= 0x3ffffff;
        /* k = 2 */
        lo = Math.imul(al2, bl0);
        mid = Math.imul(al2, bh0);
        mid = (mid + Math.imul(ah2, bl0)) | 0;
        hi = Math.imul(ah2, bh0);
        lo = (lo + Math.imul(al1, bl1)) | 0;
        mid = (mid + Math.imul(al1, bh1)) | 0;
        mid = (mid + Math.imul(ah1, bl1)) | 0;
        hi = (hi + Math.imul(ah1, bh1)) | 0;
        lo = (lo + Math.imul(al0, bl2)) | 0;
        mid = (mid + Math.imul(al0, bh2)) | 0;
        mid = (mid + Math.imul(ah0, bl2)) | 0;
        hi = (hi + Math.imul(ah0, bh2)) | 0;
        var w2 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w2 >>> 26)) | 0;
        w2 &= 0x3ffffff;
        /* k = 3 */
        lo = Math.imul(al3, bl0);
        mid = Math.imul(al3, bh0);
        mid = (mid + Math.imul(ah3, bl0)) | 0;
        hi = Math.imul(ah3, bh0);
        lo = (lo + Math.imul(al2, bl1)) | 0;
        mid = (mid + Math.imul(al2, bh1)) | 0;
        mid = (mid + Math.imul(ah2, bl1)) | 0;
        hi = (hi + Math.imul(ah2, bh1)) | 0;
        lo = (lo + Math.imul(al1, bl2)) | 0;
        mid = (mid + Math.imul(al1, bh2)) | 0;
        mid = (mid + Math.imul(ah1, bl2)) | 0;
        hi = (hi + Math.imul(ah1, bh2)) | 0;
        lo = (lo + Math.imul(al0, bl3)) | 0;
        mid = (mid + Math.imul(al0, bh3)) | 0;
        mid = (mid + Math.imul(ah0, bl3)) | 0;
        hi = (hi + Math.imul(ah0, bh3)) | 0;
        var w3 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w3 >>> 26)) | 0;
        w3 &= 0x3ffffff;
        /* k = 4 */
        lo = Math.imul(al4, bl0);
        mid = Math.imul(al4, bh0);
        mid = (mid + Math.imul(ah4, bl0)) | 0;
        hi = Math.imul(ah4, bh0);
        lo = (lo + Math.imul(al3, bl1)) | 0;
        mid = (mid + Math.imul(al3, bh1)) | 0;
        mid = (mid + Math.imul(ah3, bl1)) | 0;
        hi = (hi + Math.imul(ah3, bh1)) | 0;
        lo = (lo + Math.imul(al2, bl2)) | 0;
        mid = (mid + Math.imul(al2, bh2)) | 0;
        mid = (mid + Math.imul(ah2, bl2)) | 0;
        hi = (hi + Math.imul(ah2, bh2)) | 0;
        lo = (lo + Math.imul(al1, bl3)) | 0;
        mid = (mid + Math.imul(al1, bh3)) | 0;
        mid = (mid + Math.imul(ah1, bl3)) | 0;
        hi = (hi + Math.imul(ah1, bh3)) | 0;
        lo = (lo + Math.imul(al0, bl4)) | 0;
        mid = (mid + Math.imul(al0, bh4)) | 0;
        mid = (mid + Math.imul(ah0, bl4)) | 0;
        hi = (hi + Math.imul(ah0, bh4)) | 0;
        var w4 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w4 >>> 26)) | 0;
        w4 &= 0x3ffffff;
        /* k = 5 */
        lo = Math.imul(al5, bl0);
        mid = Math.imul(al5, bh0);
        mid = (mid + Math.imul(ah5, bl0)) | 0;
        hi = Math.imul(ah5, bh0);
        lo = (lo + Math.imul(al4, bl1)) | 0;
        mid = (mid + Math.imul(al4, bh1)) | 0;
        mid = (mid + Math.imul(ah4, bl1)) | 0;
        hi = (hi + Math.imul(ah4, bh1)) | 0;
        lo = (lo + Math.imul(al3, bl2)) | 0;
        mid = (mid + Math.imul(al3, bh2)) | 0;
        mid = (mid + Math.imul(ah3, bl2)) | 0;
        hi = (hi + Math.imul(ah3, bh2)) | 0;
        lo = (lo + Math.imul(al2, bl3)) | 0;
        mid = (mid + Math.imul(al2, bh3)) | 0;
        mid = (mid + Math.imul(ah2, bl3)) | 0;
        hi = (hi + Math.imul(ah2, bh3)) | 0;
        lo = (lo + Math.imul(al1, bl4)) | 0;
        mid = (mid + Math.imul(al1, bh4)) | 0;
        mid = (mid + Math.imul(ah1, bl4)) | 0;
        hi = (hi + Math.imul(ah1, bh4)) | 0;
        lo = (lo + Math.imul(al0, bl5)) | 0;
        mid = (mid + Math.imul(al0, bh5)) | 0;
        mid = (mid + Math.imul(ah0, bl5)) | 0;
        hi = (hi + Math.imul(ah0, bh5)) | 0;
        var w5 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w5 >>> 26)) | 0;
        w5 &= 0x3ffffff;
        /* k = 6 */
        lo = Math.imul(al6, bl0);
        mid = Math.imul(al6, bh0);
        mid = (mid + Math.imul(ah6, bl0)) | 0;
        hi = Math.imul(ah6, bh0);
        lo = (lo + Math.imul(al5, bl1)) | 0;
        mid = (mid + Math.imul(al5, bh1)) | 0;
        mid = (mid + Math.imul(ah5, bl1)) | 0;
        hi = (hi + Math.imul(ah5, bh1)) | 0;
        lo = (lo + Math.imul(al4, bl2)) | 0;
        mid = (mid + Math.imul(al4, bh2)) | 0;
        mid = (mid + Math.imul(ah4, bl2)) | 0;
        hi = (hi + Math.imul(ah4, bh2)) | 0;
        lo = (lo + Math.imul(al3, bl3)) | 0;
        mid = (mid + Math.imul(al3, bh3)) | 0;
        mid = (mid + Math.imul(ah3, bl3)) | 0;
        hi = (hi + Math.imul(ah3, bh3)) | 0;
        lo = (lo + Math.imul(al2, bl4)) | 0;
        mid = (mid + Math.imul(al2, bh4)) | 0;
        mid = (mid + Math.imul(ah2, bl4)) | 0;
        hi = (hi + Math.imul(ah2, bh4)) | 0;
        lo = (lo + Math.imul(al1, bl5)) | 0;
        mid = (mid + Math.imul(al1, bh5)) | 0;
        mid = (mid + Math.imul(ah1, bl5)) | 0;
        hi = (hi + Math.imul(ah1, bh5)) | 0;
        lo = (lo + Math.imul(al0, bl6)) | 0;
        mid = (mid + Math.imul(al0, bh6)) | 0;
        mid = (mid + Math.imul(ah0, bl6)) | 0;
        hi = (hi + Math.imul(ah0, bh6)) | 0;
        var w6 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w6 >>> 26)) | 0;
        w6 &= 0x3ffffff;
        /* k = 7 */
        lo = Math.imul(al7, bl0);
        mid = Math.imul(al7, bh0);
        mid = (mid + Math.imul(ah7, bl0)) | 0;
        hi = Math.imul(ah7, bh0);
        lo = (lo + Math.imul(al6, bl1)) | 0;
        mid = (mid + Math.imul(al6, bh1)) | 0;
        mid = (mid + Math.imul(ah6, bl1)) | 0;
        hi = (hi + Math.imul(ah6, bh1)) | 0;
        lo = (lo + Math.imul(al5, bl2)) | 0;
        mid = (mid + Math.imul(al5, bh2)) | 0;
        mid = (mid + Math.imul(ah5, bl2)) | 0;
        hi = (hi + Math.imul(ah5, bh2)) | 0;
        lo = (lo + Math.imul(al4, bl3)) | 0;
        mid = (mid + Math.imul(al4, bh3)) | 0;
        mid = (mid + Math.imul(ah4, bl3)) | 0;
        hi = (hi + Math.imul(ah4, bh3)) | 0;
        lo = (lo + Math.imul(al3, bl4)) | 0;
        mid = (mid + Math.imul(al3, bh4)) | 0;
        mid = (mid + Math.imul(ah3, bl4)) | 0;
        hi = (hi + Math.imul(ah3, bh4)) | 0;
        lo = (lo + Math.imul(al2, bl5)) | 0;
        mid = (mid + Math.imul(al2, bh5)) | 0;
        mid = (mid + Math.imul(ah2, bl5)) | 0;
        hi = (hi + Math.imul(ah2, bh5)) | 0;
        lo = (lo + Math.imul(al1, bl6)) | 0;
        mid = (mid + Math.imul(al1, bh6)) | 0;
        mid = (mid + Math.imul(ah1, bl6)) | 0;
        hi = (hi + Math.imul(ah1, bh6)) | 0;
        lo = (lo + Math.imul(al0, bl7)) | 0;
        mid = (mid + Math.imul(al0, bh7)) | 0;
        mid = (mid + Math.imul(ah0, bl7)) | 0;
        hi = (hi + Math.imul(ah0, bh7)) | 0;
        var w7 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w7 >>> 26)) | 0;
        w7 &= 0x3ffffff;
        /* k = 8 */
        lo = Math.imul(al8, bl0);
        mid = Math.imul(al8, bh0);
        mid = (mid + Math.imul(ah8, bl0)) | 0;
        hi = Math.imul(ah8, bh0);
        lo = (lo + Math.imul(al7, bl1)) | 0;
        mid = (mid + Math.imul(al7, bh1)) | 0;
        mid = (mid + Math.imul(ah7, bl1)) | 0;
        hi = (hi + Math.imul(ah7, bh1)) | 0;
        lo = (lo + Math.imul(al6, bl2)) | 0;
        mid = (mid + Math.imul(al6, bh2)) | 0;
        mid = (mid + Math.imul(ah6, bl2)) | 0;
        hi = (hi + Math.imul(ah6, bh2)) | 0;
        lo = (lo + Math.imul(al5, bl3)) | 0;
        mid = (mid + Math.imul(al5, bh3)) | 0;
        mid = (mid + Math.imul(ah5, bl3)) | 0;
        hi = (hi + Math.imul(ah5, bh3)) | 0;
        lo = (lo + Math.imul(al4, bl4)) | 0;
        mid = (mid + Math.imul(al4, bh4)) | 0;
        mid = (mid + Math.imul(ah4, bl4)) | 0;
        hi = (hi + Math.imul(ah4, bh4)) | 0;
        lo = (lo + Math.imul(al3, bl5)) | 0;
        mid = (mid + Math.imul(al3, bh5)) | 0;
        mid = (mid + Math.imul(ah3, bl5)) | 0;
        hi = (hi + Math.imul(ah3, bh5)) | 0;
        lo = (lo + Math.imul(al2, bl6)) | 0;
        mid = (mid + Math.imul(al2, bh6)) | 0;
        mid = (mid + Math.imul(ah2, bl6)) | 0;
        hi = (hi + Math.imul(ah2, bh6)) | 0;
        lo = (lo + Math.imul(al1, bl7)) | 0;
        mid = (mid + Math.imul(al1, bh7)) | 0;
        mid = (mid + Math.imul(ah1, bl7)) | 0;
        hi = (hi + Math.imul(ah1, bh7)) | 0;
        lo = (lo + Math.imul(al0, bl8)) | 0;
        mid = (mid + Math.imul(al0, bh8)) | 0;
        mid = (mid + Math.imul(ah0, bl8)) | 0;
        hi = (hi + Math.imul(ah0, bh8)) | 0;
        var w8 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w8 >>> 26)) | 0;
        w8 &= 0x3ffffff;
        /* k = 9 */
        lo = Math.imul(al9, bl0);
        mid = Math.imul(al9, bh0);
        mid = (mid + Math.imul(ah9, bl0)) | 0;
        hi = Math.imul(ah9, bh0);
        lo = (lo + Math.imul(al8, bl1)) | 0;
        mid = (mid + Math.imul(al8, bh1)) | 0;
        mid = (mid + Math.imul(ah8, bl1)) | 0;
        hi = (hi + Math.imul(ah8, bh1)) | 0;
        lo = (lo + Math.imul(al7, bl2)) | 0;
        mid = (mid + Math.imul(al7, bh2)) | 0;
        mid = (mid + Math.imul(ah7, bl2)) | 0;
        hi = (hi + Math.imul(ah7, bh2)) | 0;
        lo = (lo + Math.imul(al6, bl3)) | 0;
        mid = (mid + Math.imul(al6, bh3)) | 0;
        mid = (mid + Math.imul(ah6, bl3)) | 0;
        hi = (hi + Math.imul(ah6, bh3)) | 0;
        lo = (lo + Math.imul(al5, bl4)) | 0;
        mid = (mid + Math.imul(al5, bh4)) | 0;
        mid = (mid + Math.imul(ah5, bl4)) | 0;
        hi = (hi + Math.imul(ah5, bh4)) | 0;
        lo = (lo + Math.imul(al4, bl5)) | 0;
        mid = (mid + Math.imul(al4, bh5)) | 0;
        mid = (mid + Math.imul(ah4, bl5)) | 0;
        hi = (hi + Math.imul(ah4, bh5)) | 0;
        lo = (lo + Math.imul(al3, bl6)) | 0;
        mid = (mid + Math.imul(al3, bh6)) | 0;
        mid = (mid + Math.imul(ah3, bl6)) | 0;
        hi = (hi + Math.imul(ah3, bh6)) | 0;
        lo = (lo + Math.imul(al2, bl7)) | 0;
        mid = (mid + Math.imul(al2, bh7)) | 0;
        mid = (mid + Math.imul(ah2, bl7)) | 0;
        hi = (hi + Math.imul(ah2, bh7)) | 0;
        lo = (lo + Math.imul(al1, bl8)) | 0;
        mid = (mid + Math.imul(al1, bh8)) | 0;
        mid = (mid + Math.imul(ah1, bl8)) | 0;
        hi = (hi + Math.imul(ah1, bh8)) | 0;
        lo = (lo + Math.imul(al0, bl9)) | 0;
        mid = (mid + Math.imul(al0, bh9)) | 0;
        mid = (mid + Math.imul(ah0, bl9)) | 0;
        hi = (hi + Math.imul(ah0, bh9)) | 0;
        var w9 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w9 >>> 26)) | 0;
        w9 &= 0x3ffffff;
        /* k = 10 */
        lo = Math.imul(al9, bl1);
        mid = Math.imul(al9, bh1);
        mid = (mid + Math.imul(ah9, bl1)) | 0;
        hi = Math.imul(ah9, bh1);
        lo = (lo + Math.imul(al8, bl2)) | 0;
        mid = (mid + Math.imul(al8, bh2)) | 0;
        mid = (mid + Math.imul(ah8, bl2)) | 0;
        hi = (hi + Math.imul(ah8, bh2)) | 0;
        lo = (lo + Math.imul(al7, bl3)) | 0;
        mid = (mid + Math.imul(al7, bh3)) | 0;
        mid = (mid + Math.imul(ah7, bl3)) | 0;
        hi = (hi + Math.imul(ah7, bh3)) | 0;
        lo = (lo + Math.imul(al6, bl4)) | 0;
        mid = (mid + Math.imul(al6, bh4)) | 0;
        mid = (mid + Math.imul(ah6, bl4)) | 0;
        hi = (hi + Math.imul(ah6, bh4)) | 0;
        lo = (lo + Math.imul(al5, bl5)) | 0;
        mid = (mid + Math.imul(al5, bh5)) | 0;
        mid = (mid + Math.imul(ah5, bl5)) | 0;
        hi = (hi + Math.imul(ah5, bh5)) | 0;
        lo = (lo + Math.imul(al4, bl6)) | 0;
        mid = (mid + Math.imul(al4, bh6)) | 0;
        mid = (mid + Math.imul(ah4, bl6)) | 0;
        hi = (hi + Math.imul(ah4, bh6)) | 0;
        lo = (lo + Math.imul(al3, bl7)) | 0;
        mid = (mid + Math.imul(al3, bh7)) | 0;
        mid = (mid + Math.imul(ah3, bl7)) | 0;
        hi = (hi + Math.imul(ah3, bh7)) | 0;
        lo = (lo + Math.imul(al2, bl8)) | 0;
        mid = (mid + Math.imul(al2, bh8)) | 0;
        mid = (mid + Math.imul(ah2, bl8)) | 0;
        hi = (hi + Math.imul(ah2, bh8)) | 0;
        lo = (lo + Math.imul(al1, bl9)) | 0;
        mid = (mid + Math.imul(al1, bh9)) | 0;
        mid = (mid + Math.imul(ah1, bl9)) | 0;
        hi = (hi + Math.imul(ah1, bh9)) | 0;
        var w10 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w10 >>> 26)) | 0;
        w10 &= 0x3ffffff;
        /* k = 11 */
        lo = Math.imul(al9, bl2);
        mid = Math.imul(al9, bh2);
        mid = (mid + Math.imul(ah9, bl2)) | 0;
        hi = Math.imul(ah9, bh2);
        lo = (lo + Math.imul(al8, bl3)) | 0;
        mid = (mid + Math.imul(al8, bh3)) | 0;
        mid = (mid + Math.imul(ah8, bl3)) | 0;
        hi = (hi + Math.imul(ah8, bh3)) | 0;
        lo = (lo + Math.imul(al7, bl4)) | 0;
        mid = (mid + Math.imul(al7, bh4)) | 0;
        mid = (mid + Math.imul(ah7, bl4)) | 0;
        hi = (hi + Math.imul(ah7, bh4)) | 0;
        lo = (lo + Math.imul(al6, bl5)) | 0;
        mid = (mid + Math.imul(al6, bh5)) | 0;
        mid = (mid + Math.imul(ah6, bl5)) | 0;
        hi = (hi + Math.imul(ah6, bh5)) | 0;
        lo = (lo + Math.imul(al5, bl6)) | 0;
        mid = (mid + Math.imul(al5, bh6)) | 0;
        mid = (mid + Math.imul(ah5, bl6)) | 0;
        hi = (hi + Math.imul(ah5, bh6)) | 0;
        lo = (lo + Math.imul(al4, bl7)) | 0;
        mid = (mid + Math.imul(al4, bh7)) | 0;
        mid = (mid + Math.imul(ah4, bl7)) | 0;
        hi = (hi + Math.imul(ah4, bh7)) | 0;
        lo = (lo + Math.imul(al3, bl8)) | 0;
        mid = (mid + Math.imul(al3, bh8)) | 0;
        mid = (mid + Math.imul(ah3, bl8)) | 0;
        hi = (hi + Math.imul(ah3, bh8)) | 0;
        lo = (lo + Math.imul(al2, bl9)) | 0;
        mid = (mid + Math.imul(al2, bh9)) | 0;
        mid = (mid + Math.imul(ah2, bl9)) | 0;
        hi = (hi + Math.imul(ah2, bh9)) | 0;
        var w11 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w11 >>> 26)) | 0;
        w11 &= 0x3ffffff;
        /* k = 12 */
        lo = Math.imul(al9, bl3);
        mid = Math.imul(al9, bh3);
        mid = (mid + Math.imul(ah9, bl3)) | 0;
        hi = Math.imul(ah9, bh3);
        lo = (lo + Math.imul(al8, bl4)) | 0;
        mid = (mid + Math.imul(al8, bh4)) | 0;
        mid = (mid + Math.imul(ah8, bl4)) | 0;
        hi = (hi + Math.imul(ah8, bh4)) | 0;
        lo = (lo + Math.imul(al7, bl5)) | 0;
        mid = (mid + Math.imul(al7, bh5)) | 0;
        mid = (mid + Math.imul(ah7, bl5)) | 0;
        hi = (hi + Math.imul(ah7, bh5)) | 0;
        lo = (lo + Math.imul(al6, bl6)) | 0;
        mid = (mid + Math.imul(al6, bh6)) | 0;
        mid = (mid + Math.imul(ah6, bl6)) | 0;
        hi = (hi + Math.imul(ah6, bh6)) | 0;
        lo = (lo + Math.imul(al5, bl7)) | 0;
        mid = (mid + Math.imul(al5, bh7)) | 0;
        mid = (mid + Math.imul(ah5, bl7)) | 0;
        hi = (hi + Math.imul(ah5, bh7)) | 0;
        lo = (lo + Math.imul(al4, bl8)) | 0;
        mid = (mid + Math.imul(al4, bh8)) | 0;
        mid = (mid + Math.imul(ah4, bl8)) | 0;
        hi = (hi + Math.imul(ah4, bh8)) | 0;
        lo = (lo + Math.imul(al3, bl9)) | 0;
        mid = (mid + Math.imul(al3, bh9)) | 0;
        mid = (mid + Math.imul(ah3, bl9)) | 0;
        hi = (hi + Math.imul(ah3, bh9)) | 0;
        var w12 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w12 >>> 26)) | 0;
        w12 &= 0x3ffffff;
        /* k = 13 */
        lo = Math.imul(al9, bl4);
        mid = Math.imul(al9, bh4);
        mid = (mid + Math.imul(ah9, bl4)) | 0;
        hi = Math.imul(ah9, bh4);
        lo = (lo + Math.imul(al8, bl5)) | 0;
        mid = (mid + Math.imul(al8, bh5)) | 0;
        mid = (mid + Math.imul(ah8, bl5)) | 0;
        hi = (hi + Math.imul(ah8, bh5)) | 0;
        lo = (lo + Math.imul(al7, bl6)) | 0;
        mid = (mid + Math.imul(al7, bh6)) | 0;
        mid = (mid + Math.imul(ah7, bl6)) | 0;
        hi = (hi + Math.imul(ah7, bh6)) | 0;
        lo = (lo + Math.imul(al6, bl7)) | 0;
        mid = (mid + Math.imul(al6, bh7)) | 0;
        mid = (mid + Math.imul(ah6, bl7)) | 0;
        hi = (hi + Math.imul(ah6, bh7)) | 0;
        lo = (lo + Math.imul(al5, bl8)) | 0;
        mid = (mid + Math.imul(al5, bh8)) | 0;
        mid = (mid + Math.imul(ah5, bl8)) | 0;
        hi = (hi + Math.imul(ah5, bh8)) | 0;
        lo = (lo + Math.imul(al4, bl9)) | 0;
        mid = (mid + Math.imul(al4, bh9)) | 0;
        mid = (mid + Math.imul(ah4, bl9)) | 0;
        hi = (hi + Math.imul(ah4, bh9)) | 0;
        var w13 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w13 >>> 26)) | 0;
        w13 &= 0x3ffffff;
        /* k = 14 */
        lo = Math.imul(al9, bl5);
        mid = Math.imul(al9, bh5);
        mid = (mid + Math.imul(ah9, bl5)) | 0;
        hi = Math.imul(ah9, bh5);
        lo = (lo + Math.imul(al8, bl6)) | 0;
        mid = (mid + Math.imul(al8, bh6)) | 0;
        mid = (mid + Math.imul(ah8, bl6)) | 0;
        hi = (hi + Math.imul(ah8, bh6)) | 0;
        lo = (lo + Math.imul(al7, bl7)) | 0;
        mid = (mid + Math.imul(al7, bh7)) | 0;
        mid = (mid + Math.imul(ah7, bl7)) | 0;
        hi = (hi + Math.imul(ah7, bh7)) | 0;
        lo = (lo + Math.imul(al6, bl8)) | 0;
        mid = (mid + Math.imul(al6, bh8)) | 0;
        mid = (mid + Math.imul(ah6, bl8)) | 0;
        hi = (hi + Math.imul(ah6, bh8)) | 0;
        lo = (lo + Math.imul(al5, bl9)) | 0;
        mid = (mid + Math.imul(al5, bh9)) | 0;
        mid = (mid + Math.imul(ah5, bl9)) | 0;
        hi = (hi + Math.imul(ah5, bh9)) | 0;
        var w14 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w14 >>> 26)) | 0;
        w14 &= 0x3ffffff;
        /* k = 15 */
        lo = Math.imul(al9, bl6);
        mid = Math.imul(al9, bh6);
        mid = (mid + Math.imul(ah9, bl6)) | 0;
        hi = Math.imul(ah9, bh6);
        lo = (lo + Math.imul(al8, bl7)) | 0;
        mid = (mid + Math.imul(al8, bh7)) | 0;
        mid = (mid + Math.imul(ah8, bl7)) | 0;
        hi = (hi + Math.imul(ah8, bh7)) | 0;
        lo = (lo + Math.imul(al7, bl8)) | 0;
        mid = (mid + Math.imul(al7, bh8)) | 0;
        mid = (mid + Math.imul(ah7, bl8)) | 0;
        hi = (hi + Math.imul(ah7, bh8)) | 0;
        lo = (lo + Math.imul(al6, bl9)) | 0;
        mid = (mid + Math.imul(al6, bh9)) | 0;
        mid = (mid + Math.imul(ah6, bl9)) | 0;
        hi = (hi + Math.imul(ah6, bh9)) | 0;
        var w15 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w15 >>> 26)) | 0;
        w15 &= 0x3ffffff;
        /* k = 16 */
        lo = Math.imul(al9, bl7);
        mid = Math.imul(al9, bh7);
        mid = (mid + Math.imul(ah9, bl7)) | 0;
        hi = Math.imul(ah9, bh7);
        lo = (lo + Math.imul(al8, bl8)) | 0;
        mid = (mid + Math.imul(al8, bh8)) | 0;
        mid = (mid + Math.imul(ah8, bl8)) | 0;
        hi = (hi + Math.imul(ah8, bh8)) | 0;
        lo = (lo + Math.imul(al7, bl9)) | 0;
        mid = (mid + Math.imul(al7, bh9)) | 0;
        mid = (mid + Math.imul(ah7, bl9)) | 0;
        hi = (hi + Math.imul(ah7, bh9)) | 0;
        var w16 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w16 >>> 26)) | 0;
        w16 &= 0x3ffffff;
        /* k = 17 */
        lo = Math.imul(al9, bl8);
        mid = Math.imul(al9, bh8);
        mid = (mid + Math.imul(ah9, bl8)) | 0;
        hi = Math.imul(ah9, bh8);
        lo = (lo + Math.imul(al8, bl9)) | 0;
        mid = (mid + Math.imul(al8, bh9)) | 0;
        mid = (mid + Math.imul(ah8, bl9)) | 0;
        hi = (hi + Math.imul(ah8, bh9)) | 0;
        var w17 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w17 >>> 26)) | 0;
        w17 &= 0x3ffffff;
        /* k = 18 */
        lo = Math.imul(al9, bl9);
        mid = Math.imul(al9, bh9);
        mid = (mid + Math.imul(ah9, bl9)) | 0;
        hi = Math.imul(ah9, bh9);
        var w18 = (((c + lo) | 0) + ((mid & 0x1fff) << 13)) | 0;
        c = (((hi + (mid >>> 13)) | 0) + (w18 >>> 26)) | 0;
        w18 &= 0x3ffffff;
        o[0] = w0;
        o[1] = w1;
        o[2] = w2;
        o[3] = w3;
        o[4] = w4;
        o[5] = w5;
        o[6] = w6;
        o[7] = w7;
        o[8] = w8;
        o[9] = w9;
        o[10] = w10;
        o[11] = w11;
        o[12] = w12;
        o[13] = w13;
        o[14] = w14;
        o[15] = w15;
        o[16] = w16;
        o[17] = w17;
        o[18] = w18;
        if (c !== 0) {
          o[19] = c;
          out.length++;
        }
        return out;
      };

      // Polyfill comb
      if (!Math.imul) {
        comb10MulTo = smallMulTo;
      }

      function bigMulTo (self, num, out) {
        out.negative = num.negative ^ self.negative;
        out.length = self.length + num.length;

        var carry = 0;
        var hncarry = 0;
        for (var k = 0; k < out.length - 1; k++) {
          // Sum all words with the same `i + j = k` and accumulate `ncarry`,
          // note that ncarry could be >= 0x3ffffff
          var ncarry = hncarry;
          hncarry = 0;
          var rword = carry & 0x3ffffff;
          var maxJ = Math.min(k, num.length - 1);
          for (var j = Math.max(0, k - self.length + 1); j <= maxJ; j++) {
            var i = k - j;
            var a = self.words[i] | 0;
            var b = num.words[j] | 0;
            var r = a * b;

            var lo = r & 0x3ffffff;
            ncarry = (ncarry + ((r / 0x4000000) | 0)) | 0;
            lo = (lo + rword) | 0;
            rword = lo & 0x3ffffff;
            ncarry = (ncarry + (lo >>> 26)) | 0;

            hncarry += ncarry >>> 26;
            ncarry &= 0x3ffffff;
          }
          out.words[k] = rword;
          carry = ncarry;
          ncarry = hncarry;
        }
        if (carry !== 0) {
          out.words[k] = carry;
        } else {
          out.length--;
        }

        return out.strip();
      }

      function jumboMulTo (self, num, out) {
        var fftm = new FFTM();
        return fftm.mulp(self, num, out);
      }

      BN.prototype.mulTo = function mulTo (num, out) {
        var res;
        var len = this.length + num.length;
        if (this.length === 10 && num.length === 10) {
          res = comb10MulTo(this, num, out);
        } else if (len < 63) {
          res = smallMulTo(this, num, out);
        } else if (len < 1024) {
          res = bigMulTo(this, num, out);
        } else {
          res = jumboMulTo(this, num, out);
        }

        return res;
      };

      // Cooley-Tukey algorithm for FFT
      // slightly revisited to rely on looping instead of recursion

      function FFTM (x, y) {
        this.x = x;
        this.y = y;
      }

      FFTM.prototype.makeRBT = function makeRBT (N) {
        var t = new Array(N);
        var l = BN.prototype._countBits(N) - 1;
        for (var i = 0; i < N; i++) {
          t[i] = this.revBin(i, l, N);
        }

        return t;
      };

      // Returns binary-reversed representation of `x`
      FFTM.prototype.revBin = function revBin (x, l, N) {
        if (x === 0 || x === N - 1) return x;

        var rb = 0;
        for (var i = 0; i < l; i++) {
          rb |= (x & 1) << (l - i - 1);
          x >>= 1;
        }

        return rb;
      };

      // Performs "tweedling" phase, therefore 'emulating'
      // behaviour of the recursive algorithm
      FFTM.prototype.permute = function permute (rbt, rws, iws, rtws, itws, N) {
        for (var i = 0; i < N; i++) {
          rtws[i] = rws[rbt[i]];
          itws[i] = iws[rbt[i]];
        }
      };

      FFTM.prototype.transform = function transform (rws, iws, rtws, itws, N, rbt) {
        this.permute(rbt, rws, iws, rtws, itws, N);

        for (var s = 1; s < N; s <<= 1) {
          var l = s << 1;

          var rtwdf = Math.cos(2 * Math.PI / l);
          var itwdf = Math.sin(2 * Math.PI / l);

          for (var p = 0; p < N; p += l) {
            var rtwdf_ = rtwdf;
            var itwdf_ = itwdf;

            for (var j = 0; j < s; j++) {
              var re = rtws[p + j];
              var ie = itws[p + j];

              var ro = rtws[p + j + s];
              var io = itws[p + j + s];

              var rx = rtwdf_ * ro - itwdf_ * io;

              io = rtwdf_ * io + itwdf_ * ro;
              ro = rx;

              rtws[p + j] = re + ro;
              itws[p + j] = ie + io;

              rtws[p + j + s] = re - ro;
              itws[p + j + s] = ie - io;

              /* jshint maxdepth : false */
              if (j !== l) {
                rx = rtwdf * rtwdf_ - itwdf * itwdf_;

                itwdf_ = rtwdf * itwdf_ + itwdf * rtwdf_;
                rtwdf_ = rx;
              }
            }
          }
        }
      };

      FFTM.prototype.guessLen13b = function guessLen13b (n, m) {
        var N = Math.max(m, n) | 1;
        var odd = N & 1;
        var i = 0;
        for (N = N / 2 | 0; N; N = N >>> 1) {
          i++;
        }

        return 1 << i + 1 + odd;
      };

      FFTM.prototype.conjugate = function conjugate (rws, iws, N) {
        if (N <= 1) return;

        for (var i = 0; i < N / 2; i++) {
          var t = rws[i];

          rws[i] = rws[N - i - 1];
          rws[N - i - 1] = t;

          t = iws[i];

          iws[i] = -iws[N - i - 1];
          iws[N - i - 1] = -t;
        }
      };

      FFTM.prototype.normalize13b = function normalize13b (ws, N) {
        var carry = 0;
        for (var i = 0; i < N / 2; i++) {
          var w = Math.round(ws[2 * i + 1] / N) * 0x2000 +
            Math.round(ws[2 * i] / N) +
            carry;

          ws[i] = w & 0x3ffffff;

          if (w < 0x4000000) {
            carry = 0;
          } else {
            carry = w / 0x4000000 | 0;
          }
        }

        return ws;
      };

      FFTM.prototype.convert13b = function convert13b (ws, len, rws, N) {
        var carry = 0;
        for (var i = 0; i < len; i++) {
          carry = carry + (ws[i] | 0);

          rws[2 * i] = carry & 0x1fff; carry = carry >>> 13;
          rws[2 * i + 1] = carry & 0x1fff; carry = carry >>> 13;
        }

        // Pad with zeroes
        for (i = 2 * len; i < N; ++i) {
          rws[i] = 0;
        }

        assert(carry === 0);
        assert((carry & ~0x1fff) === 0);
      };

      FFTM.prototype.stub = function stub (N) {
        var ph = new Array(N);
        for (var i = 0; i < N; i++) {
          ph[i] = 0;
        }

        return ph;
      };

      FFTM.prototype.mulp = function mulp (x, y, out) {
        var N = 2 * this.guessLen13b(x.length, y.length);

        var rbt = this.makeRBT(N);

        var _ = this.stub(N);

        var rws = new Array(N);
        var rwst = new Array(N);
        var iwst = new Array(N);

        var nrws = new Array(N);
        var nrwst = new Array(N);
        var niwst = new Array(N);

        var rmws = out.words;
        rmws.length = N;

        this.convert13b(x.words, x.length, rws, N);
        this.convert13b(y.words, y.length, nrws, N);

        this.transform(rws, _, rwst, iwst, N, rbt);
        this.transform(nrws, _, nrwst, niwst, N, rbt);

        for (var i = 0; i < N; i++) {
          var rx = rwst[i] * nrwst[i] - iwst[i] * niwst[i];
          iwst[i] = rwst[i] * niwst[i] + iwst[i] * nrwst[i];
          rwst[i] = rx;
        }

        this.conjugate(rwst, iwst, N);
        this.transform(rwst, iwst, rmws, _, N, rbt);
        this.conjugate(rmws, _, N);
        this.normalize13b(rmws, N);

        out.negative = x.negative ^ y.negative;
        out.length = x.length + y.length;
        return out.strip();
      };

      // Multiply `this` by `num`
      BN.prototype.mul = function mul (num) {
        var out = new BN(null);
        out.words = new Array(this.length + num.length);
        return this.mulTo(num, out);
      };

      // Multiply employing FFT
      BN.prototype.mulf = function mulf (num) {
        var out = new BN(null);
        out.words = new Array(this.length + num.length);
        return jumboMulTo(this, num, out);
      };

      // In-place Multiplication
      BN.prototype.imul = function imul (num) {
        return this.clone().mulTo(num, this);
      };

      BN.prototype.imuln = function imuln (num) {
        assert(typeof num === 'number');
        assert(num < 0x4000000);

        // Carry
        var carry = 0;
        for (var i = 0; i < this.length; i++) {
          var w = (this.words[i] | 0) * num;
          var lo = (w & 0x3ffffff) + (carry & 0x3ffffff);
          carry >>= 26;
          carry += (w / 0x4000000) | 0;
          // NOTE: lo is 27bit maximum
          carry += lo >>> 26;
          this.words[i] = lo & 0x3ffffff;
        }

        if (carry !== 0) {
          this.words[i] = carry;
          this.length++;
        }

        return this;
      };

      BN.prototype.muln = function muln (num) {
        return this.clone().imuln(num);
      };

      // `this` * `this`
      BN.prototype.sqr = function sqr () {
        return this.mul(this);
      };

      // `this` * `this` in-place
      BN.prototype.isqr = function isqr () {
        return this.imul(this.clone());
      };

      // Math.pow(`this`, `num`)
      BN.prototype.pow = function pow (num) {
        var w = toBitArray(num);
        if (w.length === 0) return new BN(1);

        // Skip leading zeroes
        var res = this;
        for (var i = 0; i < w.length; i++, res = res.sqr()) {
          if (w[i] !== 0) break;
        }

        if (++i < w.length) {
          for (var q = res.sqr(); i < w.length; i++, q = q.sqr()) {
            if (w[i] === 0) continue;

            res = res.mul(q);
          }
        }

        return res;
      };

      // Shift-left in-place
      BN.prototype.iushln = function iushln (bits) {
        assert(typeof bits === 'number' && bits >= 0);
        var r = bits % 26;
        var s = (bits - r) / 26;
        var carryMask = (0x3ffffff >>> (26 - r)) << (26 - r);
        var i;

        if (r !== 0) {
          var carry = 0;

          for (i = 0; i < this.length; i++) {
            var newCarry = this.words[i] & carryMask;
            var c = ((this.words[i] | 0) - newCarry) << r;
            this.words[i] = c | carry;
            carry = newCarry >>> (26 - r);
          }

          if (carry) {
            this.words[i] = carry;
            this.length++;
          }
        }

        if (s !== 0) {
          for (i = this.length - 1; i >= 0; i--) {
            this.words[i + s] = this.words[i];
          }

          for (i = 0; i < s; i++) {
            this.words[i] = 0;
          }

          this.length += s;
        }

        return this.strip();
      };

      BN.prototype.ishln = function ishln (bits) {
        // TODO(indutny): implement me
        assert(this.negative === 0);
        return this.iushln(bits);
      };

      // Shift-right in-place
      // NOTE: `hint` is a lowest bit before trailing zeroes
      // NOTE: if `extended` is present - it will be filled with destroyed bits
      BN.prototype.iushrn = function iushrn (bits, hint, extended) {
        assert(typeof bits === 'number' && bits >= 0);
        var h;
        if (hint) {
          h = (hint - (hint % 26)) / 26;
        } else {
          h = 0;
        }

        var r = bits % 26;
        var s = Math.min((bits - r) / 26, this.length);
        var mask = 0x3ffffff ^ ((0x3ffffff >>> r) << r);
        var maskedWords = extended;

        h -= s;
        h = Math.max(0, h);

        // Extended mode, copy masked part
        if (maskedWords) {
          for (var i = 0; i < s; i++) {
            maskedWords.words[i] = this.words[i];
          }
          maskedWords.length = s;
        }

        if (s === 0) ; else if (this.length > s) {
          this.length -= s;
          for (i = 0; i < this.length; i++) {
            this.words[i] = this.words[i + s];
          }
        } else {
          this.words[0] = 0;
          this.length = 1;
        }

        var carry = 0;
        for (i = this.length - 1; i >= 0 && (carry !== 0 || i >= h); i--) {
          var word = this.words[i] | 0;
          this.words[i] = (carry << (26 - r)) | (word >>> r);
          carry = word & mask;
        }

        // Push carried bits as a mask
        if (maskedWords && carry !== 0) {
          maskedWords.words[maskedWords.length++] = carry;
        }

        if (this.length === 0) {
          this.words[0] = 0;
          this.length = 1;
        }

        return this.strip();
      };

      BN.prototype.ishrn = function ishrn (bits, hint, extended) {
        // TODO(indutny): implement me
        assert(this.negative === 0);
        return this.iushrn(bits, hint, extended);
      };

      // Shift-left
      BN.prototype.shln = function shln (bits) {
        return this.clone().ishln(bits);
      };

      BN.prototype.ushln = function ushln (bits) {
        return this.clone().iushln(bits);
      };

      // Shift-right
      BN.prototype.shrn = function shrn (bits) {
        return this.clone().ishrn(bits);
      };

      BN.prototype.ushrn = function ushrn (bits) {
        return this.clone().iushrn(bits);
      };

      // Test if n bit is set
      BN.prototype.testn = function testn (bit) {
        assert(typeof bit === 'number' && bit >= 0);
        var r = bit % 26;
        var s = (bit - r) / 26;
        var q = 1 << r;

        // Fast case: bit is much higher than all existing words
        if (this.length <= s) return false;

        // Check bit and return
        var w = this.words[s];

        return !!(w & q);
      };

      // Return only lowers bits of number (in-place)
      BN.prototype.imaskn = function imaskn (bits) {
        assert(typeof bits === 'number' && bits >= 0);
        var r = bits % 26;
        var s = (bits - r) / 26;

        assert(this.negative === 0, 'imaskn works only with positive numbers');

        if (this.length <= s) {
          return this;
        }

        if (r !== 0) {
          s++;
        }
        this.length = Math.min(s, this.length);

        if (r !== 0) {
          var mask = 0x3ffffff ^ ((0x3ffffff >>> r) << r);
          this.words[this.length - 1] &= mask;
        }

        return this.strip();
      };

      // Return only lowers bits of number
      BN.prototype.maskn = function maskn (bits) {
        return this.clone().imaskn(bits);
      };

      // Add plain number `num` to `this`
      BN.prototype.iaddn = function iaddn (num) {
        assert(typeof num === 'number');
        assert(num < 0x4000000);
        if (num < 0) return this.isubn(-num);

        // Possible sign change
        if (this.negative !== 0) {
          if (this.length === 1 && (this.words[0] | 0) < num) {
            this.words[0] = num - (this.words[0] | 0);
            this.negative = 0;
            return this;
          }

          this.negative = 0;
          this.isubn(num);
          this.negative = 1;
          return this;
        }

        // Add without checks
        return this._iaddn(num);
      };

      BN.prototype._iaddn = function _iaddn (num) {
        this.words[0] += num;

        // Carry
        for (var i = 0; i < this.length && this.words[i] >= 0x4000000; i++) {
          this.words[i] -= 0x4000000;
          if (i === this.length - 1) {
            this.words[i + 1] = 1;
          } else {
            this.words[i + 1]++;
          }
        }
        this.length = Math.max(this.length, i + 1);

        return this;
      };

      // Subtract plain number `num` from `this`
      BN.prototype.isubn = function isubn (num) {
        assert(typeof num === 'number');
        assert(num < 0x4000000);
        if (num < 0) return this.iaddn(-num);

        if (this.negative !== 0) {
          this.negative = 0;
          this.iaddn(num);
          this.negative = 1;
          return this;
        }

        this.words[0] -= num;

        if (this.length === 1 && this.words[0] < 0) {
          this.words[0] = -this.words[0];
          this.negative = 1;
        } else {
          // Carry
          for (var i = 0; i < this.length && this.words[i] < 0; i++) {
            this.words[i] += 0x4000000;
            this.words[i + 1] -= 1;
          }
        }

        return this.strip();
      };

      BN.prototype.addn = function addn (num) {
        return this.clone().iaddn(num);
      };

      BN.prototype.subn = function subn (num) {
        return this.clone().isubn(num);
      };

      BN.prototype.iabs = function iabs () {
        this.negative = 0;

        return this;
      };

      BN.prototype.abs = function abs () {
        return this.clone().iabs();
      };

      BN.prototype._ishlnsubmul = function _ishlnsubmul (num, mul, shift) {
        var len = num.length + shift;
        var i;

        this._expand(len);

        var w;
        var carry = 0;
        for (i = 0; i < num.length; i++) {
          w = (this.words[i + shift] | 0) + carry;
          var right = (num.words[i] | 0) * mul;
          w -= right & 0x3ffffff;
          carry = (w >> 26) - ((right / 0x4000000) | 0);
          this.words[i + shift] = w & 0x3ffffff;
        }
        for (; i < this.length - shift; i++) {
          w = (this.words[i + shift] | 0) + carry;
          carry = w >> 26;
          this.words[i + shift] = w & 0x3ffffff;
        }

        if (carry === 0) return this.strip();

        // Subtraction overflow
        assert(carry === -1);
        carry = 0;
        for (i = 0; i < this.length; i++) {
          w = -(this.words[i] | 0) + carry;
          carry = w >> 26;
          this.words[i] = w & 0x3ffffff;
        }
        this.negative = 1;

        return this.strip();
      };

      BN.prototype._wordDiv = function _wordDiv (num, mode) {
        var shift = this.length - num.length;

        var a = this.clone();
        var b = num;

        // Normalize
        var bhi = b.words[b.length - 1] | 0;
        var bhiBits = this._countBits(bhi);
        shift = 26 - bhiBits;
        if (shift !== 0) {
          b = b.ushln(shift);
          a.iushln(shift);
          bhi = b.words[b.length - 1] | 0;
        }

        // Initialize quotient
        var m = a.length - b.length;
        var q;

        if (mode !== 'mod') {
          q = new BN(null);
          q.length = m + 1;
          q.words = new Array(q.length);
          for (var i = 0; i < q.length; i++) {
            q.words[i] = 0;
          }
        }

        var diff = a.clone()._ishlnsubmul(b, 1, m);
        if (diff.negative === 0) {
          a = diff;
          if (q) {
            q.words[m] = 1;
          }
        }

        for (var j = m - 1; j >= 0; j--) {
          var qj = (a.words[b.length + j] | 0) * 0x4000000 +
            (a.words[b.length + j - 1] | 0);

          // NOTE: (qj / bhi) is (0x3ffffff * 0x4000000 + 0x3ffffff) / 0x2000000 max
          // (0x7ffffff)
          qj = Math.min((qj / bhi) | 0, 0x3ffffff);

          a._ishlnsubmul(b, qj, j);
          while (a.negative !== 0) {
            qj--;
            a.negative = 0;
            a._ishlnsubmul(b, 1, j);
            if (!a.isZero()) {
              a.negative ^= 1;
            }
          }
          if (q) {
            q.words[j] = qj;
          }
        }
        if (q) {
          q.strip();
        }
        a.strip();

        // Denormalize
        if (mode !== 'div' && shift !== 0) {
          a.iushrn(shift);
        }

        return {
          div: q || null,
          mod: a
        };
      };

      // NOTE: 1) `mode` can be set to `mod` to request mod only,
      //       to `div` to request div only, or be absent to
      //       request both div & mod
      //       2) `positive` is true if unsigned mod is requested
      BN.prototype.divmod = function divmod (num, mode, positive) {
        assert(!num.isZero());

        if (this.isZero()) {
          return {
            div: new BN(0),
            mod: new BN(0)
          };
        }

        var div, mod, res;
        if (this.negative !== 0 && num.negative === 0) {
          res = this.neg().divmod(num, mode);

          if (mode !== 'mod') {
            div = res.div.neg();
          }

          if (mode !== 'div') {
            mod = res.mod.neg();
            if (positive && mod.negative !== 0) {
              mod.iadd(num);
            }
          }

          return {
            div: div,
            mod: mod
          };
        }

        if (this.negative === 0 && num.negative !== 0) {
          res = this.divmod(num.neg(), mode);

          if (mode !== 'mod') {
            div = res.div.neg();
          }

          return {
            div: div,
            mod: res.mod
          };
        }

        if ((this.negative & num.negative) !== 0) {
          res = this.neg().divmod(num.neg(), mode);

          if (mode !== 'div') {
            mod = res.mod.neg();
            if (positive && mod.negative !== 0) {
              mod.isub(num);
            }
          }

          return {
            div: res.div,
            mod: mod
          };
        }

        // Both numbers are positive at this point

        // Strip both numbers to approximate shift value
        if (num.length > this.length || this.cmp(num) < 0) {
          return {
            div: new BN(0),
            mod: this
          };
        }

        // Very short reduction
        if (num.length === 1) {
          if (mode === 'div') {
            return {
              div: this.divn(num.words[0]),
              mod: null
            };
          }

          if (mode === 'mod') {
            return {
              div: null,
              mod: new BN(this.modn(num.words[0]))
            };
          }

          return {
            div: this.divn(num.words[0]),
            mod: new BN(this.modn(num.words[0]))
          };
        }

        return this._wordDiv(num, mode);
      };

      // Find `this` / `num`
      BN.prototype.div = function div (num) {
        return this.divmod(num, 'div', false).div;
      };

      // Find `this` % `num`
      BN.prototype.mod = function mod (num) {
        return this.divmod(num, 'mod', false).mod;
      };

      BN.prototype.umod = function umod (num) {
        return this.divmod(num, 'mod', true).mod;
      };

      // Find Round(`this` / `num`)
      BN.prototype.divRound = function divRound (num) {
        var dm = this.divmod(num);

        // Fast case - exact division
        if (dm.mod.isZero()) return dm.div;

        var mod = dm.div.negative !== 0 ? dm.mod.isub(num) : dm.mod;

        var half = num.ushrn(1);
        var r2 = num.andln(1);
        var cmp = mod.cmp(half);

        // Round down
        if (cmp < 0 || r2 === 1 && cmp === 0) return dm.div;

        // Round up
        return dm.div.negative !== 0 ? dm.div.isubn(1) : dm.div.iaddn(1);
      };

      BN.prototype.modn = function modn (num) {
        assert(num <= 0x3ffffff);
        var p = (1 << 26) % num;

        var acc = 0;
        for (var i = this.length - 1; i >= 0; i--) {
          acc = (p * acc + (this.words[i] | 0)) % num;
        }

        return acc;
      };

      // In-place division by number
      BN.prototype.idivn = function idivn (num) {
        assert(num <= 0x3ffffff);

        var carry = 0;
        for (var i = this.length - 1; i >= 0; i--) {
          var w = (this.words[i] | 0) + carry * 0x4000000;
          this.words[i] = (w / num) | 0;
          carry = w % num;
        }

        return this.strip();
      };

      BN.prototype.divn = function divn (num) {
        return this.clone().idivn(num);
      };

      BN.prototype.egcd = function egcd (p) {
        assert(p.negative === 0);
        assert(!p.isZero());

        var x = this;
        var y = p.clone();

        if (x.negative !== 0) {
          x = x.umod(p);
        } else {
          x = x.clone();
        }

        // A * x + B * y = x
        var A = new BN(1);
        var B = new BN(0);

        // C * x + D * y = y
        var C = new BN(0);
        var D = new BN(1);

        var g = 0;

        while (x.isEven() && y.isEven()) {
          x.iushrn(1);
          y.iushrn(1);
          ++g;
        }

        var yp = y.clone();
        var xp = x.clone();

        while (!x.isZero()) {
          for (var i = 0, im = 1; (x.words[0] & im) === 0 && i < 26; ++i, im <<= 1);
          if (i > 0) {
            x.iushrn(i);
            while (i-- > 0) {
              if (A.isOdd() || B.isOdd()) {
                A.iadd(yp);
                B.isub(xp);
              }

              A.iushrn(1);
              B.iushrn(1);
            }
          }

          for (var j = 0, jm = 1; (y.words[0] & jm) === 0 && j < 26; ++j, jm <<= 1);
          if (j > 0) {
            y.iushrn(j);
            while (j-- > 0) {
              if (C.isOdd() || D.isOdd()) {
                C.iadd(yp);
                D.isub(xp);
              }

              C.iushrn(1);
              D.iushrn(1);
            }
          }

          if (x.cmp(y) >= 0) {
            x.isub(y);
            A.isub(C);
            B.isub(D);
          } else {
            y.isub(x);
            C.isub(A);
            D.isub(B);
          }
        }

        return {
          a: C,
          b: D,
          gcd: y.iushln(g)
        };
      };

      // This is reduced incarnation of the binary EEA
      // above, designated to invert members of the
      // _prime_ fields F(p) at a maximal speed
      BN.prototype._invmp = function _invmp (p) {
        assert(p.negative === 0);
        assert(!p.isZero());

        var a = this;
        var b = p.clone();

        if (a.negative !== 0) {
          a = a.umod(p);
        } else {
          a = a.clone();
        }

        var x1 = new BN(1);
        var x2 = new BN(0);

        var delta = b.clone();

        while (a.cmpn(1) > 0 && b.cmpn(1) > 0) {
          for (var i = 0, im = 1; (a.words[0] & im) === 0 && i < 26; ++i, im <<= 1);
          if (i > 0) {
            a.iushrn(i);
            while (i-- > 0) {
              if (x1.isOdd()) {
                x1.iadd(delta);
              }

              x1.iushrn(1);
            }
          }

          for (var j = 0, jm = 1; (b.words[0] & jm) === 0 && j < 26; ++j, jm <<= 1);
          if (j > 0) {
            b.iushrn(j);
            while (j-- > 0) {
              if (x2.isOdd()) {
                x2.iadd(delta);
              }

              x2.iushrn(1);
            }
          }

          if (a.cmp(b) >= 0) {
            a.isub(b);
            x1.isub(x2);
          } else {
            b.isub(a);
            x2.isub(x1);
          }
        }

        var res;
        if (a.cmpn(1) === 0) {
          res = x1;
        } else {
          res = x2;
        }

        if (res.cmpn(0) < 0) {
          res.iadd(p);
        }

        return res;
      };

      BN.prototype.gcd = function gcd (num) {
        if (this.isZero()) return num.abs();
        if (num.isZero()) return this.abs();

        var a = this.clone();
        var b = num.clone();
        a.negative = 0;
        b.negative = 0;

        // Remove common factor of two
        for (var shift = 0; a.isEven() && b.isEven(); shift++) {
          a.iushrn(1);
          b.iushrn(1);
        }

        do {
          while (a.isEven()) {
            a.iushrn(1);
          }
          while (b.isEven()) {
            b.iushrn(1);
          }

          var r = a.cmp(b);
          if (r < 0) {
            // Swap `a` and `b` to make `a` always bigger than `b`
            var t = a;
            a = b;
            b = t;
          } else if (r === 0 || b.cmpn(1) === 0) {
            break;
          }

          a.isub(b);
        } while (true);

        return b.iushln(shift);
      };

      // Invert number in the field F(num)
      BN.prototype.invm = function invm (num) {
        return this.egcd(num).a.umod(num);
      };

      BN.prototype.isEven = function isEven () {
        return (this.words[0] & 1) === 0;
      };

      BN.prototype.isOdd = function isOdd () {
        return (this.words[0] & 1) === 1;
      };

      // And first word and num
      BN.prototype.andln = function andln (num) {
        return this.words[0] & num;
      };

      // Increment at the bit position in-line
      BN.prototype.bincn = function bincn (bit) {
        assert(typeof bit === 'number');
        var r = bit % 26;
        var s = (bit - r) / 26;
        var q = 1 << r;

        // Fast case: bit is much higher than all existing words
        if (this.length <= s) {
          this._expand(s + 1);
          this.words[s] |= q;
          return this;
        }

        // Add bit and propagate, if needed
        var carry = q;
        for (var i = s; carry !== 0 && i < this.length; i++) {
          var w = this.words[i] | 0;
          w += carry;
          carry = w >>> 26;
          w &= 0x3ffffff;
          this.words[i] = w;
        }
        if (carry !== 0) {
          this.words[i] = carry;
          this.length++;
        }
        return this;
      };

      BN.prototype.isZero = function isZero () {
        return this.length === 1 && this.words[0] === 0;
      };

      BN.prototype.cmpn = function cmpn (num) {
        var negative = num < 0;

        if (this.negative !== 0 && !negative) return -1;
        if (this.negative === 0 && negative) return 1;

        this.strip();

        var res;
        if (this.length > 1) {
          res = 1;
        } else {
          if (negative) {
            num = -num;
          }

          assert(num <= 0x3ffffff, 'Number is too big');

          var w = this.words[0] | 0;
          res = w === num ? 0 : w < num ? -1 : 1;
        }
        if (this.negative !== 0) return -res | 0;
        return res;
      };

      // Compare two numbers and return:
      // 1 - if `this` > `num`
      // 0 - if `this` == `num`
      // -1 - if `this` < `num`
      BN.prototype.cmp = function cmp (num) {
        if (this.negative !== 0 && num.negative === 0) return -1;
        if (this.negative === 0 && num.negative !== 0) return 1;

        var res = this.ucmp(num);
        if (this.negative !== 0) return -res | 0;
        return res;
      };

      // Unsigned comparison
      BN.prototype.ucmp = function ucmp (num) {
        // At this point both numbers have the same sign
        if (this.length > num.length) return 1;
        if (this.length < num.length) return -1;

        var res = 0;
        for (var i = this.length - 1; i >= 0; i--) {
          var a = this.words[i] | 0;
          var b = num.words[i] | 0;

          if (a === b) continue;
          if (a < b) {
            res = -1;
          } else if (a > b) {
            res = 1;
          }
          break;
        }
        return res;
      };

      BN.prototype.gtn = function gtn (num) {
        return this.cmpn(num) === 1;
      };

      BN.prototype.gt = function gt (num) {
        return this.cmp(num) === 1;
      };

      BN.prototype.gten = function gten (num) {
        return this.cmpn(num) >= 0;
      };

      BN.prototype.gte = function gte (num) {
        return this.cmp(num) >= 0;
      };

      BN.prototype.ltn = function ltn (num) {
        return this.cmpn(num) === -1;
      };

      BN.prototype.lt = function lt (num) {
        return this.cmp(num) === -1;
      };

      BN.prototype.lten = function lten (num) {
        return this.cmpn(num) <= 0;
      };

      BN.prototype.lte = function lte (num) {
        return this.cmp(num) <= 0;
      };

      BN.prototype.eqn = function eqn (num) {
        return this.cmpn(num) === 0;
      };

      BN.prototype.eq = function eq (num) {
        return this.cmp(num) === 0;
      };

      //
      // A reduce context, could be using montgomery or something better, depending
      // on the `m` itself.
      //
      BN.red = function red (num) {
        return new Red(num);
      };

      BN.prototype.toRed = function toRed (ctx) {
        assert(!this.red, 'Already a number in reduction context');
        assert(this.negative === 0, 'red works only with positives');
        return ctx.convertTo(this)._forceRed(ctx);
      };

      BN.prototype.fromRed = function fromRed () {
        assert(this.red, 'fromRed works only with numbers in reduction context');
        return this.red.convertFrom(this);
      };

      BN.prototype._forceRed = function _forceRed (ctx) {
        this.red = ctx;
        return this;
      };

      BN.prototype.forceRed = function forceRed (ctx) {
        assert(!this.red, 'Already a number in reduction context');
        return this._forceRed(ctx);
      };

      BN.prototype.redAdd = function redAdd (num) {
        assert(this.red, 'redAdd works only with red numbers');
        return this.red.add(this, num);
      };

      BN.prototype.redIAdd = function redIAdd (num) {
        assert(this.red, 'redIAdd works only with red numbers');
        return this.red.iadd(this, num);
      };

      BN.prototype.redSub = function redSub (num) {
        assert(this.red, 'redSub works only with red numbers');
        return this.red.sub(this, num);
      };

      BN.prototype.redISub = function redISub (num) {
        assert(this.red, 'redISub works only with red numbers');
        return this.red.isub(this, num);
      };

      BN.prototype.redShl = function redShl (num) {
        assert(this.red, 'redShl works only with red numbers');
        return this.red.shl(this, num);
      };

      BN.prototype.redMul = function redMul (num) {
        assert(this.red, 'redMul works only with red numbers');
        this.red._verify2(this, num);
        return this.red.mul(this, num);
      };

      BN.prototype.redIMul = function redIMul (num) {
        assert(this.red, 'redMul works only with red numbers');
        this.red._verify2(this, num);
        return this.red.imul(this, num);
      };

      BN.prototype.redSqr = function redSqr () {
        assert(this.red, 'redSqr works only with red numbers');
        this.red._verify1(this);
        return this.red.sqr(this);
      };

      BN.prototype.redISqr = function redISqr () {
        assert(this.red, 'redISqr works only with red numbers');
        this.red._verify1(this);
        return this.red.isqr(this);
      };

      // Square root over p
      BN.prototype.redSqrt = function redSqrt () {
        assert(this.red, 'redSqrt works only with red numbers');
        this.red._verify1(this);
        return this.red.sqrt(this);
      };

      BN.prototype.redInvm = function redInvm () {
        assert(this.red, 'redInvm works only with red numbers');
        this.red._verify1(this);
        return this.red.invm(this);
      };

      // Return negative clone of `this` % `red modulo`
      BN.prototype.redNeg = function redNeg () {
        assert(this.red, 'redNeg works only with red numbers');
        this.red._verify1(this);
        return this.red.neg(this);
      };

      BN.prototype.redPow = function redPow (num) {
        assert(this.red && !num.red, 'redPow(normalNum)');
        this.red._verify1(this);
        return this.red.pow(this, num);
      };

      // Prime numbers with efficient reduction
      var primes = {
        k256: null,
        p224: null,
        p192: null,
        p25519: null
      };

      // Pseudo-Mersenne prime
      function MPrime (name, p) {
        // P = 2 ^ N - K
        this.name = name;
        this.p = new BN(p, 16);
        this.n = this.p.bitLength();
        this.k = new BN(1).iushln(this.n).isub(this.p);

        this.tmp = this._tmp();
      }

      MPrime.prototype._tmp = function _tmp () {
        var tmp = new BN(null);
        tmp.words = new Array(Math.ceil(this.n / 13));
        return tmp;
      };

      MPrime.prototype.ireduce = function ireduce (num) {
        // Assumes that `num` is less than `P^2`
        // num = HI * (2 ^ N - K) + HI * K + LO = HI * K + LO (mod P)
        var r = num;
        var rlen;

        do {
          this.split(r, this.tmp);
          r = this.imulK(r);
          r = r.iadd(this.tmp);
          rlen = r.bitLength();
        } while (rlen > this.n);

        var cmp = rlen < this.n ? -1 : r.ucmp(this.p);
        if (cmp === 0) {
          r.words[0] = 0;
          r.length = 1;
        } else if (cmp > 0) {
          r.isub(this.p);
        } else {
          r.strip();
        }

        return r;
      };

      MPrime.prototype.split = function split (input, out) {
        input.iushrn(this.n, 0, out);
      };

      MPrime.prototype.imulK = function imulK (num) {
        return num.imul(this.k);
      };

      function K256 () {
        MPrime.call(
          this,
          'k256',
          'ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffe fffffc2f');
      }
      inherits(K256, MPrime);

      K256.prototype.split = function split (input, output) {
        // 256 = 9 * 26 + 22
        var mask = 0x3fffff;

        var outLen = Math.min(input.length, 9);
        for (var i = 0; i < outLen; i++) {
          output.words[i] = input.words[i];
        }
        output.length = outLen;

        if (input.length <= 9) {
          input.words[0] = 0;
          input.length = 1;
          return;
        }

        // Shift by 9 limbs
        var prev = input.words[9];
        output.words[output.length++] = prev & mask;

        for (i = 10; i < input.length; i++) {
          var next = input.words[i] | 0;
          input.words[i - 10] = ((next & mask) << 4) | (prev >>> 22);
          prev = next;
        }
        prev >>>= 22;
        input.words[i - 10] = prev;
        if (prev === 0 && input.length > 10) {
          input.length -= 10;
        } else {
          input.length -= 9;
        }
      };

      K256.prototype.imulK = function imulK (num) {
        // K = 0x1000003d1 = [ 0x40, 0x3d1 ]
        num.words[num.length] = 0;
        num.words[num.length + 1] = 0;
        num.length += 2;

        // bounded at: 0x40 * 0x3ffffff + 0x3d0 = 0x100000390
        var lo = 0;
        for (var i = 0; i < num.length; i++) {
          var w = num.words[i] | 0;
          lo += w * 0x3d1;
          num.words[i] = lo & 0x3ffffff;
          lo = w * 0x40 + ((lo / 0x4000000) | 0);
        }

        // Fast length reduction
        if (num.words[num.length - 1] === 0) {
          num.length--;
          if (num.words[num.length - 1] === 0) {
            num.length--;
          }
        }
        return num;
      };

      function P224 () {
        MPrime.call(
          this,
          'p224',
          'ffffffff ffffffff ffffffff ffffffff 00000000 00000000 00000001');
      }
      inherits(P224, MPrime);

      function P192 () {
        MPrime.call(
          this,
          'p192',
          'ffffffff ffffffff ffffffff fffffffe ffffffff ffffffff');
      }
      inherits(P192, MPrime);

      function P25519 () {
        // 2 ^ 255 - 19
        MPrime.call(
          this,
          '25519',
          '7fffffffffffffff ffffffffffffffff ffffffffffffffff ffffffffffffffed');
      }
      inherits(P25519, MPrime);

      P25519.prototype.imulK = function imulK (num) {
        // K = 0x13
        var carry = 0;
        for (var i = 0; i < num.length; i++) {
          var hi = (num.words[i] | 0) * 0x13 + carry;
          var lo = hi & 0x3ffffff;
          hi >>>= 26;

          num.words[i] = lo;
          carry = hi;
        }
        if (carry !== 0) {
          num.words[num.length++] = carry;
        }
        return num;
      };

      // Exported mostly for testing purposes, use plain name instead
      BN._prime = function prime (name) {
        // Cached version of prime
        if (primes[name]) return primes[name];

        var prime;
        if (name === 'k256') {
          prime = new K256();
        } else if (name === 'p224') {
          prime = new P224();
        } else if (name === 'p192') {
          prime = new P192();
        } else if (name === 'p25519') {
          prime = new P25519();
        } else {
          throw new Error('Unknown prime ' + name);
        }
        primes[name] = prime;

        return prime;
      };

      //
      // Base reduction engine
      //
      function Red (m) {
        if (typeof m === 'string') {
          var prime = BN._prime(m);
          this.m = prime.p;
          this.prime = prime;
        } else {
          assert(m.gtn(1), 'modulus must be greater than 1');
          this.m = m;
          this.prime = null;
        }
      }

      Red.prototype._verify1 = function _verify1 (a) {
        assert(a.negative === 0, 'red works only with positives');
        assert(a.red, 'red works only with red numbers');
      };

      Red.prototype._verify2 = function _verify2 (a, b) {
        assert((a.negative | b.negative) === 0, 'red works only with positives');
        assert(a.red && a.red === b.red,
          'red works only with red numbers');
      };

      Red.prototype.imod = function imod (a) {
        if (this.prime) return this.prime.ireduce(a)._forceRed(this);
        return a.umod(this.m)._forceRed(this);
      };

      Red.prototype.neg = function neg (a) {
        if (a.isZero()) {
          return a.clone();
        }

        return this.m.sub(a)._forceRed(this);
      };

      Red.prototype.add = function add (a, b) {
        this._verify2(a, b);

        var res = a.add(b);
        if (res.cmp(this.m) >= 0) {
          res.isub(this.m);
        }
        return res._forceRed(this);
      };

      Red.prototype.iadd = function iadd (a, b) {
        this._verify2(a, b);

        var res = a.iadd(b);
        if (res.cmp(this.m) >= 0) {
          res.isub(this.m);
        }
        return res;
      };

      Red.prototype.sub = function sub (a, b) {
        this._verify2(a, b);

        var res = a.sub(b);
        if (res.cmpn(0) < 0) {
          res.iadd(this.m);
        }
        return res._forceRed(this);
      };

      Red.prototype.isub = function isub (a, b) {
        this._verify2(a, b);

        var res = a.isub(b);
        if (res.cmpn(0) < 0) {
          res.iadd(this.m);
        }
        return res;
      };

      Red.prototype.shl = function shl (a, num) {
        this._verify1(a);
        return this.imod(a.ushln(num));
      };

      Red.prototype.imul = function imul (a, b) {
        this._verify2(a, b);
        return this.imod(a.imul(b));
      };

      Red.prototype.mul = function mul (a, b) {
        this._verify2(a, b);
        return this.imod(a.mul(b));
      };

      Red.prototype.isqr = function isqr (a) {
        return this.imul(a, a.clone());
      };

      Red.prototype.sqr = function sqr (a) {
        return this.mul(a, a);
      };

      Red.prototype.sqrt = function sqrt (a) {
        if (a.isZero()) return a.clone();

        var mod3 = this.m.andln(3);
        assert(mod3 % 2 === 1);

        // Fast case
        if (mod3 === 3) {
          var pow = this.m.add(new BN(1)).iushrn(2);
          return this.pow(a, pow);
        }

        // Tonelli-Shanks algorithm (Totally unoptimized and slow)
        //
        // Find Q and S, that Q * 2 ^ S = (P - 1)
        var q = this.m.subn(1);
        var s = 0;
        while (!q.isZero() && q.andln(1) === 0) {
          s++;
          q.iushrn(1);
        }
        assert(!q.isZero());

        var one = new BN(1).toRed(this);
        var nOne = one.redNeg();

        // Find quadratic non-residue
        // NOTE: Max is such because of generalized Riemann hypothesis.
        var lpow = this.m.subn(1).iushrn(1);
        var z = this.m.bitLength();
        z = new BN(2 * z * z).toRed(this);

        while (this.pow(z, lpow).cmp(nOne) !== 0) {
          z.redIAdd(nOne);
        }

        var c = this.pow(z, q);
        var r = this.pow(a, q.addn(1).iushrn(1));
        var t = this.pow(a, q);
        var m = s;
        while (t.cmp(one) !== 0) {
          var tmp = t;
          for (var i = 0; tmp.cmp(one) !== 0; i++) {
            tmp = tmp.redSqr();
          }
          assert(i < m);
          var b = this.pow(c, new BN(1).iushln(m - i - 1));

          r = r.redMul(b);
          c = b.redSqr();
          t = t.redMul(c);
          m = i;
        }

        return r;
      };

      Red.prototype.invm = function invm (a) {
        var inv = a._invmp(this.m);
        if (inv.negative !== 0) {
          inv.negative = 0;
          return this.imod(inv).redNeg();
        } else {
          return this.imod(inv);
        }
      };

      Red.prototype.pow = function pow (a, num) {
        if (num.isZero()) return new BN(1).toRed(this);
        if (num.cmpn(1) === 0) return a.clone();

        var windowSize = 4;
        var wnd = new Array(1 << windowSize);
        wnd[0] = new BN(1).toRed(this);
        wnd[1] = a;
        for (var i = 2; i < wnd.length; i++) {
          wnd[i] = this.mul(wnd[i - 1], a);
        }

        var res = wnd[0];
        var current = 0;
        var currentLen = 0;
        var start = num.bitLength() % 26;
        if (start === 0) {
          start = 26;
        }

        for (i = num.length - 1; i >= 0; i--) {
          var word = num.words[i];
          for (var j = start - 1; j >= 0; j--) {
            var bit = (word >> j) & 1;
            if (res !== wnd[0]) {
              res = this.sqr(res);
            }

            if (bit === 0 && current === 0) {
              currentLen = 0;
              continue;
            }

            current <<= 1;
            current |= bit;
            currentLen++;
            if (currentLen !== windowSize && (i !== 0 || j !== 0)) continue;

            res = this.mul(res, wnd[current]);
            currentLen = 0;
            current = 0;
          }
          start = 26;
        }

        return res;
      };

      Red.prototype.convertTo = function convertTo (num) {
        var r = num.umod(this.m);

        return r === num ? r.clone() : r;
      };

      Red.prototype.convertFrom = function convertFrom (num) {
        var res = num.clone();
        res.red = null;
        return res;
      };

      //
      // Montgomery method engine
      //

      BN.mont = function mont (num) {
        return new Mont(num);
      };

      function Mont (m) {
        Red.call(this, m);

        this.shift = this.m.bitLength();
        if (this.shift % 26 !== 0) {
          this.shift += 26 - (this.shift % 26);
        }

        this.r = new BN(1).iushln(this.shift);
        this.r2 = this.imod(this.r.sqr());
        this.rinv = this.r._invmp(this.m);

        this.minv = this.rinv.mul(this.r).isubn(1).div(this.m);
        this.minv = this.minv.umod(this.r);
        this.minv = this.r.sub(this.minv);
      }
      inherits(Mont, Red);

      Mont.prototype.convertTo = function convertTo (num) {
        return this.imod(num.ushln(this.shift));
      };

      Mont.prototype.convertFrom = function convertFrom (num) {
        var r = this.imod(num.mul(this.rinv));
        r.red = null;
        return r;
      };

      Mont.prototype.imul = function imul (a, b) {
        if (a.isZero() || b.isZero()) {
          a.words[0] = 0;
          a.length = 1;
          return a;
        }

        var t = a.imul(b);
        var c = t.maskn(this.shift).mul(this.minv).imaskn(this.shift).mul(this.m);
        var u = t.isub(c).iushrn(this.shift);
        var res = u;

        if (u.cmp(this.m) >= 0) {
          res = u.isub(this.m);
        } else if (u.cmpn(0) < 0) {
          res = u.iadd(this.m);
        }

        return res._forceRed(this);
      };

      Mont.prototype.mul = function mul (a, b) {
        if (a.isZero() || b.isZero()) return new BN(0)._forceRed(this);

        var t = a.mul(b);
        var c = t.maskn(this.shift).mul(this.minv).imaskn(this.shift).mul(this.m);
        var u = t.isub(c).iushrn(this.shift);
        var res = u;
        if (u.cmp(this.m) >= 0) {
          res = u.isub(this.m);
        } else if (u.cmpn(0) < 0) {
          res = u.iadd(this.m);
        }

        return res._forceRed(this);
      };

      Mont.prototype.invm = function invm (a) {
        // (AR)^-1 * R^2 = (A^-1 * R^-1) * R^2 = A^-1 * R
        var res = this.imod(a._invmp(this.m).mul(this.r2));
        return res._forceRed(this);
      };
    })(module, commonjsGlobal);
    });

    // This creates the private and public keys for diffie-hellman (https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange#Cryptographic_explanation)
    // REQUIREMENTS: bn.js
    // ASSUMPTIONS: Inputs to the functions are hexadecimal strings

    // <summary>
    // This class implements the Diffie-Hellman algorithm using BigIntegers.
    // It can do the 3 main things:
    // 1. Generate a random Private Key for you.
    // 2. Generate your Public Key based on your Private Key.
    // 3. Generate the Secret given their Public Key and your Private Key
    // p and g are the shared constants for the algorithm, aka primeP and primeG.
    // </summary>
    class DiffieHellman {

        constructor () {

        }

        // <summary>
        // Generates a random Private Key that you can use.
        // </summary>
        // <param name="p"></param>
        // <returns>Random Private Key</returns>
        RandomPrivateKey(maxValue) {
            let maxValueBN      = new bn(maxValue);
            let shiftDistance   = Math.floor((Math.random() * 1000) + 1);
            let randBitInt      = maxValueBN.shrn(shiftDistance); // Right shift divides by a power of 2
            let min             = new bn(2);

            if(randBitInt.cmp(min) == -1) {
                return min;
            }

            return randBitInt;
        }

        // <summary>
        // Calculates the Public Key from a Private Key.
        // </summary>
        // <param name="p"></param>
        // <param name="g"></param>
        // <param name="privateKey"></param>
        // <returns>Public Key (Hex)</returns>
        PublicKey(p, g, privateKey) {
            const aHex = new bn(privateKey, 16);
            const gHex = new bn(g, 16);
            const montPrime = bn.mont(new bn(p, 16));
            const gRed = gHex.toRed(montPrime);
            const secret = gRed.redPow(aHex).fromRed().toString(16);

            return secret;  
        }

        // <summary>
        // Calculates the shared secret given their Public Key (A) and your Private Key (b)
        // </summary>
        // <param name="p"></param>
        // <param name="theirPublicKey"></param>
        // <param name="yourPrivateKey"></param>
        // <returns></returns>
        Secret(p, theirPublicKey, yourPrivateKey) {
            const bHex = new bn(theirPublicKey, 16);
            const AHex = new bn(yourPrivateKey, 16);
            const montPrime = bn.mont(new bn(p, 16));
            const BRed = bHex.toRed(montPrime);

            return BRed.redPow(AHex).fromRed().toString(16).toUpperCase();
        }

    }

    // This is the generator used for diffie-hellman in 2048-bit MODP Group 14 as per (https://tools.ietf.org/html/rfc3526#section-3)
    const GENERATOR = 2;

    // This is the prime used for diffie-hellman using 2048-bit MODP Group 14 as per (https://tools.ietf.org/html/rfc3526#section-3)
    const GROUP14_2048_BIT_MODP = 'FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AACAA68FFFFFFFFFFFFFFFF';

    // <summary>
    // This static class helps you with the pairing process as documented here:
    // http://www.simplepaymentapi.com/#/api/pairing-process
    // </summary>
    class PairingHelper {
        // <summary>
        // Generates a pairing Request.
        // </summary>
        // <returns>New PairRequest</returns>
        static NewPairRequest() {
            return new PairRequest();
        }

        // <summary>
        // Calculates/Generates Secrets and KeyResponse given an incoming KeyRequest.
        // </summary>
        // <param name="keyRequest"></param>
        // <returns>Secrets and KeyResponse to send back.</returns>
        GenerateSecretsAndKeyResponse(keyRequest) {
            let encPubAndSec = this._calculateMyPublicKeyAndSecret(keyRequest.Aenc);
            let Benc = encPubAndSec.MyPublicKey;
            let Senc = encPubAndSec.SharedSecretKey;

            let hmacPubAndSec = this._calculateMyPublicKeyAndSecret(keyRequest.Ahmac);
            let Bhmac = hmacPubAndSec.MyPublicKey;
            let Shmac = hmacPubAndSec.SharedSecretKey;
            
            let secrets = new Secrets(Senc, Shmac);
            let keyResponse = new KeyResponse(keyRequest.RequestId, Benc, Bhmac);

            return new SecretsAndKeyResponse(secrets, keyResponse);
        }

        // <summary>
        // Turns an incoming "A" value from the PinPad into the outgoing "B" value 
        // and the secret value using DiffieHelmman helper.
        // </summary>
        // <param name="theirPublicKey">The incoming A value</param>
        // <returns>Your B value and the Secret</returns>
        _calculateMyPublicKeyAndSecret(theirPublicKey) {

            let diffieHellman = new DiffieHellman();
            let myPrivateBI = diffieHellman.RandomPrivateKey(GROUP14_2048_BIT_MODP);
            let myPublicBI = diffieHellman.PublicKey(GROUP14_2048_BIT_MODP, GENERATOR, myPrivateBI);
            let secretBI = diffieHellman.Secret(GROUP14_2048_BIT_MODP, theirPublicKey, myPrivateBI);

            let secret = this.DHSecretToSPISecret(secretBI);

            return new PublicKeyAndSecret(myPublicBI, secret);
        }

        // <summary>
        // Converts the DH secret BigInteger into the hex-string to be used as the secret.
        // There are some "gotchyas" here which is why this piece of work is abstracted so it can be tested separately.
        // See: http://www.simplepaymentapi.com/#/api/pairing-process
        // </summary>
        // <param name="secretBI">Secret as BigInteger</param>
        // <returns>Secret as Hex-String</returns>
        DHSecretToSPISecret(secret)
        {      
            // If the calculated hexadecimal secret doesn't have an even number of characters, we add an extra 0 to the start. This allows SHA-256 to operate on the hexadecimal secret as if it were a hexadecimal representation of a string.
            if (secret.length % 2 === 1){
                secret = '0' + secret;
            }

            secret = secret.padStart(512, '0');

            // We sha256 that byte array and return the hex string result
            return Crypto.GenerateHash(secret);
        }
    }

    // <summary>
    // Internal Holder class for Public and Secret, so that we can use them together in method signatures. 
    // </summary>
    class PublicKeyAndSecret {
        constructor(myPublicKey, sharedSecretKey) {
            this.MyPublicKey = myPublicKey;
            this.SharedSecretKey = sharedSecretKey;
        }
    }

    const ConnectionState = {
        Disconnected: 'Disconnected',
        Connecting: 'Connecting',
        Connected: 'Connected'
    };

    const SPI_PROTOCOL   = 'spi.2.4.0';

    class ConnectionStateEventArgs
    {
        constructor(connectionState) {
            this.ConnectionState = connectionState;
        }
    }

    class MessageEventArgs
    {
        constructor(message) {
            this.Message = message;
        }
    }

    class Connection {
        constructor() {
            this.Address    = null;
            this.Connected  = false;
            this.State      = ConnectionState.Disconnected;
            this.SpiProtocol = SPI_PROTOCOL;
            this._ws        = null;

            if(typeof WebSocket === 'undefined') {
                throw new Error('Environment does not support WebSockets');
            }
        }

        Connect(UseSecureWebSockets) {
            if(this.State === ConnectionState.Connected || this.State === ConnectionState.Connecting) {
                // already connected or connecting. disconnect first.
                return;
            }

            this.State = ConnectionState.Connecting;

            //Create a new socket instance specifying the url, SPI protocol and Websocket to use.
            //The will create a TCP/IP socket connection to the provided URL and perform HTTP websocket negotiation
            this._ws           = new WebSocket(this.Address, this.SpiProtocol);
            this._ws.onopen    = () => this.pollWebSocketConnection();
            this._ws.onmessage = (payload) => this.onMessageReceived(payload);
            this._ws.onclose   = () => this.onClosed();
            this._ws.onerror   = (err) => this.onError(err);

            document.dispatchEvent(new CustomEvent('ConnectionStatusChanged', {detail: new ConnectionStateEventArgs(ConnectionState.Connecting)}));
        }

        Disconnect() {
            if (this.State == ConnectionState.Disconnected) return;

            if(this._ws && this._ws.readyState != this._ws.CLOSED) {
                this._ws.close();
            }

            if (this._ws) {
                this._ws.onopen    = null;
                this._ws.onmessage = null;
                this._ws.onclose   = null;
                this._ws.onerror   = null;
            }

            this.onClosed();
        }

        Send(message) {
            this._ws.send(message);
        }

        onOpened() {
            this.State = ConnectionState.Connected;
            this.Connected = true;
            document.dispatchEvent(new CustomEvent('ConnectionStatusChanged', {detail: new ConnectionStateEventArgs(ConnectionState.Connected)}));
        }

        onClosed() {
            this.Connected = false;
            this.State = ConnectionState.Disconnected;
            this._ws = null;
            document.dispatchEvent(new CustomEvent('ConnectionStatusChanged', {detail: new ConnectionStateEventArgs(ConnectionState.Disconnected)}));
        }

        pollWebSocketConnection(count = 0) {
            
            if(this._ws.readyState === this._ws.OPEN) {
                this.onOpened();
                return true;
            } else if(count < 25) {
                count++;
                setTimeout(() => this.pollWebSocketConnection(count), 200);
            } else {
                this.Disconnect();
                return false;
            }
        }

        onMessageReceived(message) {
            document.dispatchEvent(new CustomEvent('MessageReceived', {detail: new MessageEventArgs(message.data)}));
        }

        onError(err) {
            document.dispatchEvent(new CustomEvent('ErrorReceived', {detail: new MessageEventArgs(err)}));
        }
    }

    class PurchaseRequest {
        constructor(amountCents, posRefId) {
            this.PosRefId = posRefId;
            this.PurchaseAmount = amountCents;
            this.TipAmount = 0;
            this.CashoutAmount = 0;
            this.PromptForCashout = false;
            this.SurchargeAmount = 0;
            this.Config = new SpiConfig();
            this.Options = new TransactionOptions();

            // Library Backwards Compatibility
            this.Id = posRefId;
            this.AmountCents = amountCents;
        }

        AmountSummary()
        {
            return `Purchase: ${(this.PurchaseAmount / 100.0).toFixed(2)}; 
            Tip: ${(this.TipAmount / 100.0).toFixed(2)}; 
            Cashout: ${(this.CashoutAmount / 100.0).toFixed(2)};`;
        }

        ToMessage() {
            let data = {
                pos_ref_id: this.PosRefId,
                purchase_amount: this.PurchaseAmount,
                tip_amount: this.TipAmount,
                cash_amount: this.CashoutAmount,
                prompt_for_cashout: this.PromptForCashout, 
                surcharge_amount: this.SurchargeAmount
            };

            this.Config.addReceiptConfig(data);
            this.Options.AddOptions(data);
            return new Message(RequestIdHelper.Id("prchs"), Events.PurchaseRequest, data, true);
        }
    }

    class PurchaseResponse
    {
        constructor(m)
        {
            this._m = m;
            this.RequestId = m.Id;
            this.PosRefId = m.Data.pos_ref_id;
            this.SchemeName = m.Data.scheme_name;
            this.SchemeAppName = m.Data.scheme_name;
            this.Success = m.GetSuccessState() == SuccessState.Success;
        }

        GetRRN()
        {
            return this._m.Data.rrn;
        }

        GetPurchaseAmount()
        {
            return this._m.Data.purchase_amount;
        }

        GetTipAmount()
        {
            return this._m.Data.tip_amount;
        }

        GetSurchargeAmount()
        {
            return this._m.Data.surcharge_amount;
        }

        GetCashoutAmount()
        {
            return this._m.Data.cash_amount;
        }

        GetBankNonCashAmount()
        {
            return this._m.Data.bank_noncash_amount;
        }

        GetBankCashAmount()
        {
            return this._m.Data.bank_cash_amount;
        }

        GetCustomerReceipt()
        {
            return this._m.Data.customer_receipt || "";
        }

        GetMerchantReceipt()
        {
            return this._m.Data.merchant_receipt || "";
        }

        GetResponseText()
        {
            return this._m.Data.host_response_text || "";
        }

        GetResponseCode()
        {
            return this._m.Data.host_response_code;
        }
        
        GetTerminalReferenceId()
        {
            return this._m.Data.terminal_ref_id;
        }

        GetCardEntry()
        {
            return this._m.Data.card_entry;
        }
        
        GetAccountType()
        {
            return this._m.Data.account_type;
        }

        GetAuthCode()
        {
            return this._m.Data.auth_code;
        }

        GetBankDate()
        {
            return this._m.Data.bank_date;
        }

        GetBankTime()
        {
            return this._m.Data.bank_time;
        }
        
        GetMaskedPan()
        {
            return this._m.Data.masked_pan;
        }
        
        GetTerminalId()
        {
            return this._m.Data.terminal_id;
        }

        WasMerchantReceiptPrinted()
        {
            return this._m.Data.merchant_receipt_printed;
        }

        WasCustomerReceiptPrinted()
        {
            return this._m.Data.customer_receipt_printed;
        }
        
        GetSettlementDate()
        {
            //"bank_settlement_date":"20042018"
            var dateStr = this._m.Data.bank_settlement_date;
            if (!dateStr) return null;
            return Message.ParseBankDate(dateStr);
        }

        GetResponseValue(attribute)
        {
            return this._m.Data[attribute];
        }

        ToPaymentSummary()
        {
            return {
                account_type: this.GetAccountType(),
                auth_code: this.GetAuthCode(),
                bank_date: this.GetBankDate(),
                bank_time: this.GetBankTime(),
                host_response_code: this.GetResponseCode(),
                host_response_text: this.GetResponseText(),
                masked_pan: this.GetMaskedPan(),
                purchase_amount: this.GetPurchaseAmount(),
                rrn: this.GetRRN(),
                scheme_name: this.SchemeName,
                terminal_id: this.GetTerminalId(),
                terminal_ref_id: this.GetTerminalReferenceId(),
                tip_amount: this.GetTipAmount(),
                surcharge_amount: this.GetSurchargeAmount()
            };
        }
    }

    class CancelTransactionRequest
    {
        
        ToMessage()
        {
            return new Message(RequestIdHelper.Id("ctx"), Events.CancelTransactionRequest, null, true);
        }
    }

    class CancelTransactionResponse
    {
        constructor(m)
        {
            this._m = m;
            this.PosRefId = this._m.Data.pos_ref_id;
            this.Success = this._m.GetSuccessState() == SuccessState.Success;
        }

        GetErrorReason()
        {
            return this._m.Data.error_reason;
        }

        GetErrorDetail()
        {
            return this._m.Data.error_detail;
        }

        GetResponseValueWithAttribute(attribute)
        {
            return this._m.Data[attribute];
        }
    }

    class GetLastTransactionRequest
    {
        ToMessage()
        {
            return new Message(RequestIdHelper.Id("glt"), Events.GetLastTransactionRequest, null, true);
        }
    }

    class GetLastTransactionResponse
    {
        constructor(m)
        {
            this._m = m;
        }

        WasRetrievedSuccessfully()
        {
            // We can't rely on checking "success" flag or "error" fields here,
            // as retrieval may be successful, but the retrieved transaction was a fail.
            // So we check if we got back an ResponseCode.
            // (as opposed to say an operation_in_progress_error)
            return !!this.GetResponseCode();
        }

        WasTimeOutOfSyncError()
        {
            return this._m.GetError().startsWith("TIME_OUT_OF_SYNC");
        }

        WasOperationInProgressError()
        {
            return this._m.GetError().startsWith("OPERATION_IN_PROGRESS");
        }

        IsWaitingForSignatureResponse()
        {
            return this._m.GetError().startsWith("OPERATION_IN_PROGRESS_AWAITING_SIGNATURE");
        }

        IsWaitingForAuthCode()
        {
            return this._m.GetError().startsWith("OPERATION_IN_PROGRESS_AWAITING_PHONE_AUTH_CODE");
        }
        
        IsStillInProgress(posRefId)
        {
            return this.WasOperationInProgressError() && this.GetPosRefId() == posRefId;
        }

        GetSuccessState()
        {
            return this._m.GetSuccessState();
        }

        WasSuccessfulTx()
        {
            return this._m.GetSuccessState() == SuccessState.Success;
        }

        GetTxType()
        {
            return this._m.Data.transaction_type;
        }

        GetPosRefId()
        {
            return this._m.Data.pos_ref_id;
        }

        GetSchemeApp()
        {
            return this._m.Data.scheme_name;
        }

        GetSchemeName()
        {
            return this._m.Data.scheme_name;
        }

        GetAmount()
        {
            return this._m.Data.amount_purchase;
        }

        GetTransactionAmount()
        {
            return this._m.Data.amount_transaction_type;
        }

        GetBankDateTimeString()
        {
            var ds = this._m.Data.bank_date + this._m.Data.bank_time;
            return ds;
        }

        GetRRN()
        {
            return this._m.Data.rrn;
        }
        
        GetResponseText()
        {
            return this._m.Data.host_response_text | "";
        }

        GetResponseCode()
        {
            return this._m.Data.host_response_code;
        }

        // <summary>
        // There is a bug, VSV-920, whereby the customer_receipt is missing from a glt response.
        // The current recommendation is to use the merchant receipt in place of it if required.
        // This method modifies the underlying incoming message data by copying
        // the merchant receipt into the customer receipt only if there 
        // is a merchant_receipt and there is not a customer_receipt.   
        // </summary>
        CopyMerchantReceiptToCustomerReceipt()
        {
            var cr = this._m.Data.customer_receipt;
            var mr = this._m.Data.merchant_receipt;
            if (mr != "" && !(cr))
            {
                this._m.Data.customer_receipt = mr;
            }
        }
    }

    class RefundRequest
    {
        constructor(amountCents, posRefId, isSuppressMerchantPassword)
        {
            this.AmountCents = amountCents;
            this.Id = RequestIdHelper.Id("refund");
            this.PosRefId = posRefId;
            this.IsSuppressMerchantPassword = isSuppressMerchantPassword;
            this.Config = new SpiConfig();
            this.Options = new TransactionOptions();
        }
        
        ToMessage()
        {
            let data = {
                refund_amount: this.AmountCents, 
                pos_ref_id: this.PosRefId,
                suppress_merchant_password: this.IsSuppressMerchantPassword
            };
            
            this.Config.addReceiptConfig(data);
            // this.Options.AddOptions(data);
            return new Message(RequestIdHelper.Id("refund"), Events.RefundRequest, data, true);
        }
    }

    class SignatureRequired
    {
        constructor(m)
        {
            this.RequestId = m.Id;
            this.PosRefId = m.Data.pos_ref_id;
            this._receiptToSign = m.Data.merchant_receipt;
        }
        
        SignatureRequired(posRefId, requestId, receiptToSign)
        {
            this.RequestId = requestId;
            this.PosRefId = posRefId;
            this._receiptToSign = receiptToSign;
        }

        GetMerchantReceipt()
        {
            return this._receiptToSign;
        }
    }

    class SignatureDecline
    {
        constructor(posRefId)
        {
            this.PosRefId = posRefId;
        }

        ToMessage()
        {
            var data = {
                pos_ref_id: this.PosRefId
            };
            return new Message(RequestIdHelper.Id("sigdec"), Events.SignatureDeclined, data, true);
        }
    }

    class SignatureAccept
    {
        constructor(posRefId)
        {
            this.PosRefId = posRefId;
        }

        ToMessage()
        {
            var data = {
                pos_ref_id: this.PosRefId
            };
            return new Message(RequestIdHelper.Id("sigacc"), Events.SignatureAccepted, data, true);
        }
    }

    class MotoPurchaseRequest
    {
        constructor(amountCents, posRefId, surchargeAmount)
        {
            this.PosRefId = posRefId;
            this.PurchaseAmount = amountCents;
            this.SurchargeAmount = surchargeAmount;
            this.Config = new SpiConfig();
            this.Options = new TransactionOptions();
        }

        ToMessage()
        {
            var data = {
                pos_ref_id: this.PosRefId,
                purchase_amount: this.PurchaseAmount,
                surcharge_amount: this.SurchargeAmount
            };
            this.Config.addReceiptConfig(data);
            return new Message(RequestIdHelper.Id("moto"), Events.MotoPurchaseRequest, data, true);
        }
    }

    class PhoneForAuthRequired
    {
        constructor(...args)
        {
            if(args.length === 4) {
                this.PosRefId = args[0];
                this.RequestId = args[1];
                this._phoneNumber = args[2];
                this._merchantId = args[3];
            } else if(args.length === 1) {
                this.RequestId = args[0].Id;
                this.PosRefId = args[0].Data.pos_ref_id;
                this._phoneNumber = args[0].Data.auth_centre_phone_number;
                this._merchantId = args[0].Data.merchant_id;
            } else {
                throw new Error('Invalid call sig for Phone auth required class');
            }
        }
        
        GetPhoneNumber()
        {
            return this._phoneNumber;
        }
        
        GetMerchantId()
        {
            return this._merchantId;
        }
    }

    class AuthCodeAdvice
    {
        constructor(posRefId, authCode)
        {
            this.PosRefId = posRefId;
            this.AuthCode = authCode;
        }

        ToMessage()
        {
            var data = {
                pos_ref_id: this.PosRefId,
                auth_code: this.AuthCode
            };
            return new Message(RequestIdHelper.Id("authad"), Events.AuthCodeAdvice, data, true);
        }
    }

    // <summary>
    // This class represents the BillDetails that the POS will be asked for throughout a PayAtTable flow.
    // </summary>
    class BillStatusResponse
    {
        constructor() {
            // <summary>
            // Set this Error accordingly if you are not able to return the BillDetails that were asked from you.
            // </summary>
            this.Result = null;
            
            // <summary>
            // This is a unique identifier that you assign to each bill.
            // It migt be for example, the timestamp of when the cover was opened.
            // </summary>
            this.BillId = null;
            
            // <summary>
            // This is the table id that this bill was for.
            // The waiter will enter it on the Eftpos at the start of the PayAtTable flow and the Eftpos will 
            // retrieve the bill using the table id. 
            // </summary>
            this.TableId = null;
            
            // <summary>
            // The Total Amount on this bill, in cents.
            // </summary>
            this.TotalAmount = 0;
            
            // <summary>
            // The currently outsanding amount on this bill, in cents.
            // </summary>
            this.OutstandingAmount = 0;

            // <summary>
            // Your POS is required to persist some state on behalf of the Eftpos so the Eftpos can recover state.
            // It is just a piece of string that you save against your billId.
            // WHenever you're asked for BillDetails, make sure you return this piece of data if you have it.
            // </summary>
            this.BillData = "";
        }

        getBillPaymentHistory()
        {
            if (!this.BillData)
            {
                return [];
            }
            let savedBillData = JSON.parse(this.BillData);

            return savedBillData.map((bill) => {
                return new PaymentHistoryEntry(bill.payment_type, bill.payment_summary);
            });
        }

        static ToBillData(ph)
        {
            if (ph.length < 1)
            {
                return "";
            }

            return JSON.stringify(ph);
        }
        
        ToMessage(messageId)
        {
            var data = {
                "success": this.Result==BillRetrievalResult.SUCCESS
            };
            
            if (this.BillId) data.bill_id = this.BillId;
            if (this.TableId) data.table_id = this.TableId;

            if (this.Result == BillRetrievalResult.SUCCESS)
            {
                data.bill_total_amount = this.TotalAmount;
                data.bill_outstanding_amount = this.OutstandingAmount;
                data.bill_payment_history = this.getBillPaymentHistory();
            }
            else
            {
                data.error_reason = this.Result.toString();
                data.error_detail = this.Result.toString();
            }

            return new Message(messageId, Events.PayAtTableBillDetails, data, true);
        }
    }

    const BillRetrievalResult = 
    {
        SUCCESS: 'SUCCESS',
        INVALID_TABLE_ID: 'INVALID_TABLE_ID',
        INVALID_BILL_ID: 'INVALID_BILL_ID',
        INVALID_OPERATOR_ID: 'INVALID_OPERATOR_ID'
    };

    class BillPayment
    {
        constructor(m)
        {
            this._incomingAdvice = m;
            this.BillId = this._incomingAdvice.Data["bill_id"];
            this.TableId = this._incomingAdvice.Data["table_id"];
            this.OperatorId = this._incomingAdvice.Data["operator_id"];
            
            var pt = this._incomingAdvice.Data["payment_type"];
            this.PaymentType = pt;
            
            // this is when we ply the sub object "payment_details" into a purchase response for convenience.
            var purchaseMsg = new Message(m.Id, "payment_details", m.Data["payment_details"], false);
            this.PurchaseResponse = new PurchaseResponse(purchaseMsg);

            this.PurchaseAmount = this.PurchaseResponse.GetPurchaseAmount();
            this.TipAmount = this.PurchaseResponse.GetTipAmount();
        }
    }

    class PaymentHistoryEntry
    {
        constructor(paymentType, paymentSummary)
        {
            this.PaymentType = paymentType;
            this.PaymentSummary = paymentSummary;
        }

        toJSON() {
            return {
                payment_type: this.PaymentType,
                payment_summary: this.PaymentSummary
            };
        }
        
        GetTerminalRefId()
        {
            return this.PaymentSummary["terminal_ref_id"];
        }
    }

    class PayAtTableConfig
    {
        constructor() {
            this.PayAtTabledEnabled = false;
            this.OperatorIdEnabled = false;
            this.SplitByAmountEnabled = false;
            this.EqualSplitEnabled = false;
        
            this.TippingEnabled = false;
        
            this.SummaryReportEnabled = false;
        
            this.LabelPayButton = '';
            this.LabelOperatorId = '';
            this.LabelTableId = '';
        
            // 
            // <summary>
            // Fill in with operator ids that the eftpos terminal will validate against. 
            // Leave Empty to allow any operator_id through. 
            // </summary>
           this.AllowedOperatorIds = [];
        }

        ToMessage(messageId)
        {
            var data = {
                "pay_at_table_enabled": this.PayAtTabledEnabled,
                "operator_id_enabled": this.OperatorIdEnabled,
                "split_by_amount_enabled": this.SplitByAmountEnabled,
                "equal_split_enabled": this.EqualSplitEnabled,
                "tipping_enabled": this.TippingEnabled,
                "summary_report_enabled": this.SummaryReportEnabled,
                "pay_button_label": this.LabelPayButton,
                "operator_id_label": this.LabelOperatorId,
                "table_id_label": this.LabelTableId,
                "operator_id_list": this.AllowedOperatorIds
            };

            return new Message(messageId, Events.PayAtTableSetTableConfig, data, true);
        }
        
        static FeatureDisableMessage(messageId) {
            var data = {
                "pay_at_table_enabled": false
            };
            return new Message(messageId, Events.PayAtTableSetTableConfig, data, true);
        }
    }

    class SpiPayAtTable
    {  
        constructor(spi)
        {
            this._spi = spi;
            this._log = console;

            this.Config = Object.assign(new PayAtTableConfig(), {
                PayAtTabledEnabled: true,
                OperatorIdEnabled: true,
                AllowedOperatorIds: [],
                EqualSplitEnabled: true,
                SplitByAmountEnabled: true,
                SummaryReportEnabled: true,
                TippingEnabled: true,
                LabelOperatorId: "Operator ID",
                LabelPayButton: "Pay at Table",
                LabelTableId: "Table Number"
            });
        }

        // <summary>
        // This delegate will be called when the Eftpos needs to know the current state of a bill for a table. 
        // <para />
        // Parameters:<para />
        // billId - The unique identifier of the bill. If empty, it means that the PayAtTable flow on the Eftpos is just starting, and the lookup is by tableId.<para />
        // tableId - The identifier of the table that the bill is for. <para />
        // operatorId - The id of the operator entered on the eftpos. <para />
        // <para />
        // Return:<para />
        // You need to return the current state of the bill.
        // </summary>
        GetBillStatus(billId, tableId, operatorId) {
            throw new Error('Method not implemented. Please overwrite this method in your POS');
        }

        // Abstract method, must implement in POS system
        BillPaymentReceived(billPayment, updatedBillData) {
            throw new Error('Method not implemented. Please overwrite this method in your POS');
        }

        PushPayAtTableConfig()
        {
            this._spi._send(this.Config.ToMessage(RequestIdHelper.Id("patconf")));
        } 
        
        _handleGetBillDetailsRequest(m)
        {
            var operatorId = m.Data["operator_id"];
            var tableId = m.Data["table_id"];

            // Ask POS for Bill Details for this tableId, inluding encoded PaymentData
            var billStatus = this.GetBillStatus(null, tableId, operatorId);
            billStatus.TableId = tableId;
            if (billStatus.TotalAmount <= 0)
            {
                this._log.info("Table has 0 total amount. not sending it to eftpos.");
                billStatus.Result = BillRetrievalResult.INVALID_TABLE_ID;
            }
            
            this._spi._send(billStatus.ToMessage(m.Id));
        }

        _handleBillPaymentAdvice(m)
        {
            var billPayment = new BillPayment(m);
            
            // Ask POS for Bill Details, inluding encoded PaymentData
            var existingBillStatus = this.GetBillStatus(billPayment.BillId, billPayment.TableId, billPayment.OperatorId);
            if (existingBillStatus.Result != BillRetrievalResult.SUCCESS)
            {
                this._log.warn("Could not retrieve Bill Status for Payment Advice. Sending Error to Eftpos.");
                this._spi._send(existingBillStatus.ToMessage(m.Id));
            }
                        
            var existingPaymentHistory = existingBillStatus.getBillPaymentHistory();
       
            var foundExistingEntry = existingPaymentHistory.find(phe => phe.GetTerminalRefId() == billPayment.PurchaseResponse.GetTerminalReferenceId());
            if (foundExistingEntry)
            {
                // We have already processed this payment.
                // perhaps Eftpos did get our acknowledgement.
                // Let's update Eftpos.
                this._log.warn("Had already received this bill_paymemnt advice from eftpos. Ignoring.");
                this._spi._send(existingBillStatus.ToMessage(m.Id));
                return;
            }

            // Let's add the new entry to the history
            var updatedHistoryEntries = existingPaymentHistory;
            updatedHistoryEntries.push(
                new PaymentHistoryEntry(billPayment.PaymentType.toLowerCase(), billPayment.PurchaseResponse.ToPaymentSummary())
            );
            
            var updatedBillData = BillStatusResponse.ToBillData(updatedHistoryEntries);

            // Advise POS of new payment against this bill, and the updated BillData to Save.
            var updatedBillStatus = this.BillPaymentReceived(billPayment, updatedBillData);

            // Just in case client forgot to set these:
            updatedBillStatus.BillId = billPayment.BillId;
            updatedBillStatus.TableId = billPayment.TableId;

            if (updatedBillStatus.Result != BillRetrievalResult.SUCCESS)
            {
                this._log.warn("POS Errored when being Advised of Payment. Letting EFTPOS know, and sending existing bill data.");
                updatedBillStatus.BillData = existingBillStatus.BillData;
            }
            else
            {
                updatedBillStatus.BillData = updatedBillData;
            }
        
            this._spi._send(updatedBillStatus.ToMessage(m.Id));
        }
        
        _handleGetTableConfig(m)
        {
            this._spi._send(this.Config.ToMessage(m.Id));
        }
    }

    const PreauthEvents = 
    {
        AccountVerifyRequest: "account_verify",
        AccountVerifyResponse: "account_verify_response",
        
        PreauthOpenRequest : "preauth",
        PreauthOpenResponse : "preauth_response",

        PreauthTopupRequest: "preauth_topup",
        PreauthTopupResponse: "preauth_topup_response",

        PreauthExtendRequest: "preauth_extend",
        PreauthExtendResponse: "preauth_extend_response",

        PreauthPartialCancellationRequest : "preauth_partial_cancellation",
        PreauthPartialCancellationResponse : "preauth_partial_cancellation_response",
        
        PreauthCancellationRequest : "preauth_cancellation",
        PreauthCancellationResponse : "preauth_cancellation_response",

        PreauthCompleteRequest : "completion",
        PreauthCompleteResponse : "completion_response"
    };

    class AccountVerifyRequest
    {
        constructor(posRefId)
        {
            this.PosRefId = posRefId;
        }

        ToMessage()
        {
            var data = {
                "pos_ref_id": this.PosRefId
            };

            return new Message(RequestIdHelper.Id("prav"), PreauthEvents.AccountVerifyRequest, data, true);
        }
    }

    class PreauthOpenRequest
    {
        constructor(amountCents, posRefId)
        {
            this.PosRefId = posRefId;
            this.PreauthAmount = amountCents;
        }

        ToMessage()
        {
            var data = {
                "pos_ref_id": this.PosRefId,
                "preauth_amount": this.PreauthAmount
            };

            return new Message(RequestIdHelper.Id("prac"), PreauthEvents.PreauthOpenRequest, data, true);
        }
    }

    class PreauthTopupRequest
    {
        constructor(preauthId, topupAmountCents, posRefId)
        {
            this.PreauthId = preauthId;
            this.TopupAmount = topupAmountCents;
            this.PosRefId = posRefId;
        }

        ToMessage()
        {
            var data = {
                "pos_ref_id": this.PosRefId,
                "preauth_id": this.PreauthId,
                "topup_amount": this.TopupAmount
            };

            return new Message(RequestIdHelper.Id("prtu"), PreauthEvents.PreauthTopupRequest, data, true);
        }
    }

    class PreauthPartialCancellationRequest
    {
        constructor(preauthId, partialCancellationAmountCents, posRefId)
        {
            this.PreauthId = preauthId;
            this.PartialCancellationAmount = partialCancellationAmountCents;
            this.PosRefId = posRefId;
        }

        ToMessage()
        {
            var data = {
                "pos_ref_id": this.PosRefId,
                "preauth_id": this.PreauthId,
                "preauth_cancel_amount": this.PartialCancellationAmount
            };

            return new Message(RequestIdHelper.Id("prpc"), PreauthEvents.PreauthPartialCancellationRequest, data, true);
        }
    }

    class PreauthExtendRequest
    {
        constructor(preauthId, posRefId)
        {
            this.PreauthId = preauthId;
            this.PosRefId = posRefId;
        }

        ToMessage()
        {
            var data = {
                "pos_ref_id": this.PosRefId,
                "preauth_id": this.PreauthId
            };

            return new Message(RequestIdHelper.Id("prext"), PreauthEvents.PreauthExtendRequest, data, true);
        }
    }

    class PreauthCancelRequest
    {
        constructor(preauthId, posRefId)
        {
            this.PreauthId = preauthId;
            this.PosRefId = posRefId;
        }

        ToMessage()
        {
            var data = {
                "pos_ref_id": this.PosRefId,
                "preauth_id": this.PreauthId
            };

            return new Message(RequestIdHelper.Id("prac"), PreauthEvents.PreauthCancellationRequest, data, true);
        }
    }

    class PreauthCompletionRequest
    {
        constructor(preauthId, completionAmountCents, posRefId, surchargeAmount)
        {
            this.PreauthId = preauthId;
            this.CompletionAmount = completionAmountCents;
            this.PosRefId = posRefId;
            this.SurchargeAmount = surchargeAmount;
        }

        ToMessage()
        {
            var data = {
                "pos_ref_id": this.PosRefId,
                "preauth_id": this.PreauthId,
                "completion_amount": this.CompletionAmount,
                "surcharge_amount": this.SurchargeAmount
            };

            return new Message(RequestIdHelper.Id("prac"), PreauthEvents.PreauthCompleteRequest, data, true);
        }
    }

    class SpiPreauth
    {
        constructor(spi)
        {
            this._spi = spi;
            this._log = console;
        }

        InitiateAccountVerifyTx(posRefId)
        {
            var verifyMsg = new AccountVerifyRequest(posRefId).ToMessage();
            var tfs = new TransactionFlowState(
                posRefId, TransactionType.AccountVerify, 0, verifyMsg,
                "Waiting for EFTPOS connection to make account verify request");
            var sentMsg = "Asked EFTPOS to verify account";
            return this._initiatePreauthTx(tfs, sentMsg);
        }
        
        InitiateOpenTx(posRefId, amountCents)
        {
            var msg = new PreauthOpenRequest(amountCents, posRefId).ToMessage();
            var tfs = new TransactionFlowState(
                posRefId, TransactionType.Preauth, amountCents, msg,
                `Waiting for EFTPOS connection to make preauth request for ${(amountCents / 100.0).toFixed(2)}`);
            var sentMsg = `Asked EFTPOS to create preauth for ${(amountCents / 100.0).toFixed(2)}`;
            return this._initiatePreauthTx(tfs, sentMsg);
        }

        InitiateTopupTx(posRefId, preauthId, amountCents)
        {
            var msg = new PreauthTopupRequest(preauthId, amountCents, posRefId).ToMessage();
            var tfs = new TransactionFlowState(
                posRefId, TransactionType.Preauth, amountCents, msg,
                `Waiting for EFTPOS connection to make preauth topup request for ${(amountCents / 100.0).toFixed(2)}`);
            var sentMsg = `Asked EFTPOS to make preauth topup for ${(amountCents / 100.0).toFixed(2)}`;
            return this._initiatePreauthTx(tfs, sentMsg);
        }

        InitiatePartialCancellationTx(posRefId, preauthId, amountCents)
        {
            var msg = new PreauthPartialCancellationRequest(preauthId, amountCents, posRefId).ToMessage();
            var tfs = new TransactionFlowState(
                posRefId, TransactionType.Preauth, amountCents, msg,
                `Waiting for EFTPOS connection to make preauth partial cancellation request for ${(amountCents / 100.0).toFixed(2)}`);
            var sentMsg = `Asked EFTPOS to make preauth partial cancellation for ${(amountCents / 100.0).toFixed(2)}`;
            return this._initiatePreauthTx(tfs, sentMsg);
        }

        InitiateExtendTx(posRefId, preauthId)
        {
            var msg = new PreauthExtendRequest(preauthId, posRefId).ToMessage();
            var tfs = new TransactionFlowState(
                posRefId, TransactionType.Preauth, 0, msg,
                "Waiting for EFTPOS connection to make preauth Extend request");
            var sentMsg = "Asked EFTPOS to make preauth Extend request";
            return this._initiatePreauthTx(tfs, sentMsg);
        }

        InitiateCompletionTx(posRefId, preauthId, amountCents, surchargeAmount)
        {
            var msg = new PreauthCompletionRequest(preauthId, amountCents, posRefId, surchargeAmount).ToMessage();
            var tfs = new TransactionFlowState(
                posRefId, TransactionType.Preauth, amountCents, msg,
                `Waiting for EFTPOS connection to make preauth completion request for ${(amountCents / 100.0).toFixed(2)}`);
            var sentMsg = `Asked EFTPOS to make preauth completion for ${(amountCents / 100.0).toFixed(2)}`;
            return this._initiatePreauthTx(tfs, sentMsg);
        }

        InitiateCancelTx(posRefId, preauthId)
        {
            var msg = new PreauthCancelRequest(preauthId, posRefId).ToMessage();
            var tfs = new TransactionFlowState(
                posRefId, TransactionType.Preauth, 0, msg,
                "Waiting for EFTPOS connection to make preauth cancellation request");
            var sentMsg = "Asked EFTPOS to make preauth cancellation request";
            return this._initiatePreauthTx(tfs, sentMsg);
        }

        _initiatePreauthTx(tfs, sentMsg)
        {
            if (this._spi.CurrentStatus == SpiStatus.Unpaired) return new InitiateTxResult(false, "Not Paired");

            if (this._spi.CurrentFlow != SpiFlow.Idle) return new InitiateTxResult(false, "Not Idle");

            this._spi.CurrentFlow = SpiFlow.Transaction;
            this._spi.CurrentTxFlowState = tfs;
            if (this._spi._send(tfs.Request))
            {
                this._spi.CurrentTxFlowState.Sent(sentMsg);
            }
        
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this._spi.CurrentTxFlowState}));
            return new InitiateTxResult(true, "Preauth Initiated");
        }

        _handlePreauthMessage(m)
        {
            switch (m.EventName)
            {
                case PreauthEvents.AccountVerifyResponse:
                    this._handleAccountVerifyResponse(m);
                    break;
                case PreauthEvents.PreauthOpenResponse:
                case PreauthEvents.PreauthTopupResponse:
                case PreauthEvents.PreauthPartialCancellationResponse:
                case PreauthEvents.PreauthExtendResponse:
                case PreauthEvents.PreauthCompleteResponse:
                case PreauthEvents.PreauthCancellationResponse:
                    this._handlePreauthResponse(m);
                    break;
                default:
                    this._log.info(`I don't Understand Preauth Event: ${m.EventName}, ${m.Data}. Perhaps I have not implemented it yet.`);
                    break;
            }
        }

        _handleAccountVerifyResponse(m)
        {
            var incomingPosRefId = m.Data.pos_ref_id;
            var currentTxFlowState = this._spi.CurrentTxFlowState;
            if (this._spi.CurrentFlow != SpiFlow.Transaction || currentTxFlowState.Finished || !currentTxFlowState.PosRefId === incomingPosRefId)
            {
                this._log.info(`Received Account Verify response but I was not waiting for one. Incoming Pos Ref ID: ${incomingPosRefId}`);
                return;
            }
            // TH-1A, TH-2A

            currentTxFlowState.Completed(m.GetSuccessState(), m, "Account Verify Transaction Ended.");
            // TH-6A, TH-6E
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this._spi.CurrentTxFlowState}));
        }
        
        _handlePreauthResponse(m)
        {
            var incomingPosRefId = m.Data.pos_ref_id;
            var currentTxFlowState = this._spi.CurrentTxFlowState;
            if (this._spi.CurrentFlow != SpiFlow.Transaction || currentTxFlowState.Finished || !currentTxFlowState.PosRefId === incomingPosRefId)
            {
                this._log.info(`Received Preauth response but I was not waiting for one. Incoming Pos Ref ID: ${incomingPosRefId}`);
                return;
            }
            // TH-1A, TH-2A

            currentTxFlowState.Completed(m.GetSuccessState(), m, "Preauth Transaction Ended.");
            // TH-6A, TH-6E
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this._spi.CurrentTxFlowState}));
        }

        static IsPreauthEvent(eventName)
        {
            return eventName.lastIndexOf("preauth",0) === 0 
                    || eventName == PreauthEvents.PreauthCompleteResponse
                    || eventName == PreauthEvents.PreauthCompleteRequest
                    || eventName == PreauthEvents.AccountVerifyRequest
                    || eventName == PreauthEvents.AccountVerifyResponse;
        }
    }

    class CashoutOnlyRequest
    {  
        constructor(amountCents, posRefId, surchargeAmount)
        {
            this.PosRefId = posRefId;
            this.CashoutAmount = amountCents;
            this.SurchargeAmount = surchargeAmount;
            this.Config = new SpiConfig();
            this.Options = new TransactionOptions();
        }
        
        ToMessage()
        {
            var data = {
                "pos_ref_id": this.PosRefId,
                "cash_amount": this.CashoutAmount,
                "surcharge_amount": this.SurchargeAmount
            };

            this.Config.addReceiptConfig(data);
            return new Message(RequestIdHelper.Id("cshout"), Events.CashoutOnlyRequest, data, true);
        }
    }

    class SettleRequest {
        constructor(id) {
            this.Id = id;
        }

        ToMessage() {
            return new Message(this.Id, Events.SettleRequest, null, true);
        }
    }

    class SettlementEnquiryRequest
    {
        constructor(id)
        {
            this.Id = id;
        }
        
        ToMessage()
        {
            return new Message(this.Id, Events.SettlementEnquiryRequest, null, true);
        }
    }

    class SetPosInfoRequest
    {
        constructor(version, vendorId, libraryLanguage, libraryVersion, otherInfo)
        {
            this._version = version;
            this._vendorId = vendorId;
            this._libraryLanguage = libraryLanguage;
            this._libraryVersion = libraryVersion;
            this._otherInfo = otherInfo;
        }

        toMessage()
        {
            var data = {
                pos_version: this._version,
                pos_vendor_id: this._vendorId,
                library_language: this._libraryLanguage,
                library_version: this._libraryVersion,
                other_info: this._otherInfo
            };

            return new Message(RequestIdHelper.Id("prav"), Events.SetPosInfoRequest, data, true);
        }
    }

    class SetPosInfoResponse
    {
        constructor(m)
        {
            this._success = m.GetSuccessState() == SuccessState.Success;
            this._m = m;
        }
        isSuccess()
        {
            return this._success;
        }
        getErrorReason()
        {
            return this._m.Data.error_reason;
        }
        getErrorDetail()
        {
            return this._m.Data.error_detail;
        }
        getResponseValueWithAttribute(attribute)
        {
            return this._m.Data[attribute];
        }
    }

    class DeviceInfo
    {
        static GetAppDeviceInfo()
        {
            var deviceInfo = {};
            deviceInfo['device_system'] = navigator.userAgent;
            // deviceInfo.Add("device_system", Environment.OSVersion.Platform.ToString() + " " + Environment.OSVersion.Version.ToString());
            return deviceInfo;
        }
    }

    class PurchaseHelper
    {
        static CreatePurchaseRequest(amountCents, purchaseId)
        {
            return new PurchaseRequest(amountCents, purchaseId);
        }
        
        static CreatePurchaseRequestV2(posRefId, purchaseAmount, tipAmount, cashoutAmount, promptForCashout, surchargeAmount)
        {
            var pr = Object.assign(new PurchaseRequest(purchaseAmount, posRefId),
            {
                CashoutAmount: cashoutAmount,
                TipAmount: tipAmount,
                PromptForCashout: promptForCashout,
                SurchargeAmount: surchargeAmount
            });

            return pr;
        }

        static CreateRefundRequest(amountCents, purchaseId, isSuppressMerchantPassword)
        {
            return new RefundRequest(amountCents, purchaseId, isSuppressMerchantPassword);
        }

    }

    class KeyRollingHelper {
        static PerformKeyRolling(krRequest, currentSecrets)
        {
            let m = new Message(krRequest.Id, Events.KeyRollResponse, {"status": "confirmed"}, true);
            let newSecrets = new Secrets(Crypto.GenerateHash(currentSecrets.EncKey).toUpperCase(),Crypto.GenerateHash(currentSecrets.HmacKey).toUpperCase());
            return new KeyRollingResult(m, newSecrets);
        }
    }

    class KeyRollingResult {
        constructor(keyRollingConfirmation, newSecrets) {
            this.KeyRollingConfirmation = keyRollingConfirmation;
            this.NewSecrets = newSecrets;
        }
    }

    class PongHelper
    {
        static GeneratePongRessponse(ping)
        {
            return new Message(ping.Id, Events.Pong, null, true);
        }
    }

    class PingHelper
    {
        static GeneratePingRequest()
        {
            return new Message(RequestIdHelper.Id("ping"), Events.Ping, null, true);
        }
    }

    class DeviceAddressStatus
    {
        get Address()
        {
            if(this.UseSecureWebSockets) 
            {
                return this.fqdn;
            } 
            else
            {
                return this.ip;
            }
        }

        set Address(address)
        {
            if(this.UseSecureWebSockets) 
            {
                this.fqdn = addreses;
            } 
            else
            {
                this.ip = address;
            }
        }

        constructor(useSecureWebSockets) 
        {
            this.UseSecureWebSockets = useSecureWebSockets;

            this.ip = null;
            this.fqdn = null;
            this.last_updated = null;
        }
    }

    class DeviceAddressService
    {
        // RetrieveService(serialNumber, apiKey = 'spi-sample-pos1', acquirerCode, useSecureWebSockets, isTestMode)
        RetrieveService(serialNumber, apiKey = 'spi-sample-pos1', acquirerCode, isSecureConnection, isTestMode)    
        {
            var path = isSecureConnection ? 'fqdn' : 'ip';
            var deviceAddressUri = isTestMode ? `https://device-address-api-sb.${acquirerCode}.msp.assemblypayments.com/v1/${serialNumber}/${path}` : `https://device-address-api.${acquirerCode}.msp.assemblypayments.com/v1/${serialNumber}/${path}`;

            return fetch(deviceAddressUri, {
                method: 'GET',
                headers: {
                    "ASM-MSP-DEVICE-ADDRESS-API-KEY": apiKey
                }
            })
            .then(response => response.json())
            .catch((response) => {
                console.error(`Status code ${response.StatusCode} received from ${deviceAddressUri} - Exception ${response.error}`);
            })
        }
    }

    class PrintingRequest
    {
        constructor(key, payload)
        {
            this._key = key;
            this._payload = payload;
        }

        toMessage()
        {
            var data = {
                "key": this._key,
                "payload": this._payload
            };

            return new Message(RequestIdHelper.Id("print"), Events.PrintingRequest, data, true);
        }
    }

    /**
     * This class is a mock printer for the terminal to print Receipts
     */
    class Printer {
        constructor(element) {
            this.buffer     = [];
            this.element    = element;
        }

        print(...args) {
            this.buffer.push(args.join(' '));
            this._render();
        }

        _render() {
            this.element.innerText = this.buffer.join(`\n\n \\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/ \n\n`);
            this.element.scrollTop = this.element.scrollHeight;
        }

        Clear() {
            this.buffer = [];
            this._render();
        }
    }

    class TerminalStatusRequest
    {
        ToMessage()
        {
            var data = {};

            return new Message(RequestIdHelper.Id("trmnl"), Events.TerminalStatusRequest, data, true);
        }
    }

    const SPI_VERSION = '2.4.0';

    class Spi {

        get CurrentStatus() {
            return this._currentStatus;
        }

        set CurrentStatus(value) {
            if(this._currentStatus === value) {
                return;
            }

            this._currentStatus = value;
            document.dispatchEvent(new CustomEvent('StatusChanged', {detail: value}));
        }

        constructor(posId, serialNumber, eftposAddress, secrets) 
        {
            this._posId = posId;
            this._serialNumber = serialNumber;
            this._secrets = secrets;
            this._forceSecureWebSockets = false;
            this._eftposAddress = "ws://" + eftposAddress;
            this._log = console;
            this.Config = new SpiConfig();

            this.CurrentDeviceStatus = null;
            this._deviceApiKey  = null;
            this._acquirerCode  = null;
            this._inTestMode    = false;
            this._autoAddressResolutionEnabled = false;

            // Our stamp for signing outgoing messages
            this._spiMessageStamp = new MessageStamp(this._posId, this._secrets, 0);

            this._posVendorId = null;
            this._posVersion = null;
            this._hasSetInfo = null;

            // We will maintain some state
            this._mostRecentPingSent = null;
            this._mostRecentPongReceived = null;
            this._missedPongsCount = 0;
            this._retriesSinceLastDeviceAddressResolution = 0;
            this._mostRecentLoginResponse = null;

            this._pongTimeout = 5000;
            this._pingFrequency = 18000;
            
            this._readyToTransact = null;
            this._periodicPingThread = null;

            this._txMonitorCheckFrequency = 1000;
            this._checkOnTxFrequency = 20000;
            this._maxWaitForCancelTx = 10000;
            this._sleepBeforeReconnectMs = 5000;
            this._missedPongsToDisconnect = 2;
            this._retriesBeforeResolvingDeviceAddress = 5;

            this.CurrentFlow                = null;
            this.CurrentPairingFlowState    = null;
            this.CurrentTxFlowState         = null;
        }

        EnablePayAtTable()
        {
            this._spiPat = new SpiPayAtTable(this);
            return this._spiPat;
        }

        DisablePayAtTable()
        {
            this._spiPat = new SpiPayAtTable(this);
            this._spiPat.Config.PayAtTabledEnabled = false;
            return this._spiPat;
        }

        EnablePreauth()
        {
            this._spiPreauth = new SpiPreauth(this);
            return this._spiPreauth;
        }

        Start() {

            if (!this._posVendorId || !this._posVersion)
            {
                // POS information is now required to be set
                this._log.warn("Missing POS vendor ID and version. posVendorId and posVersion are required before starting");
                throw new Error("Missing POS vendor ID and version. posVendorId and posVersion are required before starting");
            }

            this._resetConn();
            this._startTransactionMonitoringThread();

            this.CurrentFlow = SpiFlow.Idle;
            if (this._secrets != null)
            {
                this._log.info("Starting in Paired State");
                this._currentStatus = SpiStatus.PairedConnecting;
                this._conn.Connect(); // This is non-blocking
            }
            else
            {
                this._log.info("Starting in Unpaired State");
                this._currentStatus = SpiStatus.Unpaired;
            } 
        }

        /// <summary>
        /// Set the acquirer code of your bank, please contact Assembly's Integration Engineers for acquirer code.
        /// </summary>
        SetAcquirerCode(acquirerCode)
        {
            this._acquirerCode = acquirerCode;
            return true;
        }

        /// <summary>
        /// Set the api key used for auto address discovery feature
        /// </summary>
        /// <returns></returns>
        SetDeviceApiKey(deviceApiKey)
        {
            this._deviceApiKey = deviceApiKey;
            return true;
        }

        /// <summary>
        /// Allows you to set the serial number of the Eftpos
        /// </summary>
        SetSerialNumber(serialNumber)
        {
            if (this.CurrentStatus != SpiStatus.Unpaired)
                return false;

            var was = this._serialNumber;
            this._serialNumber = serialNumber;
            if (this._autoAddressResolutionEnabled && this.HasSerialNumberChanged(was))
            {
                this._autoResolveEftposAddress();
            }

            return true;
        }

        /// <summary>
        /// Allows you to set the auto address discovery feature. 
        /// </summary>
        /// <returns></returns>
        SetAutoAddressResolution(autoAddressResolutionEnable)
        {
            if (this.CurrentStatus == SpiStatus.PairedConnected)
                return false;

            var was = this._autoAddressResolutionEnabled;
            this._autoAddressResolutionEnabled = autoAddressResolutionEnable;
            if (autoAddressResolutionEnable && !was)
            {
                // we're turning it on
                this._autoResolveEftposAddress();
            }

            return true;
        }

        /// <summary>
        /// Call this method to set the client library test mode.
        /// Set it to true only while you are developing the integration. 
        /// It defaults to false. For a real merchant, always leave it set to false. 
        /// </summary>
        /// <param name="testMode"></param>
        /// <returns></returns>
        SetTestMode(testMode)
        {
            if (this.CurrentStatus != SpiStatus.Unpaired)
                return false;

            if (testMode == this._inTestMode)
                return true;

            // we're changing mode
            this._inTestMode = testMode;
            this._autoResolveEftposAddress();
            return true;
        }

        /// <summary>
        /// Set the client library to use secure web sockets TLS (wss protocol)
        /// </summary>
        /// <param name="isSecure"></param>
        /// <returns></returns>
        SetSecureWebSockets(useSecureWebSockets)
        {
            this._forceSecureWebSockets = useSecureWebSockets;
        }

        // <summary>
        // Allows you to set the PosId which identifies this instance of your POS.
        // Can only be called in thge Unpaired state. 
        // </summary>
        SetPosId(posId)
        {
            if (this.CurrentStatus != SpiStatus.Unpaired)
                return false;

            this._posId = posId;
            this._spiMessageStamp.PosId = posId;
            return true;
        }

        // <summary>
        // Allows you to set the PinPad address. Sometimes the PinPad might change IP address 
        // (we recommend reserving static IPs if possible).
        // Either way you need to allow your User to enter the IP address of the PinPad.
        // </summary>
        SetEftposAddress(address)
        {
            if (this.CurrentStatus == SpiStatus.PairedConnected || this._autoAddressResolutionEnabled) {
                return false;
            }

            this._eftposAddress = "ws://" + address;
            this._conn.Address = this._eftposAddress;
            return true;
        }

        static GetVersion()
        {
            return SPI_VERSION;
        }

        /**
         * Sets values used to identify the POS software to the EFTPOS terminal.
         * Must be set before starting!
         *
         * @param posVendorId Vendor identifier of the POS itself.
         * @param posVersion  Version string of the POS itself.
         */
        SetPosInfo(posVendorId, posVersion)
        {
            this._posVendorId = posVendorId;
            this._posVersion = posVersion;
        }

        // <summary>
        // Call this one when a flow is finished and you want to go back to idle state.
        // Typically when your user clicks the "OK" bubtton to acknowldge that pairing is
        // finished, or that transaction is finished.
        // When true, you can dismiss the flow screen and show back the idle screen.
        // </summary>
        // <returns>true means we have moved back to the Idle state. false means current flow was not finished yet.</returns>
        AckFlowEndedAndBackToIdle()
        {
            if (this.CurrentFlow == SpiFlow.Idle)
                return true; // already idle

            if (this.CurrentFlow == SpiFlow.Pairing && this.CurrentPairingFlowState.Finished)
            {
                this.CurrentFlow = SpiFlow.Idle;
                return true;
            }
            
            if (this.CurrentFlow == SpiFlow.Transaction && this.CurrentTxFlowState.Finished)
            {
                this.CurrentFlow = SpiFlow.Idle;
                return true;
            }

            return false;
        }

        // endregion

        // <summary>
        // This will connect to the Eftpos and start the pairing process.
        // Only call this if you are in the Unpaired state.
        // Subscribe to the PairingFlowStateChanged event to get updates on the pairing process.
        // </summary>
        // <returns>Whether pairing has initiated or not</returns>
        Pair()
        {
            if (this.CurrentStatus != SpiStatus.Unpaired) {
                this._log.warn("Tried to Pair but we're already so.");
                return false;
            }

            if (!this._posId || !this._eftposAddress)
            {
                this._log.warn("Tried to Pair but missing posId or updatedEftposAddress");
                return false;
            }

            this.CurrentFlow = SpiFlow.Pairing;
            this.CurrentPairingFlowState = new PairingFlowState
            ({
                Successful: false,
                Finished: false,
                Message: "Connecting...",
                AwaitingCheckFromEftpos: false,
                AwaitingCheckFromPos: false,
                ConfirmationCode: ""
            });

            document.dispatchEvent(new CustomEvent('PairingFlowStateChanged', {detail: this.CurrentPairingFlowState}));
            this._conn.Connect(); // Non-Blocking
            return true;
        }

        // <summary>
        // Call this when your user clicks yes to confirm the pairing code on your 
        // screen matches the one on the Eftpos.
        // </summary>
        PairingConfirmCode()
        {
            if (!this.CurrentPairingFlowState.AwaitingCheckFromPos)
            {
                // We weren't expecting this
                return;
            }

            this.CurrentPairingFlowState.AwaitingCheckFromPos = false;
            if (this.CurrentPairingFlowState.AwaitingCheckFromEftpos)
            {
                // But we are still waiting for confirmation from Eftpos side.
                this._log.info("Pair Code Confirmed from POS side, but am still waiting for confirmation from Eftpos.");
                this.CurrentPairingFlowState.Message =
                    "Click YES on EFTPOS if code is: " + this.CurrentPairingFlowState.ConfirmationCode;
                document.dispatchEvent(new CustomEvent('PairingFlowStateChanged', {detail: this.CurrentPairingFlowState}));
            }
            else
            {
                // Already confirmed from Eftpos - So all good now. We're Paired also from the POS perspective.
                this._log.info("Pair Code Confirmed from POS side, and was already confirmed from Eftpos side. Pairing finalised.");
                this._onPairingSuccess();
                this._onReadyToTransact();
            }
        }

        // <summary>
        // Call this if your user clicks CANCEL or NO during the pairing process.
        // </summary>
        PairingCancel()
        {
            if (this.CurrentFlow != SpiFlow.Pairing || this.CurrentPairingFlowState.Finished) {
                return;
            }

            if (this.CurrentPairingFlowState.AwaitingCheckFromPos && !this.CurrentPairingFlowState.AwaitingCheckFromEftpos)
            {
                // This means that the Eftpos already thinks it's paired.
                // Let's tell it to drop keys
                this._send(new DropKeysRequest().ToMessage());
            }
            this._onPairingFailed();
        }

        // <summary>
        // Call this when your uses clicks the Unpair button.
        // This will disconnect from the Eftpos and forget the secrets.
        // The CurrentState is then changed to Unpaired.
        // Call this only if you are not yet in the Unpaired state.
        // </summary>
        Unpair()
        {
            if (this.CurrentStatus == SpiStatus.Unpaired) {
                return false;
            }

            if (this.CurrentFlow != SpiFlow.Idle) {
                return false;
            }
            
            // Best effort letting the eftpos know that we're dropping the keys, so it can drop them as well.
            this._send(new DropKeysRequest().ToMessage());
            this._doUnpair();
            return true;
        }

        // endregion

        // region Transaction Methods

        // <summary>
        // Initiates a purchase transaction. Be subscribed to TxFlowStateChanged event to get updates on the process.
        // </summary>
        // <param name="posRefId">Alphanumeric Identifier for your purchase.</param>
        // <param name="amountCents">Amount in Cents to charge</param>
        // <returns>InitiateTxResult</returns>
        InitiatePurchaseTx(posRefId, amountCents)
        {
            if (this.CurrentStatus == SpiStatus.Unpaired) {
                return new InitiateTxResult(false, "Not Paired");
            }

            if (this.CurrentFlow != SpiFlow.Idle) {
                return new InitiateTxResult(false, "Not Idle");
            }

            var purchaseRequest = PurchaseHelper.CreatePurchaseRequest(amountCents, posRefId);
            purchaseRequest.Config = this.Config;
            var purchaseMsg = purchaseRequest.ToMessage();
            this.CurrentFlow = SpiFlow.Transaction;
            this.CurrentTxFlowState = new TransactionFlowState(
                posRefId, TransactionType.Purchase, amountCents, purchaseMsg,
                `Waiting for EFTPOS connection to make payment request for ${amountCents / 100.0}`);
            if (this._send(purchaseMsg))
            {
                this.CurrentTxFlowState.Sent(`Asked EFTPOS to accept payment for ${amountCents / 100.0}`);
            }
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
            return new InitiateTxResult(true, "Purchase Initiated");
        }

        // <summary>
        // Initiates a purchase transaction. Be subscribed to TxFlowStateChanged event to get updates on the process.
        // <para>Tip and cashout are not allowed simultaneously.</para>
        // </summary>
        // <param name="posRefId">An Unique Identifier for your Order/Purchase</param>
        // <param name="purchaseAmount">The Purchase Amount in Cents.</param>
        // <param name="tipAmount">The Tip Amount in Cents</param>
        // <param name="cashoutAmount">The Cashout Amount in Cents</param>
        // <param name="promptForCashout">Whether to prompt your customer for cashout on the Eftpos</param>
        // <param name="options">The Setting to set Header and Footer for the Receipt</param>
        // <param name="surchargeAmount">The Surcharge Amount in Cents</param>
        // <returns>InitiateTxResult</returns>
        InitiatePurchaseTxV2(posRefId, purchaseAmount, tipAmount, cashoutAmount, promptForCashout, options = {}, surchargeAmount = 0)
        {
            if (this.CurrentStatus == SpiStatus.Unpaired) return new InitiateTxResult(false, "Not Paired");

            if (tipAmount > 0 && (cashoutAmount > 0 || promptForCashout)) return new InitiateTxResult(false, "Cannot Accept Tips and Cashout at the same time.");
            
            if (this.CurrentFlow != SpiFlow.Idle) return new InitiateTxResult(false, "Not Idle");
            this.CurrentFlow = SpiFlow.Transaction;
            
            var purchase = PurchaseHelper.CreatePurchaseRequestV2(posRefId, purchaseAmount, tipAmount, cashoutAmount, promptForCashout, surchargeAmount);
            purchase.Config = this.Config;
            purchase.Options = options;
            var purchaseMsg = purchase.ToMessage();
            this.CurrentTxFlowState = new TransactionFlowState(
                posRefId, TransactionType.Purchase, purchaseAmount, purchaseMsg,
                `Waiting for EFTPOS connection to make payment request. ${purchase.AmountSummary()}`);
            if (this._send(purchaseMsg))
            {
                this.CurrentTxFlowState.Sent(`Asked EFTPOS to accept payment for ${purchase.AmountSummary()}`);
            }
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
            return new InitiateTxResult(true, "Purchase Initiated");
        }

        // <summary>
        // Initiates a refund transaction. Be subscribed to TxFlowStateChanged event to get updates on the process.
        // </summary>
        // <param name="posRefId">Alphanumeric Identifier for your refund.</param>
        // <param name="amountCents">Amount in Cents to charge</param>
        // <param name="isSuppressMerchantPassword">Merchant Password control in VAA</param>
        // <returns>InitiateTxResult</returns>
        InitiateRefundTx(posRefId, amountCents, isSuppressMerchantPassword = false)
        {
            if (this.CurrentStatus == SpiStatus.Unpaired) {
                return new InitiateTxResult(false, "Not Paired");
            }

            if (this.CurrentFlow != SpiFlow.Idle) {
                return new InitiateTxResult(false, "Not Idle");
            }

            var refundRequest = PurchaseHelper.CreateRefundRequest(amountCents, posRefId, isSuppressMerchantPassword);
            refundRequest.Config = this.Config;
            var refundMsg = refundRequest.ToMessage();
            this.CurrentFlow = SpiFlow.Transaction;
            this.CurrentTxFlowState = new TransactionFlowState(
                posRefId, TransactionType.Refund, amountCents, refundMsg, 
                `Waiting for EFTPOS connection to make refund request for ${(amountCents / 100.0).toFixed(2)}`);
            if (this._send(refundMsg))
            {
                this.CurrentTxFlowState.Sent(`Asked EFTPOS to refund ${(amountCents / 100.0).toFixed(2)}`);
            }
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
            return new InitiateTxResult(true, "Refund Initiated");
        }
        
        // <summary>
        // Let the EFTPOS know whether merchant accepted or declined the signature
        // </summary>
        // <param name="accepted">whether merchant accepted the signature from customer or not</param>
        AcceptSignature(accepted)
        {
            if (this.CurrentFlow != SpiFlow.Transaction || this.CurrentTxFlowState.Finished || !this.CurrentTxFlowState.AwaitingSignatureCheck)
            {
                this._log.info("Asked to accept signature but I was not waiting for one.");
                return new MidTxResult(false, "Asked to accept signature but I was not waiting for one.");
            }

            this.CurrentTxFlowState.SignatureResponded(accepted ? "Accepting Signature..." : "Declining Signature...");
            var sigReqMsg = this.CurrentTxFlowState.SignatureRequiredMessage;
            this._send(accepted
                ? new SignatureAccept(this.CurrentTxFlowState.PosRefId).ToMessage()
                : new SignatureDecline(this.CurrentTxFlowState.PosRefId).ToMessage());
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
            return new MidTxResult(true, "");
        }

        // <summary>
        // Submit the Code obtained by your user when phoning for auth. 
        // It will return immediately to tell you whether the code has a valid format or not. 
        // If valid==true is returned, no need to do anything else. Expect updates via standard callback.
        // If valid==false is returned, you can show your user the accompanying message, and invite them to enter another code. 
        // </summary>
        // <param name="authCode">The code obtained by your user from the merchant call centre. It should be a 6-character alpha-numeric value.</param>
        // <returns>Whether code has a valid format or not.</returns>
        SubmitAuthCode(authCode)
        {
            if (authCode.length != 6)
            {
                return new SubmitAuthCodeResult(false, "Not a 6-digit code.");    
            }
                    
            if (this.CurrentFlow != SpiFlow.Transaction || this.CurrentTxFlowState.Finished || !this.CurrentTxFlowState.AwaitingPhoneForAuth)
            {
                this._log.info("Asked to send auth code but I was not waiting for one.");
                return new SubmitAuthCodeResult(false, "Was not waiting for one.");
            }

            this.CurrentTxFlowState.AuthCodeSent(`Submitting Auth Code ${authCode}`);
            this._send(new AuthCodeAdvice(this.CurrentTxFlowState.PosRefId, authCode).ToMessage());
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
            return new SubmitAuthCodeResult(true, "Valid Code.");
        }

        // <summary>
        // Attempts to cancel a Transaction. 
        // Be subscribed to TxFlowStateChanged event to see how it goes.
        // Wait for the transaction to be finished and then see whether cancellation was successful or not.
        // </summary>
        // <returns>MidTxResult - false only if you called it in the wrong state</returns>
        CancelTransaction()
        {
            if (this.CurrentFlow != SpiFlow.Transaction || this.CurrentTxFlowState.Finished)
            {
                this._log.info("Asked to cancel transaction but I was not in the middle of one.");
                return new MidTxResult(false, "Asked to cancel transaction but I was not in the middle of one.");
            }

            // TH-1C, TH-3C - Merchant pressed cancel
            if (this.CurrentTxFlowState.RequestSent)
            {
                var cancelReq = new CancelTransactionRequest();
                this.CurrentTxFlowState.Cancelling("Attempting to Cancel Transaction...");
                this._send(cancelReq.ToMessage());
            }
            else
            {
                // We Had Not Even Sent Request Yet. Consider as known failed.
                this.CurrentTxFlowState.Failed(null, "Transaction Cancelled. Request Had not even been sent yet.");
            }
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
            return new MidTxResult(true, "");
        }

        // <summary>
        // Initiates a cashout only transaction. Be subscribed to TxFlowStateChanged event to get updates on the process.
        // </summary>
        // <param name="posRefId">Alphanumeric Identifier for your transaction.</param>
        // <param name="amountCents">Amount in Cents to cash out</param>
        // <param name="surchargeAmount">The Surcharge Amount in Cents</param>
        // <returns>InitiateTxResult</returns>
        InitiateCashoutOnlyTx(posRefId, amountCents, surchargeAmount = 0)
        {
            if (this.CurrentStatus == SpiStatus.Unpaired) return new InitiateTxResult(false, "Not Paired");

            if (this.CurrentFlow != SpiFlow.Idle) return new InitiateTxResult(false, "Not Idle");
            var cashoutOnlyRequest = new CashoutOnlyRequest(amountCents, posRefId, surchargeAmount);
            cashoutOnlyRequest.Config = this.Config;
            var cashoutMsg = cashoutOnlyRequest.ToMessage();
            this.CurrentFlow = SpiFlow.Transaction;
            this.CurrentTxFlowState = new TransactionFlowState(
                posRefId, TransactionType.CashoutOnly, amountCents, cashoutMsg,
                `Waiting for EFTPOS connection to send cashout request for ${(amountCents / 100).toFixed(2)}`);
            if (this._send(cashoutMsg))
            {
                this.CurrentTxFlowState.Sent(`Asked EFTPOS to do cashout for ${(amountCents / 100).toFixed(2)}`);
            }
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
            return new InitiateTxResult(true, "Cashout Initiated");
        }    

        // <summary>
        // Initiates a Mail Order / Telephone Order Purchase Transaction
        // </summary>
        // <param name="posRefId">Alphanumeric Identifier for your transaction.</param>
        // <param name="amountCents">Amount in Cents</param>
        // <param name="surchargeAmount">The Surcharge Amount in Cents</param>
        // <returns>InitiateTxResult</returns>
        InitiateMotoPurchaseTx(posRefId, amountCents, surchargeAmount = 0)
        {
            if (this.CurrentStatus == SpiStatus.Unpaired) return new InitiateTxResult(false, "Not Paired");

            if (this.CurrentFlow != SpiFlow.Idle) return new InitiateTxResult(false, "Not Idle");
            var motoPurchaseRequest = new MotoPurchaseRequest(amountCents, posRefId, surchargeAmount);
            motoPurchaseRequest.Config = this.Config;
            var cashoutMsg = motoPurchaseRequest.ToMessage();
            this.CurrentFlow = SpiFlow.Transaction;
            this.CurrentTxFlowState = new TransactionFlowState(
                posRefId, TransactionType.MOTO, amountCents, cashoutMsg,
                `Waiting for EFTPOS connection to send MOTO request for ${(amountCents / 100).toFixed(2)}`);
            if (this._send(cashoutMsg))
            {
                this.CurrentTxFlowState.Sent(`Asked EFTPOS do MOTO for ${(amountCents / 100).toFixed(2)}`);
            }
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
            return new InitiateTxResult(true, "MOTO Initiated");
        }

        // <summary>
        // Initiates a settlement transaction.
        // Be subscribed to TxFlowStateChanged event to get updates on the process.
        // </summary>
        InitiateSettleTx(posRefId)
        {
            if (this.CurrentStatus == SpiStatus.Unpaired) {
                return new InitiateTxResult(false, "Not Paired");
            }

            if (this.CurrentFlow != SpiFlow.Idle) {
                return new InitiateTxResult(false, "Not Idle");
            }

            var settleRequestMsg = new SettleRequest(RequestIdHelper.Id("settle")).ToMessage();
            this.CurrentFlow = SpiFlow.Transaction;
            this.CurrentTxFlowState = new TransactionFlowState(
                posRefId, TransactionType.Settle, 0, settleRequestMsg, 
                `Waiting for EFTPOS connection to make a settle request`);

            if (this._send(settleRequestMsg))
            {
                this.CurrentTxFlowState.Sent(`Asked EFTPOS to settle.`);
            }
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
            return new InitiateTxResult(true, "Settle Initiated");   
        }

        // <summary>
        // </summary>
        InitiateSettlementEnquiry(posRefId)
        {
            if (this.CurrentStatus == SpiStatus.Unpaired) return new InitiateTxResult(false, "Not Paired");

            if (this.CurrentFlow != SpiFlow.Idle) return new InitiateTxResult(false, "Not Idle");
            var stlEnqMsg = new SettlementEnquiryRequest(RequestIdHelper.Id("stlenq")).ToMessage();
            this.CurrentFlow = SpiFlow.Transaction;
            this.CurrentTxFlowState = new TransactionFlowState(
                posRefId, TransactionType.SettlementEnquiry, 0, stlEnqMsg,
                "Waiting for EFTPOS connection to make a settlement enquiry");
            if (this._send(stlEnqMsg))
            {
                this.CurrentTxFlowState.Sent("Asked EFTPOS to make a settlement enquiry.");
            }
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
            return new InitiateTxResult(true, "Settle Initiated");   
        }

        // <summary>
        // Initiates a Get Last Transaction. Use this when you want to retrieve the most recent transaction
        // that was processed by the Eftpos.
        // Be subscribed to TxFlowStateChanged event to get updates on the process.
        // </summary>
        InitiateGetLastTx()
        {
            if (this.CurrentStatus == SpiStatus.Unpaired) {
                return new InitiateTxResult(false, "Not Paired");
            }

            if (this.CurrentFlow != SpiFlow.Idle) {
                return new InitiateTxResult(false, "Not Idle");
            }

            var gltRequestMsg = new GetLastTransactionRequest().ToMessage();
            this.CurrentFlow = SpiFlow.Transaction;
            var posRefId = gltRequestMsg.Id; // GetLastTx is not trying to get anything specific back. So we just use the message id.
            this.CurrentTxFlowState = new TransactionFlowState(
                posRefId, TransactionType.GetLastTransaction, 0, gltRequestMsg, 
                "Waiting for EFTPOS connection to make a Get-Last-Transaction request.");
            
            if (this._send(gltRequestMsg))
            {
                this.CurrentTxFlowState.Sent(`Asked EFTPOS for last transaction.`);
            }
        
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
            return new InitiateTxResult(true, "GLT Initiated");   
        }

        // <summary>
        // This is useful to recover from your POS crashing in the middle of a transaction.
        // When you restart your POS, if you had saved enough state, you can call this method to recover the client library state.
        // You need to have the posRefId that you passed in with the original transaction, and the transaction type.
        // This method will return immediately whether recovery has started or not.
        // If recovery has started, you need to bring up the transaction modal to your user a be listening to TxFlowStateChanged.
        // </summary>
        // <param name="posRefId">The is that you had assigned to the transaction that you are trying to recover.</param>
        // <param name="txType">The transaction type.</param>
        // <returns></returns>
        InitiateRecovery(posRefId, txType)
        {
            if (this.CurrentStatus == SpiStatus.Unpaired) return new InitiateTxResult(false, "Not Paired");
        
            if (this.CurrentFlow != SpiFlow.Idle) return new InitiateTxResult(false, "Not Idle");
            
            this.CurrentFlow = SpiFlow.Transaction;
            
            var gltRequestMsg = new GetLastTransactionRequest().ToMessage();
            this.CurrentTxFlowState = new TransactionFlowState(
                posRefId, txType, 0, gltRequestMsg, 
                "Waiting for EFTPOS connection to attempt recovery.");
            
            if (this._send(gltRequestMsg))
            {
                this.CurrentTxFlowState.Sent(`Asked EFTPOS to recover state.`);
            }
        
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
            return new InitiateTxResult(true, "Recovery Initiated");
        }

        // <summary>
        // GltMatch attempts to conclude whether a gltResponse matches an expected transaction and returns
        // the outcome. 
        // If Success/Failed is returned, it means that the gtlResponse did match, and that transaction was succesful/failed.
        // If Unknown is returned, it means that the gltResponse does not match the expected transaction. 
        // </summary>
        // <param name="gltResponse">The GetLastTransactionResponse message to check</param>
        // <param name="posRefId">The Reference Id that you passed in with the original request.</param>

        // <returns></returns>
        GltMatch(gltResponse, posRefId, ...deprecatedArgs) 
        {
            // Obsolete method call check
            // Old interface: GltMatch(GetLastTransactionResponse gltResponse, TransactionType expectedType, int expectedAmount, DateTime requestTime, string posRefId)
            if(deprecatedArgs.length) {
                if(deprecatedArgs.length == 2) {
                    this._log.info("Obsolete method call detected: Use GltMatch(gltResponse, posRefId)");
                    return this.GltMatch(gltResponse, deprecatedArgs[2]);
                } else {
                    throw new Error("Obsolete method call with unknown args: Use GltMatch(GetLastTransactionResponse gltResponse, string posRefId)");
                }
            }

            this._log.info(`GLT CHECK: PosRefId: ${posRefId}->${gltResponse.GetPosRefId()}`);

            if (!posRefId == gltResponse.GetPosRefId())
            {
                return SuccessState.Unknown;
            }

            return gltResponse.GetSuccessState();
        }
        
        PrintReceipt(key, payload)
        {
            this._send(new PrintingRequest(key, payload).toMessage());
        }

        GetTerminalStatus()
        {
            this._send(new TerminalStatusRequest().ToMessage());
        }

        // endregion
            
        // region Internals for Pairing Flow

        // <summary>
        // Handling the 2nd interaction of the pairing process, i.e. an incoming KeyRequest.
        // </summary>
        // <param name="m">incoming message</param>
        _handleKeyRequest(m)
        {
            this.CurrentPairingFlowState.Message = "Negotiating Pairing...";
            document.dispatchEvent(new CustomEvent('PairingFlowStateChanged', {detail: this.CurrentPairingFlowState}));

            // Use the helper. It takes the incoming request, and generates the secrets and the response.
            var ph      = new PairingHelper();
            var result  = ph.GenerateSecretsAndKeyResponse(new KeyRequest(m));
            this._secrets = result.Secrets; // we now have secrets, although pairing is not fully finished yet.
            this._spiMessageStamp.Secrets = this._secrets; // updating our stamp with the secrets so can encrypt messages later.
            this._send(result.KeyResponse.ToMessage()); // send the key_response, i.e. interaction 3 of pairing.
        }

        // <summary>
        // Handling the 4th interaction of the pairing process i.e. an incoming KeyCheck.
        // </summary>
        // <param name="m"></param>
        _handleKeyCheck(m)
        {
            var keyCheck = new KeyCheck(m);
            this.CurrentPairingFlowState.ConfirmationCode = keyCheck.ConfirmationCode;
            this.CurrentPairingFlowState.AwaitingCheckFromEftpos = true;
            this.CurrentPairingFlowState.AwaitingCheckFromPos = true;
            this.CurrentPairingFlowState.Message = "Confirm that the following Code is showing on the Terminal";
            document.dispatchEvent(new CustomEvent('PairingFlowStateChanged', {detail: this.CurrentPairingFlowState}));
        }

        // <summary>
        // Handling the 5th and final interaction of the pairing process, i.e. an incoming PairResponse
        // </summary>
        // <param name="m"></param>
        _handlePairResponse(m)
        {
            var pairResp = new PairResponse(m);

            this.CurrentPairingFlowState.AwaitingCheckFromEftpos = false;
            if (pairResp.Success)
            {
                if (this.CurrentPairingFlowState.AwaitingCheckFromPos)
                {
                    // Still Waiting for User to say yes on POS
                    this._log.info("Got Pair Confirm from Eftpos, but still waiting for use to confirm from POS.");
                    this.CurrentPairingFlowState.Message = "Confirm that the following Code is what the EFTPOS showed";
                    document.dispatchEvent(new CustomEvent('PairingFlowStateChanged', {detail: this.CurrentPairingFlowState}));
                }
                else
                {
                    this._log.info("Got Pair Confirm from Eftpos, and already had confirm from POS. Now just waiting for first pong.");
                    this._onPairingSuccess();
                }
                // I need to ping/login even if the pos user has not said yes yet, 
                // because otherwise within 5 seconds connectiong will be dropped by eftpos.
                this._startPeriodicPing();
            }
            else
            {
                this._onPairingFailed();
            }
        }

        _handleDropKeysAdvice(m)
        {
            this._log.info("Eftpos was Unpaired. I shall unpair from my end as well.");
            this._doUnpair();
        }

        _onPairingSuccess()
        {
            this.CurrentPairingFlowState.Successful = true;
            this.CurrentPairingFlowState.Finished = true;
            this.CurrentPairingFlowState.Message = "Pairing Successful!";
            this.CurrentStatus = SpiStatus.PairedConnected;
            document.dispatchEvent(new CustomEvent('SecretsChanged', {detail: this._secrets}));
            document.dispatchEvent(new CustomEvent('PairingFlowStateChanged', {detail: this.CurrentPairingFlowState}));
        }

        _onPairingFailed()
        {
            this._secrets = null;
            this._spiMessageStamp.Secrets = null;
            this._conn.Disconnect();

            this.CurrentStatus = SpiStatus.Unpaired;
            this.CurrentPairingFlowState.Message = "Pairing Failed";
            this.CurrentPairingFlowState.Finished = true;
            this.CurrentPairingFlowState.Successful = false;
            this.CurrentPairingFlowState.AwaitingCheckFromPos = false;
            document.dispatchEvent(new CustomEvent('PairingFlowStateChanged', {detail: this.CurrentPairingFlowState}));
        }

        _doUnpair()
        {
            this.CurrentStatus = SpiStatus.Unpaired;
            this._conn.Disconnect();
            this._secrets = null;
            this._spiMessageStamp.Secrets = null;
            document.dispatchEvent(new CustomEvent('SecretsChanged', {detail: this._secrets}));
        }

        // <summary>
        // Sometimes the server asks us to roll our secrets.
        // </summary>
        // <param name="m"></param>
        _handleKeyRollingRequest(m)
        {
            // we calculate the new ones...
            var krRes = KeyRollingHelper.PerformKeyRolling(m, this._secrets);
            this._secrets = krRes.NewSecrets; // and update our secrets with them
            this._spiMessageStamp.Secrets = this._secrets; // and our stamp
            this._send(krRes.KeyRollingConfirmation); // and we tell the server that all is well.
            document.dispatchEvent(new CustomEvent('SecretsChanged', {detail: this._secrets}));
        }

        // <summary>
        // The PinPad server will send us this message when a customer signature is reqired.
        // We need to ask the customer to sign the incoming receipt.
        // And then tell the pinpad whether the signature is ok or not.
        // </summary>
        // <param name="m"></param>
        _handleSignatureRequired(m)
        {
            var incomingPosRefId = m.Data.pos_ref_id;
            if (this.CurrentFlow != SpiFlow.Transaction || this.CurrentTxFlowState.Finished || !this.CurrentTxFlowState.PosRefId == incomingPosRefId)
            {
                this._log.info(`Received Signature Required but I was not waiting for one. Incoming Pos Ref ID: ${incomingPosRefId}`);
                return;
            }
            this.CurrentTxFlowState.SignatureRequired(new SignatureRequired(m), "Ask Customer to Sign the Receipt");
        
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
        }

        // <summary>
        // The PinPad server will send us this message when an auth code is required.
        // </summary>
        // <param name="m"></param>
        _handleAuthCodeRequired(m)
        {
            var incomingPosRefId = m.Data.pos_ref_id;
            if (this.CurrentFlow != SpiFlow.Transaction || this.CurrentTxFlowState.Finished || !this.CurrentTxFlowState.PosRefId == incomingPosRefId)
            {
                _log.info(`Received Auth Code Required but I was not waiting for one. Incoming Pos Ref ID: ${incomingPosRefId}`);
                return;
            }
            var phoneForAuthRequired = new PhoneForAuthRequired(m);
            var msg = `Auth Code Required. Call ${phoneForAuthRequired.GetPhoneNumber()} and quote merchant id ${phoneForAuthRequired.GetMerchantId()}`;
            this.CurrentTxFlowState.PhoneForAuthRequired(phoneForAuthRequired, msg);
        
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
        }

        // <summary>
        // The PinPad server will reply to our PurchaseRequest with a PurchaseResponse.
        // </summary>
        // <param name="m"></param>
        _handlePurchaseResponse(m)
        {
            var incomingPosRefId = m.Data.pos_ref_id;
            if (this.CurrentFlow != SpiFlow.Transaction || this.CurrentTxFlowState.Finished || !this.CurrentTxFlowState.PosRefId == incomingPosRefId)
            {
                this._log.info(`Received Purchase response but I was not waiting for one. Incoming Pos Ref ID: ${incomingPosRefId}"`);
                return;
            }
            // TH-1A, TH-2A
            
            this.CurrentTxFlowState.Completed(m.GetSuccessState(), m, "Purchase Transaction Ended.");
            // TH-6A, TH-6E
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
        }

        // <summary>
        // The PinPad server will reply to our CashoutOnlyRequest with a CashoutOnlyResponse.
        // </summary>
        // <param name="m"></param>
        _handleCashoutOnlyResponse(m)
        {
            var incomingPosRefId = m.Data.pos_ref_id;
            if (this.CurrentFlow != SpiFlow.Transaction || this.CurrentTxFlowState.Finished || !this.CurrentTxFlowState.PosRefId == incomingPosRefId)
            {
                this._log.info(`Received Cashout Response but I was not waiting for one. Incoming Pos Ref ID: ${incomingPosRefId}`);
                return;
            }
            // TH-1A, TH-2A
            
            this.CurrentTxFlowState.Completed(m.GetSuccessState(), m, "Cashout Transaction Ended.");
            // TH-6A, TH-6E
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
        }

        // <summary>
        // The PinPad server will reply to our MotoPurchaseRequest with a MotoPurchaseResponse.
        // </summary>
        // <param name="m"></param>
        _handleMotoPurchaseResponse(m)
        {
            var incomingPosRefId = m.Data.pos_ref_id;
            if (this.CurrentFlow != SpiFlow.Transaction || this.CurrentTxFlowState.Finished || !this.CurrentTxFlowState.PosRefId == incomingPosRefId)
            {
                this._log.info(`Received Moto Response but I was not waiting for one. Incoming Pos Ref ID: ${incomingPosRefId}`);
                return;
            }
            // TH-1A, TH-2A
            
            this.CurrentTxFlowState.Completed(m.GetSuccessState(), m, "Moto Transaction Ended.");
            // TH-6A, TH-6E
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
        }   

        // <summary>
        // The PinPad server will reply to our RefundRequest with a RefundResponse.
        // </summary>
        // <param name="m"></param>
        _handleRefundResponse(m)
        {
            var incomingPosRefId = m.Data.pos_ref_id;
            if (this.CurrentFlow != SpiFlow.Transaction || this.CurrentTxFlowState.Finished | !this.CurrentTxFlowState.PosRefId == incomingPosRefId)
            {
                this._log.info(`Received Refund response but I was not waiting for this one. Incoming Pos Ref ID: ${incomingPosRefId}`);
                return;
            }
            // TH-1A, TH-2A
            
            this.CurrentTxFlowState.Completed(m.GetSuccessState(), m, "Refund Transaction Ended.");
            // TH-6A, TH-6E
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
        }

        // <summary>
        // TODO: Handle the Settlement Response received from the PinPad
        // </summary>
        // <param name="m"></param>
        HandleSettleResponse(m)
        {
            if (this.CurrentFlow != SpiFlow.Transaction || this.CurrentTxFlowState.Finished)
            {
                this._log.info(`Received Settle response but I was not waiting for one. ${m.DecryptedJson}`);
                return;
            }
            // TH-1A, TH-2A
            
            this.CurrentTxFlowState.Completed(m.GetSuccessState(), m, "Settle Transaction Ended.");
            // TH-6A, TH-6E
        
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
        }

        // <summary>
        // Handle the Settlement Enquiry Response received from the PinPad
        // </summary>
        // <param name="m"></param>
        _handleSettlementEnquiryResponse(m)
        {
            if (this.CurrentFlow != SpiFlow.Transaction || this.CurrentTxFlowState.Finished)
            {
                this._log.info(`Received Settlement Enquiry response but I was not waiting for one. ${m.DecryptedJson}`);
                return;
            }
            // TH-1A, TH-2A
            
            this.CurrentTxFlowState.Completed(m.GetSuccessState(), m, "Settlement Enquiry Ended.");
            // TH-6A, TH-6E
            
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
        }

        // <summary>
        // Sometimes we receive event type "error" from the server, such as when calling cancel_transaction and there is no transaction in progress.
        // </summary>
        // <param name="m"></param>
        _handleErrorEvent(m)
        {
            if (this.CurrentFlow == SpiFlow.Transaction
                && !this.CurrentTxFlowState.Finished
                && this.CurrentTxFlowState.AttemptingToCancel
                && m.GetError() == "NO_TRANSACTION")
            {
                // TH-2E
                this._log.info(`Was trying to cancel a transaction but there is nothing to cancel. Calling GLT to see what's up`);
                this._callGetLastTransaction();
            }
            else
            {
                this._log.info(`Received Error Event But Don't know what to do with it. ${m.DecryptedJson}`);
            }
        }

        // <summary>
        // When the PinPad returns to us what the Last Transaction was.
        // </summary>
        // <param name="m"></param>
        _handleGetLastTransactionResponse(m)
        {
            var txState = this.CurrentTxFlowState;
            if (this.CurrentFlow != SpiFlow.Transaction || txState.Finished)
            {
                // We were not in the middle of a transaction, who cares?
                return;
            }

            // TH-4 We were in the middle of a transaction.
            // Let's attempt recovery. This is step 4 of Transaction Processing Handling
            this._log.info(`Got Last Transaction..`);
            txState.GotGltResponse();
            var gtlResponse = new GetLastTransactionResponse(m);
            txState.GLTResponsePosRefId = gtlResponse.GetPosRefId();
            if (!gtlResponse.WasRetrievedSuccessfully())
            {
                if (gtlResponse.IsStillInProgress(txState.PosRefId))
                {
                    // TH-4E - Operation In Progress

                    if (gtlResponse.IsWaitingForSignatureResponse() && !txState.AwaitingSignatureCheck)
                    {
                        this._log.info("Eftpos is waiting for us to send it signature accept/decline, but we were not aware of this. " +
                                  "The user can only really decline at this stage as there is no receipt to print for signing.");
                        this.CurrentTxFlowState.SignatureRequired(new SignatureRequired(txState.PosRefId, m.Id, "MISSING RECEIPT\n DECLINE AND TRY AGAIN."), "Recovered in Signature Required but we don't have receipt. You may Decline then Retry.");
                    }
                    else if (gtlResponse.IsWaitingForAuthCode() && !txState.AwaitingPhoneForAuth)
                    {
                        this._log.info("Eftpos is waiting for us to send it auth code, but we were not aware of this. " +
                                  "We can only cancel the transaction at this stage as we don't have enough information to recover from this.");
                        this.CurrentTxFlowState.PhoneForAuthRequired(new PhoneForAuthRequired(txState.PosRefId, m.Id, "UNKNOWN", "UNKNOWN"), "Recovered mid Phone-For-Auth but don't have details. You may Cancel then Retry.");
                    }
                    else
                    {
                        this._log.info("Operation still in progress... stay waiting.");
                        // No need to publish txFlowStateChanged. Can return;
                        return;
                    }
                }
                else if (gtlResponse.WasTimeOutOfSyncError())
                {
                    // Let's not give up based on a TOOS error.
                    // Let's log it, and ignore it. 
                    this._log.info(`Time-Out-Of-Sync error in Get Last Transaction response. Let's ignore it and we'll try again.`);
                    // No need to publish txFlowStateChanged. Can return;
                    return;
                }
                else
                {
                    // TH-4X - Unexpected Response when recovering
                    this._log.info(`Unexpected Response in Get Last Transaction during - Received posRefId:${gtlResponse.GetPosRefId()} Error:${m.GetError()}`);
                    txState.UnknownCompleted("Unexpected Error when recovering Transaction Status. Check EFTPOS. ");
                }
            }
            else
            {
                if (txState.Type == TransactionType.GetLastTransaction)
                {
                    // THIS WAS A PLAIN GET LAST TRANSACTION REQUEST, NOT FOR RECOVERY PURPOSES.
                    this._log.info("Retrieved Last Transaction as asked directly by the user.");
                    gtlResponse.CopyMerchantReceiptToCustomerReceipt();
                    txState.Completed(m.GetSuccessState(), m, "Last Transaction Retrieved");
                }
                else
                {
                    // TH-4A - Let's try to match the received last transaction against the current transaction
                    var successState = this.GltMatch(gtlResponse, txState.PosRefId);
                    if (successState == SuccessState.Unknown)
                    {
                        // TH-4N: Didn't Match our transaction. Consider Unknown State.
                        this._log.info("Did not match transaction.");
                        txState.UnknownCompleted("Failed to recover Transaction Status. Check EFTPOS. ");
                    }
                    else
                    {
                        // TH-4Y: We Matched, transaction finished, let's update ourselves
                        gtlResponse.CopyMerchantReceiptToCustomerReceipt();
                        txState.Completed(successState, m, "Transaction Ended.");
                    }
                } 
            }
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: txState}));
        }

        //When the transaction cancel response is returned.
        _handleCancelTransactionResponse(m)
        {
            var incomingPosRefId = m.Data.pos_ref_id;
            if (this.CurrentFlow != SpiFlow.Transaction || this.CurrentTxFlowState.Finished || !this.CurrentTxFlowState.PosRefId == incomingPosRefId)
            {
                this._log.info(`Received Cancel Required but I was not waiting for one. Incoming Pos Ref ID: ${incomingPosRefId}`);
                return;
            }

            var txState = this.CurrentTxFlowState;
            var cancelResponse = new CancelTransactionResponse(m);

            if (cancelResponse.Success) return;

            this._log.warn("Failed to cancel transaction: reason=" + cancelResponse.GetErrorReason() + ", detail=" + cancelResponse.GetErrorDetail());

            txState.CancelFailed("Failed to cancel transaction: " + cancelResponse.GetErrorDetail() + ". Check EFTPOS.");
        
            document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: txState}));
        }

        _handleSetPosInfoResponse(m)
        {
            var response = new SetPosInfoResponse(m);
            if (response.isSuccess())
            {
                this._hasSetInfo = true;
                this._log.info("Setting POS info successful");
            }
            else
            {
                this._log.warn("Setting POS info failed: reason=" + response.getErrorReason() + ", detail=" + response.getErrorDetail());
            }
        }

        _startTransactionMonitoringThread()
        {
            var needsPublishing = false;
        
            var txState = this.CurrentTxFlowState;
            if (this.CurrentFlow == SpiFlow.Transaction && !txState.Finished)
            {
                var state = txState;
                if (state.AttemptingToCancel && Date.now() > state.CancelAttemptTime + this._maxWaitForCancelTx)
                {
                    // TH-2T - too long since cancel attempt - Consider unknown
                    this._log.info(`Been too long waiting for transaction to cancel.`);
                    txState.UnknownCompleted(`Waited long enough for Cancel Transaction result. Check EFTPOS. `);
                    needsPublishing = true;
                }
                else if (state.RequestSent && Date.now() > state.LastStateRequestTime + this._checkOnTxFrequency)
                {
                    // TH-1T, TH-4T - It's been a while since we received an update, let's call a GLT
                    this._log.info(`Checking on our transaction. Last we asked was at ${state.LastStateRequestTime}...`);
                    txState.CallingGlt();
                    this._callGetLastTransaction();
                }
            }
            
            if (needsPublishing) {
                document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
            }

            setTimeout(() => this._startTransactionMonitoringThread(), this._txMonitorCheckFrequency);
        }

        PrintingResponse(m) {
            throw new Error('Method not implemented. Please overwrite this method in your POS');
        }

        TerminalStatusResponse(m) {
            throw new Error('Method not implemented. Please overwrite this method in your POS');
        }

        BatteryLevelChanged(m) {
            throw new Error('Method not implemented. Please overwrite this method in your POS');
        }

        _handlePrintingResponse(m)
        {
            this.PrintingResponse(m);
        }

        _handleTerminalStatusResponse(m)
        {
            this.TerminalStatusResponse(m);
        }

        _handleBatteryLevelChanged(m)
        {
            this.BatteryLevelChanged(m);
        }

        // endregion
            
        // region Internals for Connection Management

        _resetConn()
        {
            // Setup the Connection
            this._conn = new Connection();

            if (this._isUsingHttps() || this._forceSecureWebSockets) {
                this._log.info("Secure connection detected.");
                this._eftposAddress = this._eftposAddress.replace("ws://", "wss://");
              }
              this._conn.Address = this._eftposAddress;
        
            // Register our Event Handlers
            document.addEventListener('ConnectionStatusChanged', (e) => this._onSpiConnectionStatusChanged(e.detail));
            document.addEventListener('MessageReceived', (e) => this._onSpiMessageReceived(e.detail));
            document.addEventListener('ErrorReceived', (e) => this._onWsErrorReceived(e.detail));
        }

        // <summary>
        // This method will be called when the connection status changes.
        // You are encouraged to display a PinPad Connection Indicator on the POS screen.
        // </summary>
        // <param name="state"></param>
        _onSpiConnectionStatusChanged(state)
        {
            switch (state.ConnectionState)
            {
                case ConnectionState.Connecting:
                    this._log.info(`I'm Connecting to the Eftpos at ${this._eftposAddress}...`);
                    break;

                case ConnectionState.Connected:
                    this._retriesSinceLastDeviceAddressResolution = 0;

                    if (this.CurrentFlow == SpiFlow.Pairing && this.CurrentStatus == SpiStatus.Unpaired)
                    {
                        this.CurrentPairingFlowState.Message = "Requesting to Pair...";
                        document.dispatchEvent(new CustomEvent('PairingFlowStateChanged', {detail: this.CurrentPairingFlowState}));
                        var pr = PairingHelper.NewPairRequest();
                        this._send(pr.ToMessage());
                    }
                    else
                    {
                        this._log.info(`I'm Connected to ${this._eftposAddress}...`);
                        this._spiMessageStamp.Secrets = this._secrets;
                        this._startPeriodicPing();
                    }
                    break;

                case ConnectionState.Disconnected:
                    // Let's reset some lifecycle related to connection state, ready for next connection
                    this._log.info(`I'm disconnected from ${this._eftposAddress}...`);
                    this._mostRecentPingSent = null;
                    this._mostRecentPongReceived = null;
                    this._missedPongsCount = 0;
                    this._stopPeriodicPing();

                    if (this.CurrentStatus != SpiStatus.Unpaired)
                    {
                        this.CurrentStatus = SpiStatus.PairedConnecting;

                        if (this.CurrentFlow == SpiFlow.Transaction && !this.CurrentTxFlowState.Finished)
                        {
                            // we're in the middle of a transaction, just so you know!
                            // TH-1D
                            this._log.info(`Lost connection in the middle of a transaction...`);
                        }
                        
                        if (this._conn == null) return; // This means the instance has been disposed. Aborting.
                        
                        if (this._autoAddressResolutionEnabled)
                        {
                            if (this._retriesSinceLastDeviceAddressResolution >= this._retriesBeforeResolvingDeviceAddress)
                            {
                                this._autoResolveEftposAddress();
                                this._retriesSinceLastDeviceAddressResolution = 0;
                            }
                            else
                            {
                                this._retriesSinceLastDeviceAddressResolution += 1;
                            }
                        }

                        this._log.info(`Will try to reconnect in ${this._sleepBeforeReconnectMs}ms...`);
                        setTimeout(() => {
                            if (this.CurrentStatus != SpiStatus.Unpaired)
                            {
                                // This is non-blocking
                                if(this._conn) 
                                {
                                    this._conn.Connect();
                                }
                            }
                        }, this._sleepBeforeReconnectMs);
                    }
                    else if (this.CurrentFlow == SpiFlow.Pairing)
                    {
                        this._log.info("Lost Connection during pairing.");
                        this.CurrentPairingFlowState.Message = "Could not Connect to Pair. Check Network and Try Again...";
                        this._onPairingFailed();
                        document.dispatchEvent(new CustomEvent('PairingFlowStateChanged', {detail: this.CurrentPairingFlowState}));
                    }
                    break;
                default:
                    throw new Error('Unknown state: ' + state);
            }
        }

        // <summary>
        // This is an important piece of the puzzle. It's a background thread that periodically
        // sends Pings to the server. If it doesn't receive Pongs, it considers the connection as broken
        // so it disconnects. 
        // </summary>
        _startPeriodicPing() {
            this._stopPeriodicPing();
            this._periodicPingThread = setInterval(() => this._periodicPing(),this._pingFrequency);
            this._periodicPing();
        }

        _periodicPing() {
            // while i'm still connected AND paired...
            if(this._conn.Connected && this._secrets != null) {
                this._doPing();

                setTimeout(() => {
                    if (this._mostRecentPingSent != null &&
                        (this._mostRecentPongReceived == null || this._mostRecentPongReceived.Id != this._mostRecentPingSent.Id))
                    {
                        this._missedPongsCount += 1;

                        this._log.info(`Eftpos didn't reply to my Ping. Missed Count: ${this._missedPongsCount}/${this._missedPongsToDisconnect}.`);

                        if (this._missedPongsCount < this._missedPongsToDisconnect)
                        {
                            this._log.info("Trying another ping...");
                            this._startPeriodicPing();
                            return;
                        }

                        // This means that we have not received a pong for our most recent ping.
                        // We consider this connection as broken.
                        // Let's Disconnect.
                        this._log.info("Disconnecting...");
                        this._conn.Disconnect();
                        this._stopPeriodicPing();
                    }

                    this._missedPongsCount = 0;

                },this._pongTimeout);

            } else {
                this._stopPeriodicPing();
                this._log.info("Cancelling periodic ping as were disconnected or not paired");
            }
        }

        // <summary>
        // We call this ourselves as soon as we're ready to transact with the PinPad after a connection is established.
        // This function is effectively called after we received the first Login Response from the PinPad.
        // </summary>
        _onReadyToTransact()
        {
            this._log.info("On Ready To Transact!");

            // So, we have just made a connection, pinged and logged in successfully.
            this.CurrentStatus = SpiStatus.PairedConnected;

            if (this.CurrentFlow == SpiFlow.Transaction && !this.CurrentTxFlowState.Finished)
            {
                if (this.CurrentTxFlowState.RequestSent)
                {
                    // TH-3A - We've just reconnected and were in the middle of Tx.
                    // Let's get the last transaction to check what we might have missed out on.
                    this.CurrentTxFlowState.CallingGlt();
                    this._callGetLastTransaction();
                }
                else
                {
                    // TH-3AR - We had not even sent the request yet. Let's do that now
                    this._send(this.CurrentTxFlowState.Request);
                    this.CurrentTxFlowState.Sent(`Sending Request Now...`);
                    document.dispatchEvent(new CustomEvent('TxFlowStateChanged', {detail: this.CurrentTxFlowState}));
                }
            }
            else
            {
                if (!this._hasSetInfo) { 
                    this._callSetPosInfo(); 
                }

                // let's also tell the eftpos our latest table configuration.
                if(this._spiPat) {
                    this._spiPat.PushPayAtTableConfig();
                }
            }
        }

        _callSetPosInfo()
        {
            var setPosInfoRequest = new SetPosInfoRequest(this._posVersion, this._posVendorId, "js", SPI_VERSION, DeviceInfo.GetAppDeviceInfo());
            this._send(setPosInfoRequest.toMessage());
        }

        // <summary>
        // When we disconnect, we should also stop the periodic ping.
        // </summary>
        _stopPeriodicPing() {
            if(this._periodicPingThread) {
                // If we were already set up, clean up before restarting.
                clearInterval(this._periodicPingThread);
                this._periodicPingThread = null;
            }
        }

        // Send a Ping to the Server
        _doPing()
        {
            var ping = PingHelper.GeneratePingRequest();
            this._mostRecentPingSent = ping;
            this._send(ping);
            this._mostRecentPingSentTime = Date.now();
        }

        // <summary>
        // Received a Pong from the server
        // </summary>
        // <param name="m"></param>
        _handleIncomingPong(m)
        {
            // We need to maintain this time delta otherwise the server will not accept our messages.
            this._spiMessageStamp.ServerTimeDelta = m.GetServerTimeDelta();

            if (this._mostRecentPongReceived == null)
            {
                // First pong received after a connection, and after the pairing process is fully finalised.
                if (this.CurrentStatus != SpiStatus.Unpaired)
                {
                    this._log.info("First pong of connection and in paired state.");
                    this._onReadyToTransact();
                }
                else
                {
                    this._log.info("First pong of connection but pairing process not finalised yet.");
                }
            }

            this._mostRecentPongReceived = m;
            this._log.debug(`PongLatency:${Date.now() - this._mostRecentPingSentTime}`);
        }

        // <summary>
        // The server will also send us pings. We need to reply with a pong so it doesn't disconnect us.
        // </summary>
        // <param name="m"></param>
        _handleIncomingPing(m)
        {
            var pong = PongHelper.GeneratePongRessponse(m);
            this._send(pong);
        }

        // <summary>
        // Ask the PinPad to tell us what the Most Recent Transaction was
        // </summary>
        _callGetLastTransaction()
        {
            var gltRequest = new GetLastTransactionRequest();
            this._send(gltRequest.ToMessage());
        }

        // <summary>
        // This method will be called whenever we receive a message from the Connection
        // </summary>
        // <param name="messageJson"></param>
        _onSpiMessageReceived(messageJson)
        {
            // First we parse the incoming message
            var m = Message.FromJson(messageJson.Message, this._secrets);
            this._log.info("Received:" + m.DecryptedJson);

            if (SpiPreauth.IsPreauthEvent(m.EventName))
            {
                this._spiPreauth._handlePreauthMessage(m);
                return;
            }

            // And then we switch on the event type.
            switch (m.EventName)
            {
                case Events.KeyRequest:
                    this._handleKeyRequest(m);
                    break;
                case Events.KeyCheck:
                    this._handleKeyCheck(m);
                    break;
                case Events.PairResponse:
                    this._handlePairResponse(m);
                    break;
                case Events.DropKeysAdvice:
                    this._handleDropKeysAdvice(m);
                    break;
                case Events.PurchaseResponse:
                    this._handlePurchaseResponse(m);
                    break;
                case Events.RefundResponse:
                    this._handleRefundResponse(m);
                    break;
                case Events.CashoutOnlyResponse:
                    this._handleCashoutOnlyResponse(m);
                    break;
                case Events.MotoPurchaseResponse:
                    this._handleMotoPurchaseResponse(m);
                    break;
                case Events.SignatureRequired:
                    this._handleSignatureRequired(m);
                    break;
                case Events.AuthCodeRequired:
                    this._handleAuthCodeRequired(m);
                    break;
                case Events.GetLastTransactionResponse:
                    this._handleGetLastTransactionResponse(m);
                    break;
                case Events.SettleResponse:
                    this.HandleSettleResponse(m);
                    break;
                case Events.SettlementEnquiryResponse:
                    this._handleSettlementEnquiryResponse(m);
                    break;
                case Events.Ping:
                    this._handleIncomingPing(m);
                    break;
                case Events.Pong:
                    this._handleIncomingPong(m);
                    break;
                case Events.KeyRollRequest:
                    this._handleKeyRollingRequest(m);
                    break;
                case Events.CancelTransactionResponse:
                    this._handleCancelTransactionResponse(m);
                    break;
                case Events.SetPosInfoResponse:
                    this._handleSetPosInfoResponse(m);
                    break;
                case Events.PayAtTableGetTableConfig:
                    if (this._spiPat == null)
                    {
                        this._send(PayAtTableConfig.FeatureDisableMessage(RequestIdHelper.Id("patconf")));
                        break;
                    }
                    this._spiPat._handleGetTableConfig(m);
                    break;
                case Events.PayAtTableGetBillDetails:
                    this._spiPat._handleGetBillDetailsRequest(m);
                    break;
                case Events.PayAtTableBillPayment:
                    this._spiPat._handleBillPaymentAdvice(m);
                    break;
                case Events.PrintingResponse:
                    this._handlePrintingResponse(m);
                    break;
                case Events.TerminalStatusResponse:
                    this._handleTerminalStatusResponse(m);
                    break;
                case Events.BatteryLevelChanged:
                    this._handleBatteryLevelChanged(m);
                    break;
                case Events.Error:
                    this._handleErrorEvent(m);
                    break;
                case Events.InvalidHmacSignature:
                    this._log.info("I could not verify message from Eftpos. You might have to Un-pair Eftpos and then reconnect.");
                    break;
                default:
                    this._log.info(`I don't Understand Event: ${m.EventName}, ${m.Data}. Perhaps I have not implemented it yet.`);
                    break;
            }
        }

        _onWsErrorReceived(error)
        {
            this._log.warn("Received WS Error: " + error.Message);
        }

        _send(message)
        {
            var json = message.ToJson(this._spiMessageStamp);
            if (this._conn.Connected)
            {
                this._log.info("Sending: " + message.DecryptedJson);
                this._conn.Send(json);
                return true;
            }
            else
            {
                this._log.info("Asked to send, but not connected: " + message.DecryptedJson);
                return false;
            }
        }

        HasSerialNumberChanged(updatedSerialNumber)
        {
            return this._serialNumber != updatedSerialNumber;
        }

        HasEftposAddressChanged(updatedEftposAddress)
        {
            return this._eftposAddress != updatedEftposAddress;
        }

        _autoResolveEftposAddress()
        {
            if (!this._autoAddressResolutionEnabled)
                return;
        
            if (!this._serialNumber)
                return;

            var service = new DeviceAddressService();
            var isSecureConnection = false;
             
            // determine whether to use wss or not
            if (this._isUsingHttps() || this._forceSecureWebSockets)
            {
                this._log.info(`Secure connection detected.`);    
                isSecureConnection = true;
            }

            this._log.info(`Resolving address for device ${this._serialNumber}.`);

            // return service.RetrieveService(this._serialNumber, this._deviceApiKey, this._acquirerCode, this._useSecureWebSockets, this._inTestMode).then((response) => 
            return service.RetrieveService(this._serialNumber, this._deviceApiKey, this._acquirerCode, isSecureConnection, this._inTestMode).then((response) => 
            {
                var deviceAddressStatus = Object.assign(new DeviceAddressStatus(isSecureConnection), response);

                if(!deviceAddressStatus || !deviceAddressStatus.Address)
                {
                    this._log.info(`Could not resolve device address.`);
                    return;
                }
                    
                this._log.info(`Address for device resolved to ${deviceAddressStatus.Address}`);
                if (!this.HasEftposAddressChanged(deviceAddressStatus.Address))
                    return;

                // update device and connection address
                var protocol = isSecureConnection ? "wss" : "ws";
                this._eftposAddress = protocol + "://" + deviceAddressStatus.Address;
                this._conn.Address = this._eftposAddress;

                this.CurrentDeviceStatus = deviceAddressStatus;

                document.dispatchEvent(new CustomEvent('DeviceAddressChanged', {detail: this.CurrentDeviceStatus}));
                
                return this.CurrentDeviceStatus;
            });
        }

        _isUsingHttps() 
        {
            return 'https:' == document.location.protocol ? true : false;
        }
    }

    class Logger {
        constructor(element, lineSeperator = '\n') {
            this.buffer     = [];
            this.element    = element;
            this.lineSeperator = lineSeperator;
        }

        Info(...args) {
            this.buffer.push(args.join(' '));
            this._render();
        }

        Debug(...args) {
            this.buffer.push(args.join(' '));
            this._render();
        }

        Warn(...args) {
            this.buffer.push(args.join(' '));
            this._render();
        }

        Error(...args) {
            this.buffer.push(args.join(' '));
            this._render();
        }

        Console(...args) {
            console.log(args.join(' '));
        }

        _render() {
            this.element.innerText = this.buffer.join(this.lineSeperator);
            this.element.scrollTop = this.element.scrollHeight;
        }

        Clear() {
            this.buffer = [];
            this._render();
        }
    }

    window.Spi = Spi;
    window.Logger = Logger;
    window.Printer = Printer;

    /// <reference path="SpiClient.d.ts" />
    var AssemblyEvent;
    (function (AssemblyEvent) {
        AssemblyEvent["STATUS_CHANGED"] = "StatusChanged";
        AssemblyEvent["PAIRING_FLOW_STATE_CHANGED"] = "PairingFlowStateChange";
        AssemblyEvent["SECRETS_CHANGED"] = "SecretsChanged";
        AssemblyEvent["TRANSACTION_FLOW_STATE_CHANGED"] = "TxFlowStateChanged";
    })(AssemblyEvent || (AssemblyEvent = {}));
    class EventHandler {
        constructor() {
            this.listeners = {};
            this.prepare();
        }
        /**
         * Prepare the event handler
         */
        prepare() {
            for (let event in AssemblyEvent) {
                this.listeners[event] = [];
            }
            document.addEventListener('StatusChanged', this.handleAssemblyEvent);
            document.addEventListener('PairingFlowStateChanged', this.handleAssemblyEvent);
            document.addEventListener('SecretsChanged', this.handleAssemblyEvent);
            document.addEventListener('TxFlowStateChanged', this.handleAssemblyEvent);
        }
        /**
         * Destroy the event handler (call this when it is no longer needed)
         */
        destroy() {
            this.listeners = {};
            document.removeEventListener('StatusChanged', this.handleAssemblyEvent);
            document.removeEventListener('PairingFlowStateChanged', this.handleAssemblyEvent);
            document.removeEventListener('SecretsChanged', this.handleAssemblyEvent);
            document.removeEventListener('TxFlowStateChanged', this.handleAssemblyEvent);
        }
        /**
         * Handle an event from AssemblyPayments
         * @param {Event} event
         */
        handleAssemblyEvent(event) {
            let type;
            switch (event.type) {
                case "StatusChanged":
                    type = AssemblyEvent.STATUS_CHANGED;
                    break;
                case "PairingFlowStateChanged":
                    type = AssemblyEvent.PAIRING_FLOW_STATE_CHANGED;
                    break;
                case "SecretsChanged":
                    type = AssemblyEvent.SECRETS_CHANGED;
                    break;
                case "TxFlowStateChanged":
                    type = AssemblyEvent.TRANSACTION_FLOW_STATE_CHANGED;
                    break;
                default:
                    return; // Not from Assembly
            }
            if (event instanceof CustomEvent) {
                if (!this.validateEvent(type, event)) {
                    return; // Also not from Assembly
                }
            }
            else {
                return; // Not from assembly
            }
            // Emit the event
            this.emit(type, event);
        }
        /**
         * Validate an event from AssemblyPayments to ensure it is correctly formed (and from them)
         * @param {AssemblyEvent} type
         * @param {CustomEvent} event
         * @returns {boolean}
         */
        validateEvent(type, event) {
            if (!event.detail) {
                return false;
            }
            switch (type) {
                case AssemblyEvent.STATUS_CHANGED:
                    if (typeof SpiStatus[event.detail] !== 'undefined') {
                        return true;
                    }
                    break;
                case AssemblyEvent.PAIRING_FLOW_STATE_CHANGED:
                    if (typeof event.detail.Message !== 'undefined' &&
                        typeof event.detail.ConfirmationCode !== 'undefined' &&
                        typeof event.detail.AwaitingCheckFromPos !== 'undefined') {
                        return true;
                    }
                    break;
                case AssemblyEvent.SECRETS_CHANGED:
                    if (typeof event.detail.EncKey !== 'undefined' &&
                        typeof event.detail.HmacKey !== 'undefined') {
                        return true;
                    }
                    break;
                case AssemblyEvent.TRANSACTION_FLOW_STATE_CHANGED:
                    if (typeof event.detail.PosRefId !== 'undefined' &&
                        typeof event.detail.Id !== 'undefined' &&
                        typeof event.detail.AmountCents !== 'undefined' &&
                        typeof event.detail.AwaitingPhoneForAuth !== 'undefined') {
                        return true;
                    }
                    break;
            }
            return false;
        }
        /**
         * Add an event listener
         * @param {AssemblyEvent} event
         * @param {EventCallback} callback
         */
        addEventListener(event, callback) {
            this.listeners[event].push(callback);
        }
        /**
         * Remove an event listener
         * @param {AssemblyEvent} event
         * @param {EventCallback} callback
         */
        removeEventListener(event, callback) {
            for (let i = 0, l = this.listeners[event].length; i < l; i++) {
                if (this.listeners[event][i] === callback) {
                    this.listeners[event] = [
                        ...this.listeners[event].slice(0, i),
                        ...this.listeners[event].slice(i + 1),
                    ];
                }
            }
        }
        /**
         * Send an event to all listeners
         * @param {AssemblyEvent} eventName
         * @param {Event} eventData
         */
        emit(eventName, eventData) {
            for (let i = 0, l = this.listeners[eventName].length; i < l; i++) {
                this.listeners[eventName][i](eventData);
            }
        }
    }

    class Settings {
        constructor() {
            this.settingKey = "SF_APPC_";
        }
        parseItem(item) {
            if (item === 'true') {
                return true;
            }
            else if (item === 'false') {
                return false;
            }
            let n = Number(item);
            let p = parseFloat(item);
            if (!isNaN(n) && !isNaN(p)) {
                return p;
            }
            return item;
        }
        getItem(setting, defaultValue) {
            return __awaiter(this, void 0, void 0, function* () {
                let item = localStorage.getItem(`${this.settingKey}${setting}`);
                if (item === null) {
                    if (defaultValue !== null) {
                        yield this.setItem(setting, defaultValue);
                    }
                    return defaultValue;
                }
                return this.parseItem(item);
            });
        }
        setItem(setting, value) {
            return __awaiter(this, void 0, void 0, function* () {
                if (value === true) {
                    value = 'true';
                }
                else if (value === false) {
                    value = 'false';
                }
                else {
                    value = value.toString();
                }
                localStorage.setItem(setting, value);
            });
        }
    }

    class InvalidClientException extends Error {
        constructor(field) {
            super(`The ${field} option is invalid`);
        }
    }

    class UnpairedException extends Error {
        constructor() {
            super(`The EFTPOS terminal is not currently paired`);
        }
    }

    class AlreadyPairedException extends Error {
        constructor() {
            super("An EFTPOS terminal is already paired with this PaymentClient");
        }
    }

    class PairingFailedException extends Error {
        constructor() {
            super("An unknown pairing failure occurred");
        }
    }

    class TransactionInProgressException extends Error {
        constructor() {
            super("There is currently a transaction in progress, please finish that one before starting this one");
        }
    }

    class NoTransactionInProgressException extends Error {
        constructor() {
            super("No transaction is currently in progress");
        }
    }

    class CancelTransactionFailedException extends Error {
        constructor() {
            super("An unknown error occurred while cancelling the transaction");
        }
    }

    class RecoveryFailedException extends Error {
        constructor() {
            super("Transaction recovery failed");
        }
    }

    class SettlementUnsuccessfulException extends Error {
        constructor() {
            super("The settlement was unsuccessful");
        }
    }

    class PaymentClient {
        constructor(apiKey, testMode) {
            this.optionsWaiting = null;
            this.apiKey = apiKey;
            this.testMode = testMode;
            this.Events = new EventHandler();
            this.spiClient = null;
            this.printCallback = (message) => {
                PaymentClient.LOGGER.info("PaymentClient attempted to print, but no method has been implemented", {
                    message,
                });
            };
            this.signatureCallback = () => __awaiter(this, void 0, void 0, function* () {
                PaymentClient.LOGGER.info("PaymentClient attempted to request a signature, but no method has been implemented");
                return true;
            });
            this.options = {
                registerName: null,
                serialNumber: null,
                posName: "",
                posVersion: "",
                cashoutPrompt: false,
                receipts: {
                    promptForCustomerCopy: false,
                    additionalHeaderText: "",
                    additionalFooterText: "",
                },
            };
            PaymentClient.LOGGER.debug("PaymentClient initialized");
            // noinspection JSIgnoredPromiseFromCall
            this.loadOptions();
        }
        /**
         * Destroy the PaymentClient
         */
        destroy() {
            PaymentClient.LOGGER.debug("About to destroy the PaymentClient");
            this.Events.destroy();
            PaymentClient.LOGGER.debug("PaymentClient destroyed");
        }
        /**
         * Load the options from the data store
         * @returns {Promise<void>}
         */
        loadOptions() {
            return __awaiter(this, void 0, void 0, function* () {
                PaymentClient.LOGGER.debug("About to start loading options");
                this.optionsWaiting = new Promise((res) => __awaiter(this, void 0, void 0, function* () {
                    [
                        this.options.registerName,
                        this.options.serialNumber,
                        this.options.posName,
                        this.options.posVersion,
                        this.options.cashoutPrompt,
                        this.options.receipts.promptForCustomerCopy,
                        this.options.receipts.additionalHeaderText,
                        this.options.receipts.additionalFooterText,
                    ] = (yield Promise.all([
                        PaymentClient.SETTINGS.getItem('REGISTER_NAME', null),
                        PaymentClient.SETTINGS.getItem('SERIAL_NUMBER', null),
                        PaymentClient.SETTINGS.getItem('POS_NAME', 'Unknown (Shopfront-APPC)'),
                        PaymentClient.SETTINGS.getItem('POS_VERSION', 'Unknown (Shopfront-APPC)'),
                        PaymentClient.SETTINGS.getItem('CASHOUT_PROMPT', false),
                        PaymentClient.SETTINGS.getItem('RECEIPT_PROMPT_CUSTOMER_COPY', false),
                        PaymentClient.SETTINGS.getItem('RECEIPT_HEADER_TEXT', ''),
                        PaymentClient.SETTINGS.getItem('RECEIPT_FOOTER_TEXT', ''),
                    ]));
                    PaymentClient.LOGGER.debug("Finished loading options");
                    res();
                }));
                return this.optionsWaiting;
            });
        }
        /**
         * Handle a print event from the SPI client
         * @param {Message} message
         */
        handlePrintFromSpi(message) {
            PaymentClient.LOGGER.debug("Received a print message from the SPI Client", message);
        }
        /**
         * Connect to the SPI client
         * @returns {Promise<void>}
         */
        connect() {
            return __awaiter(this, void 0, void 0, function* () {
                if (this.spiClient !== null) {
                    PaymentClient.LOGGER.debug("An SPI client already exists");
                    throw new Error("A SPI client already exists, please create a new PaymentClient instead");
                }
                PaymentClient.LOGGER.debug("About to start connecting");
                // Wait for the options to load if required
                if (this.optionsWaiting !== null) {
                    PaymentClient.LOGGER.debug("Waiting for options to load before continuing");
                    yield this.optionsWaiting;
                }
                // Ensure the options that were loaded are valid
                if (this.options.registerName === null) {
                    PaymentClient.LOGGER.debug("Invalid registerName option");
                    throw new InvalidClientException('registerName');
                }
                if (this.options.serialNumber === null) {
                    PaymentClient.LOGGER.debug("Invalid serialNumber option");
                    throw new InvalidClientException('serialNumber');
                }
                // Create the Simple Payment Interface client
                PaymentClient.LOGGER.debug("About to create the SPI client");
                this.spiClient = new Spi(this.options.registerName, this.options.serialNumber, '', null);
                PaymentClient.LOGGER.debug("SPI client successfully created, configuring");
                // Configure the client
                this.spiClient.Config.PromptForCustomerCopyOnEftpos = this.options.receipts.promptForCustomerCopy;
                this.spiClient.Config.SignatureFlowOnEftpos = false;
                this.spiClient.SetPosInfo(this.options.posName, this.options.posVersion);
                // Register our callbacks with the SPI client
                this.spiClient.PrintingResponse = this.handlePrintFromSpi;
                // Ensure that the SPI client works how we want
                this.spiClient.SetTestMode(this.testMode);
                this.spiClient.SetDeviceApiKey(this.apiKey);
                this.spiClient.SetSecureWebSockets(true);
                this.spiClient.SetAcquirerCode('wbc');
                // Automatically resolve the address
                PaymentClient.LOGGER.debug("SPI client configured, about to resolve the EFTPOS address");
                this.spiClient.SetAutoAddressResolution(true);
                // TODO: Wait to hear DeviceAddressChanged event
                PaymentClient.LOGGER.debug("SPI client EFTPOS address resolved");
                // Start the SPI client
                PaymentClient.LOGGER.debug("About to start the SPI client");
                this.spiClient.Start();
                PaymentClient.LOGGER.debug("SPI client successfully started");
                // If we have secrets, wait to hear about the ConnectionStatusChanged event
                // TODO: Implement secrets
                // Otherwise, check if we are unpaired
                if (this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
                    PaymentClient.LOGGER.debug("The EFTPOS terminal is currently unpaired");
                    throw new UnpairedException();
                }
            });
        }
        disconnect() {
            PaymentClient.LOGGER.debug("Attempting to disconnect the PaymentClient");
            if (this.spiClient === null) {
                PaymentClient.LOGGER.debug("No SPI client is connected");
                throw new Error("No SPI client is connected, please connect one first");
            }
            if (this.spiClient.CurrentStatus !== SpiStatus.Unpaired) {
                PaymentClient.LOGGER.debug("The EFTPOS terminal is currently paired");
                throw new Error("The EFTPOS terminal is paired, please unpair before disconnecting");
            }
            this.spiClient = null;
        }
        /**
         * Set whether test mode should be used
         * @param {boolean} enabled
         */
        setTestMode(enabled) {
            if (this.testMode === enabled) {
                PaymentClient.LOGGER.debug("Test Mode has not changed, ignoring", {
                    previous: this.testMode,
                    new: enabled,
                });
                return; // No-op
            }
            PaymentClient.LOGGER.debug("Test Mode has changed", {
                previous: this.testMode,
                new: enabled,
            });
            this.testMode = enabled;
            // TODO: Reconnect if required
        }
        /**
         * Set the options to use
         * @param {PaymentClientOptions} options
         * @returns {Promise<void>}
         */
        setOptions(options) {
            return __awaiter(this, void 0, void 0, function* () {
                PaymentClient.LOGGER.debug("About to set options", options);
                // Ensure the options are valid
                if (options.registerName === null) {
                    PaymentClient.LOGGER.debug("registerName is invalid");
                    throw new TypeError("null is not a valid name for the register");
                }
                if (options.serialNumber === null) {
                    PaymentClient.LOGGER.debug("serialNumber is invalid");
                    throw new TypeError("null is not a valid serial number");
                }
                // Store the options
                yield Promise.all([
                    PaymentClient.SETTINGS.setItem('REGISTER_NAME', options.registerName),
                    PaymentClient.SETTINGS.setItem('SERIAL_NUMBER', options.serialNumber),
                    PaymentClient.SETTINGS.setItem('POS_NAME', options.posName),
                    PaymentClient.SETTINGS.setItem('POS_VERSION', options.posVersion),
                    PaymentClient.SETTINGS.setItem('CASHOUT_PROMPT', options.cashoutPrompt),
                    PaymentClient.SETTINGS.setItem('RECEIPT_PROMPT_CUSTOMER_COPY', options.receipts.promptForCustomerCopy),
                    PaymentClient.SETTINGS.setItem('RECEIPT_HEADER_TEXT', options.receipts.additionalHeaderText),
                    PaymentClient.SETTINGS.setItem('RECEIPT_FOOTER_TEXT', options.receipts.additionalFooterText),
                ]);
                // Update the settings
                this.options = options;
                PaymentClient.LOGGER.debug("Options updated");
                // TODO: Reconnect if required
            });
        }
        /**
         * Pair the EFTPOS terminal with the Payment Client
         * @param {PairCallbackInterface} callbacks
         * @returns {Promise<void>}
         */
        pair(callbacks) {
            return __awaiter(this, void 0, void 0, function* () {
                PaymentClient.LOGGER.debug("About to start pairing the EFTPOS terminal");
                if (this.spiClient === null) {
                    PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
                    throw new Error("You must call .connect() before calling .pair()");
                }
                if (this.spiClient.CurrentStatus === SpiStatus.PairedConnected ||
                    this.spiClient.CurrentStatus === SpiStatus.PairedConnecting) {
                    PaymentClient.LOGGER.debug("An EFTPOS terminal is already paired", {
                        status: this.spiClient.CurrentStatus,
                    });
                    throw new AlreadyPairedException();
                }
                PaymentClient.LOGGER.debug("About to call Pair on the SPI client");
                const pairResult = this.spiClient.Pair();
                if (pairResult === false) {
                    PaymentClient.LOGGER.debug("Pairing failed due to negative result from SPI client");
                    throw new PairingFailedException();
                }
                // TODO: Listen for ConnectionStatusChanged event
                PaymentClient.LOGGER.debug("EFTPOS terminal successfully paired");
            });
        }
        /**
         * Cancel pairing the EFTPOS terminal with the PaymentClient
         * @returns {Promise<void>}
         */
        cancelPair() {
            return __awaiter(this, void 0, void 0, function* () {
                PaymentClient.LOGGER.debug("About to cancel the current pairing");
                if (this.spiClient === null) {
                    PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
                    throw new Error("You must call .connect() before calling .cancelPair()");
                }
                if (this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
                    PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently paired");
                    throw new UnpairedException();
                }
                this.spiClient.PairingCancel();
                // We sort of have to just trust that it worked unfortunately.
                PaymentClient.LOGGER.debug('Cancel pairing request sent');
            });
        }
        /**
         * Unpair the EFTPOS terminal with the PaymentClient
         * @returns {Promise<void>}
         */
        unpair() {
            return __awaiter(this, void 0, void 0, function* () {
                PaymentClient.LOGGER.debug("About to unpair the currently paired EFTPOS terminal");
                if (this.spiClient === null) {
                    PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
                    throw new Error("You must call .connect() before calling .unpair()");
                }
                if (this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
                    PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently paired");
                    throw new UnpairedException();
                }
                this.spiClient.Unpair();
                // We sort of have to just trust that it worked unfortunately.
                PaymentClient.LOGGER.debug('Unpair request sent');
            });
        }
        /**
         * Register the callback to use when printing is required
         * @param {PrintCallback} callback
         */
        onPrint(callback) {
            this.printCallback = callback;
        }
        /**
         * Register the callback to use whan a signature is required
         * @param {SignatureCallback} callback
         */
        onSignature(callback) {
            this.signatureCallback = callback;
        }
        /**
         * Get the transaction ID for the sale
         * @returns {string}
         */
        getTransactionId() {
            return `APPC-${Date.now().toString()}`;
        }
        /**
         * Perform a transaction
         * @param {number} amount
         * @param {number} cashout
         * @param {number} tip
         * @returns {Promise<TransactionResult>}
         */
        transaction(amount, cashout, tip) {
            return __awaiter(this, void 0, void 0, function* () {
                PaymentClient.LOGGER.debug("About to start transaction");
                let type = TransactionType.Purchase;
                if (amount < 0) {
                    type = TransactionType.Refund;
                }
                else if (amount === 0 && typeof cashout === 'number') {
                    type = TransactionType.CashoutOnly;
                }
                if (type === TransactionType.Refund) {
                    if (typeof cashout !== 'undefined' && cashout !== 0) {
                        PaymentClient.LOGGER.error("You cannot use cashout with a refund", {
                            amount,
                            cashout,
                            tip,
                        });
                        throw new TypeError("You cannot use cashout with a refund");
                    }
                    if (typeof tip !== 'undefined' && tip !== 0) {
                        PaymentClient.LOGGER.error("You cannot use tip with a refund", {
                            amount,
                            cashout,
                            tip,
                        });
                        throw new TypeError("You cannot use tip with a refund");
                    }
                }
                else if (type === TransactionType.CashoutOnly) {
                    if (typeof tip !== 'undefined' && tip !== 0) {
                        PaymentClient.LOGGER.error("You cannot use tip with a cashout only transaction", {
                            amount,
                            cashout,
                            tip,
                        });
                        throw new TypeError("You cannot use tip with a cashout only transaction");
                    }
                }
                if (typeof cashout === 'undefined') {
                    cashout = 0;
                }
                if (typeof tip === 'undefined') {
                    tip = 0;
                }
                if (cashout < 0) {
                    throw new TypeError("Cashout amounts cannot be negative");
                }
                if (tip < 0) {
                    throw new TypeError("Tip amounts cannot be negative");
                }
                if (this.spiClient === null) {
                    PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
                    throw new Error("You must call .connect() before calling .transaction()");
                }
                if (this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
                    PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently paired");
                    throw new UnpairedException();
                }
                if (tip > 0 && (cashout > 0 || this.options.cashoutPrompt)) {
                    throw new TypeError("Cannot provide a tip and cashout at the same time");
                }
                const id = this.getTransactionId();
                PaymentClient.LOGGER.info("[TRANSACTION START] About to start transaction", {
                    id,
                    amount,
                    cashout,
                    tip,
                    cashoutPrompt: this.options.cashoutPrompt
                });
                // Save the transactional data for recovery
                yield PaymentClient.SETTINGS.setItem('TRANSACTION_RECOVERY_ID', id);
                yield PaymentClient.SETTINGS.setItem('TRANSACTION_RECOVERY_TYPE', type);
                // Finish setting the options
                const transactionOptions = new TransactionOptions();
                transactionOptions.SetCustomerReceiptHeader(this.options.receipts.additionalHeaderText);
                transactionOptions.SetCustomerReceiptFooter(this.options.receipts.additionalFooterText);
                transactionOptions.SetMerchantReceiptHeader(this.options.receipts.additionalHeaderText);
                transactionOptions.SetMerchantReceiptFooter(this.options.receipts.additionalFooterText);
                let startTransactionResult;
                if (type === TransactionType.Purchase) {
                    PaymentClient.LOGGER.debug("Starting purchase");
                    startTransactionResult = this.spiClient.InitiatePurchaseTxV2(id, amount, cashout, tip, this.options.cashoutPrompt, transactionOptions);
                }
                else if (type === TransactionType.Refund) {
                    PaymentClient.LOGGER.debug("Starting refund");
                    startTransactionResult = this.spiClient.InitiateRefundTx(id, amount, false);
                }
                else if (type === TransactionType.CashoutOnly) {
                    PaymentClient.LOGGER.debug("Starting cashout");
                    startTransactionResult = this.spiClient.InitiateCashoutOnlyTx(id, cashout);
                }
                else {
                    // This should really never get here
                    throw new Error("Unknown transaction type specified");
                }
                if (!startTransactionResult.Initiated) {
                    PaymentClient.LOGGER.debug("The transaction was not initiated, trying to find out why");
                    if (this.spiClient.CurrentFlow === SpiFlow.Transaction) {
                        // There is currently a transaction in progress
                        PaymentClient.LOGGER.debug("There is currently a transaction in progress");
                        throw new TransactionInProgressException();
                    }
                    PaymentClient.LOGGER.debug("Unknown reason for not starting transaction", startTransactionResult);
                    throw new Error(`Could not start transaction for an unknown reason: ${startTransactionResult.Message}`);
                }
                // TODO: Wait for TxFlowStateChanged
            });
        }
        /**
         * Cancel the transaction currently in progress
         * @returns {Promise<void>}
         */
        cancelTransaction() {
            return __awaiter(this, void 0, void 0, function* () {
                PaymentClient.LOGGER.debug("About to cancel the current transaction");
                if (this.spiClient === null) {
                    PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
                    throw new Error("You must call .connect() before calling .cancelTransaction()");
                }
                if (this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
                    PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently paired");
                    throw new UnpairedException();
                }
                const cancelResult = this.spiClient.CancelTransaction();
                if (cancelResult.Valid === false) {
                    PaymentClient.LOGGER.debug("Cancelling the transaction failed, attempting to determine why", cancelResult);
                    if (this.spiClient.CurrentFlow !== SpiFlow.Transaction) {
                        PaymentClient.LOGGER.debug("There is no transaction currently in progress");
                        throw new NoTransactionInProgressException();
                    }
                    if (this.spiClient.CurrentTxFlowState.Finished) {
                        PaymentClient.LOGGER.debug("The transaction is already finished");
                        throw new NoTransactionInProgressException();
                    }
                    PaymentClient.LOGGER.debug("Cancelling the transaction failed for an unknown reason");
                    throw new CancelTransactionFailedException();
                }
                // TODO: Listen for TxFlowStateChanged
            });
        }
        /**
         * Recover the previous transaction
         * @returns {Promise<TransactionResult>}
         */
        recover() {
            return __awaiter(this, void 0, void 0, function* () {
                PaymentClient.LOGGER.debug("About to start transaction recovery");
                if (this.spiClient === null) {
                    PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
                    throw new Error("You must call .connect() before calling .recover()");
                }
                if (this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
                    PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently paired");
                    throw new UnpairedException();
                }
                PaymentClient.LOGGER.debug("About to get the recovery data from the database");
                const id = yield PaymentClient.SETTINGS.getItem('TRANSACTION_RECOVERY_ID', null);
                const type = yield PaymentClient.SETTINGS.getItem('TRANSACTION_RECOVERY_TYPE', null);
                if (id === null || type === null) {
                    PaymentClient.LOGGER.debug("No transaction recovery data was found");
                    throw new RecoveryFailedException();
                }
                PaymentClient.LOGGER.debug("Recovery data obtained, initiating recovery");
                const recoveryResult = this.spiClient.InitiateRecovery(id, type);
                if (recoveryResult.Initiated === false) {
                    PaymentClient.LOGGER.debug("Could not initiate recovery, trying to determine the reason", recoveryResult);
                    PaymentClient.LOGGER.debug("Recovery could not initiate for an unknown reason");
                    throw new RecoveryFailedException();
                }
                // TODO: Listen for TxFlowStateChanged
            });
        }
        /**
         * Perform a settlement on the EFTPOS terminal
         * @returns {Promise<SettlementResult>}
         */
        settlement() {
            return __awaiter(this, void 0, void 0, function* () {
                PaymentClient.LOGGER.debug("About to perform settlement");
                if (this.spiClient === null) {
                    PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
                    throw new Error("You must call .connect() before calling .settlement()");
                }
                if (this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
                    PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently paired");
                    throw new UnpairedException();
                }
                const id = this.getTransactionId();
                PaymentClient.LOGGER.info("[SETTLEMENT START] About to start settlement", {
                    id,
                });
                const settlementResult = this.spiClient.InitiateSettleTx(id);
                if (settlementResult.Initiated === false) {
                    PaymentClient.LOGGER.debug("Settlement could not initialise, attempting to find reason");
                    if (this.spiClient.CurrentFlow !== SpiFlow.Idle) {
                        PaymentClient.LOGGER.debug("The EFTPOS terminal is not idle");
                        throw new SettlementUnsuccessfulException();
                    }
                    PaymentClient.LOGGER.debug("Settlement could not be performed for an unknown reason");
                    throw new SettlementUnsuccessfulException();
                }
                // TODO: Listen for TxFlowStateChanged event
            });
        }
        /**
         * Perform a settlement enquiry on the EFTPOS terminal
         * @returns {Promise<SettlementResult>}
         */
        settlementEnquiry() {
            return __awaiter(this, void 0, void 0, function* () {
                PaymentClient.LOGGER.debug("About to perform settlement");
                if (this.spiClient === null) {
                    PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
                    throw new Error("You must call .connect() before calling .settlementEnquiry()");
                }
                if (this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
                    PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently paired");
                    throw new UnpairedException();
                }
                const id = this.getTransactionId();
                PaymentClient.LOGGER.info("[SETTLEMENT START] About to start settlement", {
                    id,
                });
                const settlementResult = this.spiClient.InitiateSettleTx(id);
                if (settlementResult.Initiated === false) {
                    PaymentClient.LOGGER.debug("Settlement could not initialise, attempting to find reason");
                    if (this.spiClient.CurrentFlow !== SpiFlow.Idle) {
                        PaymentClient.LOGGER.debug("The EFTPOS terminal is not idle");
                        throw new SettlementUnsuccessfulException();
                    }
                    PaymentClient.LOGGER.debug("Settlement could not be performed for an unknown reason");
                    throw new SettlementUnsuccessfulException();
                }
                // TODO: Listen for TxFlowStateChanged event
            });
        }
    }
    /**
     * The logger to use
     * @type {LogInterface}
     */
    PaymentClient.LOGGER = console;
    /**
     * The settings interface to use
     * @type {Settings}
     */
    PaymentClient.SETTINGS = new Settings();

    exports.PaymentClient = PaymentClient;
    exports.AlreadyPairedException = AlreadyPairedException;
    exports.CancelTransactionFailedException = CancelTransactionFailedException;
    exports.InvalidClientException = InvalidClientException;
    exports.NoTransactionInProgressException = NoTransactionInProgressException;
    exports.PairingFailedException = PairingFailedException;
    exports.RecoveryFailedException = RecoveryFailedException;
    exports.SettlementUnsuccessfulException = SettlementUnsuccessfulException;
    exports.TransactionInProgressException = TransactionInProgressException;
    exports.UnpairedException = UnpairedException;

    return exports;

}({}, false));
