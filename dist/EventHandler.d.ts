/// <reference path="src/SpiClient.d.ts" />
interface ListenerMap<T> {
    [key: string]: T;
}
export interface EventCallback {
    (event: CustomEvent): void;
}
export declare enum AssemblyEvent {
    STATUS_CHANGED = "StatusChanged",
    PAIRING_FLOW_STATE_CHANGED = "PairingFlowStateChanged",
    SECRETS_CHANGED = "SecretsChanged",
    TRANSACTION_FLOW_STATE_CHANGED = "TxFlowStateChanged",
    DEVICE_ADDRESS_CHANGED = "DeviceAddressChanged",
    CONNECTION_STATUS_CHANGED = "ConnectionStatusChanged"
}
export interface EventEmitter {
    addEventListener(event: AssemblyEvent, callback: EventCallback): void;
    removeEventListener(event: AssemblyEvent, callback: EventCallback): void;
    emit(eventName: AssemblyEvent, eventData: CustomEvent): void;
}
export declare class EventHandler implements EventEmitter {
    protected listeners: ListenerMap<EventCallback[]>;
    private emitting;
    private afterEmit;
    constructor();
    /**
     * Prepare the event handler
     */
    protected prepare(): void;
    /**
     * Destroy the event handler (call this when it is no longer needed)
     */
    destroy(): void;
    /**
     * Handle an event from AssemblyPayments
     * @param {Event} event
     */
    protected handleAssemblyEvent: (event: Event) => void;
    /**
     * Validate an event from AssemblyPayments to ensure it is correctly formed (and from them)
     * @param {AssemblyEvent} type
     * @param {CustomEvent} event
     * @returns {boolean}
     */
    protected validateEvent(type: AssemblyEvent, event: CustomEvent): boolean;
    /**
     * Add an event listener
     * @param {AssemblyEvent} event
     * @param {EventCallback} callback
     */
    addEventListener(event: AssemblyEvent, callback: EventCallback): void;
    /**
     * Remove an event listener
     * @param {AssemblyEvent} event
     * @param {EventCallback} callback
     */
    removeEventListener(event: AssemblyEvent, callback: EventCallback): void;
    /**
     * Send an event to all listeners
     * @param {AssemblyEvent} eventName
     * @param {CustomEvent} eventData
     */
    emit(eventName: AssemblyEvent, eventData: CustomEvent): void;
}
export {};
