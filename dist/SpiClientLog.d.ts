import { SpiLog } from "@assemblypayments/spi-client-js/index.js";
export declare type MessageParser = {
    (message: string): void;
};
export declare class SpiClientLog implements SpiLog {
    protected parseMessage: MessageParser;
    constructor(parseMessage: MessageParser);
    protected handleMethod(parameters: Array<any>): void;
    assert(...obj: Array<any>): void;
    clear(): void;
    count(label?: string): void;
    countReset(label?: string): void;
    debug(...obj: Array<any>): void;
    dir(obj: object): void;
    dirxml(obj: object): void;
    error(...obj: Array<any>): void;
    group(label?: string): void;
    groupCollapsed(label?: string): void;
    groupEnd(): void;
    info(...obj: Array<any>): void;
    log(...obj: Array<any>): void;
    table(data: Array<any> | object, columns?: Array<string>): void;
    time(label: string): void;
    timeEnd(label: string): void;
    timeLog(label: string): void;
    trace(): void;
    warn(...obj: Array<any>): void;
}
