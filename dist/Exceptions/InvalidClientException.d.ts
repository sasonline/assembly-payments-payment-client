export declare class InvalidClientException extends Error {
    constructor(field: string);
}
