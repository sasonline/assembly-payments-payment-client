import { EventHandler } from "./EventHandler";
import { Spi } from "@assemblypayments/spi-client-js/index.js";
import { SettingsInterface } from "./Settings";
interface PaymentClientOptions {
    registerName: string | null;
    serialNumber: string | null;
    posName: string;
    posVersion: string;
    cashoutPrompt: boolean;
    receipts: {
        promptForCustomerCopy: boolean;
        additionalHeaderText: string;
        additionalFooterText: string;
    };
}
interface PrintCallback {
    (receipt: string, force: boolean): void;
}
interface SignatureCallback {
    (): Promise<boolean>;
}
interface AuthorizationCallback {
    (phoneNumber: string, merchantId: string): Promise<string>;
}
interface TransactionResult {
    rrn: string;
    response: string;
    amount: number;
    tip: number;
    surcharge: number;
    cashout: number;
}
declare type SettlementResult = void;
declare type AddressResolverFunction = {
    (success: boolean): void;
};
declare type ConnectionResolverFunction = {
    (success: boolean): void;
};
export declare class PaymentClient {
    /**
     * The logger to use
     * @type {LogInterface}
     */
    static LOGGER: LogInterface;
    /**
     * The settings interface to use
     * @type {Settings}
     */
    static SETTINGS: SettingsInterface;
    /**
     * The EventHandler for the PaymentClient
     * @type {EventHandler}
     */
    Events: EventHandler;
    protected apiKey: string;
    protected testMode: boolean;
    protected options: PaymentClientOptions;
    protected printCallback: PrintCallback;
    protected signatureCallback: SignatureCallback;
    protected phoneCallback: AuthorizationCallback;
    protected spiClient: Spi | null;
    protected optionsWaiting: null | Promise<void>;
    protected secrets: {
        EncKey: string | null;
        HmacKey: string | null;
    };
    protected listeners: {
        addressResolver: null | AddressResolverFunction;
        connectionResolver: null | ConnectionResolverFunction;
    };
    constructor(apiKey: string, testMode: boolean);
    /**
     * Destroy the PaymentClient
     */
    destroy(): void;
    /**
     * Load the options from the data store
     * @returns {Promise<void>}
     */
    protected loadOptions(): Promise<void>;
    /**
     * Handle a print event from the SPI client
     * @param {Message} message
     */
    private handlePrintFromSpi;
    /**
     * Parse the log from the SPI (as it's the only way to know some statuses)
     * @param {string} message
     */
    private parseLogFromSPI;
    /**
     * Connect to the SPI client
     * @returns {Promise<void>}
     */
    connect(): Promise<void>;
    /**
     * Disconnect the SPI client from the PaymentClient
     * @returns {Promise<void>}
     */
    disconnect(): Promise<void>;
    /**
     * Set whether test mode should be used
     * @param {boolean} enabled
     */
    setTestMode(enabled: boolean): Promise<void>;
    /**
     * Set the options to use
     * @param {PaymentClientOptions} options
     * @returns {Promise<void>}
     */
    setOptions(options: PaymentClientOptions): Promise<void>;
    /**
     * Update the secrets to communicate with the EFTPOS terminal
     * @param {CustomEvent} event
     * @returns {Promise<void>}
     */
    protected updateSecrets(event: CustomEvent): Promise<void>;
    /**
     * Update the device's URL to be able to automatically reconnect
     * @param {CustomEvent} event
     * @returns {Promise<void>}
     */
    protected updateDeviceAddress(event: CustomEvent): Promise<void>;
    /**
     * Pair the EFTPOS terminal with the Payment Client
     * @param {PairCallbackInterface} callbacks
     * @returns {Promise<void>}
     */
    pair(callbacks: PairCallbackInterface): Promise<void>;
    /**
     * Cancel pairing the EFTPOS terminal with the PaymentClient
     * @returns {Promise<void>}
     */
    cancelPair(): Promise<void>;
    /**
     * Unpair the EFTPOS terminal with the PaymentClient
     * @returns {Promise<void>}
     */
    unpair(): Promise<void>;
    /**
     * Register the callback to use when printing is required
     * @param {PrintCallback} callback
     */
    onPrint(callback: PrintCallback): void;
    /**
     * Register the callback to use when a signature is required
     * @param {SignatureCallback} callback
     */
    onSignature(callback: SignatureCallback): void;
    /**
     * Register the callback to use when phone authorization is required
     * @param {AuthorizationCallback} callback
     */
    onPhoneAuthorization(callback: AuthorizationCallback): void;
    /**
     * Get the transaction ID for the sale
     * @returns {string}
     */
    protected getTransactionId(): string;
    /**
     * Create a promise that will resolve when the EFTPOS terminal is connected
     * @returns {Promise<void>}
     */
    private createConnectionPromise;
    /**
     * Create the promise to listen for the transaction completion
     * @returns {Promise<TransactionResult>}
     */
    private createTransactionPromise;
    /**
     * Perform a transaction
     * @param {number} amount
     * @param {number} cashout
     * @param {number} tip
     * @returns {Promise<TransactionResult>}
     */
    transaction(amount: number, cashout?: number, tip?: number): Promise<TransactionResult>;
    /**
     * Cancel the transaction currently in progress
     * @returns {Promise<void>}
     */
    cancelTransaction(): Promise<void>;
    /**
     * Recover the previous transaction
     * @returns {Promise<TransactionResult>}
     */
    recover(): Promise<TransactionResult>;
    /**
     * Perform a settlement on the EFTPOS terminal
     * @returns {Promise<SettlementResult>}
     */
    settlement(): Promise<SettlementResult>;
    /**
     * Perform a settlement enquiry on the EFTPOS terminal
     * @returns {Promise<SettlementResult>}
     */
    settlementEnquiry(): Promise<SettlementResult>;
}
export {};
