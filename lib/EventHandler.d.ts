/// <reference path="../src/SpiClient.d.ts" />
interface ListenerMap<T> {
    [key: string]: T;
}
export declare class EventHandler implements EventEmitter {
    protected listeners: ListenerMap<EventCallback[]>;
    constructor();
    protected prepare(): void;
    destroy(): void;
    handleAssemblyEvent(event: Event): void;
    validateEvent(type: AssemblyEvent, event: CustomEvent): boolean;
    addEventListener(event: AssemblyEvent, callback: EventCallback): void;
    removeEventListener(event: AssemblyEvent, callback: EventCallback): void;
    emit(eventName: AssemblyEvent, eventData: Event): void;
}
export {};
