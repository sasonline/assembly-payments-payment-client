export declare class PaymentClient {
    protected apiKey: string;
    protected testMode: boolean;
    protected logger: LogInterface;
    protected registerId: string | null;
    constructor(apiKey: string, testMode: boolean);
    destroy(): void;
    setAPIKey(key: string): void;
    setTestMode(enabled: boolean): void;
    setLogger(logger: LogInterface): void;
}
