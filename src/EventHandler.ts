/// <reference path="SpiClient.d.ts" />
import {
    SpiStatus,
} from "@assemblypayments/spi-client-js/index.js";

interface ListenerMap<T> {
    [key: string]: T,
}

export interface EventCallback {
    (event: CustomEvent): void
}

export enum AssemblyEvent {
    STATUS_CHANGED                 = "StatusChanged",
    PAIRING_FLOW_STATE_CHANGED     = "PairingFlowStateChanged",
    SECRETS_CHANGED                = "SecretsChanged",
    TRANSACTION_FLOW_STATE_CHANGED = "TxFlowStateChanged",
    DEVICE_ADDRESS_CHANGED         = "DeviceAddressChanged",
    CONNECTION_STATUS_CHANGED      = "ConnectionStatusChanged",
}

export interface EventEmitter {
    addEventListener(event: AssemblyEvent, callback: EventCallback): void,
    removeEventListener(event: AssemblyEvent, callback: EventCallback): void,
    emit(eventName: AssemblyEvent, eventData: CustomEvent): void,
}

export class EventHandler implements EventEmitter {
    protected listeners: ListenerMap<EventCallback[]> = {};

    private emitting: boolean = false;
    private afterEmit: Array<{event: AssemblyEvent, callback: EventCallback}> = [];

    constructor() {
        this.prepare();
    }

    /**
     * Prepare the event handler
     */
    protected prepare(): void {
        for(let event in AssemblyEvent) {
            this.listeners[AssemblyEvent[event]] = [];
            document.addEventListener(AssemblyEvent[event], this.handleAssemblyEvent);
        }

        /*
        document.addEventListener('StatusChanged',           this.handleAssemblyEvent);
        document.addEventListener('PairingFlowStateChanged', this.handleAssemblyEvent);
        document.addEventListener('SecretsChanged',          this.handleAssemblyEvent);
        document.addEventListener('TxFlowStateChanged',      this.handleAssemblyEvent);
        document.addEventListener('DeviceAddressChanged',    this.handleAssemblyEvent);
        */
    }

    /**
     * Destroy the event handler (call this when it is no longer needed)
     */
    public destroy(): void {
        this.listeners = {};

        for(let event in AssemblyEvent) {
            document.removeEventListener(AssemblyEvent[event], this.handleAssemblyEvent);
        }

        /*
        document.removeEventListener('StatusChanged',           this.handleAssemblyEvent);
        document.removeEventListener('PairingFlowStateChanged', this.handleAssemblyEvent);
        document.removeEventListener('SecretsChanged',          this.handleAssemblyEvent);
        document.removeEventListener('TxFlowStateChanged',      this.handleAssemblyEvent);
        document.removeEventListener('DeviceAddressChanged',    this.handleAssemblyEvent);
        */
    };

    /**
     * Handle an event from AssemblyPayments
     * @param {Event} event
     */
    protected handleAssemblyEvent = (event: Event): void => {
        let type;
        switch(event.type) {
            case "StatusChanged":
                type = AssemblyEvent.STATUS_CHANGED;
                break;
            case "PairingFlowStateChanged":
                type = AssemblyEvent.PAIRING_FLOW_STATE_CHANGED;
                break;
            case "SecretsChanged":
                type = AssemblyEvent.SECRETS_CHANGED;
                break;
            case "TxFlowStateChanged":
                type = AssemblyEvent.TRANSACTION_FLOW_STATE_CHANGED;
                break;
            case "DeviceAddressChanged":
                type = AssemblyEvent.DEVICE_ADDRESS_CHANGED;
                break;
            case "ConnectionStatusChanged":
                type = AssemblyEvent.CONNECTION_STATUS_CHANGED;
                break;
            default:
                return; // Not from Assembly
        }

        if(event instanceof CustomEvent) {
            if(!this.validateEvent(type, event)) {
                return; // Also not from Assembly
            }
        } else {
            return; // Not from assembly
        }

        // Emit the event
        this.emit(type, event);
    };

    /**
     * Validate an event from AssemblyPayments to ensure it is correctly formed (and from them)
     * @param {AssemblyEvent} type
     * @param {CustomEvent} event
     * @returns {boolean}
     */
    protected validateEvent(type: AssemblyEvent, event: CustomEvent): boolean {
        if(!event.detail) {
            return false;
        }

        switch(type) {
            case AssemblyEvent.STATUS_CHANGED:
                if(typeof SpiStatus[event.detail] !== 'undefined') {
                    return true;
                }
                break;
            case AssemblyEvent.PAIRING_FLOW_STATE_CHANGED:
                if(
                    typeof event.detail.Message !== 'undefined' &&
                    typeof event.detail.ConfirmationCode !== 'undefined' &&
                    typeof event.detail.AwaitingCheckFromPos !== 'undefined'
                ) {
                    return true;
                }
                break;
            case AssemblyEvent.SECRETS_CHANGED:
                if(
                    typeof event.detail.EncKey !== 'undefined' &&
                    typeof event.detail.HmacKey !== 'undefined'
                ) {
                    return true;
                }
                break;
            case AssemblyEvent.TRANSACTION_FLOW_STATE_CHANGED:
                if(
                    typeof event.detail.PosRefId !== 'undefined' &&
                    typeof event.detail.Id !== 'undefined' &&
                    typeof event.detail.AmountCents !== 'undefined' &&
                    typeof event.detail.AwaitingPhoneForAuth !== 'undefined'
                ) {
                    return true;
                }
                break;
            case AssemblyEvent.DEVICE_ADDRESS_CHANGED:
                if(
                    typeof event.detail.UseSecureWebSockets === 'boolean' &&
                    event.detail.UseSecureWebSockets === true &&
                    typeof event.detail.fqdn !== 'undefined'
                ) {
                    return true;
                }
                break;
            case AssemblyEvent.CONNECTION_STATUS_CHANGED:
                if(
                    typeof event.detail.ConnectionState === 'string'
                ) {
                    return true;
                }
                break;
        }

        return false;
    }

    /**
     * Add an event listener
     * @param {AssemblyEvent} event
     * @param {EventCallback} callback
     */
    public addEventListener(event: AssemblyEvent, callback: EventCallback): void {
        this.listeners[event].push(callback);
    };

    /**
     * Remove an event listener
     * @param {AssemblyEvent} event
     * @param {EventCallback} callback
     */
    public removeEventListener(event: AssemblyEvent, callback: EventCallback): void {
        if(this.emitting) {
            this.afterEmit.push({
                event,
                callback,
            });

            return;
        }

        for(let i = 0, l = this.listeners[event].length; i < l; i++) {
            if(this.listeners[event][i] === callback) {
                this.listeners[event] = [
                    ...this.listeners[event].slice(0, i),
                    ...this.listeners[event].slice(i + 1),
                ];
            }
        }
    };

    /**
     * Send an event to all listeners
     * @param {AssemblyEvent} eventName
     * @param {CustomEvent} eventData
     */
    public emit(eventName: AssemblyEvent, eventData: CustomEvent): void {
        this.emitting = true;

        for(let i = 0, l = this.listeners[eventName].length; i < l; i++) {
            this.listeners[eventName][i](eventData);
        }

        for(let i = 0, l = this.afterEmit.length; i < l; i++) {
            this.removeEventListener(this.afterEmit[i].event, this.afterEmit[i].callback);
        }

        this.afterEmit = [];

        this.emitting = false;
    };
}