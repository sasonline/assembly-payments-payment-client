export type SettingValue = string | number | boolean;

export interface SettingsInterface {
    getItem(setting: string, defaultValue: SettingValue | null): Promise<SettingValue>,
    setItem(setting: string, value: SettingValue): Promise<void>;
    deleteItem(setting: string): Promise<void>;
}

export class Settings implements SettingsInterface {
    protected settingKey = "SF_APPC_";

    protected parseItem(item: string): SettingValue {
        if(item === 'true') {
            return true;
        } else if(item === 'false') {
            return false;
        }

        let n = Number(item);
        let p = parseFloat(item);

        if(!isNaN(n) && !isNaN(p)) {
            return p;
        }

        return item;
    }

    async getItem(setting: string, defaultValue: SettingValue): Promise<SettingValue> {
        let item: string | null = localStorage.getItem(`${this.settingKey}${setting}`);

        if(item === null) {
            if(defaultValue !== null) {
                await this.setItem(setting, defaultValue);
            }

            return defaultValue;
        }

        return this.parseItem(item);
    }

    async setItem(setting: string, value: SettingValue): Promise<void> {
        if(value === true) {
            value = 'true';
        } else if(value === false) {
            value = 'false';
        } else {
            value = value.toString();
        }

        localStorage.setItem(`${this.settingKey}${setting}`, value);
    }

    async deleteItem(setting: string): Promise<void> {
        localStorage.removeItem(setting);
    }
}