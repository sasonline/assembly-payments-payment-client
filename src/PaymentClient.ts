import {AssemblyEvent, EventHandler} from "./EventHandler";
/// <reference path="SpiClient.d.ts" />
import {
    Message,
    MidTxResult,
    PurchaseResponse,
    SecretObject,
    Spi,
    SpiFlow,
    SpiStatus,
    SuccessState,
    TransactionOptions,
    TransactionType,
} from "@assemblypayments/spi-client-js/index.js";
/// <reference path="SpiClient.d.ts" />
import {ConnectionState} from "@assemblypayments/spi-client-js/src/Connection.js";
import {Settings, SettingsInterface} from "./Settings";
import {InvalidClientException} from "./Exceptions/InvalidClientException";
import {UnpairedException} from "./Exceptions/UnpairedException";
import {AlreadyPairedException} from "./Exceptions/AlreadyPairedException";
import {PairingFailedException} from "./Exceptions/PairingFailedException";
import {TransactionInProgressException} from "./Exceptions/TransactionInProgressException";
import {NoTransactionInProgressException} from "./Exceptions/NoTransactionInProgressException";
import {CancelTransactionFailedException} from "./Exceptions/CancelTransactionFailedException";
import {RecoveryFailedException} from "./Exceptions/RecoveryFailedException";
import {SettlementUnsuccessfulException} from "./Exceptions/SettlementUnsuccessfulException";
import {SpiClientLog} from "./SpiClientLog";
import {NotPairingException} from "./Exceptions/NotPairingException";
import {TransactionUnknownException} from "./Exceptions/TransactionUnknownException";
import {TransactionDeclinedException} from "./Exceptions/TransactionDeclinedException";
import {TransactionCancelledException} from "./Exceptions/TransactionCancelledException";

interface PaymentClientOptions {
    registerName : string | null,
    serialNumber : string | null,
    posName      : string,
    posVersion   : string,
    cashoutPrompt: boolean,
    receipts     : {
        promptForCustomerCopy: boolean,
        additionalHeaderText : string,
        additionalFooterText : string,
    },
}

interface PrintCallback {
    (receipt: string, force: boolean): void,
}

interface SignatureCallback {
    (): Promise<boolean>,
}

interface AuthorizationCallback {
    (phoneNumber: string, merchantId: string): Promise<string>,
}

interface TransactionResult {
    rrn      : string,
    response : string,
    amount   : number,
    tip      : number,
    surcharge: number,
    cashout  : number,
}

type SettlementResult = void;

type AddressResolverFunction = {
    (success: boolean): void,
}

type ConnectionResolverFunction = {
    (success: boolean): void,
}

export class PaymentClient {
    /**
     * The logger to use
     * @type {LogInterface}
     */
    public static LOGGER: LogInterface = console;

    /**
     * The settings interface to use
     * @type {Settings}
     */
    public static SETTINGS: SettingsInterface = new Settings();

    /**
     * The EventHandler for the PaymentClient
     * @type {EventHandler}
     */
    public Events: EventHandler;

    protected apiKey: string;
    protected testMode: boolean;
    protected options: PaymentClientOptions;
    protected printCallback: PrintCallback;
    protected signatureCallback: SignatureCallback;
    protected phoneCallback: AuthorizationCallback;
    protected spiClient: Spi | null;
    protected optionsWaiting: null | Promise<void> = null;
    protected secrets: {
        EncKey : string | null,
        HmacKey: string | null,
    };

    protected listeners: {
        addressResolver   : null | AddressResolverFunction,
        connectionResolver: null | ConnectionResolverFunction,
    } = {
        addressResolver   : null,
        connectionResolver: null,
    };

    constructor(apiKey: string, testMode: boolean) {
        this.apiKey   = apiKey;
        this.testMode = testMode;

        this.Events    = new EventHandler();
        this.spiClient = null;

        this.printCallback = (message) => {
            PaymentClient.LOGGER.info("PaymentClient attempted to print, but no method has been implemented", {
                message,
            });
        };

        this.signatureCallback = async () => {
            PaymentClient.LOGGER.info("PaymentClient attempted to request a signature, but no method has been implemented");

            return true;
        };

        this.phoneCallback = async () => {
            PaymentClient.LOGGER.info("PaymentClient attempted to request phone authorization, but no method has been implemented");

            return '';
        };

        this.options = {
            registerName : null,
            serialNumber : null,
            posName      : "",
            posVersion   : "",
            cashoutPrompt: false,
            receipts     : {
                promptForCustomerCopy: false,
                additionalHeaderText : "",
                additionalFooterText : "",
            },
        };

        this.secrets = {
            EncKey : null,
            HmacKey: null,
        };

        this.Events.addEventListener(AssemblyEvent.SECRETS_CHANGED, this.updateSecrets);
        this.Events.addEventListener(AssemblyEvent.DEVICE_ADDRESS_CHANGED, this.updateDeviceAddress);

        PaymentClient.LOGGER.debug("PaymentClient initialized");

        // noinspection JSIgnoredPromiseFromCall
        this.loadOptions();
    }

    /**
     * Destroy the PaymentClient
     */
    public destroy(): void {
        PaymentClient.LOGGER.debug("About to destroy the PaymentClient");

        this.Events.destroy();

        PaymentClient.LOGGER.debug("PaymentClient destroyed");
    }

    /**
     * Load the options from the data store
     * @returns {Promise<void>}
     */
    protected async loadOptions(): Promise<void> {
        PaymentClient.LOGGER.debug("About to start loading options");

        this.optionsWaiting = new Promise(async res => {
            [
                this.options.registerName,
                this.options.serialNumber,
                this.options.posName,
                this.options.posVersion,
                this.options.cashoutPrompt,
                this.options.receipts.promptForCustomerCopy,
                this.options.receipts.additionalHeaderText,
                this.options.receipts.additionalFooterText,
                this.secrets.EncKey,
                this.secrets.HmacKey,
            ] = (await Promise.all([
                PaymentClient.SETTINGS.getItem('REGISTER_NAME', null),
                PaymentClient.SETTINGS.getItem('SERIAL_NUMBER', null),
                PaymentClient.SETTINGS.getItem('POS_NAME', 'Unknown (Shopfront-APPC)'),
                PaymentClient.SETTINGS.getItem('POS_VERSION', 'Unknown (Shopfront-APPC)'),
                PaymentClient.SETTINGS.getItem('CASHOUT_PROMPT', false),
                PaymentClient.SETTINGS.getItem('RECEIPT_PROMPT_CUSTOMER_COPY', false),
                PaymentClient.SETTINGS.getItem('RECEIPT_HEADER_TEXT', ''),
                PaymentClient.SETTINGS.getItem('RECEIPT_FOOTER_TEXT', ''),
                PaymentClient.SETTINGS.getItem('SECRET_ENC_KEY', null),
                PaymentClient.SETTINGS.getItem('SECRET_HMAC_KEY', null),
            ]) as [
                string | null,
                string | null,
                string,
                string,
                boolean,
                boolean,
                string,
                string,
                string | null,
                string | null
            ]);

            PaymentClient.LOGGER.debug("Finished loading options");
            res();
        });

        return this.optionsWaiting;
    }

    /**
     * Handle a print event from the SPI client
     * @param {Message} message
     */
    private handlePrintFromSpi(message: Message): void {
        PaymentClient.LOGGER.debug("Received a print message from the SPI Client", message);
    }

    /**
     * Parse the log from the SPI (as it's the only way to know some statuses)
     * @param {string} message
     */
    private parseLogFromSPI = (message: string): void => {
        if(message === 'Could not resolve device address.') {
            if(typeof this.listeners.addressResolver === 'function') {
                this.listeners.addressResolver(false);
            }
        } else if (message === 'On Ready To Transact!') {
            if(typeof this.listeners.connectionResolver === 'function') {
                this.listeners.connectionResolver(true);
            }
        }
    };

    /**
     * Connect to the SPI client
     * @returns {Promise<void>}
     */
    public async connect(): Promise<void> {
        if(this.spiClient !== null) {
            PaymentClient.LOGGER.debug("An SPI client already exists");
            throw new Error("A SPI client already exists, please create a new PaymentClient instead");
        }

        PaymentClient.LOGGER.debug("About to start connecting");

        // Wait for the options to load if required
        if(this.optionsWaiting !== null) {
            PaymentClient.LOGGER.debug("Waiting for options to load before continuing");
            await this.optionsWaiting;
        }

        // Ensure the options that were loaded are valid
        if(this.options.registerName === null || this.options.registerName === '') {
            PaymentClient.LOGGER.debug("Invalid registerName option");
            throw new InvalidClientException('registerName');
        }

        if(this.options.serialNumber === null || this.options.serialNumber === '') {
            PaymentClient.LOGGER.debug("Invalid serialNumber option");
            throw new InvalidClientException('serialNumber');
        }

        // Prepare the pairing resolver function
        const pairingResolver = this.createConnectionPromise();

        // Create the Simple Payment Interface client
        PaymentClient.LOGGER.debug("About to create the SPI client");

        let secrets: null | SecretObject = null;
        if(this.secrets.EncKey !== null && this.secrets.HmacKey !== null) {
            secrets = this.secrets as SecretObject;
        }

        let eftposAddress = await PaymentClient.SETTINGS.getItem('PREVIOUS_EFTPOS_ADDRESS', '') as string;
        this.spiClient    = new Spi(this.options.registerName, this.options.serialNumber, eftposAddress, secrets);

        PaymentClient.LOGGER.debug("SPI client successfully created, configuring");

        // Set the log (yay! another undocumented part)
        this.spiClient._log = new SpiClientLog(this.parseLogFromSPI);

        // Configure the client
        this.spiClient.Config.PromptForCustomerCopyOnEftpos = this.options.receipts.promptForCustomerCopy;
        this.spiClient.Config.SignatureFlowOnEftpos         = false;

        this.spiClient.SetPosInfo(this.options.posName, this.options.posVersion);

        // Register our callbacks with the SPI client
        this.spiClient.PrintingResponse    = this.handlePrintFromSpi;
        this.spiClient.BatteryLevelChanged = () => {}; // We can just ignore this

        // Ensure that the SPI client works how we want
        this.spiClient.SetDeviceApiKey(this.apiKey);
        this.spiClient.SetAcquirerCode('wbc');

        // Start the SPI client
        PaymentClient.LOGGER.debug("About to start the SPI client");
        this.spiClient.Start();
        PaymentClient.LOGGER.debug("SPI client successfully started");

        // Set the address resolution variables
        // This must be done after calling Start (due to checking the pairing status)
        this.spiClient.SetTestMode(this.testMode);
        this.spiClient.SetSecureWebSockets(true);

        if(secrets !== null && this.testMode) {
            // Ensure the terminal is in test mode
            // This is not meant to be used, however it allows the
            // auto address resolution to work with test mode
            this.spiClient._inTestMode = true;
        }

        // Prepare the resolver function
        const addressResolver = new Promise((res, rej) => {
            const eventHandler = (event: CustomEvent) => {
                PaymentClient.LOGGER.debug("Received DeviceAddressChanged event", event);

                if(event.detail.fqdn !== null && this.listeners.addressResolver !== null) {
                    this.listeners.addressResolver(true);
                } else if(this.listeners.addressResolver !== null) {
                    this.listeners.addressResolver(false);
                }
            };

            let handled = false;
            this.listeners.addressResolver = (success: boolean) => {
                PaymentClient.LOGGER.debug("Address resolver has been called", {
                    success,
                });

                if(handled) {
                    return;
                }

                this.Events.removeEventListener(AssemblyEvent.DEVICE_ADDRESS_CHANGED, eventHandler);
                this.listeners.addressResolver = null;
                handled = true;

                if(!success) {
                    rej();
                    return;
                }

                res();
            };

            this.Events.addEventListener(AssemblyEvent.DEVICE_ADDRESS_CHANGED, eventHandler);
        });

        // Automatically resolve the address
        PaymentClient.LOGGER.debug("SPI client configured, about to resolve the EFTPOS address");
        this.spiClient.SetAutoAddressResolution(true);

        try {
            await addressResolver;
        } catch(e) {
            PaymentClient.LOGGER.debug('Could not resolve EFTPOS address', e);

            // Delete the secrets
            await Promise.all([
                PaymentClient.SETTINGS.deleteItem('SECRET_ENC_KEY'),
                PaymentClient.SETTINGS.deleteItem('SECRET_HMAC_KEY'),
            ]);

            // Throw invalid client exception
            throw new InvalidClientException('serialNumber');
        }

        PaymentClient.LOGGER.debug("SPI client EFTPOS address resolved");

        // Check if we are unpaired
        if(this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
            PaymentClient.LOGGER.debug("The EFTPOS terminal is currently unpaired");
            throw new UnpairedException();
        }

        try {
            await pairingResolver;
        } catch(e) {
            PaymentClient.LOGGER.debug('Could not successfully reconnect to the EFTPOS terminal', e);

            throw new UnpairedException();
        }
    }

    /**
     * Disconnect the SPI client from the PaymentClient
     * @returns {Promise<void>}
     */
    public async disconnect(): Promise<void> {
        PaymentClient.LOGGER.debug("Attempting to disconnect the PaymentClient");

        if(this.spiClient === null) {
            PaymentClient.LOGGER.debug("No SPI client is connected");
            throw new Error("No SPI client is connected, please connect one first");
        }

        if(typeof this.spiClient.CurrentStatus !== 'undefined' && this.spiClient.CurrentStatus !== SpiStatus.Unpaired) {
            PaymentClient.LOGGER.debug("The EFTPOS terminal is currently paired");
            throw new Error("The EFTPOS terminal is paired, please unpair before disconnecting");
        }

        try {
            // Not a public API so we probably shouldn't be doing this,
            // but there is no other way
            this.spiClient._conn.Disconnect();
        } catch {
            // Do nothing
        }

        this.spiClient = null;

        PaymentClient.LOGGER.debug("PaymentClient disconnected");
    }

    /**
     * Set whether test mode should be used
     * @param {boolean} enabled
     */
    public async setTestMode(enabled: boolean): Promise<void> {
        try {
            await this.unpair();
        } catch {}

        try {
            await this.disconnect();
        } catch {}

        if(this.testMode === enabled) {
            PaymentClient.LOGGER.debug("Test Mode has not changed, ignoring", {
                previous: this.testMode,
                new     : enabled,
            });

            return; // No-op
        }

        PaymentClient.LOGGER.debug("Test Mode has changed", {
            previous: this.testMode,
            new     : enabled,
        });

        this.testMode = enabled;
    }

    /**
     * Set the options to use
     * @param {PaymentClientOptions} options
     * @returns {Promise<void>}
     */
    public async setOptions(options: PaymentClientOptions): Promise<void> {
        try {
            await this.unpair();
        } catch {}

        try {
            await this.disconnect();
        } catch {}

        PaymentClient.LOGGER.debug("About to set options", options);

        // Ensure the options are valid
        if(options.registerName === null) {
            PaymentClient.LOGGER.debug("registerName is invalid");
            throw new TypeError("null is not a valid name for the register");
        } else if(options.registerName === '') {
            PaymentClient.LOGGER.debug("registerName must be set");
            throw new TypeError("registerName must not be a blank string");
        } else if(!/([A-Z0-9])/.test(options.registerName)) {
            PaymentClient.LOGGER.debug("registerName contains invalid characters");
            throw new TypeError("registerName must contain only uppercase alphanumeric characters");
        } else if(options.registerName.length > 16) {
            PaymentClient.LOGGER.debug("registerName must be less than or equal to 17 characters");
            throw new TypeError("registerName must be less than or equal to 17 characters");
        }

        if(options.serialNumber === null) {
            PaymentClient.LOGGER.debug("serialNumber is invalid");
            throw new TypeError("null is not a valid serial number");
        } else if(!/[0-9]{3}-?[0-9]{3}-?[0-9]{3}/.test(options.serialNumber)) {
            PaymentClient.LOGGER.debug("serialNumber is invalid (must be in format xxx-xxx-xxx where x is a number between 0 and 9)");
            throw new TypeError("serialNumber is invalid");
        }

        let normalized = options.serialNumber.split('-').join('');
        options.serialNumber = `${normalized.slice(0, 3)}-${normalized.slice(3, 6)}-${normalized.slice(6)}`

        // Store the options
        await Promise.all([
            PaymentClient.SETTINGS.setItem('REGISTER_NAME', options.registerName),
            PaymentClient.SETTINGS.setItem('SERIAL_NUMBER', options.serialNumber),
            PaymentClient.SETTINGS.setItem('POS_NAME', options.posName),
            PaymentClient.SETTINGS.setItem('POS_VERSION', options.posVersion),
            PaymentClient.SETTINGS.setItem('CASHOUT_PROMPT', options.cashoutPrompt),
            PaymentClient.SETTINGS.setItem('RECEIPT_PROMPT_CUSTOMER_COPY', options.receipts.promptForCustomerCopy),
            PaymentClient.SETTINGS.setItem('RECEIPT_HEADER_TEXT', options.receipts.additionalHeaderText),
            PaymentClient.SETTINGS.setItem('RECEIPT_FOOTER_TEXT', options.receipts.additionalFooterText),
            PaymentClient.SETTINGS.deleteItem('SECRET_ENC_KEY'),
            PaymentClient.SETTINGS.deleteItem('SECRET_HMAC_KEY'),
            PaymentClient.SETTINGS.deleteItem('PREVIOUS_EFTPOS_ADDRESS'),
        ]);

        // Update the settings
        this.options = options;

        // Remove the secrets
        this.secrets = {
            EncKey : null,
            HmacKey: null,
        };

        PaymentClient.LOGGER.debug("Options updated");
    }

    /**
     * Update the secrets to communicate with the EFTPOS terminal
     * @param {CustomEvent} event
     * @returns {Promise<void>}
     */
    protected async updateSecrets(event: CustomEvent): Promise<void> {
        PaymentClient.LOGGER.debug('Received updated secrets', event);

        let enc  = null;
        let hmac = null;

        if(event.detail === null) {
            PaymentClient.LOGGER.debug('Secrets have been removed');
        } else {
            PaymentClient.LOGGER.debug('Secrets have been updated');
            enc  = event.detail.EncKey;
            hmac = event.detail.HmacKey;
        }

        this.secrets = {
            EncKey : enc,
            HmacKey: hmac,
        };

        await Promise.all([
            PaymentClient.SETTINGS.setItem('SECRET_ENC_KEY', enc),
            PaymentClient.SETTINGS.setItem('SECRET_HMAC_KEY', hmac),
        ]);

        PaymentClient.LOGGER.debug('Finished updating secrets');
    }

    /**
     * Update the device's URL to be able to automatically reconnect
     * @param {CustomEvent} event
     * @returns {Promise<void>}
     */
    protected async updateDeviceAddress(event: CustomEvent): Promise<void> {
        PaymentClient.LOGGER.debug('Received updated device address', event);

        let address = event.detail.fqdn === null ? '' : event.detail.fqdn;
        await PaymentClient.SETTINGS.setItem('PREVIOUS_EFTPOS_ADDRESS', address);

        PaymentClient.LOGGER.debug('Finished updating the device address');
    }

    /**
     * Pair the EFTPOS terminal with the Payment Client
     * @param {PairCallbackInterface} callbacks
     * @returns {Promise<void>}
     */
    public async pair(callbacks: PairCallbackInterface): Promise<void> {
        PaymentClient.LOGGER.debug("About to start pairing the EFTPOS terminal");

        if(this.spiClient === null) {
            PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
            throw new Error("You must call .connect() before calling .pair()");
        }

        if(
            this.spiClient.CurrentStatus === SpiStatus.PairedConnected ||
            this.spiClient.CurrentStatus === SpiStatus.PairedConnecting
        ) {
            PaymentClient.LOGGER.debug("An EFTPOS terminal is already paired", {
                status: this.spiClient.CurrentStatus,
            });

            throw new AlreadyPairedException();
        }

        // Prepare the resolver function
        const pairResolver = new Promise((res, rej) => {
            let eftposPairingComplete = false;
            const hasCompleted = () => {
                return eftposPairingComplete;
            };

            const eventHandler = (event: CustomEvent) => {
                eftposPairingComplete = !event.detail.AwaitingCheckFromEftpos;

                if(this.spiClient === null) {
                    PaymentClient.LOGGER.debug('The SPI client has been disconnected while pairing');
                    rej();
                    return;
                }

                PaymentClient.LOGGER.debug('Received PairingFlowStateChange event', event);

                if(event.detail.Finished === true) {
                    this.Events.removeEventListener(AssemblyEvent.PAIRING_FLOW_STATE_CHANGED, eventHandler);

                    if(event.detail.Successful === false) {
                        // We failed :(
                        rej();
                    } else {
                        // Success!
                        res();
                    }
                } else {
                    if(event.detail.AwaitingCheckFromPos) {
                        callbacks.keyCheck(event.detail.ConfirmationCode)
                            .then(result => {
                                if(this.spiClient === null) {
                                    this.Events.removeEventListener(AssemblyEvent.PAIRING_FLOW_STATE_CHANGED, eventHandler);
                                    PaymentClient.LOGGER.debug('The SPI client has been disconnected while pairing');
                                    rej();
                                    return;
                                }

                                if(result === true) {
                                    this.spiClient.PairingConfirmCode();
                                } else if(result === false) {
                                    this.Events.removeEventListener(AssemblyEvent.PAIRING_FLOW_STATE_CHANGED, eventHandler);

                                    this.cancelPair()
                                        .then(() => {
                                            rej();
                                        }).catch(() => {
                                            rej();
                                        });
                                } else {
                                    // Unexpected result
                                    throw new TypeError('Result returned from keyCheck should be of type boolean');
                                }
                            })
                            .catch(err => {
                                PaymentClient.LOGGER.error('An unexpected error occurred while awaiting for the POS to confirm the pairing code', err);

                                rej();
                            });
                    } else if(event.detail.AwaitingCheckFromEftpos) {
                        callbacks.terminalConfirmation(event.detail.ConfirmationCode, hasCompleted)
                            .catch(() => {
                                this.Events.removeEventListener(AssemblyEvent.PAIRING_FLOW_STATE_CHANGED, eventHandler);

                                this.cancelPair()
                                    .then(() => {
                                        rej();
                                    })
                                    .catch(() => {
                                        rej();
                                    });
                            });
                    }
                }
            };

            this.Events.addEventListener(AssemblyEvent.PAIRING_FLOW_STATE_CHANGED, eventHandler);
        });

        PaymentClient.LOGGER.debug("About to call Pair on the SPI client");
        const pairResult: boolean = this.spiClient.Pair();

        if(pairResult === false) {
            PaymentClient.LOGGER.debug("Pairing failed due to negative result from SPI client");
            throw new PairingFailedException();
        }

        const connectionResolver = this.createConnectionPromise();

        try {
            await pairResolver;
        } catch(e) {
            PaymentClient.LOGGER.debug('Could not pair the EFTPOS terminal');
            throw new PairingFailedException();
        }

        // Acknowledge that the pairing has finished
        this.spiClient.AckFlowEndedAndBackToIdle();

        try {
            // Ensure we are connected and ready to go
            await connectionResolver;
        } catch(e) {
            PaymentClient.LOGGER.debug('Could not connect to the EFTPOS terminal after pairing');

            try {
                await this.unpair();
            } catch {}

            throw new PairingFailedException();
        }

        PaymentClient.LOGGER.debug("EFTPOS terminal successfully paired");
    }

    /**
     * Cancel pairing the EFTPOS terminal with the PaymentClient
     * @returns {Promise<void>}
     */
    public async cancelPair(): Promise<void> {
        PaymentClient.LOGGER.debug("About to cancel the current pairing");
        if(this.spiClient === null) {
            PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
            throw new Error("You must call .connect() before calling .cancelPair()");
        }

        if(this.spiClient.CurrentFlow !== SpiFlow.Pairing) {
            PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently pairing");
            throw new NotPairingException();
        }

        this.spiClient.PairingCancel();

        // We sort of have to just trust that it worked unfortunately.
        PaymentClient.LOGGER.debug('Cancel pairing request sent');
    }

    /**
     * Unpair the EFTPOS terminal with the PaymentClient
     * @returns {Promise<void>}
     */
    public async unpair(): Promise<void> {
        PaymentClient.LOGGER.debug("About to unpair the currently paired EFTPOS terminal");

        // Remove the secrets
        await Promise.all([
            PaymentClient.SETTINGS.deleteItem('SECRET_ENC_KEY'),
            PaymentClient.SETTINGS.deleteItem('SECRET_HMAC_KEY'),
        ]);

        // Ensure we have a client
        if(this.spiClient === null) {
            PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
            throw new Error("You must call .connect() before calling .unpair()");
        }

        // Check the current pairing status
        if(this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
            PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently paired");
            throw new UnpairedException();
        }

        // Perform the unpair
        this.spiClient.Unpair();

        // We sort of have to just trust that it worked unfortunately.
        PaymentClient.LOGGER.debug('Unpair request sent');
    }

    /**
     * Register the callback to use when printing is required
     * @param {PrintCallback} callback
     */
    public onPrint(callback: PrintCallback): void {
        this.printCallback = callback;
    }

    /**
     * Register the callback to use when a signature is required
     * @param {SignatureCallback} callback
     */
    public onSignature(callback: SignatureCallback): void {
        this.signatureCallback = callback;
    }

    /**
     * Register the callback to use when phone authorization is required
     * @param {AuthorizationCallback} callback
     */
    public onPhoneAuthorization(callback: AuthorizationCallback): void {
        this.phoneCallback = callback;
    }

    /**
     * Get the transaction ID for the sale
     * @returns {string}
     */
    protected getTransactionId(): string {
        return `APPC-${Date.now().toString()}`;
    }

    /**
     * Create a promise that will resolve when the EFTPOS terminal is connected
     * @returns {Promise<void>}
     */
    private createConnectionPromise(): Promise<void> {
        return new Promise((res, rej) => {
            const eventHandler = (event: CustomEvent) => {
                PaymentClient.LOGGER.debug('Received ConnectionStatusChanged event', event);

                if(this.listeners.connectionResolver === null) {
                    return;
                }

                if(this.spiClient === null) {
                    PaymentClient.LOGGER.debug("The SPI client has been disconnected before pairing could be resolved");
                    this.listeners.connectionResolver(false);
                    return;
                }

                if(event.detail.ConnectionState === ConnectionState.Disconnected) {
                    PaymentClient.LOGGER.debug("The SPI client has been unpaired");
                    this.listeners.connectionResolver(false);
                }
            };

            let handled = false;
            this.listeners.connectionResolver = (success: boolean) => {
                PaymentClient.LOGGER.debug('Connection resolver has been called', {
                    success,
                });

                if(handled) {
                    return;
                }

                this.Events.removeEventListener(AssemblyEvent.CONNECTION_STATUS_CHANGED, eventHandler);
                this.listeners.connectionResolver = null;
                handled = true;

                if(!success) {
                    rej();
                    return;
                }

                res();
            };

            this.Events.addEventListener(AssemblyEvent.CONNECTION_STATUS_CHANGED, eventHandler);
        });
    }

    /**
     * Create the promise to listen for the transaction completion
     * @returns {Promise<TransactionResult>}
     */
    private createTransactionPromise(): Promise<TransactionResult> {
        return new Promise((res, rej) => {
            const eventHandler = (event: CustomEvent) => {
                if(this.spiClient === null) {
                    PaymentClient.LOGGER.debug('The SPI client has been disconnected while a transaction is in progress');
                    return rej(new TransactionUnknownException());
                }

                PaymentClient.LOGGER.debug('Received TxFlowStateChange event', event);

                if(event.detail.Finished === true) {
                    this.Events.removeEventListener(AssemblyEvent.TRANSACTION_FLOW_STATE_CHANGED, eventHandler);

                    if(event.detail.Success === SuccessState.Unknown) {
                        // :O
                        PaymentClient.LOGGER.error('The SPI client was unsure about what happened to the transaction');

                        return rej(new TransactionUnknownException());
                    } else if(event.detail.Success === SuccessState.Failed) {
                        // :(
                        PaymentClient.LOGGER.debug('The purchase failed :(');

                        if(event.detail.Response !== null) {
                            const response = new PurchaseResponse(event.detail.Response);

                            if(!response.WasCustomerReceiptPrinted()) {
                                this.printCallback(response.GetCustomerReceipt().trim(), false);
                            }

                            if(response.GetResponseCode() === "511" || response.GetResponseText() === 'TRANS CANCELLED') {
                                // This seems to be the only way to tell - it may not be 100% accurate
                                return rej(new TransactionCancelledException());
                            }
                        }

                        return rej(new TransactionDeclinedException());
                    } else if(event.detail.Success === SuccessState.Success) {
                        // :)
                        PaymentClient.LOGGER.debug('The purchase was successful!');

                        const response = new PurchaseResponse(event.detail.Response);

                        if(!response.WasCustomerReceiptPrinted()) {
                            this.printCallback(response.GetCustomerReceipt().trim(), false);
                        }

                        return res({
                            rrn      : response.GetRRN(),
                            response : response.GetResponseText(),
                            amount   : response.GetPurchaseAmount(),
                            tip      : response.GetTipAmount(),
                            surcharge: response.GetSurchargeAmount(),
                            cashout  : typeof response.GetCashoutAmount() === 'undefined' ? 0 : response.GetCashoutAmount(),
                        });
                    } else {
                        // :/
                        PaymentClient.LOGGER.error('The SPI client returned an invalid success state');

                        return rej(new TransactionUnknownException());
                    }
                } else {
                    if(event.detail.AwaitingSignatureCheck) {
                        this.printCallback(
                            event.detail.SignatureRequiredMessage.GetMerchantReceipt().trim(),
                            true
                        );

                        this.signatureCallback()
                            .then(result => {
                                if(this.spiClient === null) {
                                    PaymentClient.LOGGER.debug('The SPI client has been disconnected while a transaction is in progress');
                                    return rej(new TransactionUnknownException());
                                }

                                if(result === true) {
                                    this.spiClient.AcceptSignature(true);
                                } else if(result === false) {
                                    this.spiClient.AcceptSignature(false);
                                } else {
                                    PaymentClient.LOGGER.error("Received an unknown result from the signature callback", result);
                                    return rej(new TypeError('Result returned from signatureCallback should be of type boolean'));
                                }
                            })
                            .catch(err => {
                                PaymentClient.LOGGER.error('An unknown error occurred while waiting for a signature', err);
                                return rej(new TransactionUnknownException());
                            });
                    } else if(event.detail.AwaitingPhoneForAuth) {
                        this.phoneCallback(
                            event.detail.PhoneForAuthRequiredMessage.GetPhoneNumber(),
                            event.detail.PhoneForAuthRequiredMessage.GetMerchantId()
                        )
                            .then(result => {
                                if(this.spiClient === null) {
                                    PaymentClient.LOGGER.debug('The SPI client has been disconnected while a transaction is in progress');
                                    return rej(new TransactionUnknownException());
                                }

                                if(typeof result === 'string') {
                                    const authCodeResult = this.spiClient.SubmitAuthCode(result);

                                    if(authCodeResult.ValidFormat === false) {
                                        PaymentClient.LOGGER.error("The Auth Code provided was invalid", authCodeResult);
                                        return rej(new TransactionUnknownException());
                                    }
                                } else {
                                    PaymentClient.LOGGER.error("Received an unknown result from the phone callback", result);
                                    return rej(new TypeError('Result returned from phoneCallback should be of type string'));
                                }
                            })
                            .catch(err => {
                                PaymentClient.LOGGER.error('An unknown error occurred while waiting for phone authorization', err);
                                return rej(new TransactionUnknownException());
                            });
                    }
                }
            };

            this.Events.addEventListener(AssemblyEvent.TRANSACTION_FLOW_STATE_CHANGED, eventHandler);
        });
    }

    /**
     * Perform a transaction
     * @param {number} amount
     * @param {number} cashout
     * @param {number} tip
     * @returns {Promise<TransactionResult>}
     */
    public async transaction(amount: number, cashout?: number, tip?: number): Promise<TransactionResult> {
        PaymentClient.LOGGER.debug("About to start transaction");

        let type: TransactionType = TransactionType.Purchase;
        if(amount < 0) {
            type = TransactionType.Refund;
        } else if(amount === 0 && typeof cashout === 'number') {
            type = TransactionType.CashoutOnly;
        }

        if(type === TransactionType.Refund) {
            if(typeof cashout !== 'undefined' && cashout !== 0) {
                PaymentClient.LOGGER.error("You cannot use cashout with a refund", {
                    amount,
                    cashout,
                    tip,
                });

                throw new TypeError("You cannot use cashout with a refund");
            }

            if(typeof tip !== 'undefined' && tip !== 0) {
                PaymentClient.LOGGER.error("You cannot use tip with a refund", {
                    amount,
                    cashout,
                    tip,
                });

                throw new TypeError("You cannot use tip with a refund");
            }
        } else if(type === TransactionType.CashoutOnly) {
            if(typeof tip !== 'undefined' && tip !== 0) {
                PaymentClient.LOGGER.error("You cannot use tip with a cashout only transaction", {
                    amount,
                    cashout,
                    tip,
                });

                throw new TypeError("You cannot use tip with a cashout only transaction");
            }
        }

        if(typeof cashout === 'undefined') {
            cashout = 0;
        }

        if(typeof tip === 'undefined') {
            tip = 0;
        }

        if(cashout < 0) {
            throw new TypeError("Cashout amounts cannot be negative");
        }

        if(tip < 0) {
            throw new TypeError("Tip amounts cannot be negative");
        }

        if(this.spiClient === null) {
            PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
            throw new Error("You must call .connect() before calling .transaction()");
        }

        if(this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
            PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently paired");
            throw new UnpairedException();
        }

        if(tip > 0 && (cashout > 0 || this.options.cashoutPrompt)) {
            throw new TypeError("Cannot provide a tip and cashout at the same time");
        }

        const id: string = this.getTransactionId();
        PaymentClient.LOGGER.info("[TRANSACTION START] About to start transaction", {
            id,
            amount,
            cashout,
            tip,
            cashoutPrompt: this.options.cashoutPrompt
        });

        // Save the transactional data for recovery
        await PaymentClient.SETTINGS.setItem('TRANSACTION_RECOVERY_ID', id);
        await PaymentClient.SETTINGS.setItem('TRANSACTION_RECOVERY_TYPE', type);

        // Finish setting the options
        const transactionOptions = new TransactionOptions();
        transactionOptions.SetCustomerReceiptHeader(this.options.receipts.additionalHeaderText);
        transactionOptions.SetCustomerReceiptFooter(this.options.receipts.additionalFooterText);
        transactionOptions.SetMerchantReceiptHeader(this.options.receipts.additionalHeaderText);
        transactionOptions.SetMerchantReceiptFooter(this.options.receipts.additionalFooterText);

        const purchasePromise: Promise<TransactionResult> = this.createTransactionPromise();

        let startTransactionResult;
        if(type === TransactionType.Purchase) {
            PaymentClient.LOGGER.debug("Starting purchase");
            startTransactionResult = this.spiClient.InitiatePurchaseTxV2(
                id,
                amount,
                tip,
                cashout,
                this.options.cashoutPrompt,
                transactionOptions
            );
        } else if(type === TransactionType.Refund) {
            PaymentClient.LOGGER.debug("Starting refund");
            startTransactionResult = this.spiClient.InitiateRefundTx(
                id,
                amount * -1,
                false
            );
        } else if(type === TransactionType.CashoutOnly) {
            PaymentClient.LOGGER.debug("Starting cashout");
            startTransactionResult = this.spiClient.InitiateCashoutOnlyTx(
                id,
                cashout
            );
        } else {
            // This should really never get here
            throw new Error("Unknown transaction type specified");
        }

        if(!startTransactionResult.Initiated) {
            PaymentClient.LOGGER.debug("The transaction was not initiated, trying to find out why");

            if(this.spiClient.CurrentFlow === SpiFlow.Transaction) {
                // There is currently a transaction in progress
                PaymentClient.LOGGER.debug("There is currently a transaction in progress");
                throw new TransactionInProgressException();
            }

            PaymentClient.LOGGER.debug("Unknown reason for not starting transaction", startTransactionResult);
            throw new Error(`Could not start transaction for an unknown reason: ${startTransactionResult.Message}`);
        }

        return purchasePromise;
    }

    /**
     * Cancel the transaction currently in progress
     * @returns {Promise<void>}
     */
    public async cancelTransaction(): Promise<void> {
        PaymentClient.LOGGER.debug("About to cancel the current transaction");

        if(this.spiClient === null) {
            PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
            throw new Error("You must call .connect() before calling .cancelTransaction()");
        }

        if(this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
            PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently paired");
            throw new UnpairedException();
        }

        const cancelPromise: Promise<void> = new Promise((res, rej) => {
            const eventHandler = (event: CustomEvent) => {
                if(this.spiClient === null) {
                    PaymentClient.LOGGER.debug('The SPI client has been disconnected while a transaction is in progress');
                    return rej(new CancelTransactionFailedException());
                }

                PaymentClient.LOGGER.debug('Received TxFlowStateChange event', event);

                if(event.detail.Finished === true) {
                    this.Events.removeEventListener(AssemblyEvent.TRANSACTION_FLOW_STATE_CHANGED, eventHandler);

                    if(event.detail.Success === SuccessState.Failed) {
                        res();
                    } else {
                        PaymentClient.LOGGER.error('Could not cancel the transaction');
                        throw new CancelTransactionFailedException();
                    }
                }
            };

            this.Events.addEventListener(AssemblyEvent.TRANSACTION_FLOW_STATE_CHANGED, eventHandler);
        });

        const cancelResult: MidTxResult = this.spiClient.CancelTransaction();

        if(cancelResult.Valid === false) {
            PaymentClient.LOGGER.debug("Cancelling the transaction failed, attempting to determine why", cancelResult);

            if(this.spiClient.CurrentFlow !== SpiFlow.Transaction) {
                PaymentClient.LOGGER.debug("There is no transaction currently in progress");
                throw new NoTransactionInProgressException();
            }

            if(this.spiClient.CurrentTxFlowState.Finished) {
                PaymentClient.LOGGER.debug("The transaction is already finished");
                throw new NoTransactionInProgressException();
            }

            PaymentClient.LOGGER.debug("Cancelling the transaction failed for an unknown reason");
            throw new CancelTransactionFailedException();
        }

        return cancelPromise;
    }

    /**
     * Recover the previous transaction
     * @returns {Promise<TransactionResult>}
     */
    public async recover(): Promise<TransactionResult> {
        PaymentClient.LOGGER.debug("About to start transaction recovery");

        if(this.spiClient === null) {
            PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
            throw new Error("You must call .connect() before calling .recover()");
        }

        if(this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
            PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently paired");
            throw new UnpairedException();
        }

        PaymentClient.LOGGER.debug("About to get the recovery data from the database");
        const id: string | null   = await PaymentClient.SETTINGS.getItem('TRANSACTION_RECOVERY_ID', null) as string | null;
        const type: string | null = await PaymentClient.SETTINGS.getItem('TRANSACTION_RECOVERY_TYPE', null) as string | null;

        if(id === null || type === null) {
            PaymentClient.LOGGER.debug("No transaction recovery data was found");
            throw new RecoveryFailedException();
        }

        const purchasePromise = this.createTransactionPromise();

        PaymentClient.LOGGER.debug("Recovery data obtained, initiating recovery");
        const recoveryResult = this.spiClient.InitiateRecovery(id, type);

        if(recoveryResult.Initiated === false) {
            PaymentClient.LOGGER.debug("Could not initiate recovery, trying to determine the reason", recoveryResult);

            PaymentClient.LOGGER.debug("Recovery could not initiate for an unknown reason");
            throw new RecoveryFailedException();
        }

        return purchasePromise;
    }

    /**
     * Perform a settlement on the EFTPOS terminal
     * @returns {Promise<SettlementResult>}
     */
    public async settlement(): Promise<SettlementResult> {
        PaymentClient.LOGGER.debug("About to perform settlement");

        if(this.spiClient === null) {
            PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
            throw new Error("You must call .connect() before calling .settlement()");
        }

        if(this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
            PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently paired");
            throw new UnpairedException();
        }

        const id: string = this.getTransactionId();
        PaymentClient.LOGGER.info("[SETTLEMENT START] About to start settlement", {
            id,
        });

        const settlementResult = this.spiClient.InitiateSettleTx(id);

        if(settlementResult.Initiated === false) {
            PaymentClient.LOGGER.debug("Settlement could not initialise, attempting to find reason");

            if(this.spiClient.CurrentFlow !== SpiFlow.Idle) {
                PaymentClient.LOGGER.debug("The EFTPOS terminal is not idle");
                throw new SettlementUnsuccessfulException();
            }

            PaymentClient.LOGGER.debug("Settlement could not be performed for an unknown reason");
            throw new SettlementUnsuccessfulException();
        }

        // TODO: Listen for TxFlowStateChanged event
    }

    /**
     * Perform a settlement enquiry on the EFTPOS terminal
     * @returns {Promise<SettlementResult>}
     */
    public async settlementEnquiry(): Promise<SettlementResult> {
        PaymentClient.LOGGER.debug("About to perform settlement");

        if(this.spiClient === null) {
            PaymentClient.LOGGER.debug("The SPI client has not been configured, call .connect() first");
            throw new Error("You must call .connect() before calling .settlementEnquiry()");
        }

        if(this.spiClient.CurrentStatus === SpiStatus.Unpaired) {
            PaymentClient.LOGGER.debug("The EFTPOS terminal is not currently paired");
            throw new UnpairedException();
        }

        const id: string = this.getTransactionId();
        PaymentClient.LOGGER.info("[SETTLEMENT START] About to start settlement", {
            id,
        });

        const settlementResult = this.spiClient.InitiateSettleTx(id);

        if(settlementResult.Initiated === false) {
            PaymentClient.LOGGER.debug("Settlement could not initialise, attempting to find reason");

            if(this.spiClient.CurrentFlow !== SpiFlow.Idle) {
                PaymentClient.LOGGER.debug("The EFTPOS terminal is not idle");
                throw new SettlementUnsuccessfulException();
            }

            PaymentClient.LOGGER.debug("Settlement could not be performed for an unknown reason");
            throw new SettlementUnsuccessfulException();
        }

        // TODO: Listen for TxFlowStateChanged event
    }
}