import {SpiLog} from "@assemblypayments/spi-client-js/index.js";

export type MessageParser = {
    (message: string): void
}

// TODO: Use a proxy instead
export class SpiClientLog implements SpiLog {
    protected parseMessage: MessageParser;

    constructor(parseMessage: MessageParser) {
        this.parseMessage = parseMessage;
    }

    protected handleMethod(parameters: Array<any>) {
        if(typeof parameters[0] === 'string') {
            this.parseMessage(parameters[0]);
        }

        console.info('SPI client output from Assembly Payments below:');
    }

    public assert(...obj: Array<any>): void {
        console.assert(...obj);
    }

    public clear(): void {
        console.clear();
    }

    public count(label?: string): void {
        console.count(label);
    }

    public countReset(label?: string): void {
        console.countReset(label);
    }

    public debug(...obj: Array<any>): void {
        this.handleMethod(obj);
        console.debug(...obj);
    }

    public dir(obj: object): void {
        console.dir(obj);
    }

    public dirxml(obj: object): void {
        console.dir(obj);
    }

    public error(...obj: Array<any>): void {
        this.handleMethod(obj);
        console.error(...obj);
    }

    public group(label?: string): void {
        console.group(label);
    }

    public groupCollapsed(label?: string): void {
        console.groupCollapsed(label);
    }

    public groupEnd(): void {
        console.groupEnd();
    }

    public info(...obj: Array<any>): void {
        this.handleMethod(obj);
        console.info(...obj);
    }

    public log(...obj: Array<any>): void {
        this.handleMethod(obj);
        console.log(...obj);
    }

    public table(data: Array<any> | object, columns?: Array<string>): void {
        console.table(data, columns);
    }

    public time(label: string): void {
        console.time(label);
    }

    public timeEnd(label: string): void {
        console.timeEnd(label);
    }

    public timeLog(label: string): void {
        console.timeLog(label);
    }

    public trace(): void {
        console.trace();
    }

    public warn(...obj: Array<any>): void {
        this.handleMethod(obj);
        console.warn(...obj);
    }
}