export class NoTransactionInProgressException extends Error {
    constructor() {
        super("No transaction is currently in progress");
    }
}