export class UnpairedException extends Error {
    constructor() {
        super(`The EFTPOS terminal is not currently paired`);
    }
}