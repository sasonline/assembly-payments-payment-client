export class TransactionDeclinedException extends Error {
    constructor() {
        super('The transaction was declined');
    }
}