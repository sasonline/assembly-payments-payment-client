export class NotPairingException extends Error {
    constructor() {
        super("This PaymentClient is not currently pairing");
    }
}