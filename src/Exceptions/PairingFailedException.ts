export class PairingFailedException extends Error {
    constructor() {
        super("An unknown pairing failure occurred");
    }
}