export class TransactionInProgressException extends Error {
    constructor() {
        super("There is currently a transaction in progress, please finish that one before starting this one");
    }
}