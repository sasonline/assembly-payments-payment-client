export class InvalidClientException extends Error {
    constructor(field: string) {
        super(`The ${field} option is invalid`);
    }
}