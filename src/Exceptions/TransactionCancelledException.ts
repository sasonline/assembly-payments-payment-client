export class TransactionCancelledException extends Error {
    constructor() {
        super("The transaction in progress was cancelled");
    }
}