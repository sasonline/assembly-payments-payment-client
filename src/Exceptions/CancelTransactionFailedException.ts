export class CancelTransactionFailedException extends Error {
    constructor() {
        super("An unknown error occurred while cancelling the transaction");
    }
}