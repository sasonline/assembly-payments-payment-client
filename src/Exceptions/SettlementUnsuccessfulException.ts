export class SettlementUnsuccessfulException extends Error {
    constructor() {
        super("The settlement was unsuccessful");
    }
}