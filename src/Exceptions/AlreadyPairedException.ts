export class AlreadyPairedException extends Error {
    constructor() {
        super("An EFTPOS terminal is already paired with this PaymentClient");
    }
}