export class TransactionUnknownException extends Error {
    constructor() {
        super('An unknown error has occurred while processing the transaction, please check the EFTPOS terminal');
    }
}