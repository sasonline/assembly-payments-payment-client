export class RecoveryFailedException extends Error {
    constructor() {
        super("Transaction recovery failed");
    }
}