declare module "@assemblypayments/spi-client-js/index.js" {
    import {Connection} from "@assemblypayments/spi-client-js/src/Connection.js";

    export class Spi {
        _conn: Connection; // Not a public API so slightly dangerous
        _log: SpiLog; // Not a public API so slightly dangerous
        _inTestMode: boolean; // Not a public API so slightly dangerous
        CurrentStatus: SpiStatus;
        Config: SpiConfig;
        CurrentDeviceStatus: null;
        CurrentFlow: null | SpiFlow;
        CurrentPairingFlowState: null;
        CurrentTxFlowState: TransactionFlowState;
        constructor(posId: string, serialNumber: string, eftposAddress: string, secrets: null | {EncKey: string, HmacKey: string});
        EnablePayAtTable(): SpiPayAtTable;
        DisablePayAtTable(): SpiPayAtTable;
        EnablePreauth(): SpiPreauth;
        Start(): void;
        SetAcquirerCode(acquirerCode: string): true;
        SetDeviceApiKey(deviceApiKey: string): true;
        SetSerialNumber(serialNumber: string): boolean;
        SetAutoAddressResolution(autoAddressResolutionEnable: boolean): boolean;
        SetTestMode(testMode: boolean): boolean;
        SetSecureWebSockets(useSecureWebSockets: boolean): void;
        SetPosId(posId: string): boolean;
        SetEftposAddress(address: string): boolean;
        static GetVersion(): string;
        SetPosInfo(posVendorId: string, posVersion: string | number): void;
        AckFlowEndedAndBackToIdle(): boolean;
        Pair(): boolean;
        PairingConfirmCode(): void;
        PairingCancel(): void;
        Unpair(): boolean;
        InitiatePurchaseTx(posRefId: string, amountCents: number): InitiateTxResult;
        InitiatePurchaseTxV2(posRefId: string, purchaseAmount: number, tipAmount: number, cashoutAmount: number, promptForCashout: boolean, options?: object, surchargeAmount?: number): InitiateTxResult;
        InitiateRefundTx(posRefId: string, amountCents: number, isSuppressMerchantPassword?: boolean): InitiateTxResult;
        AcceptSignature(accepted: boolean): MidTxResult;
        SubmitAuthCode(authCode: string): SubmitAuthCodeResult;
        CancelTransaction(): MidTxResult;
        InitiateCashoutOnlyTx(posRefId: string, amountCents: number, surchargeAmount?: number): InitiateTxResult;
        InitiateMotoPurchaseTx(posRefId: string, amountCents: number, surchargeAmount?: number): InitiateTxResult;
        InitiateSettleTx(posRefId: string): InitiateTxResult;
        InitiateSettlementEnquiry(posRefId: string): InitiateTxResult;
        InitiateGetLastTx(): InitiateTxResult;
        InitiateRecovery(posRefId: string, txType: string): InitiateTxResult;
        GltMatch(gltResponse: GetLastTransactionResponse, posRefId: string): SuccessState;
        PrintReceipt(key: string, payload: object): void;
        GetTerminalStatus(): boolean;
        PrintingResponse(message: Message): void;
        TerminalStatusResponse(message: TerminalStatusResponse): void;
        BatteryLevelChanged(message: TerminalBattery): void;
        HasSerialNumberChanged(updatedSerialNumber: string): boolean;
        HasEftposAddressChanged(updatedEftposAddress: string): boolean;
    }

    class SpiConfig {
        PromptForCustomerCopyOnEftpos: boolean;
        SignatureFlowOnEftpos: boolean;
        PrintMerchantCopy: boolean;
        constructor();
        addReceiptConfig<T>(messageData: T): T;
        ToString(): string;
    }

    class SpiPayAtTable {} // TODO: Add type checking
    class SpiPreauth {} // TODO: Add type checking

    class TransactionFlowState {
        PosRefId: string;
        Id: string;
        Type: TransactionType;
        DisplayMessage: string;
        AmountCents: number;
        RequestSent: boolean;
        RequestTime: null | Date;
        LastStateRequestTime: null | Date;
        AttemptingToCancel: null | boolean;
        AwaitingSignatureCheck: boolean;
        AwaitingPhoneForAuth: null | boolean;
        Finished: boolean;
        Success: SuccessState;
        Response: null | Message;
        SignatureRequiredMessage: null | string;
        PhoneForAuthRequiredMessage: null | string;
        CancelAttemptTime: null | string;
        Request: Message;
        AwaitingGltResponse: null | boolean;
        GLTResponsePosRefId: null | string;
        constructor(posRefId: string, type: TransactionType, amountCents: number, message: Message, msg: string);
        Sent(msg: string): void;
        Cancelling(msg: string): void;
        CancelFailed(msg: string): void;
        CallingGlt(): void;
        GotGltResponse(): void;
        Failed(response: Message, msg: string): void;
        SignatureRequired(spiMessage: string, msg: string): void;
        PhoneForAuthRequired(spiMessage: string, msg: string): void;
        AuthCodeSent(msg: string): void;
        Completed(state: SuccessState, response: Message, msg: string): void;
        UnknownCompleted(msg: string): void;
    }

    class SpiLog {
        assert(assertion: boolean, ...obj: Array<any>): void;
        clear(): void;
        count(label?: string): void;
        countReset(label?: string): void;
        debug(...obj: Array<any>): void;
        dir(obj: object): void;
        dirxml(obj: object): void;
        error(...obj: Array<any>): void;
        group(label?: string): void;
        groupCollapsed(label?: string): void;
        groupEnd(): void;
        info(...obj: Array<any>): void;
        log(...obj: Array<any>): void;
        table(data: Array<any> | object, columns?: Array<string>): void;
        time(label: string): void;
        timeEnd(label: string): void;
        timeLog(label: string): void;
        trace(): void;
        warn(...obj: Array<any>): void;
    }

    class InitiateTxResult {
        Initiated: boolean;
        Message: string;
        constructor(initiated: boolean, message: string);
    }

    class MidTxResult {
        Valid: boolean;
        Message: string;
        constructor(valid: boolean, message: string);
    }

    class SubmitAuthCodeResult {
        ValidFormat: boolean;
        Message: string;
        constructor(validFormat: boolean, message: string);
    }

    export class Logger {
        constructor(element: HTMLElement, lineSeperator?: string);
        Info(...args: string[]): void;
        Debug(...args: string[]): void;
        Warn(...args: string[]): void;
        Error(...args: string[]): void;
        Console(...args: string[]): void;
        Clear(): void;
    }

    export class Secrets {
        constructor(encKey: string, hmacKey: string);
        static save(EncKey: string, HmacKey: string): void;
        static restore(): Secrets;
        static isSaved(): boolean;
        static Reset(): void;
    }

    interface SecretObject {
        HmacKey: string,
        EncKey: string,
    }

    export enum SuccessState {
        Unknown = "Unknown",
        Success = "Success",
        Failed  = "Failed",
    }

    export class TransactionOptions {
        constructor();
        SetCustomerReceiptHeader(customerReceiptHeader: null | string): void;
        SetCustomerReceiptFooter(customerReceiptFooter: null | string): void;
        SetMerchantReceiptHeader(merchantReceiptHeader: null | string): void;
        SetMerchantReceiptFooter(merchantReceiptFooter: null | string): void;
        AddOptions<T>(messageData: T): T;
    }

    export enum TransactionType {
        Purchase           = "Purchase",
        Refund             = "Refund",
        CashoutOnly        = "CashoutOnly",
        MOTO               = "MOTO",
        Settle             = "Settle",
        SettlementEnquiry  = "SettlementEnquiry",
        GetLastTransaction = "GetLastTransaction",
        Preauth            = "Preauth",
        AccountVerify      = "AccountVerify",
    }

    export enum SpiFlow {
        Pairing     = "Pairing",
        Transaction = "Transaction",
        Idle        = "Idle",
    }

    export enum SpiStatus {
        PairedConnected  = "PairedConnected",
        PairedConnecting = "PairedConnecting",
        Unpaired         = "Unpaired",
    }

    class Message {
        Id: string;
        EventName: string;
        Data: object;
        DateTimeStamp: string;
        PosId: string;
        IncommingHmac: string;
        DecryptedJson: string;
        constructor(id: string, eventName: string, data: object, needsEncryption: boolean);
        GetSuccessState(): SuccessState;
        GetError(): string;
        GetErrorDetail(): string;
        GetServerTimeDetail(): string;
        GetServerTimeDelta(): Date;
        static ParseBankDate(bankDate: string): null | Date;
        static ParseBankDateTimeStr(date: string, time: string): Date;
        static FromJson(msgJson: string, secrets: SecretObject): Message;
        ToJson(stamp: MessageStamp): string;
    }

    class MessageStamp {
        PosId: string;
        Secrets: SecretObject;
        ServerTimeDelta: number;
        constructor(posId: string, secrets: SecretObject, serverTimeDelta: number);
    }

    export class PrintingResponse {
        constructor(message: Message);
        isSuccess(): boolean;
        getErrorReason(): string;
        getErrorDetail(): string;
        getResponseValueWithAttribute(attribute: string): any;
    }

    class TransactionResponse { // Not a real class, but easier than rewriting
        RequestId: string;
        PosRefId: string;
        SchemeName: string;
        SchemeAppName: string;
        Success: boolean;
        constructor(message: Message);
        GetRRN(): string; // Doesn't seem to be correctly set
        GetCustomerReceipt(): string;
        GetMerchantReceipt(): string;
        GetResponseText(): string;
        GetResponseCode(): string;
        GetTerminalReferenceId(): string;
        GetCardEntry(): string;
        GetAccountType(): string;
        GetAuthCode(): string;
        GetBankDate(): string;
        GetBankTime(): string;
        GetMaskedPan(): string;
        GetTerminalId(): string;
        WasMerchantReceiptPrinted(): boolean;
        WasCustomerReceiptPrinted(): boolean;
        GetSettlementDate(): null | Date;
        GetResponseValue(attribute: string): any;
    }

    interface PaymentSummary {
        account_type: string,
        auth_code: string,
        bank_date: string,
        bank_time: string,
        host_response_code: string,
        host_response_text: string,
        masked_pan: string,
        purchase_amount: number,
        rrn: string,
        scheme_name: string,
        terminal_id: string,
        terminal_ref_id: string,
        tip_amount: number,
        surcharge_amount: number,
    }

    export class RefundResponse extends TransactionResponse {
        GetRefundAmount(): number;
    }

    export class PurchaseResponse extends TransactionResponse {
        GetPurchaseAmount(): number;
        GetTipAmount(): number;
        GetSurchargeAmount(): number;
        GetCashoutAmount(): number;
        GetBankNonCashAmount(): number;
        GetBankCashAmount(): number;
        ToPaymentSummary(): PaymentSummary
    }

    export class GetLastTransactionResponse {
        constructor(message: Message);
        WasRetrievedSuccessfully(): boolean;
        WasTimeOutOfSyncError(): boolean;
        WasOperationInProgressError(): boolean;
        IsWaitingForSignatureResponse(): boolean;
        IsWaitingForAuthCode(): boolean;
        IsStillInProgress(posRefId: string): boolean;
        GetSuccessState(): SuccessState;
        WasSuccessfulTx():boolean;
        GetTxType(): string;
        GetPosRefId(): string;
        GetSchemeApp(): string;
        GetSchemeName(): string;
        GetAmount(): number;
        GetTransactionAmount(): number;
        GetBankDateTimeString(): string;
        GetRRN(): string;
        GetResponseText(): string;
        GetResponseCode(): string;
        CopyMerchantReceiptToCustomerReceipt(): void;
    }

    export class MotoPurchaseResponse {
        PurchaseResponse: PurchaseResponse;
        PosRefId: string;
        constructor(message: Message);
    }

    export class TerminalStatusResponse {
        constructor(message: Message);
        GetStatus(): null;
        GetBatteryLevel(): number;
        IsCharging(): boolean;
    }

    export class TerminalBattery {
        BatteryLevel: number;
        constructor(message: Message);
    }

    export class CashoutOnlyResponse extends PurchaseResponse {
        constructor(message: Message);
    }

    export class Settlement {
        RequestId: string;
        Success: boolean;
        constructor(message: Message);
        GetSettleByAcquirerCount(): number;
        GetSettleByAcquirerValue(): number;
        GetTotalCount(): number;
        GetTotalValue(): number;
        GetPeriodStartTime(): Date;
        GetPeriodEndTime(): Date;
        GetTriggeredTime(): Date;
        GetResponseText(): string;
        GetReceipt(): string;
        GetTransactionRange(): string;
        GetTerminalId(): string;
        GetSchemeSettlementEntries(): SchemeSettlementEntry[];
    }

    class SchemeSettlementEntry {
        SchemeName: string;
        SettleByAcquirer: boolean;
        TotalValue: number;
        TotalCount: number;
        constructor(schemeName: string, settleByAcquirer: boolean, totalCount: number, totalValue: number);
        constructor(args: {scheme_name: string, settle_by_acquirer: string, total_value: string, total_count: string});
        ToString(): string;
    }

    export class RequestIdHelper {
        static Id(prefix: string): string;
    }
}

declare module "@assemblypayments/spi-client-js/src/Connection.js" {
    import {Message} from "@assemblypayments/spi-client-js/index.js";

    export class Connection {
        constructor();
        Connect(UseSecureWebSockets: boolean): void;
        Disconnect(): void;
        Send(message: Message): void;
        onOpened(): void;
        onClosed(): void;
        pollWebSocketConnection(count: number): void | boolean;
        onMessageReceived(message: object): void;
        onError(err: object): void;
    }

    export enum ConnectionState {
        Disconnected = "Disconnected",
        Connecting   = "Connecting",
        Connected    = "Connected",
    }
}