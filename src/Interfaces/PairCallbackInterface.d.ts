interface PairCallbackInterface {
    keyCheck            : KeyCheckCallback,
    terminalConfirmation: TerminalConfirmationCallback,
}

interface KeyCheckCallback {
    (code: string): Promise<boolean>,
}

interface TerminalConfirmationCallback {
    (code: string, hasCompleted: () => boolean): Promise<void>,
}