interface LogInterface {
    debug(description: string, relatedData?: object): void,
    info(description: string, relatedData?: object): void,
    error(description: string, relatedData?: object): void,
}